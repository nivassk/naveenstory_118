﻿
Create FUNCTION [dbo].[fn_HCP_GeLatestUpload]
(
       
)
RETURNS TABLE
AS
RETURN
(
  SELECT DISTINCT   TotalErrors, IsErrorProcessed, TotalExpenses ,QuarterlyFHAPI , QuarterlyMIP, MIPCumulative ,FHAPICumulative , hascalculated, IsProcessed ,IsCalcProcessed , LDI_ID, FHANumber , ProjectName, ServiceName ,   UnitsInFacility , PeriodEnding , MonthsInPeriod , NOICumulative , QuaterlyNoi ,NOIQuaterbyQuater , NOIPercentageChange  , CumulativeRevenue, CumulativeExpenses , QuaterlyOperatingRevenue , QuarterlyOperatingExpense, RevenueQuarterbyQuarter ,RevenuePercentageChange , DSCRDifferencePerQuarter, DSCRPercentageChange , CumulativeResidentDays , CONVERT ( int, QuarterlyResidentDays ) as QuarterlyResidentDays, ADRperQuarter , ADRDifferencePerQuarter, ADRPercentageChange ,DebtCoverageRatio2 , AverageDailyRateRatio, QuarterlyDSCR ,IsQtrlyORevError , IsQtrlyNOIError, IsQtrlyNOIPTError , IsQtrlyDSCRError , IsQtrlyDSCRPTError , IsADRError , IsProjectError, fhainsuredprincipalInterestpayment ,DataInserted         
  FROM dbo.Lender_DataUpload_Intermediate a 
  JOIN ( SELECT  FHANumber as FHA  , MonthsInPeriod as MIP , max (DataInserted) as createddt, max(LDI_ID) as LDIRecent , YEAR(CONVERT( DATETIME, PeriodEnding)) as YR
		 FROM dbo.Lender_DataUpload_Intermediate
		 GROUP by FHANumber,MonthsInPeriod,YEAR(CONVERT(DATETIME, PeriodEnding))
		) b on a.FHANumber = b.FHA and a.DataInserted = b.createddt and a.LDI_ID= b.LDIRecent   

);

----------------------------------------------------

GO

