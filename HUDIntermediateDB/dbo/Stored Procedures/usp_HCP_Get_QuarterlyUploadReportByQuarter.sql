
CREATE PROCEDURE [dbo].[usp_HCP_Get_QuarterlyUploadReportByQuarter]
(
  @Username nvarchar(max), 
  @UserType nvarchar(100), 
  @LenderId int,
  @MonthsInPeriod int, 
  @Year int
)
AS
SET FMTONLY OFF
DECLARE @UserId INT
declare @QuarterStart datetime
declare @QuarterEnd datetime
select @QuarterStart = CONVERT(datetime, CONVERT(varchar(10), (@MonthsInPeriod - 2)) + '/1/' + CONVERT(varchar(10), @Year))
select @QuarterEnd = dateadd(dd, -1, dateadd(mm, 3, @QuarterStart))
DECLARE @ReportingPeriod NVARCHAR(100)
SELECT @ReportingPeriod =  CONVERT(varchar(5), @MonthsInPeriod) + '/' + CONVERT(varchar(5), @Year)
DECLARE @ManagerId INT
IF (@UserType<> NULL OR @UserType <> '') AND @UserType = 'AccountExecutive'
  SET @ManagerId = (SELECT HUD_Project_Manager_ID FROM [$(LiveDB)].dbo.HUD_Project_Manager AE 
    INNER JOIN [$(LiveDB)].dbo.Address AD ON AE.AddressID = AD.AddressID WHERE AD.Email = @Username)
ELSE IF (@UserType<> NULL OR @UserType <> '') AND @UserType = 'WorkflowManager'           
  SET @ManagerId = (SELECT HUD_WorkLoad_Manager_ID FROM [$(LiveDB)].dbo.HUD_WorkLoad_Manager WLM 
    INNER JOIN [$(LiveDB)].dbo.Address AD ON WLM.AddressID = AD.AddressID WHERE AD.Email = @Username)
SET @UserId = (SELECT UserId FROM [$(LiveDB)].dbo.HCP_Authentication WHERE UserName = @Username)



select B.LenderID, L.Lender_Name as LenderName, count(PropertyID) as TotalExpected, count(LDI_ID) as Received,
  count(PropertyID) - count(LDI_ID) as Missing, @ReportingPeriod as ReportingPeriod
from [$(LiveDB)].[dbo].[LenderInfo] L
  inner join (

    --not sold
    select P.PropertyID, P.FHANumber, P.LenderID, FHA_StartDate, FHA_EndDate,
      PeriodEnding, LDI_ID, DataInserted
    from [$(LiveDB)].dbo.ProjectInfo P
      inner join [$(LiveDB)].dbo.Lender_FHANumber N on P.FHANumber = N.FHANumber
      left join (
        select LenderID, PeriodEnding, FHANumber, PropertyID, LDI_ID, datainserted, rn
        from(
          select PropertyID, FHANumber, LenderID, PeriodEnding, MonthsInPeriod, DataInserted, LDI_ID,
          ROW_NUMBER() over (partition by FHANumber order by LDI_ID desc) as rn
          from [$(DatabaseName)].dbo.Lender_DataUpload_Intermediate I
          where (@LenderId = -1 or I.LenderID = @LenderId) and convert(int, I.MonthsInPeriod) = @MonthsInPeriod
            and Year(convert(datetime, I.PeriodEnding)) = @Year
        ) a where rn = 1
      ) I2 on P.PropertyID = I2.PropertyID and P.LenderID = I2.LenderID
      left join [$(LiveDB)].dbo.User_Lender U on U.FHANumber = P.FHANumber and
        (@UserType = 'Servicer' and U.[User_ID] = @UserId) or
        (@UserType = 'InternalSpecialOptionUser' and U.[User_ID] = @UserId)
    where (@LenderId = -1 or P.LenderID = @LenderId) and P.LenderID = N.LenderID and
      (FHA_EndDate is null or FHA_EndDate > @QuarterEnd) and --ending check
      ((dateadd(dd, 90, FHA_StartDate) < @QuarterStart) or --regular starting check
       (@MonthsInPeriod = 12 and FHA_StartDate < --Q4 is special: need boundary check
          [$(DatabaseName)].[dbo].[fn_HCP_GetPeriodEndingDate](P.FHANumber, 12, @Year) and
        dateadd(dd, 90, FHA_StartDate) >= 
          [$(DatabaseName)].[dbo].[fn_HCP_GetPeriodEndingDate](P.FHANumber, 12, @Year)) or
       (@MonthsInPeriod = 3 and FHA_StartDate < --Q1 is special: if Q4 cross, then Q1 should in
          [$(DatabaseName)].[dbo].[fn_HCP_GetPeriodEndingDate](P.FHANumber, 12, @Year - 1) and
        dateadd(dd, 90, FHA_StartDate) >= 
          [$(DatabaseName)].[dbo].[fn_HCP_GetPeriodEndingDate](P.FHANumber, 12, @Year - 1))) and
       ((@UserType = 'AccountExective' and P.HUD_Project_Manager_ID = @ManagerId) or
        (@UserType = 'WorkflowManager' and P.HUD_WorkLoad_Manager_ID = @ManagerId) or
        (@UserType <> 'WorkflowManager' and @UserType <> 'WorkflowManager'))

    union

    --sold
    select P.PropertyID, P.FHANumber, N.LenderID, FHA_StartDate, FHA_EndDate,
      PeriodEnding, LDI_ID, DataInserted
    from [$(LiveDB)].dbo.ProjectInfo P
      inner join [$(LiveDB)].dbo.Lender_FHANumber N on P.FHANumber = N.FHANumber
      left join (
        select LenderID, PeriodEnding, FHANumber, PropertyID, LDI_ID, datainserted, rn
        from(
          select PropertyID, FHANumber, LenderID, PeriodEnding, MonthsInPeriod, DataInserted, LDI_ID,
          ROW_NUMBER() over (partition by FHANumber order by LDI_ID desc) as rn
          from [$(DatabaseName)].dbo.Lender_DataUpload_Intermediate I
          where (@LenderId = -1 or I.LenderID = @LenderId) and convert(int, I.MonthsInPeriod) = @MonthsInPeriod
            and Year(convert(datetime, I.PeriodEnding)) = @Year
        ) a where rn = 1
      ) I2 on P.PropertyID = I2.PropertyID and N.LenderID = I2.LenderID
      left join [$(LiveDB)].dbo.User_Lender U on U.FHANumber = N.FHANumber and
        (@UserType = 'Servicer' and U.[User_ID] = @UserId) or
        (@UserType = 'InternalSpecialOptionUser' and U.[User_ID] = @UserId)
    where (@LenderId = -1 or N.LenderID = @LenderId) and P.LenderID <> N.LenderID and
      (FHA_EndDate is null or FHA_EndDate > @QuarterEnd) and --ending check
      ((dateadd(dd, 90, FHA_StartDate) < @QuarterStart) or --regular starting check
       (@MonthsInPeriod = 12 and FHA_StartDate < --Q4 is special: need boundary check
          [$(DatabaseName)].[dbo].[fn_HCP_GetPeriodEndingDate](P.FHANumber, 12, @Year) and
        dateadd(dd, 90, FHA_StartDate) >= 
          [$(DatabaseName)].[dbo].[fn_HCP_GetPeriodEndingDate](P.FHANumber, 12, @Year)) or
       (@MonthsInPeriod = 3 and FHA_StartDate < --Q1 is special: if Q4 cross, then Q1 should in
          [$(DatabaseName)].[dbo].[fn_HCP_GetPeriodEndingDate](P.FHANumber, 12, @Year - 1) and
        dateadd(dd, 90, FHA_StartDate) >= 
          [$(DatabaseName)].[dbo].[fn_HCP_GetPeriodEndingDate](P.FHANumber, 12, @Year - 1))) and
      ((@UserType = 'AccountExective' and P.HUD_Project_Manager_ID = @ManagerId) or
        (@UserType = 'WorkflowManager' and P.HUD_WorkLoad_Manager_ID = @ManagerId) or
        (@UserType <> 'WorkflowManager' and @UserType <> 'WorkflowManager'))

  ) B on L.LenderID = B.LenderID --AND ISNULL(L.Deleted_Ind, 0) = 0
  AND (@LenderId = -1 OR B.LenderID = @LenderId)
group by B.LenderID, L.Lender_Name
order by L.Lender_Name




GO

