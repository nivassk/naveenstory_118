CREATE PROCEDURE [dbo].[usp_HCP_Get_MissingProject_ManagementReportHeader]
(
@UserId  int,
@ReportingPeriod datetime
)
AS
SET FMTONLY OFF
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	DECLARE @UserType NVARCHAR(100)
	SELECT @UserType = 
	(
		SELECT R.RoleName FROM [$(LiveDB)].dbo.webpages_UsersInRoles U INNER JOIN [$(LiveDB)].dbo.webpages_Roles R ON
		U.RoleId = R.RoleId AND U.UserId = @UserId
	)
	CREATE TABLE #MissingReportHeader
	(
		UnpaidPrincipalBalance DECIMAL(19,2),
		ReportingPeriod DATETIME,
		NumberOfProjects INT,
		HUD_Workload_Manager_Name NVARCHAR(100),
		HUD_Project_Manager_Name NVARCHAR(100)
	);
	if @UserType = 'AccountExecutive'
		BEGIN 
			INSERT INTO #MissingReportHeader
			SELECT SUM(PF.Amortized_Unpaid_Principal_Bal),@ReportingPeriod, COUNT(*), MAX(WLM.HUD_WorkLoad_Manager_Name), max(PM.HUD_Project_Manager_Name) 
			FROM [$(LiveDB)].dbo.ProjectInfo PF INNER JOIN 
			 [$(LiveDB)].dbo.Lender_FHANumber LF ON PF.FHANumber = LF.FHANumber 
			 INNER JOIN  [$(LiveDB)].dbo.HUD_WorkLoad_Manager WLM ON PF.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
			 INNER JOIN [$(LiveDB)].dbo.HUD_Project_Manager PM ON PF.HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
			 AND PF.HUD_Project_Manager_ID = 
			(SELECT HUD_Project_Manager_ID FROM [$(LiveDB)].dbo.User_WLM_PM WHERE UserID = @UserId)	
			AND 
			(
				(LF.FHA_StartDate <=  @ReportingPeriod AND LF.FHA_EndDate IS NULL)
				OR 
				(LF.FHA_EndDate IS NOT NULL AND LF.FHA_EndDate > @ReportingPeriod)
			)
			AND PF.FHANumber NOT IN
			(
				SELECT LDI.FHANumber 
				FROM [$(LiveDB)].dbo.ProjectInfo PF INNER JOIN
				[$(DatabaseName)].dbo.Lender_DataUpload_Intermediate LDI 
				ON PF.FHANumber = LDI.FHANumber 
				AND LDI.DataInserted IN
					(
						SELECT MAX(LDI1.DataInserted) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Intermediate LDI1
						WHERE CONVERT(DATETIME,LDI1.PeriodEnding) = @ReportingPeriod
						GROUP BY LDI1.FHANumber
					) 
			)		
		END
	else if @UserType = 'WorkflowManager'
		BEGIN
			INSERT INTO #MissingReportHeader
			SELECT SUM(PF.Amortized_Unpaid_Principal_Bal),@ReportingPeriod, COUNT(*), MAX(WLM.HUD_WorkLoad_Manager_Name), NULL 
			FROM [$(LiveDB)].dbo.ProjectInfo PF INNER JOIN 
			 [$(LiveDB)].dbo.Lender_FHANumber LF ON PF.FHANumber = LF.FHANumber 
			 INNER JOIN  [$(LiveDB)].dbo.HUD_WorkLoad_Manager WLM ON PF.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
			 AND PF.HUD_WorkLoad_Manager_ID = 
			(SELECT HUD_WorkLoad_Manager_ID FROM [$(LiveDB)].dbo.User_WLM_PM WHERE UserID = @UserId)		
			AND 
			(
				(LF.FHA_StartDate <=  @ReportingPeriod AND LF.FHA_EndDate IS NULL)
				OR 
				(LF.FHA_EndDate IS NOT NULL AND LF.FHA_EndDate > @ReportingPeriod)
			)
			AND PF.FHANumber NOT IN
			(
				SELECT LDI.FHANumber 
				FROM [$(LiveDB)].dbo.ProjectInfo PF INNER JOIN
				[$(DatabaseName)].dbo.Lender_DataUpload_Intermediate LDI 
				ON PF.FHANumber = LDI.FHANumber 
				AND LDI.DataInserted IN
					(
						SELECT MAX(LDI1.DataInserted) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Intermediate LDI1
						WHERE CONVERT(DATETIME,LDI1.PeriodEnding) = @ReportingPeriod
						GROUP BY LDI1.FHANumber
					) 
			)
		END
	else
		BEGIN
			INSERT INTO #MissingReportHeader
			SELECT SUM(PF.Amortized_Unpaid_Principal_Bal),@ReportingPeriod, COUNT(*), NULL, NULL 
			FROM [$(LiveDB)].dbo.ProjectInfo PF INNER JOIN 
			 [$(LiveDB)].dbo.Lender_FHANumber LF ON PF.FHANumber = LF.FHANumber 
			AND 
			(
				(LF.FHA_StartDate <=  @ReportingPeriod AND LF.FHA_EndDate IS NULL)
				OR 
				(LF.FHA_EndDate IS NOT NULL AND LF.FHA_EndDate > @ReportingPeriod)
			)
			AND PF.FHANumber NOT IN
			(
				SELECT LDI.FHANumber 
				FROM [$(LiveDB)].dbo.ProjectInfo PF INNER JOIN
				[$(DatabaseName)].dbo.Lender_DataUpload_Intermediate LDI 
				ON PF.FHANumber = LDI.FHANumber 
				AND LDI.DataInserted IN
					(
						SELECT MAX(LDI1.DataInserted) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Intermediate LDI1
						WHERE CONVERT(DATETIME,LDI1.PeriodEnding) = @ReportingPeriod
						GROUP BY LDI1.FHANumber
					) 
			)
		END
		SELECT UnpaidPrincipalBalance,ReportingPeriod,NumberOfProjects,HUD_Workload_Manager_Name,HUD_Project_Manager_Name FROM #MissingReportHeader
END
SET FMTONLY ON