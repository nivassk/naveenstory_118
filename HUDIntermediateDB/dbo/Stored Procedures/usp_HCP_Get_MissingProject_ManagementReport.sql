CREATE PROCEDURE [dbo].[usp_HCP_Get_MissingProject_ManagementReport]
(
@UserId  int,
@ReportingPeriod datetime
)

AS
SET FMTONLY OFF
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 

DECLARE @UserType NVARCHAR(100)
SELECT @UserType = 
(
	SELECT R.RoleName FROM [$(LiveDB)].dbo.webpages_UsersInRoles U INNER JOIN [$(LiveDB)].dbo.webpages_Roles R ON
	U.RoleId = R.RoleId AND U.UserId = @UserId
)


CREATE TABLE #MissingProjects
(
	ProjectName NVARCHAR(100) null,
	FHANumber NVARCHAR(100) null,
	DebtCoverageRatio DECIMAL(19,2) null,
	WorkingCapital DECIMAL(19,2) null,
	DaysCashOnHand	DECIMAL(19,2) null,
	DaysInAcctReceivable DECIMAL(19,2) null,
	AvgPaymentPeriod DECIMAL(19,2) null,
	Amortized_Unpaid_Principal_Bal DECIMAL(19,2) null,
	HUD_Project_Manager_ID INT null,
	HUD_Project_Manager_Name NVARCHAR(100) null,
	HUD_Project_Manager_EmailAddress NVARCHAR(100) null,
	HUD_WorkLoad_Manager_ID INT null,
	HUD_WorkLoad_Manager_Name NVARCHAR(100) null,
	HUD_WorkLoad_Manager_EmailAddress NVARCHAR(100) null,	
	Total_Number_Of_Projects INT null
);
if @UserType = 'AccountExecutive'
	BEGIN		
		INSERT INTO #MissingProjects
			SELECT PF.ProjectName,PF.FHANumber, LD.DebtCoverageRatio, LD.WorkingCapital, LD.DaysCashOnHand, 
			LD.DaysInAcctReceivable, LD.AvgPaymentPeriod, PF.Amortized_Unpaid_Principal_Bal,
			NULL,NULL,NULL,NULL,NULL,NULL,NULL
			FROM [$(LiveDB)].dbo.ProjectInfo PF 
			INNER JOIN [$(LiveDB)].dbo.Lender_FHANumber LF ON PF.FHANumber = LF.FHANumber
			AND PF.HUD_Project_Manager_ID = 
			(SELECT HUD_Project_Manager_ID FROM [$(LiveDB)].dbo.User_WLM_PM WHERE UserID = @UserId)		
			AND 
			(
				(LF.FHA_StartDate <=  @ReportingPeriod AND LF.FHA_EndDate IS NULL)
					OR 
				(LF.FHA_EndDate IS NOT NULL AND LF.FHA_EndDate > @ReportingPeriod)
			)
			AND PF.FHANumber NOT IN
			(
				SELECT LDI.FHANumber 
				FROM [$(LiveDB)].dbo.ProjectInfo PF INNER JOIN
				dbo.Lender_DataUpload_Intermediate LDI 
				ON PF.FHANumber = LDI.FHANumber 
				AND PF.HUD_Project_Manager_ID = 
				(SELECT HUD_Project_Manager_ID FROM [$(LiveDB)].dbo.User_WLM_PM WHERE UserID = @UserId)
				 AND LDI.DataInserted IN
				(
					SELECT MAX(DataInserted) FROM dbo.Lender_DataUpload_Intermediate
					WHERE CONVERT(DATETIME,PeriodEnding) = @ReportingPeriod
					GROUP BY FHANumber
				) 
			) LEFT JOIN dbo.Lender_DataUpload_Intermediate LD 
			ON PF.FHANumber = LD.FHANumber AND CONVERT(DATETIME,PeriodEnding) = @ReportingPeriod	
		--SELECT MP.ProjectName,MP.FHANumber, MP.DebtCoverageRatio, MP.WorkingCapital, MP.DaysCashOnHand, MP.DaysInAcctReceivable, 
		--MP.AvgPaymentPeriod, MP.Amortized_Unpaid_Principal_Bal FROM #MissingProjects MP	
	END
else if @UserType = 'WorkflowManager'
	BEGIN
		
		INSERT INTO #MissingProjects
			SELECT NULL,NULL,SUM(LD.DebtCoverageRatio), SUM(LD.WorkingCapital), SUM(LD.DaysCashOnHand), SUM(LD.DaysInAcctReceivable), 
			SUM(LD.AvgPaymentPeriod), SUM(PF.Amortized_Unpaid_Principal_Bal),
			PM.HUD_Project_Manager_ID, PM.HUD_Project_Manager_Name, MAX(A.Email),NULL,NULL,NULL,COUNT(LF.FHANumber)
			FROM [$(LiveDB)].dbo.ProjectInfo PF 
			INNER JOIN [$(LiveDB)].dbo.Lender_FHANumber LF ON PF.FHANumber = LF.FHANumber 
			INNER JOIN [$(LiveDB)].dbo.HUD_Project_Manager PM ON PF.HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
			INNER JOIN [$(LiveDB)].dbo.Address A ON PM.AddressID = A.AddressID
			AND PF.HUD_WorkLoad_Manager_ID = 
			(SELECT HUD_WorkLoad_Manager_ID FROM [$(LiveDB)].dbo.User_WLM_PM WHERE UserID = @UserId)		
			AND 
			(
				(LF.FHA_StartDate <=  @ReportingPeriod AND LF.FHA_EndDate IS NULL)
					OR 
				(LF.FHA_EndDate IS NOT NULL AND LF.FHA_EndDate > @ReportingPeriod)
			)
			AND PF.FHANumber NOT IN
			(
				SELECT LDI.FHANumber 
				FROM [$(LiveDB)].dbo.ProjectInfo PF INNER JOIN
				dbo.Lender_DataUpload_Intermediate LDI 
				ON PF.FHANumber = LDI.FHANumber 
				AND PF.HUD_WorkLoad_Manager_ID = 
				(SELECT HUD_WorkLoad_Manager_ID FROM [$(LiveDB)].dbo.User_WLM_PM WHERE UserID = @UserId)
				 AND LDI.DataInserted IN
				(
					SELECT MAX(DataInserted) FROM dbo.Lender_DataUpload_Intermediate
					WHERE CONVERT(DATETIME,PeriodEnding) = @ReportingPeriod
					GROUP BY FHANumber
				) 
			) LEFT JOIN dbo.Lender_DataUpload_Intermediate LD 
			ON PF.FHANumber = LD.FHANumber AND CONVERT(DATETIME,PeriodEnding) = @ReportingPeriod
			GROUP BY PM.HUD_Project_Manager_ID,pm.HUD_Project_Manager_Name
		--SELECT MP.HUD_Project_Manager_ID, MP.HUD_Project_Manager_Name, MP.HUD_Project_Manager_EmailAddress, MP.FHANumber,
		--	MP.DebtCoverageRatio, MP.WorkingCapital, MP.DaysCashOnHand, MP.DaysInAcctReceivable, 
		--	MP.AvgPaymentPeriod, MP.Amortized_Unpaid_Principal_Bal FROM #MissingProjects MP
	END
else 
	BEGIN
		INSERT INTO #MissingProjects
			SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
			WLM.HUD_WorkLoad_Manager_ID,WLM.HUD_WorkLoad_Manager_Name,MAX(A.Email),COUNT(LF.FHANumber)
			FROM [$(LiveDB)].dbo.ProjectInfo PF INNER JOIN [$(LiveDB)].dbo.HUD_WorkLoad_Manager WLM
			ON PF.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID 
			INNER JOIN [$(LiveDB)].dbo.Address A ON WLM.AddressID = A.AddressID 
			INNER JOIN [$(LiveDB)].dbo.Lender_FHANumber LF ON PF.FHANumber = LF.FHANumber 
			AND 
			(
				(LF.FHA_StartDate <=  @ReportingPeriod AND LF.FHA_EndDate IS NULL)
				OR 
				(LF.FHA_EndDate IS NOT NULL AND LF.FHA_EndDate > @ReportingPeriod)
			)
			AND PF.FHANumber NOT IN
			(
				SELECT LDI.FHANumber 
				FROM [$(LiveDB)].dbo.ProjectInfo PF INNER JOIN
				dbo.Lender_DataUpload_Intermediate LDI 
				ON PF.FHANumber = LDI.FHANumber 
				AND LDI.DataInserted IN
					(
						SELECT MAX(LDI1.DataInserted) FROM dbo.Lender_DataUpload_Intermediate LDI1
						WHERE CONVERT(DATETIME,LDI1.PeriodEnding) = @ReportingPeriod
						GROUP BY LDI1.FHANumber
					) 
			)GROUP BY WLM.HUD_WorkLoad_Manager_ID, WLM.HUD_WorkLoad_Manager_Name
		--SELECT MP.HUD_WorkLoad_Manager_ID,MP.HUD_WorkLoad_Manager_Name,MP.HUD_WorkLoad_Manager_EmailAddress,
		--MP.Total_Number_Of_Projects FROM #MissingProjects MP
	END
	SELECT ProjectName,FHANumber,DebtCoverageRatio,WorkingCapital,DaysCashOnHand,DaysInAcctReceivable,
	AvgPaymentPeriod,Amortized_Unpaid_Principal_Bal,HUD_Project_Manager_ID,HUD_Project_Manager_Name,HUD_Project_Manager_EmailAddress,
	HUD_WorkLoad_Manager_ID, HUD_WorkLoad_Manager_Name, HUD_WorkLoad_Manager_EmailAddress,Total_Number_Of_Projects FROM #MissingProjects
END



SET FMTONLY ON
