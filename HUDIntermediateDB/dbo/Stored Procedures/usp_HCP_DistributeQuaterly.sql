CREATE PROCEDURE [dbo].[usp_HCP_DistributeQuaterly]
@QueryDate NVARCHAR( 50 )

AS

BEGIN try
        Begin TRANSACTION
declare @LDI_ID varchar ( 11 ), @MIP decimal, @PEYear nvarchar( 50 ),@PE DATETIME ,@CurrentYear int , @FHANumber varchar ( 30 ), @NOICumulative DECIMAL (19 , 2), @QuaterlyOperatingRevenue DECIMAL( 19 ,2 ), @QuarterlyResidentDays DECIMAL ( 19 , 2)
, @ADRperQuarter DECIMAL ( 19, 2 ),@QuarterlyOperatingExpense  DECIMAL (19 , 2), @QuarterlyFHAPI DECIMAL (19 , 2), @QuarterlyMIP DECIMAL( 19 ,2 ), @QuarterlyDSCR DECIMAL (19  , 2), @IMDate DATETIME
, @AverageDailyRateRatio   DECIMAL ( 19, 2 ),@A   DECIMAL (19 , 2), @B DECIMAL ( 19 , 2 )
declare @dt  Datetime


set @dt= convert( datetime , @QueryDate , 101 )

UPDATE [$(DatabaseName)].dbo.Lender_DataUpload_Intermediate SET NOICumulative= NOIRatio
UPDATE [$(DatabaseName)].dbo.Lender_DataUpload_Intermediate SET CumulativeRevenue= TotalRevenues
UPDATE [$(DatabaseName)].dbo.Lender_DataUpload_Intermediate SET CumulativeResidentDays= actualnumberofresidentdays
UPDATE [$(DatabaseName)].dbo.Lender_DataUpload_Intermediate SET   CumulativeExpenses= TotalExpenses
UPDATE [$(DatabaseName)].dbo.Lender_DataUpload_Intermediate SET FHAPICumulative= fhainsuredprincipalInterestpayment
UPDATE [$(DatabaseName)].dbo.Lender_DataUpload_Intermediate SET MIPCumulative= MortgageInsurancePremium

--Block for Selecting the Latest Record
select @LDI_ID = min( LDI_ID ) from [$(DatabaseName)]. [dbo]. [fn_HCP_GeLatestUpload] () a where     CONVERT (VARCHAR ( 10), cast (a . datainserted as date), 101 )>@dt and    a. IsProcessed is null

while @LDI_ID is not null
begin
select   @MIP= MonthsInPeriod from [$(DatabaseName)]. dbo. Lender_DataUpload_Intermediate where LDI_ID = @LDI_ID
select   @PEYear = YEAR( CONVERT( DATETIME , PeriodEnding)) from [$(DatabaseName)]. dbo. Lender_DataUpload_Intermediate where LDI_ID = @LDI_ID
select   @PE=    CONVERT( DATETIME , PeriodEnding) from [$(DatabaseName)]. dbo. Lender_DataUpload_Intermediate where LDI_ID = @LDI_ID
select   @FHANumber = FHANumber from  [$(DatabaseName)]. dbo. Lender_DataUpload_Intermediate where LDI_ID = @LDI_ID

--Print @LDI_ID
print @PEYear
print @MIP
print @FHANumber

--Start of  Logic of Change in  NOI
--Calculation of Noi Cumulative
declare @TempData_NOI TABLE (
LDI_ID INT,
FHANumber nvarchar( 50 )  NULL,
MonthsInPeriod DECIMAL( 19 ,2 )   NULL,
QuaterlyNoi DECIMAL( 19 ,2 ) NULL,
DataInserted nvarchar( 50 ) NULL,
NOIQuaterbyQuater DECIMAL( 19 ,2 ) NULL,
NOIPercentageChange DECIMAL( 19 ,2 ) NULL,
NOICumulative DECIMAL( 19 ,2 ) NULL,
CumulativeRevenue DECIMAL( 19 ,2 ) NULL,
CumulativeResidentDays DECIMAL (19 , 2) NULL,
AverageDailyRateRatio DECIMAL( 19 ,2 ) NULL,
CumulativeExpenses DECIMAL( 19 ,2 ) NULL,
FHAPICumulative DECIMAL( 19 ,2 ) NULL,
MIPCumulative  DECIMAL( 19 ,2 ) NULL,
DebtCoverageRatio2 DECIMAL( 19 ,2 ) NULL,
periodending nvarchar( 50 ) null

)


IF (@MIP = 3)
BEGIN
SET @IMDate= @pe
END
ELSE IF ( @MIP = 6 )
BEGIN
SELECT @IMDate= DATEADD( MONTH ,- 3 , @pe )
END
ELSE IF ( @MIP = 9 )
BEGIN
SELECT @IMDate= DATEADD( MONTH ,- 6 , @pe )
END
ELSE IF ( @MIP = 12 )
BEGIN
SELECT @IMDate= DATEADD( MONTH ,- 9 , @pe )
End




Declare @QuaterlyNOI decimal(19,2) , @NOIPercentageChange decimal(19,2)
delete from @TempData_NOI
insert into @TempData_NOI  select DISTINCT a. LDI_ID ,FHANumber , MonthsInPeriod, QuaterlyNOI, DataInserted, NOIQuaterbyQuater ,NOIPercentageChange , NOICumulative, CumulativeRevenue , CumulativeResidentDays , AverageDailyRateRatio, CumulativeExpenses , FHAPICumulative , MIPCumulative, DebtCoverageRatio2 , a . PeriodEnding from [$(DatabaseName)]. [dbo]. [fn_HCP_GeLatestUpload] () a
  where     a . FHANumber= @FHANumber and CONVERT ( DATETIME , a. PeriodEnding )BETWEEN   @IMDate AND @PE and a . MonthsInPeriod<= @MIP AND a . HasCalculated = 1 and CONVERT (VARCHAR ( 10), cast( a .datainserted as date ), 101 )> @dt


--Select * from @TempData_NOI


IF (@MIP = 3)
Begin




   -- SET @QuaterlyNOI = ( select COALESCE (MAX (NOICumulative ), 0) from @TempData_NOI where MonthsInPeriod = @MIP )
    SET @QuaterlyOperatingRevenue = ( select COALESCE ( MAX ( CumulativeRevenue ), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP )
    SET @QuarterlyResidentDays = ( select COALESCE ( MAX ( CumulativeResidentDays ), 0 ) from  @TempData_NOI where MonthsInPeriod = @MIP )
    SET @ADRperQuarter = ( select COALESCE ( MAX ( AverageDailyRateRatio ), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP )
    SET @QuarterlyOperatingExpense = ( select COALESCE ( MAX ( CumulativeExpenses ), 0 ) from  @TempData_NOI where MonthsInPeriod = @MIP )
    SET @QuarterlyFHAPI = ( select COALESCE ( MAX ( FHAPICumulative ), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP )
    SET @QuarterlyMIP = ( select COALESCE ( MAX ( MIPCumulative ), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP )
    SET @QuarterlyDSCR = ( select COALESCE ( MAX ( DebtCoverageRatio2 ), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP )

	       --Quaterly NOi Calc
        set @QuaterlyNOI=( COALESCE ( MAX (@QuaterlyOperatingRevenue ), 0)- COALESCE ( MAX (@QuarterlyOperatingExpense ), 0))
           IF ( ( select COALESCE ( MAX ( CumulativeResidentDays ), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP ) > 0 )
                BEGIN
                SET @AverageDailyRateRatio = ((( select COALESCE (MAX ( CumulativeRevenue), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP )))/ ( select COALESCE (MAX ( CumulativeResidentDays), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP )
                END
       
               set @NOICumulative =( select CumulativeRevenue from @TempData_NOI where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( CumulativeExpenses), 0 )  from @TempData_NOI where MonthsInPeriod = @MIP )
      --  if @QuarterlyDSCR is null
      -- --there is no orderID so set value to C COALESCE(MAX(CumulativeRevenue), 0)
      --SET @QuarterlyDSCR = (select COALESCE( MAX(CumulativeRevenue ), 0) from @TempData_NOI where MonthsInPeriod= @MIP)-(select COALESCE( MAX(CumulativeExpenses ), 0) from @TempData_NOI where MonthsInPeriod =@MIP)
      --  -- Update [$(DatabaseName)]. [dbo] . [Lender_DataUpload_Intermediate] set DebtCoverageRatio2 = @QuarterlyDSCR WHERE LDI_ID = @LDI_ID
      
  End
 

ELSE IF ( @MIP = 6 )
begin


if NOT exists ( select *   from @TempData_NOI where MonthsInPeriod = 3)
  BEGIN
  Update [$(DatabaseName)]. [dbo] . [Lender_DataUpload_Intermediate] set  Ismissing= 1 where LDI_ID = @LDI_ID
  -- set @QuaterlyNOI = ( select NOICumulative from @TempData_NOI where MonthsInPeriod=@MIP)-( select COALESCE(MAX (NOICumulative), 0) from @TempData_NOI where MonthsInPeriod = 3)
        set @QuaterlyOperatingRevenue = ( select CumulativeRevenue from @TempData_NOI where  MonthsInPeriod= @MIP )
        set @QuarterlyResidentDays = (select CumulativeResidentDays from @TempData_NOI where MonthsInPeriod= @MIP )
       -- set @ADRperQuarter = (select AverageDailyRateRatio from @TempData_NOI where MonthsInPeriod=@MIP)-
        set @QuarterlyOperatingExpense = (select CumulativeExpenses from @TempData_NOI where MonthsInPeriod= @MIP )
        set @QuarterlyFHAPI = (select FHAPICumulative from @TempData_NOI where MonthsInPeriod= @MIP)
        set @QuarterlyMIP = (select MIPCumulative from @TempData_NOI where MonthsInPeriod= @MIP)

		       --Quaterly NOi Calc
        set @QuaterlyNOI=( COALESCE ( MAX (@QuaterlyOperatingRevenue ), 0)- COALESCE ( MAX (@QuarterlyOperatingExpense ), 0))


  END

  ELSE

       -- set @QuaterlyNOI = ( select NOICumulative from @TempData_NOI where MonthsInPeriod=@MIP)-( select COALESCE(MAX (NOICumulative), 0) from @TempData_NOI where MonthsInPeriod = 3)
        set @QuaterlyOperatingRevenue = ( select CumulativeRevenue from @TempData_NOI where  MonthsInPeriod= @MIP )-( select COALESCE (MAX ( CumulativeRevenue), 0 )   from @TempData_NOI where MonthsInPeriod = 3 )
        set @QuarterlyResidentDays = (select CumulativeResidentDays from @TempData_NOI where MonthsInPeriod= @MIP )-( select COALESCE ( MAX ( CumulativeResidentDays), 0 ) from @TempData_NOI where MonthsInPeriod =3 )
       -- set @ADRperQuarter = (select AverageDailyRateRatio from @TempData_NOI where MonthsInPeriod=@MIP)-( select COALESCE(MAX (AverageDailyRateRatio), 0) from @TempData_NOI where MonthsInPeriod = 3)
        set @QuarterlyOperatingExpense = (select CumulativeExpenses from @TempData_NOI where MonthsInPeriod= @MIP )-( select COALESCE ( MAX ( CumulativeExpenses), 0 ) from @TempData_NOI where MonthsInPeriod =3 )
        set @QuarterlyFHAPI = (select FHAPICumulative from @TempData_NOI where MonthsInPeriod= @MIP)-( select COALESCE (MAX (FHAPICumulative ), 0) from @TempData_NOI where  MonthsInPeriod = 3 )
        set @QuarterlyMIP = (select MIPCumulative from @TempData_NOI where MonthsInPeriod= @MIP)-( select COALESCE (MAX (MIPCumulative ), 0) from @TempData_NOI where MonthsInPeriod = 3 )


               --Quaterly NOi Calc
        set @QuaterlyNOI=( COALESCE ( MAX (@QuaterlyOperatingRevenue ), 0)- COALESCE ( MAX (@QuarterlyOperatingExpense ), 0))

               --Quaterly ADR Calc
                IF ( ISNULL ( @QuarterlyResidentDays , 0 ) != 0 )
                BEGIN
              
                set @ADRperQuarter =(( COALESCE ( MAX (@QuaterlyOperatingRevenue ), 0))/ @QuarterlyResidentDays)
                END
				ELSE
				  BEGIN
				    set @ADRperQuarter=NULL
				   END

                --DSCR Calc
                  IF ( ISNULL ( @QuarterlyFHAPI , 0 ) + ISNULL ( @QuarterlyMIP , 0 ) != 0 )
                BEGIN
                 set @A=( COALESCE ( MAX (@QuaterlyNOI ), 0))
                 set @B= ISNULL ( @QuarterlyFHAPI , 0 ) + ISNULL ( @QuarterlyMIP , 0 )
                  set @QuarterlyDSCR = @A /@B
                END
				ELSE
				  BEGIN
				     set @QuarterlyDSCR=NULL
				  END
                ----------------------------------------------------------------------------------------------------------------
                 IF ( ( select COALESCE ( MAX ( CumulativeResidentDays ), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP ) != 0 )
                BEGIN
                SET @AverageDailyRateRatio = ((( select COALESCE (MAX ( CumulativeRevenue), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP )))/ ( select COALESCE (MAX ( CumulativeResidentDays), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP )
                END

               set @NOICumulative =( select CumulativeRevenue from @TempData_NOI where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( CumulativeExpenses), 0 )  from @TempData_NOI where MonthsInPeriod = @MIP )
       

End
ELSE IF ( @MIP = 9 )
begin

if NOT exists ( select *   from @TempData_NOI where MonthsInPeriod = 6)
  BEGIN
  Update [$(DatabaseName)]. [dbo] . [Lender_DataUpload_Intermediate] set  Ismissing= 1 where LDI_ID = @LDI_ID
  -- set @QuaterlyNOI = ( select NOICumulative from @TempData_NOI where MonthsInPeriod=@MIP)-( select COALESCE(MAX (NOICumulative), 0) from @TempData_NOI where MonthsInPeriod = 3)
        set @QuaterlyOperatingRevenue = ( select CumulativeRevenue from @TempData_NOI where  MonthsInPeriod= @MIP )
        set @QuarterlyResidentDays = (select CumulativeResidentDays from @TempData_NOI where MonthsInPeriod= @MIP )
       -- set @ADRperQuarter = (select AverageDailyRateRatio from @TempData_NOI where MonthsInPeriod=@MIP)-
        set @QuarterlyOperatingExpense = (select CumulativeExpenses from @TempData_NOI where MonthsInPeriod= @MIP )
        set @QuarterlyFHAPI = (select FHAPICumulative from @TempData_NOI where MonthsInPeriod= @MIP)
        set @QuarterlyMIP = (select MIPCumulative from @TempData_NOI where MonthsInPeriod= @MIP)

		       --Quaterly NOi Calc
        set @QuaterlyNOI=( COALESCE ( MAX (@QuaterlyOperatingRevenue ), 0)- COALESCE ( MAX (@QuarterlyOperatingExpense ), 0))


  END

  ELSE


  --set @QuaterlyNOI = ( select NOICumulative from @TempData_NOI where MonthsInPeriod=@MIP)-( select COALESCE(MAX (NOICumulative), 0) from @TempData_NOI where MonthsInPeriod = 6)
  set @QuaterlyOperatingRevenue = ( select CumulativeRevenue from @TempData_NOI where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( CumulativeRevenue), 0 ) from @TempData_NOI where MonthsInPeriod = 6 )
  set @QuarterlyResidentDays = ( select CumulativeResidentDays from @TempData_NOI where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( CumulativeResidentDays), 0 ) from @TempData_NOI where MonthsInPeriod = 6 )
  set @ADRperQuarter = ( select AverageDailyRateRatio from @TempData_NOI where MonthsInPeriod= @MIP)-( select COALESCE (MAX (AverageDailyRateRatio ), 0) from @TempData_NOI where MonthsInPeriod = 6 )
  set @QuarterlyOperatingExpense = ( select CumulativeExpenses from @TempData_NOI where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( CumulativeExpenses), 0 ) from @TempData_NOI where MonthsInPeriod = 6 )
  set @QuarterlyFHAPI = ( select FHAPICumulative from @TempData_NOI where MonthsInPeriod= @MIP)-( select COALESCE (MAX (FHAPICumulative ), 0) from @TempData_NOI where MonthsInPeriod =  6 )
  set @QuarterlyMIP = ( select MIPCumulative from @TempData_NOI where MonthsInPeriod= @MIP)-( select COALESCE (MAX (MIPCumulative ), 0) from @TempData_NOI where MonthsInPeriod = 6 )
  --Quaterly NOi Calc
        set @QuaterlyNOI=( COALESCE ( MAX (@QuaterlyOperatingRevenue ), 0)- COALESCE ( MAX (@QuarterlyOperatingExpense ), 0))

               --Quaterly ADR Calc
                IF ( ISNULL ( @QuarterlyResidentDays , 0 ) != 0 )
                BEGIN
              
                set @ADRperQuarter =(( COALESCE ( MAX (@QuaterlyOperatingRevenue ), 0))/ @QuarterlyResidentDays)
                END
				ELSE
				
				  BEGIN
				    set @ADRperQuarter=NULL
				   END
  --DSCR Calc
                  IF ( ISNULL ( @QuarterlyFHAPI , 0 ) + ISNULL ( @QuarterlyMIP , 0 ) != 0 )
                BEGIN
                 set @A=( COALESCE ( MAX (@QuaterlyNOI ), 0))
                 set @B= ISNULL ( @QuarterlyFHAPI , 0 ) + ISNULL ( @QuarterlyMIP , 0 )
                  set @QuarterlyDSCR = @A /@B
                END
				ELSE
				  BEGIN
				     set @QuarterlyDSCR=NULL
				  END
-------------------------------------------------------------------------------
    IF ( (select COALESCE ( MAX( CumulativeResidentDays ), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP ) != 0 )
                BEGIN
                SET @AverageDailyRateRatio = ((( select COALESCE (MAX ( CumulativeRevenue), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP )))/ ( select COALESCE (MAX ( CumulativeResidentDays), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP )
                END
                      set @NOICumulative =( select CumulativeRevenue from @TempData_NOI where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( CumulativeExpenses), 0 )   from @TempData_NOI where MonthsInPeriod = @MIP )

 
  end
ELSE IF ( @MIP = 12 )
begin

if NOT exists ( select *   from @TempData_NOI where MonthsInPeriod = 9)
  BEGIN
  Update [$(DatabaseName)]. [dbo] . [Lender_DataUpload_Intermediate] set  Ismissing= 1 where LDI_ID = @LDI_ID
  -- set @QuaterlyNOI = ( select NOICumulative from @TempData_NOI where MonthsInPeriod=@MIP)-( select COALESCE(MAX (NOICumulative), 0) from @TempData_NOI where MonthsInPeriod = 3)
        set @QuaterlyOperatingRevenue = ( select CumulativeRevenue from @TempData_NOI where  MonthsInPeriod= @MIP )
        set @QuarterlyResidentDays = (select CumulativeResidentDays from @TempData_NOI where MonthsInPeriod= @MIP )
       -- set @ADRperQuarter = (select AverageDailyRateRatio from @TempData_NOI where MonthsInPeriod=@MIP)-
        set @QuarterlyOperatingExpense = (select CumulativeExpenses from @TempData_NOI where MonthsInPeriod= @MIP )
        set @QuarterlyFHAPI = (select FHAPICumulative from @TempData_NOI where MonthsInPeriod= @MIP)
        set @QuarterlyMIP = (select MIPCumulative from @TempData_NOI where MonthsInPeriod= @MIP)
		       --Quaterly NOi Calc
        set @QuaterlyNOI=( COALESCE ( MAX (@QuaterlyOperatingRevenue ), 0)- COALESCE ( MAX (@QuarterlyOperatingExpense ), 0))

  END

  ELSE


--set @QuaterlyNOI= (select NOICumulative from @TempData_NOI where MonthsInPeriod=@MIP)-( select COALESCE(MAX (NOICumulative), 0) from @TempData_NOI where MonthsInPeriod = 9)
set @QuaterlyOperatingRevenue= (select CumulativeRevenue from @TempData_NOI where MonthsInPeriod= @MIP)-( select COALESCE (MAX (CumulativeRevenue ), 0) from @TempData_NOI where MonthsInPeriod = 9 )
set @QuarterlyResidentDays= (select CumulativeResidentDays from @TempData_NOI where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( CumulativeResidentDays), 0 ) from @TempData_NOI where MonthsInPeriod = 9 )
set @ADRperQuarter= (select AverageDailyRateRatio from @TempData_NOI where MonthsInPeriod= @MIP)-( select COALESCE (MAX (AverageDailyRateRatio ), 0) from @TempData_NOI where MonthsInPeriod = 9 )
set @QuarterlyOperatingExpense= (select CumulativeExpenses from @TempData_NOI where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( CumulativeExpenses), 0 ) from @TempData_NOI where MonthsInPeriod = 9 )
set @QuarterlyFHAPI= (select FHAPICumulative from @TempData_NOI where MonthsInPeriod= @MIP)-( select COALESCE (MAX (FHAPICumulative ), 0) from @TempData_NOI where MonthsInPeriod = 9 )
set @QuarterlyMIP= (select MIPCumulative from @TempData_NOI where MonthsInPeriod= @MIP)-( select COALESCE (MAX (MIPCumulative ), 0) from @TempData_NOI where MonthsInPeriod = 9 )
--Quaterly NOi Calc
        set @QuaterlyNOI=( COALESCE ( MAX (@QuaterlyOperatingRevenue ), 0)- COALESCE ( MAX (@QuarterlyOperatingExpense ), 0))

               --Quaterly ADR Calc
                IF ( ISNULL ( @QuarterlyResidentDays , 0 ) != 0 )
                BEGIN
              
                set @ADRperQuarter =(( COALESCE ( MAX (@QuaterlyOperatingRevenue ), 0))/ @QuarterlyResidentDays)
                END
				ELSE
				  BEGIN
				    set @ADRperQuarter=NULL
				   END
                --DSCR Calc
                  IF ( ISNULL ( @QuarterlyFHAPI , 0 ) + ISNULL ( @QuarterlyMIP , 0 ) != 0 )
                BEGIN
                 set @A=( COALESCE ( MAX (@QuaterlyNOI ), 0))
                 set @B= ISNULL ( @QuarterlyFHAPI , 0 ) + ISNULL ( @QuarterlyMIP , 0 )
                  set @QuarterlyDSCR = @A /@B
                END
				ELSE
				  BEGIN
				     set @QuarterlyDSCR=NULL
				  END
                ------------------------------------------
   IF ( (select COALESCE ( MAX( CumulativeResidentDays ), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP ) != 0 )
                BEGIN
                SET @AverageDailyRateRatio = ((( select COALESCE (MAX ( CumulativeRevenue), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP )))/ ( select COALESCE (MAX ( CumulativeResidentDays), 0 ) from @TempData_NOI where MonthsInPeriod = @MIP )
                END
                      set @NOICumulative =( select CumulativeRevenue from @TempData_NOI where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( CumulativeExpenses), 0 )   from @TempData_NOI where MonthsInPeriod = @MIP )

End
--Logic to Update NOI QuarterbyQuarter
Update [$(DatabaseName)]. [dbo] . [Lender_DataUpload_Intermediate] set QuaterlyNOI =  @QuaterlyNOI, IsProcessed =1 where LDI_ID= @LDI_ID
Update [$(DatabaseName)]. [dbo] . [Lender_DataUpload_Intermediate] set QuaterlyOperatingRevenue = @QuaterlyOperatingRevenue , IsProcessed =1 WHERE LDI_ID= @LDI_ID
Update [$(DatabaseName)]. [dbo] . [Lender_DataUpload_Intermediate] set QuarterlyResidentDays = @QuarterlyResidentDays , IsProcessed =1 WHERE LDI_ID= @LDI_ID
Update [$(DatabaseName)]. [dbo] . [Lender_DataUpload_Intermediate] set ADRperQuarter = @ADRperQuarter , IsProcessed =1 WHERE LDI_ID= @LDI_ID
Update [$(DatabaseName)]. [dbo] . [Lender_DataUpload_Intermediate] set QuarterlyOperatingExpense = @QuarterlyOperatingExpense , IsProcessed = 1 WHERE LDI_ID= @LDI_ID
Update [$(DatabaseName)]. [dbo] . [Lender_DataUpload_Intermediate] set QuarterlyFHAPI = @QuarterlyFHAPI , IsProcessed =1 WHERE LDI_ID= @LDI_ID
Update [$(DatabaseName)]. [dbo] . [Lender_DataUpload_Intermediate] set QuarterlyMIP = @QuarterlyMIP , IsProcessed =1 WHERE LDI_ID= @LDI_ID
Update [$(DatabaseName)]. [dbo] . [Lender_DataUpload_Intermediate] set QuarterlyDSCR = @QuarterlyDSCR , IsProcessed =1 WHERE LDI_ID= @LDI_ID
Update [$(DatabaseName)]. [dbo] . [Lender_DataUpload_Intermediate] set NOICumulative  = @NOICumulative  , IsProcessed =1 WHERE LDI_ID= @LDI_ID
Update [$(DatabaseName)]. [dbo] . [Lender_DataUpload_Intermediate] set AverageDailyRateRatio  = @AverageDailyRateRatio  , IsProcessed = 1 WHERE LDI_ID = @LDI_ID
select @LDI_ID = min ( LDI_ID ) from  [$(DatabaseName)]. [dbo]. [fn_HCP_GeLatestUpload] ()  a  where a. LDI_ID > @LDI_ID and CONVERT (VARCHAR ( 10), cast (a . datainserted as date), 101 )>@dt and    a. IsProcessed is null

end
   
   
   

Commit transaction
      
End try
Begin catch
        print ERROR_MESSAGE ()
        Rollback transaction
End catch
