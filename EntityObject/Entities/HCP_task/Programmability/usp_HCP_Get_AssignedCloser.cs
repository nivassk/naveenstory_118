﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    public partial class usp_HCP_Get_AssignedCloser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
