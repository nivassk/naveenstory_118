﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    public partial class usp_HCP_GetReviewerUserIdByTaskInstanceIdResult
    {
        public int UserId { get; set; }
    }
}
