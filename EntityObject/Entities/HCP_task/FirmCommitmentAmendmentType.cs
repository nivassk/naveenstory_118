﻿namespace EntityObject.Entities.HCP_task
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Table("FirmCommitmentAmendmentType")]
	public partial class FirmCommitmentAmendmentType
	{
		[Key]
		public int FirmCommitmentAmendmentTypeId { get; set; }
		public string FirmCommitmentAmendment { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }
		
	}
}

