﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    public class ReviewFileStatus
    {
        [Key]
        public Guid ReviewFileStatusId { get; set;}
        public Guid TaskFileId { get; set;}
        public int ReviewerUserId { get; set;}
        public int Status { get; set;}
        public DateTime  ModifiedOn { get; set;}
        public int ModifiedBy { get; set; }
        public bool? IsChildTaskCreated { get; set; }
        public int? ReviewerProdViewId { get; set; }
    }
}
