﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace EntityObject.Entities.HCP_task
{
    public class TaskReAssignment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
         public int TaskReAssignId { get; set; }
         public Guid TaskInstanceId { get; set; }
         public string FromAE { get; set; }
         [Required]
         [StringLength(150)]
         public string ReAssignedTo { get; set; }
         public DateTime CreatedOn { get; set; }
         public int CreatedBy { get; set; }
         public bool? Deleted_Ind { get; set; }
    }
}
