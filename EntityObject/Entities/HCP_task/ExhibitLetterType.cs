﻿namespace EntityObject.Entities.HCP_task
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Table("ExhibitLetterType")]
	public partial class ExhibitLetterType
	{
		[Key]
		public int ExhibitLetterTypeId { get; set; }
		public string ExhibitLetter { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }

	}
}

