﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
   public class DocumentTypes
    {
        [Key]
       public int DocumentTypeId { get; set; }
       [Required]
        public string DocumentType { get; set; }
       [Required]
       public string ShortDocTypeName { get; set; }
       public int FolderKey { get; set; }
       
    }
}
