﻿

namespace EntityObject.Entities.HCP_task
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

     [Table("Prod_TaskXref")]
    public partial class Prod_TaskXref
    {
       
        [Key]
        public Guid TaskXrefid { get; set; }
        public int TaskId { get; set; }
        public Guid TaskInstanceId { get; set; }
        public int ? AssignedBy { get; set; }
        public int ? AssignedTo { get; set; }
        public string Comments { get; set; }
        public bool IsReviewer { get; set; }
        public int ViewId { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public int Status { get; set; }
        public DateTime ? CompletedOn { get; set; }
        public DateTime? AssignedDate { get; set; }
    }
}
