﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    [Table("RequestAdditionalInfoFiles")]
    public class RequestAdditionalInfoFiles
    {
        [Key]
        public Guid RequestAdditionalFileId { get; set; }
        public Guid TaskFileId { get; set; }
        public Guid ChildTaskInstanceId { get; set; }
    }
}
