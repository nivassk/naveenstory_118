﻿using EntityObject.Interfaces;
using HUDHealthcarePortal.Core;

namespace EntityObject.Entities.HCP_intermediate
{
    // put derive from FundamentalFinancial here, so it won't be overwritten with auto code gen (database updates)
    public partial class Lender_DataUpload_Intermediate : FundamentalFinancial, IAuditable
    {
    }
}

namespace EntityObject.Entities.HCP_live
{
    public partial class usp_HCP_Get_Portfolio_Aggregate_Result : FundamentalFinancial
    {
        public decimal? ReserveForReplacementBalancePerUnit { get; set; }
        public decimal? WorkingCapital { get; set; }
        public decimal? DebtCoverageRatio { get; set; }
        public decimal? DaysCashOnHand { get; set; }
        public decimal? DaysInAcctReceivable { get; set; }
        public decimal? AvgPaymentPeriod { get; set; }
        public decimal? WorkingCapitalScore { get; set; }
        public decimal? DebtCoverageRatioScore { get; set; }
        public decimal? DaysCashOnHandScore { get; set; }
        public decimal? DaysInAcctReceivableScore { get; set; }
        public decimal? AvgPaymentPeriodScore { get; set; }
        public bool? HasCalculated { get; set; }
        public decimal? ScoreTotal { get; set; }
    }

    public partial class HCP_Authentication : IAuditable
    { }
    public partial class HCP_Attornies : IAuditable
    { }
    public partial class HCP_FunctionPoint_Ref : IAuditable
    { }
    public partial class HCP_Project_Action : IAuditable
    { }
    public partial class HCP_Sections : IAuditable
    { }
    public partial class HUD_Project_Manager : IAuditable
    { }
    public partial class HUD_WorkLoad_Manager : IAuditable
    { }
    public partial class Address : IAuditable
    { }
    public partial class User_Lender : IAuditable
    { }
    public partial class Comment : IAuditable
    { }
    public partial class HCP_Email : IAuditable
    { }
    public partial class User_WLM_PM : IAuditable
    { }
}