namespace EntityObject.Entities.HCP_intermediate
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vwComment")]
    public partial class vwComment
    {
        [Key]
        [Column(Order = 0)]
        public int CommentId { get; set; }

        public int? PropertyId { get; set; }

        [StringLength(15)]
        public string FHANumber { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(256)]
        public string Subject { get; set; }

        [Column(TypeName = "xml")]
        public string Comments { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ModifiedBy { get; set; }

        [Key]
        [Column(Order = 3)]
        public DateTime ModifiedOn { get; set; }

        public int? OnBehalfOfBy { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LDI_ID { get; set; }
    }
}
