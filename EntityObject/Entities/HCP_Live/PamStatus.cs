﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
    
    [Table("PamStatus")]
    public partial class PamStatus
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public int PamStatusId { get; set;}
        public string StatusDescription { get; set; }
        public int ModuleId { get; set; }
        public int displayorder { get; set; }
    }
}
