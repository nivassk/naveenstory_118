﻿namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class NonCriticalLateTimeSpan
    {
        [Required] 
        public int TimeSpanId { get; set; }
        public string Description { get; set; } 
    }
}