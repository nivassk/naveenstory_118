namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HCP_Attornies
    {
        [Key]
        public int AttorneyID { get; set; }

        [Required]
        [StringLength(100)]
        public string AttorneyName { get; set; }

        [StringLength(100)]
        public string AttorneyTitle { get; set; }

        [StringLength(50)]
        public string Office { get; set; }

        [StringLength(50)]
        public string Region { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public bool? Deleted_Ind { get; set; }
    }
}
