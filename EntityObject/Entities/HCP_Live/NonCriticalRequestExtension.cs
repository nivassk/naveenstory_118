﻿
namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class NonCriticalRequestExtension
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid NonCriticalRequestExtensionId { get; set; }
        public int PropertyID { get; set; }

        [Required]
        [StringLength(15)]
        public string FHANumber { get; set; }
        public int? LenderID { get; set; }
        public DateTime? ClosingDate { get; set; }
        public int? ExtensionPeriod { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int ExtensionRequestStatus { get; set; }
        public string Comments { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public int? TaskId { get; set; }

    }
}