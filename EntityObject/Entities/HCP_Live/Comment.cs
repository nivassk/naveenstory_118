namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Comment")]
    public partial class Comment
    {
        public int CommentId { get; set; }

        public int? PropertyId { get; set; }

        [StringLength(15)]
        public string FHANumber { get; set; }

        [Required]
        [StringLength(256)]
        public string Subject { get; set; }

        [Column(TypeName = "xml")]
        public string Comments { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public int LDI_ID { get; set; }
    }
}
