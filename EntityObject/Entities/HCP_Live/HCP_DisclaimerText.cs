﻿
namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

     [Table("HCP_DisclaimerText")]
    public partial class HCP_DisclaimerText
    {
         [Key]
         public int DisclaimerTextId { get; set; }

         [Required]
         [MaxLength]
         public string DisclaimerTextDescription { get; set; }
    }
}
