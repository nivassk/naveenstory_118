﻿namespace EntityObject.Entities.HCP_live
{
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;


    public partial class HUD_WorkLoad_Manager_Portfolio_Manager
    {

        [Key]
        [Column(Order = 0) ]
        public int HUD_WorkLoad_Manager_ID { get; set; }
         [Key]
         [Column(Order = 1)]
        public int HUD_Project_Manager_ID{get;set;}
      
    }
}
