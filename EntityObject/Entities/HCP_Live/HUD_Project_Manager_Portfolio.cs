namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HUD_Project_Manager_Portfolio
    {
        [Key]
        public int HUD_Project_Manager_Portfolio_ID { get; set; }

        public int? HUD_Project_Manager_ID { get; set; }

        [StringLength(15)]
        public string ProjectID_FHANumber { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public virtual FHAInfo_iREMS FHAInfo_iREMS { get; set; }

        public virtual HUD_Project_Manager HUD_Project_Manager { get; set; }
    }
}
