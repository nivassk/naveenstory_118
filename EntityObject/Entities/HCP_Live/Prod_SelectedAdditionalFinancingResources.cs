﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
    [Table("Prod_SelectedAdditionalFinancingResources")]
    public class Prod_SelectedAdditionalFinancingResources
    {
        public int Id { get; set; }
        public Guid FHANumberRequestId { get; set; }
        public int ResourceId { get; set; }
    }
}
