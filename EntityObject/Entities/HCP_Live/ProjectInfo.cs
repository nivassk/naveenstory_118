namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProjectInfo")]
    public partial class ProjectInfo
    {
        public int ProjectInfoID { get; set; }

        public int PropertyID { get; set; }

        [Required]
        [StringLength(15)]
        public string FHANumber { get; set; }

        [StringLength(100)]
        public string ProjectName { get; set; }

        public int? Property_AddressID { get; set; }

        public int? LenderID { get; set; }

        public int? ServicerID { get; set; }

        public int? ManagementID { get; set; }

        public int? HUD_WorkLoad_Manager_ID { get; set; }

        public int? HUD_Project_Manager_ID { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public int? Primary_Loan_Code { get; set; }

        public DateTime? Initial_Endorsement_Date { get; set; }

        public DateTime? Final_Endorsement_Date { get; set; }

        [StringLength(35)]
        public string Field_Office_Status_Name { get; set; }

        [StringLength(5)]
        public string Soa_Code { get; set; }

        [StringLength(50)]
        public string Soa_Numeric_Name { get; set; }

        [StringLength(100)]
        public string Soa_Description_Text { get; set; }

        [StringLength(25)]
        public string Troubled_Code { get; set; }

        [StringLength(100)]
        public string Troubled_status_update_date { get; set; }

        [StringLength(30)]
        public string Reac_Last_Inspection_Score { get; set; }

        public DateTime? Reac_Last_Inspection_Date { get; set; }

        [StringLength(15)]
        public string Mddr_In_Default_Status_Name { get; set; }

        [StringLength(5)]
        public string Is_Active_Dec_Case_Ind { get; set; }

        public decimal? Original_Loan_Amount { get; set; }

        public decimal? Original_Interest_Rate { get; set; }

        public decimal? Amortized_Unpaid_Principal_Bal { get; set; }

        [StringLength(15)]
        public string Is_Nursing_Home_Ind { get; set; }

        [StringLength(15)]
        public string Is_Board_And_Care_Ind { get; set; }

        [StringLength(15)]
        public string Is_Assisted_Living_Ind { get; set; }

        public bool? Deleted_Ind { get; set; }

        public int? Lender_AddressID { get; set; }

        [StringLength(80)]
        public string Lender_Mortgagee_Name { get; set; }

        public int? Servicer_AddressID { get; set; }

        [StringLength(80)]
        public string Servicing_Mortgagee_Name { get; set; }

        public int? Owner_AddressID { get; set; }

        [StringLength(25)]
        public string Owner_Company_Type { get; set; }

        [StringLength(50)]
        public string Owner_Legal_Structure_Name { get; set; }

        [StringLength(80)]
        public string Owner_Organization_Name { get; set; }

        public int? Owner_Contact_AddressID { get; set; }

        [StringLength(50)]
        public string Owner_Contact_Indv_Fullname { get; set; }

        [StringLength(50)]
        public string Owner_Contact_Indv_Title { get; set; }

        public int? Mgmt_Agent_AddressID { get; set; }

        [StringLength(25)]
        public string Mgmt_Agent_Company_Type { get; set; }

        [StringLength(80)]
        public string Mgmt_Agent_org_name { get; set; }

        public int? Mgmt_Contact_AddressID { get; set; }

        [StringLength(50)]
        public string Mgmt_Contact_FullName { get; set; }

        [StringLength(50)]
        public string Mgmt_Contact_Indv_Title { get; set; }

        [StringLength(15)]
        public string Portfolio_Number { get; set; }

        [StringLength(100)]
        public string Portfolio_Name { get; set; }

        public virtual HUD_Project_Manager HUD_Project_Manager { get; set; }

        public virtual HUD_WorkLoad_Manager HUD_WorkLoad_Manager { get; set; }

        public virtual LenderInfo LenderInfo { get; set; }

        public virtual PropertyInfo_iREMS PropertyInfo_iREMS { get; set; }
    }
}
