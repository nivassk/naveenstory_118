namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User_SecurityQuestion
    {
        public int User_SecurityQuestionID { get; set; }

        public int User_ID { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public bool? Deleted_Ind { get; set; }

        [StringLength(128)]
        public string Answer { get; set; }

        public int SecurityQuestionID { get; set; }

        public virtual HCP_Authentication HCP_Authentication { get; set; }

        public virtual SecurityQuestion SecurityQuestion { get; set; }
    }
}
