﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
    public class HCP_PageType
    {
        [Key]
        public byte PageTypeId { get; set; }
        public string PageTypeDescription { get; set; }
        public int ModuleId { get; set; }

        public int Orderby { get; set; }
    }
}
