namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("State")]
    public partial class State
    {
        public State()
        {
            Addresses = new HashSet<Address>();
        }

        [Key]
        [StringLength(2)]
        public string StateCode { get; set; }

        [Required]
        [StringLength(40)]
        public string StateName { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }
    }
}
