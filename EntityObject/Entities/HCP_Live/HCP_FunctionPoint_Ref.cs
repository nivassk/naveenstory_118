namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HCP_FunctionPoint_Ref
    {
        public HCP_FunctionPoint_Ref()
        {
            HCP_User_FunctionPoint = new HashSet<HCP_User_FunctionPoint>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HCP_FunctionPoint_ID { get; set; }

        [Required]
        [StringLength(50)]
        public string HCP_FunctionPoint_Name { get; set; }

        [Required]
        [StringLength(256)]
        public string HCP_FunctionPoint_Desc { get; set; }

        public bool Deleted_Ind { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public virtual ICollection<HCP_User_FunctionPoint> HCP_User_FunctionPoint { get; set; }
    }
}
