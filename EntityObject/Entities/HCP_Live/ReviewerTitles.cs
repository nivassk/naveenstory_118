﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
    [Table("Reviewer_Titles")]
   public partial class ReviewerTitles
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public int Reviewer_Id {get; set;}
        public string Reviewer_Name { get; set; }
        public int? TitleTypeId { get; set; }

    }
}
