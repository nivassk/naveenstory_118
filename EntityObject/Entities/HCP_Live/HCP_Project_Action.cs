namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HCP_Project_Action
    {
        [Key]
        public int ProjectActionID { get; set; }

        [Required]
        [StringLength(100)]
        public string ProjectActionName { get; set; }

        [Required]
        [StringLength(200)]
        public string ProjectActionLongName { get; set; }

        public int? AE_DTC { get; set; }

        public int? Hours { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public bool? Deleted_Ind { get; set; }

        public byte PageTypeId { get; set; }
    }
}
