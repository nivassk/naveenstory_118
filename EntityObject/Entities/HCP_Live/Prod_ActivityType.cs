﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
    [Table("Prod_ActivityType")]
    public class Prod_ActivityType
    {
        [Key]
        public int ActivityTypeId { get; set;}
        public string ActivityName { get; set;}
    }
}
