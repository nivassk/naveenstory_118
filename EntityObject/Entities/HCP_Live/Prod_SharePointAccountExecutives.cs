﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
    [Table("Prod_SharePointAccountExecutives")]
    public class Prod_SharePointAccountExecutives
    {
        [Key]
        public int SharePointAEId { get; set; }
        public string SharePointAEName { get; set; }
    }
}
