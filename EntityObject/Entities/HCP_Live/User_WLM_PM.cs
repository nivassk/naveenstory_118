namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User_WLM_PM
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserID { get; set; }

        public int? HUD_WorkLoad_Manager_ID { get; set; }

        public int? HUD_Project_Manager_ID { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public bool? Deleted_Ind { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public virtual HCP_Authentication HCP_Authentication { get; set; }

        public virtual HUD_Project_Manager HUD_Project_Manager { get; set; }

        public virtual HUD_WorkLoad_Manager HUD_WorkLoad_Manager { get; set; }
    }
}
