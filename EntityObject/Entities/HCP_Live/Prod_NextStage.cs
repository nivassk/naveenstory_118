﻿

 namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

     [Table("Prod_NextStage")]
    public partial class Prod_NextStage
    {
       
        [Key]
        public Guid NextStgId { get; set; }
        public int CompletedPgTypeId { get; set; }
        public int NextPgTypeId { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public string FhaNumber { get; set; }
        public Guid? CompletedPgTaskInstanceId { get; set; }
        public Guid? NextPgTaskInstanceId { get; set; }
        public Boolean? IsNew { get; set; }

        
    }
}
