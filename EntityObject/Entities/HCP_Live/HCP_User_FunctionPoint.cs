namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HCP_User_FunctionPoint
    {
        [Key]
        public int HCP_User_FunctionPoint_ID { get; set; }

        public int UserID { get; set; }

        public int HCP_FunctionPoint_ID { get; set; }

        public bool Deleted_Ind { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public virtual HCP_Authentication HCP_Authentication { get; set; }

        public virtual HCP_FunctionPoint_Ref HCP_FunctionPoint_Ref { get; set; }
    }
}
