﻿namespace EntityObject.Entities.HCP_live
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Table("HUDSignature")]
	public partial class HUDSignature
	{
		[Key]
		public int Id { get; set; }
		public int UserId { get; set; }
		public byte[] Signature { get; set; }

	}
}
