﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace EntityObject.Entities.HCP_live
{
    public partial class HUD_WorkloadManager_InternalSpecialOptionUser
    {
        [Key]
        [Column(Order = 0)]
        public int HUD_WorkloadManagerId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int InternalSpecialOptionUserId { get; set; }
    }
}
