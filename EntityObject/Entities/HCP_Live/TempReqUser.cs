namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TempReqUser
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string LastName { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string UserMInitial { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string UserName { get; set; }

        [Key]
        [Column(Order = 4)]
        public DateTime ModifiedOn { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public int? AddressID { get; set; }

        public bool? Deleted_Ind { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(50)]
        public string PreferredTimeZone { get; set; }

        public string PasswordHist { get; set; }

        public bool? IsRegisterComplete { get; set; }
    }
}
