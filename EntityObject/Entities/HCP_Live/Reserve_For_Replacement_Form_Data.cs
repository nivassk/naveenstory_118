﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.ComponentModel.DataAnnotations;
    public partial  class Reserve_For_Replacement_Form_Data
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid R4RId { get; set; }

        [Required]
        public DateTime BorrowersRequestDate { get; set; }

        [Required]
        public DateTime ServicerSubmissionDate { get; set; }

        [Required]
        [StringLength(15)]
        public string FHANumber { get; set; }

        [Required]
        [StringLength(100)]
        public string PropertyName { get; set; }

        [Required]
        public int PropertyId { get; set; }

        [Required]
        public bool IsRequestForAdvance { get; set; }

        [Required]
        public int NumberOfUnits { get; set; }

        [Required]
        public decimal ReserveAccountBalance { get; set; }

        [Required]
        public DateTime ReserveAccountBalanceAsOfDate { get; set; }

        [Required]
        public decimal TotalPurchaseAmount { get; set; }

        [Required]
        public decimal TotalRequestedAmount { get; set; }

        [Required]
        public decimal TotalApprovedAmount { get; set; }

        [Required]
        public bool IsSingleItemExceedsFiftyThousand { get; set; }

        [Required]
        public bool IsPurchaseYearOlderThanR4RRequest { get; set; }

        [Required]
        public bool IsCommentEntered { get; set; }

        [StringLength(500)]
        public string ServicerRemarks { get; set; }

        [StringLength(500)]
        public string HudRemarks { get; set; }

        [StringLength(500)]
        public string AdditionalRemarks { get; set; }

        [Required]
        public int RequestStatus { get; set; }

        [StringLength(500)]
        public string AttachmentDescription { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? CreatedBy { get; set; }

        public bool? Deleted_Ind { get; set; }

        public int? TaskId { get; set; }
        
        public bool IsRemodelingProposed { get; set; }
        public bool IsMortgagePaymentsCovered { get; set; }
        public bool? IsLenderDelegate { get; set; }
        public bool? IsSuspension { get; set; }
        public Guid? TaskInstanceId { get; set; }
    }
}
