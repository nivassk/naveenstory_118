//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EntityObject.Entities.HCP_live
{
    using System;
    
    public partial class usp_HCP_Get_Multiple_Loans_PropertyID_Result
    {
        public int PropertyID { get; set; }
        public string ProjectName { get; set; }
        public string FHANumber { get; set; }
        public string FHADescription { get; set; }
        public Nullable<int> LenderID { get; set; }
        public string Lender_Name { get; set; }
        public string Soa_Description_Text { get; set; }
        public Nullable<int> PropertyID_Count { get; set; }
    }
}
