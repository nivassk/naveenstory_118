﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace EntityObject.Entities.HCP_live
{
    public class NonCriticalRulesChecklist
    {
        //public NonCriticalRulesChecklist()
        //{
        //    this.NonCriticalReferReasons = new HashSet<NonCriticalReferReason>();
        //}
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RuleId { get; set; }
        
        public string Description { get; set;}

        //public virtual ICollection<NonCriticalReferReason> NonCriticalReferReasons { get; set; } 
    }
}
