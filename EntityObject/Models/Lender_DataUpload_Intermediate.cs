using System;
using System.Collections.Generic;

namespace EntityObject.Models
{
    public partial class Lender_DataUpload_Intermediate
    {
        public string ProjectName { get; set; }
        public string ServiceName { get; set; }
        public string FHANumber { get; set; }
        public Nullable<int> LenderID { get; set; }
        public string OperatorOwner { get; set; }
        public string PeriodEnding { get; set; }
        public string MonthsInPeriod { get; set; }
        public string FinancialStatementType { get; set; }
        public Nullable<int> UnitsInFacility { get; set; }
        public Nullable<decimal> OperatingCash { get; set; }
        public Nullable<decimal> Investments { get; set; }
        public Nullable<decimal> ReserveForReplacementEscrowBalance { get; set; }
        public Nullable<decimal> AccountsReceivable { get; set; }
        public Nullable<decimal> CurrentAssets { get; set; }
        public Nullable<decimal> CurrentLiabilities { get; set; }
        public Nullable<decimal> TotalRevenues { get; set; }
        public Nullable<decimal> RentLeaseExpense { get; set; }
        public Nullable<decimal> DepreciationExpense { get; set; }
        public Nullable<decimal> AmortizationExpense { get; set; }
        public Nullable<decimal> TotalExpenses { get; set; }
        public Nullable<decimal> NetIncome { get; set; }
        public Nullable<decimal> ReserveForReplacementDeposit { get; set; }
        public Nullable<decimal> FHAInsuredPrincipalPayment { get; set; }
        public Nullable<decimal> FHAInsuredInterestPayment { get; set; }
        public Nullable<decimal> MortgageInsurancePremium { get; set; }
        public Nullable<int> PropertyID { get; set; }
        public int LDI_ID { get; set; }
        public Nullable<decimal> ReserveForReplacementBalancePerUnit { get; set; }
        public Nullable<decimal> WorkingCapital { get; set; }
        public Nullable<decimal> DebtCoverageRatio { get; set; }
        public Nullable<decimal> DaysCashOnHand { get; set; }
        public Nullable<decimal> DaysInAcctReceivable { get; set; }
        public Nullable<decimal> AvgPaymentPeriod { get; set; }
        public Nullable<decimal> WorkingCapitalScore { get; set; }
        public Nullable<decimal> DebtCoverageRatioScore { get; set; }
        public Nullable<decimal> DaysCashOnHandScore { get; set; }
        public Nullable<decimal> DaysInAcctReceivableScore { get; set; }
        public Nullable<decimal> AvgPaymentPeriodScore { get; set; }
        public Nullable<bool> HasCalculated { get; set; }
        public Nullable<decimal> ScoreTotal { get; set; }
        public int ModifiedBy { get; set; }
        public Nullable<int> OnBehalfOfBy { get; set; }
        public int UserID { get; set; }
        public System.DateTime DataInserted { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public Nullable<bool> IsUploadNotComplete { get; set; }
        public Nullable<bool> IsLate { get; set; }
    }
}
