using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EntityObject.Models.Mapping
{
    public class vwCommentMap : EntityTypeConfiguration<vwComment>
    {
        public vwCommentMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CommentId, t.Subject, t.ModifiedBy, t.ModifiedOn, t.LDI_ID });

            // Properties
            this.Property(t => t.CommentId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.FHANumber)
                .HasMaxLength(15);

            this.Property(t => t.Subject)
                .IsRequired()
                .HasMaxLength(256);

            this.Property(t => t.ModifiedBy)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LDI_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vwComment");
            this.Property(t => t.CommentId).HasColumnName("CommentId");
            this.Property(t => t.PropertyId).HasColumnName("PropertyId");
            this.Property(t => t.FHANumber).HasColumnName("FHANumber");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.Comments).HasColumnName("Comments");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.OnBehalfOfBy).HasColumnName("OnBehalfOfBy");
            this.Property(t => t.LDI_ID).HasColumnName("LDI_ID");
        }
    }
}
