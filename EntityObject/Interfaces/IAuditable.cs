﻿using System;

namespace EntityObject.Interfaces
{
    interface IAuditable
    {
        int ModifiedBy { get; set; }
        Nullable<int> OnBehalfOfBy { get; set; }
        DateTime ModifiedOn { get; set; } 
    }
}
