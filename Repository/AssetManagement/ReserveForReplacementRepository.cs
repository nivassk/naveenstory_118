﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model.AssetManagement;
using HUDHealthcarePortal.Repository.Interfaces.AssetManagement;


namespace HUDHealthcarePortal.Repository.AssetManagement
{
    public class ReserveForReplacementRepository : BaseRepository<Reserve_For_Replacement_Form_Data>, IReserveForReplacementRepository
    {
        public ReserveForReplacementRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }

        public ReserveForReplacementRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IQueryable<Reserve_For_Replacement_Form_Data> DataToJoin { get; set; }

        public IEnumerable<ReserveForReplacementFormModel> GetR4RFormByPropertyId(int propertyId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetR4RFormByPropertyId, please pass in correct context in unit of work.");

            var results = from r in context.Reserve_For_Replacement_Form_Data
                          where r.PropertyId == propertyId
                          select r;

            return Mapper.Map<IEnumerable<Reserve_For_Replacement_Form_Data>, IEnumerable<ReserveForReplacementFormModel>>(results);
        }

        public Guid SaveR4RForm(ReserveForReplacementFormModel model)
        {
            try
            {
                var formData = Mapper.Map<Reserve_For_Replacement_Form_Data>(model);
                formData.ModifiedOn = DateTime.UtcNow;
                formData.R4RId = Guid.NewGuid();
                formData.ModifiedBy = UserPrincipal.Current.UserId;
                formData.CreatedBy = UserPrincipal.Current.UserId;
                formData.AttachmentDescription = model.AttachmentsDescription;
                this.InsertNew(formData);
                return formData.R4RId;
            }
            catch (Exception ex)
            {

                //throw;
            }
            return new Guid();
        }

        public void UpdateR4RForm(ReserveForReplacementFormModel model)
        {
            try
            {
                var resultData = this.Find(p => p.R4RId == model.R4RId).First();
                if (resultData != null)
                {
                    resultData.BorrowersRequestDate = (DateTime)model.BorrowersRequestDate;
                    resultData.FHANumber = model.FHANumber;
                    resultData.PropertyId = model.PropertyId;                    
                    resultData.NumberOfUnits = Convert.ToInt32(model.NumberOfUnits);
                    resultData.ReserveAccountBalance = Convert.ToDecimal(model.ReserveAccountBalance);                    
                    resultData.ReserveAccountBalanceAsOfDate = Convert.ToDateTime(model.ReserveAccountBalanceAsOfDate);
                    resultData.TotalPurchaseAmount = Convert.ToDecimal(model.TotalPurchaseAmount);
                    resultData.TotalRequestedAmount = Convert.ToDecimal(model.TotalRequestedAmount);
                    resultData.TotalApprovedAmount = Convert.ToDecimal(model.TotalApprovedAmount);
                    resultData.IsSingleItemExceedsFiftyThousand = model.IsSingleItemExceedsFiftyThousand;
                    resultData.IsPurchaseYearOlderThanR4RRequest = model.IsPurchaseYearOlderThanR4RRequest;
                    resultData.IsRequestForAdvance = model.IsRequestForAdvance;
                    resultData.IsSuspension = model.IsSuspension;
                    resultData.IsCommentEntered = model.IsCommentEntered;
                    resultData.HudRemarks = model.HudRemarks;
                    resultData.AdditionalRemarks = model.AddnRemarks;
                    resultData.AttachmentDescription = model.AttachmentsDescription;
                    resultData.ModifiedOn = DateTime.UtcNow;
                    resultData.ModifiedBy = UserPrincipal.Current.UserId;
                    resultData.RequestStatus = model.RequestStatus;
                }

                this.Update(resultData);
            }
            catch (Exception ex)
            {

                //throw;
            }
        }

        public ReserveForReplacementFormModel GetR4RFormById(Guid r4rId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetR4RFormById, please pass in correct context in unit of work.");

            var result = (from r in context.Reserve_For_Replacement_Form_Data
                          where r.R4RId == r4rId
                          select r).FirstOrDefault();

            return Mapper.Map<Reserve_For_Replacement_Form_Data, ReserveForReplacementFormModel>(result);

        }

		public ReserveForReplacementFormModel GetR4RFormByFHANumber(string fhaNumber)
		{
			var context = this.Context as HCP_live;
			if (context == null)
				throw new InvalidCastException("context is not from db live in GetR4RFormByFHANumber, please pass in correct context in unit of work.");


			var results = (from r in context.Reserve_For_Replacement_Form_Data
						   where r.FHANumber == fhaNumber 
						   orderby r.ServicerSubmissionDate descending
						   select r
						   ).FirstOrDefault();
			return Mapper.Map<Reserve_For_Replacement_Form_Data, ReserveForReplacementFormModel>(results);
		}


		public ReserveForReplacementFormModel GetApprovedAmountByFHANumber(string fhaNumber)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetApprovedAmountByFHANumber, please pass in correct context in unit of work.");


            var results = (from r in context.Reserve_For_Replacement_Form_Data
                           where r.FHANumber == fhaNumber && r.RequestStatus == 2
                           orderby r.ServicerSubmissionDate descending
                           select r
                           ).FirstOrDefault();
            return Mapper.Map<Reserve_For_Replacement_Form_Data, ReserveForReplacementFormModel>(results);
        }

        public List<string> GetAllFHANumbersListForR4R()
        {
            var context = (HCP_live)this.Context;

            // Denied FHA number shouldn't be populated for Approved R4R Change amount
            var deniedFHA = (from fha in context.Reserve_For_Replacement_Form_Data
                             where fha.RequestStatus == (int)RequestStatus.Deny
                             select fha.FHANumber).Distinct().ToList();
            var R4RFHANumbers = (from r in context.Reserve_For_Replacement_Form_Data
                                 where r.RequestStatus == 2
                                 orderby r.FHANumber
                                 select r.FHANumber).Distinct().ToList();
            var filterdFha = R4RFHANumbers.Where(m => !deniedFHA.Contains(m)).ToList();
            return filterdFha;
        }

        public void UpdateApprovedAmount(decimal changeApprovedAmount, string FHANumber, string HudRemarks)
        {
            var context = (HCP_live)this.Context;

            var r4rRequest = (from r in context.Reserve_For_Replacement_Form_Data
                              where r.FHANumber == FHANumber && r.RequestStatus == 2
                              orderby r.ServicerSubmissionDate descending
                              select r).FirstOrDefault();

            r4rRequest.TotalApprovedAmount = changeApprovedAmount;
            r4rRequest.HudRemarks = HudRemarks;
            this.Update(r4rRequest);
        }

        /// <summary>
        /// Updated the lastest TaskId, this would be useful to find the latest task details
        /// </summary>
        /// <param name="model"></param>
        public void UpdateTaskId(ReserveForReplacementFormModel model)
        {
            var context = (HCP_live)this.Context;

            var r4rRequest = (from r in context.Reserve_For_Replacement_Form_Data
                              where r.R4RId == model.R4RId
                              select r).FirstOrDefault();

            r4rRequest.TaskId = model.TaskId;
            this.Update(r4rRequest);
        }
        public List<String> GetSubmitedFHANumbers(int lenderID)
        {
            var context = (HCP_live)this.Context;

            var fhaNumbers = (from fha in context.Reserve_For_Replacement_Form_Data
                              join lenderFHa in context.Lender_FHANumber on
                               fha.FHANumber equals lenderFHa.FHANumber
                              where fha.RequestStatus == (int)RequestStatus.Submit
                                    && lenderFHa.LenderID == lenderID
                              select fha.FHANumber).ToList();
            return fhaNumbers;
        }

        public ReserveForReplacementFormModel GetR4RbyTaskInstanceId(Guid TaskInstanceId)
        {
            try
            {
                var context = this.Context as HCP_live;
                if (context == null)
                throw new InvalidCastException("context is not from db live in GetApprovedAmountByFHANumber, please pass in correct context in unit of work.");

                var results = this.Find(m => m.TaskInstanceId == TaskInstanceId).FirstOrDefault();
                //var results = (from r in context.Reserve_For_Replacement_Form_Data
                //              where r.TaskInstanceId == TaskInstanceId
                //            select r
                //             ).FirstOrDefault();
                return Mapper.Map<Reserve_For_Replacement_Form_Data, ReserveForReplacementFormModel>(results);
            }
            catch (Exception ex)
            {

                //throw;
            }
            return null;
        }
    }
}
