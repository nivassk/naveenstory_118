﻿using System;
using System.Collections.Generic;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using System.Linq;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;
using System.Data;
using System.Data.Entity.SqlServer;

namespace HUDHealthcarePortal.Repository
{
    /// <summary>
    /// this class is never meant to be called by upper level layers, so don't need to have an interface
    /// </summary>
    public class HUDWorkLoadManagerRepository : BaseRepository<HUD_WorkLoad_Manager>, IHUDWorkloadManagerRepository
    {
        public HUDWorkLoadManagerRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public HUDWorkLoadManagerRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public HUD_WorkLoad_Manager GetWorkloadMgrById(int id)
        {
            var workloadMgr = this.Find(p => p.HUD_WorkLoad_Manager_ID == id, "Address").FirstOrDefault();
            return workloadMgr;
        }

        public IEnumerable<KeyValuePair<int, string>> GetWorkloadManagers()
        {
            //naveen 03june2019-- added .Where(X => X.Deleted_Ind != true)
            return this.GetAll().Where(X => X.Deleted_Ind != true).Select(p => new KeyValuePair<int, string>(p.HUD_WorkLoad_Manager_ID, p.HUD_WorkLoad_Manager_Name)).OrderBy(p => p.Value);
        }

        public UserViewModel GetWorkloadManagerDetail(int id)
        {
            var workloadManagerDetail = GetWorkloadMgrById(id);
            return Mapper.Map<UserViewModel>(workloadManagerDetail);
        }

        /// <summary>
        /// pass the ae ids as a string and get the ae names as a list
        /// </summary>
        /// <param name="aeids"></param>
        /// <returns></returns>
        public IList<string> GetWLMNamesListByIds(string wlmIds)
        {
            if (wlmIds == null) { wlmIds = string.Empty; }
            List<string> wlmTempArray = new List<string>(wlmIds.Split(",".ToCharArray()));

            int[] wlmIntArray = new int[wlmTempArray.Count];

            for (int i = 0; i < wlmTempArray.Count; i++)
            {
                if (wlmTempArray[i] != "")
                {
                    wlmIntArray[i] = Convert.ToInt32(wlmTempArray[i]);
                }
            }

            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetWLMNamesListByIds, please pass in correct context in unit of work.");

            return (from w in context.HUD_WorkLoad_Manager
                    where wlmIntArray.Contains(w.HUD_WorkLoad_Manager_ID)
                    select w.HUD_WorkLoad_Manager_Name).ToList();


        }

    
        /// <summary>
        /// Get All AE names
        /// </summary>
        /// <returns></returns>
        public IList<string> GetALLWLMNamesList()
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetALLWLMNamesList, please pass in correct context in unit of work.");

            return (from w in context.HUD_WorkLoad_Manager
                    select w.HUD_WorkLoad_Manager_Name).ToList();

        }

        /// <summary>
        /// Get all subordinate AEs by Work Load Manager ID
        /// retrive all project manager working under WLM
        /// </summary> 
        /// <param name="id"></param>
        /// <returns></returns>
        public IList<HUDManagerModel> GetSubordinateAEsByWLMId(string wlmId)
        {
            if (wlmId == null) { wlmId = string.Empty; } 
            List<string> wlmTempArray = new List<string>(wlmId.Split(",".ToCharArray()));
           
            int[] wlmIntArray = new int[wlmTempArray.Count];

            for (int i = 0; i < wlmTempArray.Count; i++)
            {
                if (wlmTempArray[i] != "")
                {
                    wlmIntArray[i] = Convert.ToInt32(wlmTempArray[i]);
                }
            }

            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetSubordinateAEsByWLMId, please pass in correct context in unit of work.");

            var hudProjectMgrs =  (from wae in context.HUD_WorkLoad_Manager_Portfolio_Manager 
                         join  ae in context.HUD_Project_Manager on wae.HUD_Project_Manager_ID equals ae.HUD_Project_Manager_ID
                                   where wlmIntArray.Contains(wae.HUD_WorkLoad_Manager_ID)
                       select ae).OrderBy(p => p.HUD_Project_Manager_Name).ToList();
           return  Mapper.Map<IList<HUD_Project_Manager>, IList<HUDManagerModel>>(hudProjectMgrs);

        }

        public IList<HUDManagerModel> GetSubordinateISOUsByWLMId(string isou)
        {
            if (isou == null) { isou = string.Empty; }
            List<string> wlmTempArray = new List<string>(isou.Split(",".ToCharArray()));

            int[] wlmIntArray = new int[wlmTempArray.Count];

            for (int i = 0; i < wlmTempArray.Count; i++)
            {
                if (wlmTempArray[i] != "")
                {
                    wlmIntArray[i] = Convert.ToInt32(wlmTempArray[i]);
                }
            }

            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetSubordinateAEsByWLMId, please pass in correct context in unit of work.");

            var hudProjectMgrs = (from wae in context.HUD_WorkloadManager_InternalSpecialOptionUser
                                  join ae in context.HUD_Project_Manager on wae.HUD_WorkloadManagerId equals ae.HUD_Project_Manager_ID
                                  where wlmIntArray.Contains(wae.HUD_WorkloadManagerId)
                                  select ae).OrderBy(p => p.HUD_Project_Manager_Name).ToList();
            return Mapper.Map<IList<HUD_Project_Manager>, IList<HUDManagerModel>>(hudProjectMgrs);

        }

        public IList<HUDManagerModel> GetRegisteredAEsByWLMId(string wlmId, IHUDAccountExecutiveRepository aeRepository)
        {
            if (wlmId == null) { wlmId = string.Empty; }
            List<string> wlmTempArray = new List<string>(wlmId.Split(",".ToCharArray()));

            int[] wlmIntArray = new int[wlmTempArray.Count];

            for (int i = 0; i < wlmTempArray.Count; i++)
            {
                if (wlmTempArray[i] != "")
                {
                    wlmIntArray[i] = Convert.ToInt32(wlmTempArray[i]);
                }
            }

            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetRegisteredAEsByWLMId, please pass in correct context in unit of work.");
            
            var hudProjectMgrIds = (from wae in context.HUD_WorkLoad_Manager_Portfolio_Manager
                                  join ae in context.HUD_Project_Manager on wae.HUD_Project_Manager_ID equals ae.HUD_Project_Manager_ID
                                  join uwae in context.User_WLM_PM on ae.HUD_Project_Manager_ID equals uwae.HUD_Project_Manager_ID
                                    //naveen 03june2019 added where uwae.Deleted_Ind != true
                                  where uwae.Deleted_Ind != true
                                  where wlmIntArray.Contains(wae.HUD_WorkLoad_Manager_ID)
                                  group uwae by uwae.HUD_Project_Manager_ID into g
                                  select new
                                  {
                                      g.Key,
                                      modifiedon = (from t2 in g select t2.ModifiedOn).Max()
                                  })
                                  .ToList();
            var hudProjectMgrs = new List<HUDManagerModel>();
            foreach (var item in hudProjectMgrIds)
            {
                var projectManager = aeRepository.GetProjectManagerById(item.Key.Value);
                hudProjectMgrs.Add(Mapper.Map<HUDManagerModel>(projectManager));
            }
            return hudProjectMgrs;

        }
    }
}
