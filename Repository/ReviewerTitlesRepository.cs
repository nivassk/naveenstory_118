﻿using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class ReviewerTitlesRepository : BaseRepository<ReviewerTitles>, IReviewerTitlesRepository
    {

          public ReviewerTitlesRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }

          public ReviewerTitlesRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }
          public List<ReviewerTitlesModel> GetReviewerTitles(int TitleTypeId)
        {
            var context = (HCP_live)this.Context;

            var UL = (from n in context.ReviewerTitles where n.TitleTypeId == TitleTypeId select n).ToList();  
                     
            return Mapper.Map<List<ReviewerTitles>, List<ReviewerTitlesModel>>(UL);

        }
        public ReviewerTitlesModel GetReviewerTitle(int titleId)
        {
            var context = (HCP_live)this.Context;

            var titleName = this.Find(m => m.Reviewer_Id == titleId).FirstOrDefault();

            return Mapper.Map<ReviewerTitles, ReviewerTitlesModel>(titleName);



        }
    }
}
