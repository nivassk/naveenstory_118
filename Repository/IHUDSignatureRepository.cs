﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;


namespace Repository.Interfaces
{
	public interface IHUDSignatureRepository
	{
		int AddNewSignature(HUDSignatureModel pModel);
		HUDSignatureModel GetHUDSignatureByUserId(int pId);
		void UpdateHUDSignatureByUserId(HUDSignatureModel pModel);
	}
}
