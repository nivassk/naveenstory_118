﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public abstract class DerivedEntityRepoBase<TDerivedEntity> : IDerivedEntityRepository<TDerivedEntity> where TDerivedEntity : class, new()
    {
        private DbContext _context;
        private UnitOfWork _unitOfWork;

        public DerivedEntityRepoBase(UnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this._context = unitOfWork.Context;
        }

        protected UnitOfWork UnitOfWork
        {
            get { return _unitOfWork; }
        }

        protected virtual string SqlQueryPageSort
        {
            get { throw new NotImplementedException(); }
        }

        public IEnumerable<TDerivedEntity> GetBySqlQuery(string sqlQuery, params object[] parameters)
        {
            var result = _context.Database.SqlQuery<TDerivedEntity>(sqlQuery, parameters);
            return result;
        }

        public IEnumerable<TDerivedEntity> GetBySqlQueryWithPageSort(int pageNum, int pageSize, string sortBy, string sortDir, params object[] parameters)
        {
            int startRecord = (pageNum - 1) * pageSize + 1;
            int endRecord = pageNum * pageSize;
            var result = this.GetBySqlQuery(string.Format(SqlQueryPageSort, sortBy, startRecord, endRecord, sortDir), parameters);
            return result;
        }
    }
}
