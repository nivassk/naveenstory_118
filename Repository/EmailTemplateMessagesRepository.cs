﻿using System;
using System.Linq;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Model;
using Repository.Interfaces;
using TaskDB = EntityObject.Entities.HCP_task;
using System.Collections.Generic;

namespace Repository
{
    public class EmailTemplateMessagesRepository : BaseRepository<HCP_EmailTemplateMessages>, IEmailTemplateMessagesRepository
    {
        public EmailTemplateMessagesRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }

        public EmailTemplateMessagesRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<HCP_EmailTemplateMessages> DataToJoin
        {
            get { return DbQueryRoot; }
        }

        public EmailTemplateMessagesModel GetEmailTemplate(EmailType emailTypeID)
        {
            var data = this.GetAll().FirstOrDefault(p => p.EmailTypeId == (int)emailTypeID);
           
            data.MessageBody = "**** PLEASE DO NOT REPLY TO THIS EMAIL. THIS EMAIL ACCOUNT IS AUTOMATED AND NOT REGULARLY MONITORED BY STAFF ****\r\n" + System.Environment.NewLine + data.MessageBody;
            var results = Mapper.Map<EmailTemplateMessagesModel>(data);

            return results;
        }


        public EmailRecipientModel GetRecipentbyView(Guid Taskinstanceid, int viewid)
        {
            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetFilteredProductionTasks, please pass in correct context in unit of work.");
            var filteredProductionTasks = Mapper.Map<IEnumerable<EmailRecipientModel>>(context.Database.SqlQuerySimple<TaskDB.Programmability.EmailRecipient_Result>("GetMailRecipients",
                new
                {
                    @Taskinstanceid = Taskinstanceid,

                    @Viewid = viewid
                })).ToList();
            var result = new EmailRecipientModel();
            if (filteredProductionTasks!=null)
            {
                result = filteredProductionTasks[0];
            }
            return result;
        }


        public IEnumerable<string> GetAeEmailbyInstanceid(Guid Taskinstanceid)
        {
            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetFilteredProductionTasks, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<string>("GetAelistforMail",
                      new
                      {
                          TaskInstanceId = Taskinstanceid
                      });
            return results;
        }
    }
}
