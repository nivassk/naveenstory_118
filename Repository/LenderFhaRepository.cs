﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using AutoMapper;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
   public class LenderFhaRepository:BaseRepository<Lender_FHANumber>,ILenderFhaRepository
    {
         public LenderFhaRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
         public LenderFhaRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

         public IQueryable<Lender_FHANumber> DataToJoin 
        {
            get { return DbQueryRoot; }
        }

         public DateTime GetLatestQuarter()
         {
             var data = this.GetAll().OrderByDescending(p => p.ModifiedOn).First();
             var results = Mapper.Map<LenderFhaModel>(data);
             var latestQtr = Convert.ToDateTime(results.ModifiedOn);

             return latestQtr;
         }

        public IList<string> GetFhaNumbersForLender(int lenderId)
        {
            var context = (HCP_live) this.Context;

            var results = (from n in context.Lender_FHANumber 
                           where n.LenderID == lenderId && n.FHA_EndDate == null
                           select n.FHANumber).ToList();
            return results;

            ////return (from n in context.Lender_FHANumber
            ////        where (n.LenderID == lenderId)
            ////        select n.FHANumber).ToList();
        }
    }
}
