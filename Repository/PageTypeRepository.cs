﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class PageTypeRepository:BaseRepository<HCP_PageType>,IPageTypeRepository
    {
        public PageTypeRepository():base(new UnitOfWork(DBSource.Live))
        { }
        public PageTypeRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public int GetPageTypeIdByName(string pageTypeName)
        {
            var hcpPageType = this.Find(p => p.PageTypeDescription == pageTypeName).FirstOrDefault();
            if (hcpPageType != null)
                return hcpPageType.PageTypeId;
            return 0;
        }


        /// <summary>
        /// get form name from pagetypeid
        /// </summary>
        /// <param name="pPageTypeId"></param>
        /// <returns></returns>
        public string GetPageTypeNameById(int pPageTypeId)
        {
            var hcpPageType = this.Find(p => p.PageTypeId == pPageTypeId).FirstOrDefault();
            if (hcpPageType != null)
                return hcpPageType.PageTypeDescription;
            return null;
        }
    }
}
