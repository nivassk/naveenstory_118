﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Proxies;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class FormTypeRepository : BaseRepository<HCP_PageType>, IFormTypeRepository
    {
        public FormTypeRepository() : base(new UnitOfWork(DBSource.Live))
        {
            
        }

        public FormTypeRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
            
        }

        public IList<FormTypeModel> GetAllFormTypeWithProjectAction()
        {
            var context = (HCP_live) this.Context;
            var results = (from n in context.HCP_PageType
                           join x in context.HCP_Project_Action 
                           on n.PageTypeId equals x.PageTypeId
                           select new FormTypeModel()
                           {
                               PageTypeId =  (int)n.PageTypeId,
                               PageTypeDescription =  n.PageTypeDescription
                           }
                          ).Distinct();

            return results.ToList();

    }

        public IQueryable<FormTypeModel> GetProductionPageTypes(int moduleId)
        {
            var context = (HCP_live)this.Context;
            return (from n in context.HCP_PageType
                where n.ModuleId == moduleId  
                orderby n.Orderby 
                select new FormTypeModel()
                {
                    PageTypeId = (int) n.PageTypeId,
                    PageTypeDescription = n.PageTypeDescription
                }
                );

        }
    }
}

