﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_live;
using Model;
using Repository.Interfaces;

namespace Repository
{
   public interface IPDRColumnUserPreferenceRepository:IJoinable<PDRUserPreference>
    {
        int AddNewColumn(PDRUserPreferenceViewModel model);
        void UpdatePDRUserPreference(PDRUserPreferenceViewModel model);
        IEnumerable<PDRUserPreferenceViewModel> GetUserPreferenceByUser(int userId);
        void DeletePDRUserPreference(int userID, int pdrColumnID);
        void SetDefaultUserPreference(int userID);
    }
}
