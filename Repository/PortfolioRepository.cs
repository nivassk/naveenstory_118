﻿using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using Repository;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class PortfolioRepository : BaseRepository<usp_HCP_Get_Portfolio_Aggregate_Result>, IPortfolioRepository
    {
        public PortfolioRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public PortfolioRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<DerivedUploadData> GetPortfolios(string quarterToken)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in usp_HCP_Get_Portfolio_Aggregate, please pass in correct context in unit of work.");
            var results =
                context.Database.SqlQuerySimple<usp_HCP_Get_Portfolio_Aggregate_Result>(
                    "usp_HCP_Get_Portfolio_Aggregate", new {QuarterToken = quarterToken}).ToList();
            return Mapper.Map<IEnumerable<usp_HCP_Get_Portfolio_Aggregate_Result>, IEnumerable<DerivedUploadData>>(results);
        }

        /// <summary>
        /// sort and page at portfolio manager level, as now scores are not calculated, when you
        /// do sorting and paging by score the numbers are incorrect.
        /// </summary>
        /// <param name="pageNum"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="sortOrder"></param>
        /// <param name="quarterToken"></param>
        /// <returns></returns>
        public IEnumerable<DerivedUploadData> GetPortfolios(int pageNum, int pageSize, string sortBy, SqlOrderByDirecton sortOrder, string quarterToken)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in usp_HCP_Get_Portfolio_Aggregate, please pass in correct context in unit of work.");
            var resultsRaw = context.Database.SqlQuerySimple<usp_HCP_Get_Portfolio_Aggregate_Result>("usp_HCP_Get_Portfolio_Aggregate",
                new { QuarterToken = quarterToken }).ToList();
            return Mapper.Map<IEnumerable<usp_HCP_Get_Portfolio_Aggregate_Result>, IEnumerable<DerivedUploadData>>(resultsRaw);
        }

        public IEnumerable<DerivedUploadData> GetPortfolioDetail(int pageNum, int pageSize, string sortBy, SqlOrderByDirecton sortOrder, string portfolioNumber, string quarterToken)
         {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in usp_HCP_Get_Portfolio_Detail, please pass in correct context in unit of work.");
            var resultsRaw = context.Database.SqlQuerySimple<usp_HCP_Get_Portfolio_Detail_Result>("usp_HCP_Get_Portfolio_Detail",
                new { QuarterToken = quarterToken, PortfolioNumber = portfolioNumber });
            return Mapper.Map<IEnumerable<usp_HCP_Get_Portfolio_Detail_Result>, IEnumerable<DerivedUploadData>>(resultsRaw);
        }
    }
}
