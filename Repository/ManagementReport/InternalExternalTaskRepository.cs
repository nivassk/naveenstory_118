﻿using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.Interfaces.InternalExternalTask;
using HUDHealthcarePortal.Core;
using Model.InternalExternalTask;
using AutoMapper;

namespace Repository.ManagementReport
{
    public class InternalExternalTaskRepository : BaseRepository<InternalExternalTask>, IInternalExternalTaskRepository
    {

        public InternalExternalTaskRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public InternalExternalTaskRepository(UnitOfWork unitOfWork) 
            : base(unitOfWork)
        {
        }

        public Guid AddInternalExternalTask(InternalExternalTaskModel model)
        {
            var task = Mapper.Map<InternalExternalTask>(model);
            task.TaskInstanceID = Guid.NewGuid();
            this.InsertNew(task);
            UnitOfWork.Save();
            return task.TaskInstanceID;

        }      

        public InternalExternalTaskModel FindByFHANuber(string fhaNumber)
        {
            var task = this.Find(m => m.FHANumber == fhaNumber).FirstOrDefault();
            return Mapper.Map<InternalExternalTaskModel>(task);
        }

        public InternalExternalTaskModel FindByTaskInstanceID(Guid taskInstanceID)
        {
            var task = this.Find(m => m.TaskInstanceID==taskInstanceID).FirstOrDefault();
            return Mapper.Map<InternalExternalTaskModel>(task);

        }

        public List<InternalExternalTaskModel> GetAllTasks()
        {
            var tasks = this.GetAll();
            return Mapper.Map<List<InternalExternalTaskModel>>(tasks.ToList());
        }

        public List<string> GetAvailableFHANumbers()
        {
              var context = (HCP_live)this.Context;
            var availableFHANumbers = (from projectInfo in context.ProjectInfoes
                                       join lenderFha in context.Lender_FHANumber on projectInfo.LenderID equals lenderFha.LenderID
                                       where lenderFha.FHA_EndDate == null                                      
                                       select projectInfo.FHANumber).Distinct();
            return availableFHANumbers.ToList();
        }

        public bool UpdateInternalExternalTask(InternalExternalTaskModel internalExternalTask)
        {
            var taskToUpdate = this.GetByIDFast(internalExternalTask.InternalExternalTaskID);
            if(taskToUpdate != null)
            {
                this.Update(taskToUpdate);
                UnitOfWork.Save();
                return true;
            }
            return false;
        }
        public InternalExternalTaskModel GetNotCompletedTask(int pageTypeID, string fhaNumber)
        {
            var task = this.Find(m => m.PageTypeID == pageTypeID && m.FHANumber == fhaNumber && m.StatusID!=(int)InternalExternalTaskStatus.completed).FirstOrDefault();
            return Mapper.Map<InternalExternalTaskModel>(task);
            
        }
        public bool UpdateNoiTask(InternalExternalTaskModel model)
        {
            var taskToUpdate = this.Find(m => m.TaskInstanceID==model.TaskInstanceID).FirstOrDefault();
            if(taskToUpdate != null)
            {
                taskToUpdate.SurveyDate = model.SurveyDate;
                taskToUpdate.AdditionalComments = model.AdditionalComments;
                taskToUpdate.StatusID = model.StatusID;
                if (model.InUse != null)
                {
                    taskToUpdate.InUse = model.InUse;
                }
                this.Update(taskToUpdate);
                UnitOfWork.Save();
                return true;
            }
            return false;
        }

    }
}
