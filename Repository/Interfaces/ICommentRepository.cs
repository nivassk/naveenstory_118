﻿using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface ICommentRepository : IJoinable<Comment>
    {
        IEnumerable<CommentModel> GetCommentsByProject(string fhaNumber);
        IEnumerable<CommentModel> GetCommentsByLdiID(int ldiID);
        void AddCommentForProject(CommentModel comment);
        void UpdateCommentForProject(CommentModel comment);
    }
}
    