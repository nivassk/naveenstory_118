﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_live;
using Model;

namespace Repository.Interfaces
{
    public interface IPamStatusRepository
    {
        IEnumerable<PamStatusViewModel> GetAllPamStausesByModuleId(int moduleId);
    }
}
