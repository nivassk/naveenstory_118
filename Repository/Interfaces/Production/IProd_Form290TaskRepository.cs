﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Production;

namespace Repository.Interfaces.Production
{
   public interface IProd_Form290TaskRepository
    {
         Guid AddForm290Task(Prod_Form290TaskModel model);
        Prod_Form290TaskModel GetForm290TaskBySingleTaskInstanceID(Guid taskInstanceID);
        List<Prod_Form290TaskModel> GetForm290TaskByTaskInstanceID(List<Guid> taskInstanceID);
        Prod_Form290TaskModel GetFrom290TaskByClosingTaskInstanceID(Guid ClosingTaskInstanceID);
    }
}
