﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.Production
{
    public interface IProd_NextStageRepository
    {
        Guid AddNextStage(Prod_NextStageModel model);
        List<string> GetFhasforNxtStage(int pagetypeid, int lenderId);
        bool UpdateTaskInstanceIdForNxtStage(Prod_NextStageModel model);
		Guid? GetNxtStageTaskid(Guid guidCurrentTaskinstanceid);
		Guid? GetCompletedStageTaskid(Guid guidCurrentTaskinstanceid);
	}
}
