﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.Production
{
    public interface IProd_RestfulWebApiAddPropertyInfoRepository
    {
        RestfulWebApiResultModel AddPropertyInfoUsingWebApi(string jSonData, string token);

    }
}
