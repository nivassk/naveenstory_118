﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.Production
{
    public interface IProductionReassignReportRepository
    {
        ProductionReasignReportModel GetProdReassignmentTasks(string appType, string programType, string prodUserIds, DateTime? fromDate, DateTime? toDate);

       void  ReassignTasks(List<string> kvp, int AssignUserId);
    }
}
