﻿using System.Collections.Generic;

namespace Repository.Interfaces
{
    public interface IServicerInfoRepository
    {
        System.Collections.Generic.IEnumerable<KeyValuePair<int, string>> GetAllServicers();
    }
}
