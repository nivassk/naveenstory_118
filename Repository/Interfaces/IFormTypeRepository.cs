﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;

namespace HUDHealthcarePortal.Repository.Interfaces
{
    public interface IFormTypeRepository
    {
        IList<FormTypeModel> GetAllFormTypeWithProjectAction();
        IQueryable<FormTypeModel> GetProductionPageTypes(int moduleId);
    }
}
