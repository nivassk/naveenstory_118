﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_live;

namespace Repository.Interfaces.ProjectAction
{
    public interface IProjectActionAEFormStatusRepository
    {
        Dictionary<Guid, int?> GetProjectActionAEFormStatus(Guid projectActionFormId);
        bool IsApprovalStatusTrueForAllQuestions(Guid projectActionFormId);
        IList<ProjectActionAEFormStatus> GetProjectActionAEFormStatusList(Guid projectActionFormId);
        void UpdateAECheckListItem(ProjectActionAEFormStatus projectActionAEFormStatus);
    }
}
