﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;

namespace HUDHealthcarePortal.Repository.Interfaces.ProjectAction
{
    public interface IProjectActionRepository
    {
        IList<ProjectActionTypeViewModel> GetAllProjectActions();
        string GetProjectActionName(int projectActionTypeId);
        IList<ProjectActionTypeViewModel> GetAllProjectActionsByPageId(int pageTypeId);
    }
}
