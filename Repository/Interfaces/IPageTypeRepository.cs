﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Repository.Interfaces
{
    public interface IPageTypeRepository
    {
        int GetPageTypeIdByName(string pageTypeName);
        string GetPageTypeNameById(int pPageTypeId);
    }
}
