﻿using System;
using HUDHealthcarePortal.Model;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
	public interface IEscrowEstimateTypeRepository
	{
		List<EscrowEstimateTypeModel> GetEscrowEstimateTypes();
		string GetEscrowEstimateTypeName(int pId);

	}
}