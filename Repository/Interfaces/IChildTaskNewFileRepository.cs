﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Repository.Interfaces
{
    public interface IChildTaskNewFileRepository
    {
        void SaveChildTaskNewFiles(ChildTaskNewFileModel newFile);
        bool IsFileExistsForChildTask(Guid childTaskInstanceId);
    }
}
