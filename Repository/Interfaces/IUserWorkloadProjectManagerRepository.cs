﻿
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IUserWorkloadProjectManagerRepository
    {
        void AddNewUser(UserViewModel model);
    }
}
