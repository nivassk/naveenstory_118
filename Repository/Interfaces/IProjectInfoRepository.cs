﻿using System.Collections;
using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Model;
using Model;

namespace Repository.Interfaces
{
    public interface IProjectInfoRepository : IJoinable<ProjectInfo>
    {
        ProjectInfo GetProjectInfoByFhaNumber(string fhaNumber);
        IEnumerable<ProjectInfoModel> GetProjectInfoListAssociatedToWorkloadManager(int workloadManagerId);

        IEnumerable<ProjectInfoModel> GetProjectInfoListAssociatedToAccountExecutive(int accountExecutiveId);

        PropertyInfoModel GetPropertyInfo(string fhaNumber);

        int GetAeUserIdByFhaNumber(string fhaNumber);

        string GetAeEmailByFhaNumber(string fhaNumber);
        PropertyInfoModel GetProdPropertyInfo(string fhaNumber);
        PropertyInfoModel GetAssetManagementPropertyInfo(string fhaNumber);
        AdditionalPropertyInfo GetPropertyInfoWithAEandWLM(string fhaNumber);
    }
}
