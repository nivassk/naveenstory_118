﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IReviewFileCommentRepository
    {
        void SaveReviewFileComment(IList<ReviewFileCommentModel> reviewCommentList);
    }
}
