﻿using System.Collections.Generic;
using Model;

namespace Repository.Interfaces
{
    public interface INonCriticalLateAlertsRepository
    {
        void SaveNonCriticalLateAlerts(IList<NonCriticalLateAlertsModel> models);
    }
}