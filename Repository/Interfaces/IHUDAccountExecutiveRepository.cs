﻿
using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHUDAccountExecutiveRepository
    {
        IEnumerable<KeyValuePair<int, string>> GetAccountExecutives();
        UserViewModel GetAccountExecutiveDetail(int id);
        IList<string> GetAENamesListByIds(string aeids);
        HUD_Project_Manager GetProjectManagerById(int id);
    }
}
