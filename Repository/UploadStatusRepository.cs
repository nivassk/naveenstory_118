﻿using AutoMapper;
using EntityObject.Entities.HCP_intermediate;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Repository;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class UploadStatusRepository : BaseRepository<usp_HCP_Get_QuarterlyUploadReport_View_Result>, IUploadStatusRepository
    {
        public UploadStatusRepository()
            : base(new UnitOfWork(DBSource.Intermediate))
        {
        }
        public UploadStatusRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        [Obsolete("use GetUploadStatusDetailByQuarter instead")]
        public IEnumerable<UploadStatusViewModel> GetUploadStatusByLenderId(int lendId)
        {
            var context = this.Context as HCP_intermediate;
            if (context == null)
                throw new InvalidCastException("context is not from db intermediate in GetUploadStatusByLenderId, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_Get_QuarterlyUploadReport_View_Result>("usp_HCP_Get_QuarterlyUploadReport_View",
                new {LenderID = lendId})
                .Where(p => (lendId == -1 || p.LenderID == lendId)).ToList();
            return Mapper.Map<IEnumerable<UploadStatusViewModel>>(results);
        }

        public IEnumerable<UploadStatusViewModel> GetUploadStatusDetailByQuarter(string username, string usertype, int lenderId, int monthsInPeriod, int year)
        {
            var context = this.Context as HCP_intermediate;
            if (context == null)
                throw new InvalidCastException("context is not from db intermediate in GetUploadStatusByLenderId, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_Get_QuarterlyUploadReportByQuarter_Result>("usp_HCP_Get_QuarterlyUploadReportByQuarter",
                new {Username = username, UserType = usertype, LenderId = lenderId, MonthsInPeriod = monthsInPeriod, Year = year})
                .Where(p => (lenderId == -1 || p.LenderID == lenderId)).ToList();
            return Mapper.Map<IEnumerable<UploadStatusViewModel>>(results);
        }
    }
}
