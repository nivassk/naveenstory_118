﻿using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class CommentRepository : BaseRepository<Comment>, ICommentRepository
    {
        public CommentRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public CommentRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<Comment> DataToJoin 
        {
            get { return DbQueryRoot; }
        }

        [Obsolete]
        public IEnumerable<CommentModel> GetCommentsByProject(string fhaNumber)
        {
            var comments = this.Find(p => p.FHANumber == fhaNumber).ToList();
            return Mapper.Map<List<Comment>, List<CommentModel>>(comments);
        }

        public IEnumerable<CommentModel> GetCommentsByLdiID(int ldiID)
        {
            var comments = this.Find(p => p.LDI_ID == ldiID).ToList();
            return Mapper.Map<List<Comment>, List<CommentModel>>(comments);
        }

        public void AddCommentForProject(CommentModel comment)
        {
            var commentEntity = Mapper.Map<CommentModel, Comment>(comment);
            this.AttachNew(commentEntity);
        }

        public void UpdateCommentForProject(CommentModel comment)
        {
            //var entityToUpdate = this.Find(p => (p.FHANumber == comment.FHANumber) && (p.Subject == comment.Subject)).FirstOrDefault();
            var entityToUpdate = this.Find(p => (p.LDI_ID == comment.LDI_ID) && (p.Subject == comment.Subject)).FirstOrDefault();
            entityToUpdate.Comments = comment.Comments;
            this.Update(entityToUpdate);
        }
    }
}
