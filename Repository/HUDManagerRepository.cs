﻿using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using AutoMapper;
using System;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class HUDManagerRepository : IHUDManagerRepository
    {
        HUDAccountExecutiveRepository acctExecRepository;
        HUDWorkLoadManagerRepository workloadMgrRepository;
        ProjectInfoRepository projectInfoRepository;
        
        public HUDManagerRepository()
        {
            acctExecRepository = new HUDAccountExecutiveRepository();
            workloadMgrRepository = new HUDWorkLoadManagerRepository();
            projectInfoRepository = new ProjectInfoRepository();
        }

        /// <summary>
        /// first one is wlm, second one is ae, third one is lender name
        /// </summary>
        /// <param name="fhaNumber"></param>
        /// <returns></returns>
        public Tuple<HUDManagerModel, HUDManagerModel, string> GetHUDManagersByFhaNumber(string fhaNumber)
        {
            var projectInfo = projectInfoRepository.GetProjectInfoByFhaNumber(fhaNumber);
            var aeMgr = acctExecRepository.GetProjectManagerById(projectInfo.HUD_Project_Manager_ID ?? -1);
            var wlMgr = workloadMgrRepository.GetWorkloadMgrById(projectInfo.HUD_WorkLoad_Manager_ID ?? -1);

            var aeModel = Mapper.Map<HUDManagerModel>(aeMgr);
            aeModel.AddressModel = Mapper.Map<AddressModel>(aeMgr.Address);
            aeModel.ManagerType = HUDManagerType.AccountExecutive;
            var wlModel = Mapper.Map<HUDManagerModel>(wlMgr);
            wlModel.AddressModel = Mapper.Map<AddressModel>(wlMgr.Address);
            wlModel.ManagerType = HUDManagerType.WorkloadManager;
            return new Tuple<HUDManagerModel,HUDManagerModel, string>(wlModel, aeModel, projectInfo.Lender_Mortgagee_Name);
        }

        public HUDManagerModel GetAccountExecutiveByFHANumber(string fhaNumber)
        {
            var projectInfo = projectInfoRepository.GetProjectInfoByFhaNumber(fhaNumber);
            var aeMgr = acctExecRepository.GetProjectManagerById(projectInfo.HUD_Project_Manager_ID ?? -1);
            var aeModel = Mapper.Map<HUDManagerModel>(aeMgr);
            aeModel.AddressModel = Mapper.Map<AddressModel>(aeMgr.Address);
            aeModel.ManagerType = HUDManagerType.AccountExecutive;

            return aeModel;
        }
    }
}

