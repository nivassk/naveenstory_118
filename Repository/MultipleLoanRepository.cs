﻿using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using Repository;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class MultipleLoanRepository : BaseRepository<usp_HCP_Get_Multiple_Loans_PropertyID_View_Result>, IMultiLoanRepository
    {
        public MultipleLoanRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public MultipleLoanRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<MultiLoanModel> GetMultiLoans()
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetDataUploadSummaryByUserId, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_Get_Multiple_Loans_PropertyID_View_Result>("usp_HCP_Get_Multiple_Loans_PropertyID_View").ToList();
            return Mapper.Map<IEnumerable<usp_HCP_Get_Multiple_Loans_PropertyID_View_Result>, IEnumerable<MultiLoanModel>>(results);
        }

        public IEnumerable<MultiLoanDetailModel> GetMultiLoansDetailByPropertyID(int propertyId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetMultiLoansDetailByPropertyID, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_Get_Multiple_Loans_By_PropertyID_Result>("usp_HCP_Get_Multiple_Loans_By_PropertyID", 
                new {PropertyID = propertyId}).ToList();
            return Mapper.Map<IEnumerable<usp_HCP_Get_Multiple_Loans_By_PropertyID_Result>, IEnumerable<MultiLoanDetailModel>>(results);
        }
    }
}
