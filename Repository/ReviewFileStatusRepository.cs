﻿using AutoMapper;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDB = EntityObject.Entities.HCP_task;
namespace HUDHealthcarePortal.Repository
{
    public class ReviewFileStatusRepository : BaseRepository<ReviewFileStatus>, IReviewFileStatusRepository
    {
        public ReviewFileStatusRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }

        public ReviewFileStatusRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public void SaveReviewFileStatus(ReviewFileStatusModel model)
        {
            var context = (TaskDB.HCP_task)this.Context;

            var result = Mapper.Map<ReviewFileStatus>(model);
            this.InsertNew(result);
        }

        public IList<Guid> GetRequestAdditionalFileIdByParentTaskInstanceId(Guid parentTaskInstanceId)
        {
            var context = (TaskDB.HCP_task)this.Context;
            var result = (from r in context.ReviewFileStatus
                          join t in context.TaskFiles on r.TaskFileId equals t.TaskFileId
                          where t.TaskInstanceId == parentTaskInstanceId && r.Status == 1 && r.IsChildTaskCreated == false
                          select r.TaskFileId).ToList();
            return result;
        }

        public IList<RequestAdditionalInfoFileModel> GetTaskFileIdByChildTaskInstanceId(Guid ChildTaskInstanceId)
        {
            var context = (TaskDB.HCP_task)this.Context;
            var result = (from r in context.RequestAdditionalInfoFiles
                          where r.ChildTaskInstanceId == ChildTaskInstanceId
                          select r).ToList();
            return Mapper.Map<IList<RequestAdditionalInfoFileModel>>(result);
        }

        public bool UpdateReviewFileStatusForTaskFileId(Guid parentTaskFileId)
        {
            var context = (TaskDB.HCP_task)this.Context;
            var resultList = (from r in context.ReviewFileStatus
                              where r.TaskFileId == parentTaskFileId
                              select r).ToList();
            foreach (var item in resultList)
            {
                if (item.Status == 4);
                {
                    item.Status = 2;
                }

                this.Update(item);
            }
            context.SaveChanges();
            return true;
        }


        public bool UpdateReviewFileStatusForRequestAdditionalResponse(Guid parentTaskFileId)
        {
            var context = (TaskDB.HCP_task)this.Context;
            var resultList = (from r in context.ReviewFileStatus
                              where r.TaskFileId == parentTaskFileId
                              select r).ToList();
            foreach (var item in resultList)
            {
                // If status was request addtional, then change it to Response received or else change it to New
                item.Status = item.Status == 4 ? 2 : 1;
                this.Update(item);
            }
            context.SaveChanges();
            return true;
        }

        public bool UpdateReviewFileStatusForFileDownload(Guid parentTaskFileId)
        {
            var context = (TaskDB.HCP_task)this.Context;
            var taskfile = (from r in context.ReviewFileStatus
                            where r.TaskFileId == parentTaskFileId
                            select r).FirstOrDefault();
         
            if (UserPrincipal.Current.UserRole.Contains("AccountExecutive"))
            {
                if (taskfile != null)
                {
                    taskfile.Status = 3;
                    this.Update(taskfile);

                    context.SaveChanges();
                }
            }
            return true;
        }

        public ReviewFileStatusModel GetReviewFileStatusByTaskFileId(Guid taskFileId)
        {
            var context = (TaskDB.HCP_task)this.Context;
            var result = (from r in context.ReviewFileStatus
                          where r.TaskFileId == taskFileId
                          select r).FirstOrDefault();
            return Mapper.Map<ReviewFileStatusModel>(result);
        }

        public bool UpdateReviewFileStatus(IList<ReviewFileStatusModel> reviewStatusList)
        {
            var context = (TaskDB.HCP_task)this.Context;
            foreach (var item in reviewStatusList)
            {
                var result = (from r in context.ReviewFileStatus
                              where r.ReviewFileStatusId == item.ReviewFileStatusId
                              select r).FirstOrDefault();
                if (result != null)
                {
                    result.Status = item.Status;
                    result.ModifiedOn = item.ModifiedOn;
                    result.ModifiedBy = item.ModifiedBy;
                    this.Update(result);
                }
            }

            context.SaveChanges();
            return true;
        }

        #region Production Application
        public ReviewFileStatusModel GetReviewFileStatusByTaskFileIdandViewId(Guid taskFileId, int viewid)
        {
            var context = (TaskDB.HCP_task)this.Context;
            var result = (from r in context.ReviewFileStatus
                          where r.TaskFileId == taskFileId && r.ReviewerProdViewId == viewid
                          select r).FirstOrDefault();
            return Mapper.Map<ReviewFileStatusModel>(result);
        }
        #endregion
    }
}
