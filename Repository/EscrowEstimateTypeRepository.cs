﻿using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using AutoMapper;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;

namespace Repository
{
	public class EscrowEstimateTypeRepository : BaseRepository<EscrowEstimateType>, IEscrowEstimateTypeRepository
	{
		public EscrowEstimateTypeRepository()
			: base(new UnitOfWork(DBSource.Task))
		{
		}
		public EscrowEstimateTypeRepository(UnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

		public IQueryable<EscrowEstimateType> DataToJoin
		{
			get { return DbQueryRoot; }
		}

		public List<EscrowEstimateTypeModel> GetEscrowEstimateTypes()
		{
			var escrowEstimateTypes = this.GetAll().ToList();
			return Mapper.Map<List<EscrowEstimateTypeModel>>(escrowEstimateTypes);
		}

		public string GetEscrowEstimateTypeName(int pId)
		{
			EscrowEstimateType objEscrowEstimateType = this.GetByID(pId);
			if (objEscrowEstimateType != null)
				return objEscrowEstimateType.EscrowEstimate;
			return null;
		}
	}
}
