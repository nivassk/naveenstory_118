﻿using AutoMapper;
using EntityObject.Entities.HCP_intermediate;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Repository;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class UploadStatusDetailRepository : BaseRepository<usp_HCP_Get_QuarterlyUploadReportDetail_Result>, IUploadStatusDetailRepository
    {
        public UploadStatusDetailRepository()
            : base(new UnitOfWork(DBSource.Intermediate))
        {
        }
        public UploadStatusDetailRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<UploadStatusDetailModel> GetUploadStatusDetailByLenderId(string username, string usertype, int lendId, int monthsInPeriod, int year)
        {
            var context = this.Context as HCP_intermediate;
            if (context == null)
                throw new InvalidCastException("context is not from db intermediate in GetUploadStatusDetailByLenderId, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_Get_QuarterlyUploadReportDetail_Result>("usp_HCP_Get_QuarterlyUploadReportDetail", new 
                { Username = username, UserType = usertype, LenderId = lendId, MonthsInPeriod = monthsInPeriod, Year = year })
                .Where(p => (lendId == -1 || p.LenderID == lendId)).ToList();
            return Mapper.Map<IEnumerable<UploadStatusDetailModel>>(results);
        }

   }
}
