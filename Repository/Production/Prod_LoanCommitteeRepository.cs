﻿using AutoMapper;
using EntityObject.Entities.HCP_live;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Production
{
    public class Prod_LoanCommitteeRepository : BaseRepository<Prod_LoanCommittee>, IProd_LoanCommitteeRepository
    {
        public Prod_LoanCommitteeRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        public Prod_LoanCommitteeRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }

        public Prod_LoanCommitteeViewModel GetLoanCommitteeDetails(Guid applicationTaskInstanceId)
        {
            var lcDetails = this.GetAll().ToList().Where(a => a.ApplicationTaskinstanceId == applicationTaskInstanceId).FirstOrDefault();
            return Mapper.Map<Prod_LoanCommittee, Prod_LoanCommitteeViewModel>(lcDetails);
        }
        public List<Prod_LoanCommitteeViewModel> GetSubmitedDates(Guid applicationTaskInstanceId)
        {
            var lcDetails = this.GetAll().ToList().Where(a => a.ApplicationTaskinstanceId == applicationTaskInstanceId).ToList();
            return Mapper.Map<List<Prod_LoanCommittee>, List<Prod_LoanCommitteeViewModel>>(lcDetails);
        }
        public Prod_LoanCommitteeViewModel GetLoanCommitteeSelctedDateInfo(Guid taskInstanceId, string SelectedDate)
        {
            var lcDetails = this.GetAll().ToList().Where(a => a.ApplicationTaskinstanceId == taskInstanceId && a.LoanCommitteDate == Convert.ToDateTime(SelectedDate)).FirstOrDefault();
            return Mapper.Map<Prod_LoanCommittee, Prod_LoanCommitteeViewModel>(lcDetails);
        }
        public Guid SaveLoanCommitteeDetails(Prod_LoanCommitteeViewModel lcModel)
        {
            var context = (HCP_live)this.Context;
            var loanCommittee = new Prod_LoanCommittee();
            var isNew = false;
            if (lcModel.ChangeToLoanCommitteeDate == "Reschedule Date" || lcModel.ChangeToLoanCommitteeDate == "Extend Date")
            {
                lcModel.LoanCommitteeId = Guid.Empty;
            }

            if (lcModel.LoanCommitteeId == Guid.Empty)
            {
                loanCommittee = Mapper.Map<Prod_LoanCommittee>(lcModel);
                loanCommittee.LoanCommitteeId = Guid.NewGuid();
                loanCommittee.CreatedBy = UserPrincipal.Current.UserId;
                loanCommittee.CreatedOn = DateTime.UtcNow;
                if (!string.IsNullOrEmpty(lcModel.LCComments))
                    loanCommittee.LCComments = "" + lcModel.LCComments + "- BY:" +
                                                           UserPrincipal.Current.UserName + ">";
                isNew = true;


            }
            else
            {
                loanCommittee = this.Find(x => x.LoanCommitteeId == lcModel.LoanCommitteeId).FirstOrDefault();
                loanCommittee.ProjectScheduledForLC = lcModel.ProjectScheduledForLC;
                loanCommittee.LCRecommendation = lcModel.LCRecommendation;
                loanCommittee.A7Presentation = lcModel.A7Presentation;

                loanCommittee.TwoStageSubmittal = lcModel.TwoStageSubmittal;
                if (!string.IsNullOrEmpty(lcModel.LCComments))
                    loanCommittee.LCComments += "" + lcModel.LCComments + "- BY:" +
                                                           UserPrincipal.Current.UserName + ">";

            }
            if (isNew)
            //&& (lcModel.LoanCommitteDate != loanCommittee.LoanCommitteDate))            
            {
                var result = this.Find(x => x.LoanCommitteDate == lcModel.LoanCommitteDate)
                    .OrderByDescending(x => x.SequenceId).FirstOrDefault();
                loanCommittee.SequenceId = result != null ? result.SequenceId + 1 : 1;
            }
            loanCommittee.ModifiedBy = UserPrincipal.Current.UserId;
            loanCommittee.ModifiedOn = DateTime.UtcNow;
            loanCommittee.DecisionDate = lcModel.DecisionDate;
            loanCommittee.LCDecision = lcModel.LCDecision;
            loanCommittee.ChangeToLoanCommitteeDate = lcModel.ChangeToLoanCommitteeDate;
            loanCommittee.LoanCommitteDate = lcModel.LoanCommitteDate;
            if (isNew)
            {
                this.InsertNew(loanCommittee);
            }
            else
            {

                //loanCommittee.LoanCommitteDate = lcModel.LoanCommitteDate;
                this.Update(loanCommittee);
            }

            context.SaveChanges();


            return loanCommittee.LoanCommitteeId;
        }
        public IList<Prod_LoanCommitteeViewModel> GetLCFiltersValues()
        {
            try
            {
                var context = (HCP_live)this.Context;
                var result = (from n in context.Prod_LoanCommittee
                              select n).ToList();
                var Filter = Mapper.Map<IList<Prod_LoanCommittee>, IList<Prod_LoanCommitteeViewModel>>(result);

                return Filter;
            }
            catch (Exception ex)
            {
            }

            return null;
        }
        public IList<string> AllProductionUsers()
        {
            try
            {

                var context = (HCP_live)this.Context;
                var lenders = (from PL in context.Prod_LoanCommittee
                               join PT in context.Prod_ProjectType on PL.ProjectType equals PT.ProjectTypeId
                               select PT.ProjectTypeName).ToList();
                //var Filter = Mapper.Map<IList<Prod_LoanCommittee>, IList<Prod_LoanCommitteeViewModel>>(lenders);

                return lenders;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public object Pwlm()
        {
            try
            {
                var context = (HCP_live)this.Context;
                var lender = (from n in context.LenderInfoes
                              select new { n.Lender_Name }).ToList();
                return lender;
            }
            catch (Exception ex)
            {
            }
            return null;
        }


        public IList<Prod_LoanCommitteeViewModel> UpdateLCGridResults(IList<Prod_LoanCommitteeViewModel> Model)
        {
            foreach (var item in Model)
            {
                var LCData = Find(p => p.LoanCommitteeId == item.LoanCommitteeId).First();
                LCData.SequenceId = item.SequenceId;
                LCData.ModifiedBy = UserPrincipal.Current.UserId;
                LCData.ModifiedOn = DateTime.UtcNow;
                this.Update(LCData);
                Context.SaveChanges();
            }
            return null;
        }



        public int GetLCDateCount(DateTime lcDate)
        {
            return this.Find(x => x.LoanCommitteDate == lcDate).Count();
        }

        public IList<string> GetLoanCommitteeDatesToDisable()
        {
            var context = (HCP_live)this.Context;

            var dateList = (from n in context.Prod_LoanCommittee
                            select n).AsEnumerable().GroupBy(x => ((DateTime)x.LoanCommitteDate).ToString("M/d/yyyy"))
                                 .Where(grp => grp.Count() >= 12)
                                 .Select(grp => grp.Key).ToList();
            return dateList;
        }

        public Prod_LoanCommitteeViewModel GetLoanCommitteeDetailByFha(string pFHANumber)
        {
            var lcDetails = this.GetAll().ToList().Where(a => a.FHANumber == pFHANumber).FirstOrDefault();
            return Mapper.Map<Prod_LoanCommittee, Prod_LoanCommitteeViewModel>(lcDetails);
        }
    }
}
