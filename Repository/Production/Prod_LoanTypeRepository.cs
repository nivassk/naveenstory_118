﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;

namespace Repository.Production
{
    public class Prod_LoanTypeRepository : BaseRepository<Prod_LoanType>, IProd_LoanTypeRepository
    {
        public Prod_LoanTypeRepository()
            : base(new UnitOfWork(DBSource.Live))
        {

        }
        public Prod_LoanTypeRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IList<LoanTypeModel> GetAllMortgageInsuranceTypes()
        {
            var mortgageInsuranceTypes = this.GetAll().ToList();
            return
                Mapper.Map<IList<Prod_LoanType>, IList<LoanTypeModel>>(mortgageInsuranceTypes);
        }
    }
}
