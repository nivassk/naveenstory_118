﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessService.Interfaces.Production;
using EntityObject.Entities.HCP_live;
using EntityObject.Entities.HCP_live.Programmability;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces;
using EntityObject.Entities.HCP_task;
using Repository.Interfaces.Production;

namespace Repository.Production
{
    public class ProductionReassignReportRepository : BaseRepository<ProductionReassignViewModel>, IProductionReassignReportRepository
    {

        //  private ProductionPAMReportModel _plmReportModel;

        const int TrueInt = 1;
        const int FalseInt = 0;


        public ProductionReassignReportRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
            // _plmReportModel = new ProductionPAMReportModel(ReportType.HudProductionReport);
        }

        public ProductionReassignReportRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        public ProductionReasignReportModel GetProdReassignmentTasks(string appType, string programType, string prodUserIds, DateTime? fromDate, DateTime? toDate)
        {
            if (fromDate == null && toDate == null)
            {
                fromDate = DateTime.Now.AddYears(-1);
            }
            var reportModel = new ProductionReasignReportModel(ReportType.ProdTaskReassignment);
            try
            {

                var context = (HCP_task)this.Context;

                if (context == null)
                    throw new InvalidCastException(
                        "context is not from db Live in USP_GetProdReassignmentTasks, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                string Spname = string.Empty;
               
                var results = context.Database.SqlQuerySimple<usp_HCP_GetProdPAMReportDetail_Result>("USP_GetProdReassignmentTasks",
                  new
                  {

                      AppType = string.IsNullOrEmpty(appType) ? null : appType,
                      ProgramType = string.IsNullOrEmpty(programType) ? null : programType,
                  
                      Userlist = string.IsNullOrEmpty(prodUserIds) ? null : prodUserIds,
                     
                      startTime = fromDate,
                      endTime = toDate

                  }).ToList();
                var Vm = Mapper.Map<IList<usp_HCP_GetProdPAMReportDetail_Result>, IList<ProductionReassignViewModel>>(results);
                reportModel.ReassignGridProductionlList = Vm;

            }
            catch (Exception ex)
            {

            }
            return reportModel;
        }


        public void ReassignTasks(List<string> kvp, int AssignUserId)
        {
            var context = (HCP_task)this.Context;
         
            try
            {
                if (kvp != null)
                {

                    foreach (var item in kvp)
                    {
                        string[] pair = item.Split('|');

                        if (context == null)
                            throw new InvalidCastException("context is not from db live in InsertReviewerTaskFileData, please pass in correct context in unit of work.");
                        var results =
                            context.Database.ExecuteSqlCommandSimple(
                                "usp_Prod_UpdatesForTaskReassignment",
                                new
                                {
                                    TaskInstanceId = new Guid(pair[0]),
                                    ViewId = int.Parse(pair[1]),
                                    ReassignedUserId = AssignUserId,
									UserId = int.Parse(pair[7])
								});

                    }
                }
            }

            catch (Exception ex)
            {


            }
   
         
        }
    }
}
