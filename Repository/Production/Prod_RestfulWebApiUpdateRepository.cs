﻿using Model.Production;
using Newtonsoft.Json;
using Repository.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Production
{
    public class Prod_RestfulWebApiUpdateRepository : IProd_RestfulWebApiUpdateRepository
    {
        public Prod_RestfulWebApiUpdateRepository()
        {

        }
        public RestfulWebApiResultModel UpdateDocumentInfo(RestfulWebApiUpdateModel fileInfo, string token)
        {
            RestfulWebApiUploadHelper helper = new RestfulWebApiUploadHelper();
            RestfulWebApiResultModel rst = new RestfulWebApiResultModel();
            string URL = ConfigurationManager.AppSettings["UpdateDocument"].ToString() + token;
            using (var content = new MultipartFormDataContent())
            {
                var formData = new[]
                    {
                          new KeyValuePair<string, string>("propertyID", fileInfo.propertyId),
                          new KeyValuePair<string, string>("documentType", fileInfo.documentType),
                          new KeyValuePair<string, string>("indexType", fileInfo.indexType),
                          new KeyValuePair<string, string>("indexValue", fileInfo.indexValue),
                          new KeyValuePair<string, string>("documentStatus", fileInfo.documentStatus),
                          new KeyValuePair<string, string>("folderName", ""),
                          new KeyValuePair<string, string>("docId", fileInfo.docId),
               
                    };
                foreach (var KeyValuePair in formData)
                {
                    if (!string.IsNullOrEmpty(KeyValuePair.Value))
                    {
                        content.Add(new StringContent(KeyValuePair.Value.ToString()), KeyValuePair.Key.ToString());
                    }
                    else 
                    {
                        content.Add(new StringContent(""), KeyValuePair.Key.ToString());
                    }
                   
                }
                rst = helper.UpdateReplacePostRequest(URL, content);
                rst.documentType = fileInfo.documentType;
                return rst;

            }
        }
      
    }
}
