﻿using Model.Production;
using Newtonsoft.Json;
using Repository.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Repository.Production
{
    public class Prod_RestfulWebApiAddPropertyInfoRepository : IProd_RestfulWebApiAddPropertyInfoRepository
    {

        public Prod_RestfulWebApiAddPropertyInfoRepository()
        {

        }

        public RestfulWebApiResultModel AddPropertyInfoUsingWebApi(string JsonString, string token)
        {

            string url = ConfigurationManager.AppSettings["AddPropertInfo"].ToString() + token;
            RestfulWebApiResultModel result = new RestfulWebApiResultModel();
            RestfulWebApiUploadHelper helper = new RestfulWebApiUploadHelper();
           
            return helper.PostRequestFotProperty(url, JsonString);



        }
       
    }
}

