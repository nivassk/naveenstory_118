﻿using System;
using System.Linq;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;
using HUDHealthcarePortal.Core;
using AutoMapper;
using System.Collections.Generic;

namespace Repository.Production
{
    public class Prod_From290TaskRepository : BaseRepository<Prod_Form290Task>, IProd_Form290TaskRepository
    {
        public Prod_From290TaskRepository()
            : base(new UnitOfWork(DBSource.Task))
        {

        }
        public Prod_From290TaskRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }
        public Guid AddForm290Task(Prod_Form290TaskModel model)
        {
            var task290 = Mapper.Map<Prod_Form290Task>(model);
            this.InsertNew(task290);
            this.UnitOfWork.Save();
            return task290.TaskInstanceID;
        }

        public Prod_Form290TaskModel GetForm290TaskBySingleTaskInstanceID(Guid taskInstanceID)
        {
            var result = this.Find(m => m.TaskInstanceID == taskInstanceID).FirstOrDefault();
            return Mapper.Map<Prod_Form290TaskModel>(result);
        }
        public List<Prod_Form290TaskModel> GetForm290TaskByTaskInstanceID(List<Guid> taskInstanceIDs)
        {
            var result = this.GetAll().Where(m=>taskInstanceIDs.Contains(m.TaskInstanceID)).ToList();
            return Mapper.Map<List<Prod_Form290TaskModel>>(result);
        }

        public Prod_Form290TaskModel GetFrom290TaskByClosingTaskInstanceID(Guid ClosingTaskInstanceID)
        {
            var result = this.Find(m => m.ClosingTaskInstanceID == ClosingTaskInstanceID).LastOrDefault();
            return Mapper.Map<Prod_Form290TaskModel>(result);
        }
    }
}
