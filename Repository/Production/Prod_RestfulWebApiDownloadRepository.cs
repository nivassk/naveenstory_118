﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using HUDHealthcarePortal;
using System.Threading.Tasks;
using Repository.Interfaces.Production;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Repository.Production
{
    public class Prod_RestfulWebApiDownloadRepository : IProd_RestfulWebApiDownloadRepository
    {
        public Prod_RestfulWebApiDownloadRepository()
        {

        }

        public Stream DownloadDocumentUsingWebApi(string JsonString, string token, string filename)
        {
            
            Stream file = null;
            string URL = ConfigurationManager.AppSettings["DownloadDocument"].ToString() + token;
            String encoded = System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(
                                                          string.Format("{0}:{1}", "restapp", "restapp")));
            string BaseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//3072

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(BaseURL + URL);
            request.Method = WebRequestMethods.Http.Post;
            request.Headers.Add("Authorization", "Basic " + encoded);
            request.ContentType = "application/json";
            request.Headers.Add("Data-Type", "application/json");

            
            try
            {

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    //string json = "{\"docId\":\"5092962\",\"version\":\"0\"}";

                    streamWriter.Write(JsonString);
                    streamWriter.Flush();
                }
                //nitin
                var resp = HttpContext.Current.Response;


                var httpResponse = (HttpWebResponse)request.GetResponse();



                if (httpResponse.ContentLength > 0)
                    file = httpResponse.GetResponseStream();
                return file;
                //var resp = HttpContext.Current.Response;
                //resp.ContentType = "application/octet-stream";karri
                int bytesToRead = 8 * 10000;
                byte[] buffer = new Byte[bytesToRead];
                //resp.AddHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");karri
                //resp.AddHeader("Content-Length", httpResponse.ContentLength.ToString());karri
                int length;

                do
                {
                    if (resp.IsClientConnected)
                    {
                        length = file.Read(buffer, 0, bytesToRead);
                        resp.OutputStream.Write(buffer, 0, length);
                        resp.Flush();
                       // resp.End();
                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {

                        length = -1;
                    }
                }
                while (length > 0);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (file != null)
                {
                    //file.Seek(0, 0);
                    //file.Close();karri
                }
            }
            return file;
        }

        private void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }
        public Byte[] DownloadByteFile(string JsonString, string token, string filename)
        {
           
            string URL = ConfigurationManager.AppSettings["DownloadDocument"].ToString() + token;
            String encoded = System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(
                                                          string.Format("{0}:{1}", "restapp", "restapp")));
            string BaseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//3072

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(BaseURL + URL);
            request.Method = WebRequestMethods.Http.Post;
            request.Headers.Add("Authorization", "Basic " + encoded);
            request.ContentType = "application/json";
            request.Headers.Add("Data-Type", "application/json");
            var bytes = default(byte[]);
            MemoryStream memstream = new MemoryStream();
            try
            {

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    //string json = "{\"docId\":\"5092962\",\"version\":\"0\"}";

                    streamWriter.Write(JsonString);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)request.GetResponse();  
               
                
               
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {                   
                    streamReader.BaseStream.CopyTo(memstream);
                    bytes = memstream.ToArray();
                }

              
               
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                if (memstream != null)
                {

                    memstream.Close();
                }
            }
            return bytes;
        }
        
    }
}
