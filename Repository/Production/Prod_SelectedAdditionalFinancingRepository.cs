﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;

namespace Repository.Production
{
    public class Prod_SelectedAdditionalFinancingRepository : BaseRepository<Prod_SelectedAdditionalFinancingResources>, IProd_SelectedAdditionalFinancingRepository
    {
        public Prod_SelectedAdditionalFinancingRepository()
            : base(new UnitOfWork(DBSource.Live))
        { }
        public Prod_SelectedAdditionalFinancingRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public void AddSelectedAdditionalFinancing(IList<SelectedAdditionalFinancingModel> financingList)
        {
            try
            {
                var result =
                        Mapper.Map<IList<SelectedAdditionalFinancingModel>, IList<Prod_SelectedAdditionalFinancingResources>>(
                            financingList);
                foreach (var financingResource in result)
                {
                    this.InsertNew(financingResource);
                    Context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                
                throw;
            }
        }

        public IList<SelectedAdditionalFinancingModel> GetSelectedAdditionalFinancingByFHARequestId(Guid fhaRequestId)
        {
            var result = Find(p => p.FHANumberRequestId == fhaRequestId).ToList();

            return Mapper.Map<IList<Prod_SelectedAdditionalFinancingResources>, IList<SelectedAdditionalFinancingModel>>(
                    result);

        }

        public void UpdateSelectedAdditionalFinancingByFHARequestId(Guid fhaRequestId, IList<SelectedAdditionalFinancingModel> financingList)
        {
            var result = Find(p => p.FHANumberRequestId == fhaRequestId).ToList();
            if (financingList.Any())
            {
                foreach (var item in result)
                {
                    this.Delete(item);
                    Context.SaveChanges();
                }
                AddSelectedAdditionalFinancing(financingList);
            }
            
        }
    }
}
