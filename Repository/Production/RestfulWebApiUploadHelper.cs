﻿using Model.Production;
using Newtonsoft.Json;
using Repository.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Repository.Production
{
    public class RestfulWebApiUploadHelper
    {
       
        public RestfulWebApiResultModel PostRequest(string URL, MultipartFormDataContent contentVals)
        {
            string BaseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            RestfulWebApiResultModel result = new RestfulWebApiResultModel();
            using (HttpClient client = new HttpClient())
            {
                using (var content = contentVals)
                {
                    client.BaseAddress = new Uri(BaseURL);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var request = new HttpRequestMessage(HttpMethod.Post, URL);
                    request.Content = content;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                                                                 Convert.ToBase64String(
                                                                 System.Text.ASCIIEncoding.ASCII.GetBytes(
                                                                 string.Format("{0}:{1}", "restapp", "restapp"))));

                    try
                    {
                        //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//3072
                        using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
                        {
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                                result = JsonConvert.DeserializeObject<RestfulWebApiResultModel>(responseData);
                                if (result.documents != null)
                                {
                                    dynamic jsonObj = JsonConvert.DeserializeObject(result.documents.ToString());

                                    foreach (var obj in jsonObj)
                                    {
                                        try
                                        {

                                            result.docId = obj.docId;
                                            result.version = obj.version;
                                        }
                                        catch (Exception ex)
                                        {

                                        }


                                    }
                                   
                                }
                                //dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(responseData);
                                //foreach (var item in jsonResponse)
                                //{

                                //    result.status = item.GetValue("status");
                                //    result.message = item.GetValue("message");
                                //    result.docId = item.GetValue("docId");
                                //    result.version = item.GetValue("version");

                                //}

                                //result = JsonConvert.DeserializeObject<RestfulWebApiResultModel>(responseData);

                            }
                            else
                            {
                                result.status = "failed";
                                result.message = responseMessage.ReasonPhrase;
                                return result;
                            }
                        }
                    }
                    catch (Exception ex)
                    {


                    }
                    return result;
                }
            }
        }
        public RestfulWebApiResultModel PostRequestFotProperty(string URL, string jsondata)
        {

            
            String encoded = System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(
                                                          string.Format("{0}:{1}", "restapp", "restapp")));
            string BaseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            RestfulWebApiResultModel result = new RestfulWebApiResultModel();

           System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//3072

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(BaseURL + URL);
            request.Method = WebRequestMethods.Http.Post;
            request.Headers.Add("Authorization", "Basic " + encoded);
            request.ContentType = "application/json";
            request.Headers.Add("Data-Type", "application/json");
            var bytes = default(byte[]);
            Stream file = null;
            MemoryStream memstream = new MemoryStream();
            try
            {

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                   
                    streamWriter.Write(jsondata);
                    streamWriter.Flush();
                }
                var httpResponse = (HttpWebResponse)request.GetResponse();


                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    streamReader.BaseStream.CopyTo(memstream);
                    bytes = memstream.ToArray();
                    var str = System.Text.Encoding.Default.GetString(bytes);
                    result = JsonConvert.DeserializeObject<RestfulWebApiResultModel>(str);

                }



            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (memstream != null)
                {

                    memstream.Close();
                }
            }
            return result;
            
        }
        public RestfulWebApiResultModel UpdateReplacePostRequest(string URL, MultipartFormDataContent contentVals)
        {
            string BaseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            RestfulWebApiResultModel rslt = new RestfulWebApiResultModel();

            using (HttpClient client = new HttpClient())
            {
                using (var content = contentVals)
                {

                    var maxVersion = 0;
                    var preVersion = 0;
                    var currVersion = 0;
                    client.BaseAddress = new Uri(BaseURL);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var request = new HttpRequestMessage(HttpMethod.Post, URL);
                    request.Content = content;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                                                                 Convert.ToBase64String(
                                                                 System.Text.ASCIIEncoding.ASCII.GetBytes(
                                                                 string.Format("{0}:{1}", "restapp", "restapp"))));

                    //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//3072
                    using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
                    {

                        if (responseMessage.IsSuccessStatusCode)
                        {
                            var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                            rslt = JsonConvert.DeserializeObject<RestfulWebApiResultModel>(responseData);
                            if (!string.IsNullOrEmpty(rslt.status) && rslt.status.ToLower() == "success")
                            {
                                if (rslt.listData != null)
                                {
                                    dynamic jsonObj = JsonConvert.DeserializeObject(rslt.listData.ToString());

                                    foreach (var obj in jsonObj)
                                    {
                                        try
                                        {

                                            currVersion = obj.version;


                                            if (preVersion > currVersion)
                                            {
                                                maxVersion = preVersion;
                                            }
                                            else
                                            {
                                                maxVersion = currVersion;
                                            }
                                        }
                                        catch (Exception ex)
                                        {

                                        }


                                    }
                                    rslt.version = Convert.ToString(maxVersion);
                                }
                            }
                           
                            return rslt;
                        }
                        else
                        {
                            rslt.status = "failed";
                            rslt.message = responseMessage.ReasonPhrase;
                            return rslt;
                        }
                    }
                }


            }

        }
        public RestfulWebApiResultModel DeletePostRequest(string URL, MultipartFormDataContent contentVals)
        {
            string BaseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            RestfulWebApiResultModel result = new RestfulWebApiResultModel();
            using (HttpClient client = new HttpClient())
            {
                using (var content = contentVals)
                {
                    client.BaseAddress = new Uri(BaseURL);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var request = new HttpRequestMessage(HttpMethod.Delete, URL);
                    request.Content = content;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                                                                 Convert.ToBase64String(
                                                                 System.Text.ASCIIEncoding.ASCII.GetBytes(
                                                                 string.Format("{0}:{1}", "restapp", "restapp"))));
                    //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//3072
                    try
                    {
                        using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
                        {
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                                result = JsonConvert.DeserializeObject<RestfulWebApiResultModel>(responseData);
                                if (result.documents != null)
                                {
                                    dynamic jsonObj = JsonConvert.DeserializeObject(result.documents.ToString());

                                    foreach (var obj in jsonObj)
                                    {
                                        try
                                        {

                                            result.key = obj.Key;
                                            result.message = obj.Status;
                                        }
                                        catch (Exception ex)
                                        {

                                        }


                                    }

                                }
                                
                            }
                            else
                            {
                                result.status = "failed";
                                result.message = responseMessage.ReasonPhrase;
                                return result;
                            }
                        }
                    }
                    catch (Exception ex)
                    {


                    }
                    return result;
                }
            }
        }



    }
}

