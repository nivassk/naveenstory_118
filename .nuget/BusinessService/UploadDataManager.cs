﻿using System.Globalization;
using BusinessService;
using BusinessService.Interfaces;
using Core;
using Core.Utilities;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Interfaces;


namespace HUDHealthcarePortal.BusinessService
{
    public class UploadDataManager : IUploadDataManager
    {
        private IDataUploadIntermediateRepository dataUploadIntermediateRepository;
        private IUploadDataSummaryRepository uploadDataSummaryRepository;
        private IUserLenderServicerRepository userLenderServicerRepository;
        private IDataUploadLiveRepository dataUploadLiveRepository;
        private IUploadStatusRepository uploadStatusRepository;
        private IUploadStatusDetailRepository uploadStatusDetailRepository;
        private ICommentRepository commentRepository;
        private ICommentViewRepository commentViewRepository;
        private IScoreManager scoreManager;
        // don't use static unit of work, aka context, context should only last per http request due to entity framework identity pattern
        // may get stale data
        private UnitOfWork unitOfWorkIntermediate;
        private UnitOfWork unitOfWorkLive;
        private IFhaRepository fhaRepository;
        private ILenderFhaRepository lenderFhaRepository;
       

        public UploadDataManager()
        {
            unitOfWorkIntermediate = new UnitOfWork(DBSource.Intermediate);
            unitOfWorkLive = new UnitOfWork(DBSource.Live);

            dataUploadIntermediateRepository = new DataUploadIntermediateRepository(unitOfWorkIntermediate);
            uploadDataSummaryRepository = new UploadDataSummaryRepository(unitOfWorkLive);
            uploadStatusRepository = new UploadStatusRepository(unitOfWorkIntermediate);
            uploadStatusDetailRepository = new UploadStatusDetailRepository(unitOfWorkIntermediate);
            userLenderServicerRepository = new UserLenderServicerRepository(unitOfWorkLive);
            dataUploadLiveRepository = new DataUploadLiveRepository(unitOfWorkLive);
            commentRepository = new CommentRepository(unitOfWorkLive);
            commentViewRepository = new CommentViewRepository(unitOfWorkIntermediate);
            fhaRepository = new FhaRepository(unitOfWorkLive);
            lenderFhaRepository = new LenderFhaRepository(unitOfWorkLive);
        }

        public ExcelUploadView_Model GetProjectDetailByID(int ldiID, DBSource source)
        {
            return source == DBSource.Intermediate ? dataUploadIntermediateRepository.GetProjectDetail(ldiID)
                : dataUploadLiveRepository.GetProjectDetailByLDI_ID(ldiID);
        }

        public IEnumerable<ReportExcelUpload_Model> GetDataUploadSummaryByUserId(int iUserId)
        {
            var results = uploadDataSummaryRepository.GetDataUploadSummaryByUserId(iUserId)
                .OrderByDescending(p => p.DataInserted);
            foreach(var item in results)
            {
                item.DataInserted = TimezoneManager.GetPreferredTimeFromUtc(item.DataInserted); 
            }
            return results;
        }

        public List<string> GetAllowedFhaByLarUserId(int userId)
        {
            return userLenderServicerRepository.GetAllowedFhaByLarUserId(userId);
        }

        public List<string> GetAllowedFhaByLenderUserId(int lenderUserId)
        {
            return userLenderServicerRepository.GetAllowedFhaByLenderUserId(lenderUserId, lenderFhaRepository);
        }

        public List<string> GetAllowedFhaByAeOrWlmUserId(int userId)
        {
            return userLenderServicerRepository.GetAllowedFhaByAeOrWlmUserId(userId);
        }

        public IEnumerable<DerivedUploadData> GetUploadDataByRole(int iUserId, string[] userRoles)
        {
            //int? iLenderId = userLenderServicerRepository.GetLenderId(iUserId, userRoles);
            //if ((userRoles.Contains("Lender") || RoleManager.IsLenderAdmin(userRoles)) && !iLenderId.HasValue)
            int? iLenderId = UserPrincipal.Current.LenderId;
            if ((userRoles.Contains("Lender") || RoleManager.IsCurrentUserLenderRoles()) && !iLenderId.HasValue)
            {
                throw new InvalidOperationException(string.Format("User id: {0} is a lender, but does not have corresponding lender id in UserLender table", UserPrincipal.Current.UserId));
            }

            var results = dataUploadIntermediateRepository.GetUploadedData(iLenderId);
            return results.Select(p =>
                    new DerivedUploadData()
                    {
                        LDI_ID = p.LDI_ID,
                        ProjectName = p.ProjectName,
                        ServiceName = p.ServiceName,
                        PeriodEnding = p.PeriodEnding,
                        OperatorOwner = p.OperatorOwner,
                        FinancialStatementType = p.FinancialStatementType,
                        UnitsInFacility = p.UnitsInFacility,
                        MonthsInPeriod = p.MonthsInPeriod,
                        FHANumber = p.FHANumber,
                        ResidentPatientDays = p.ResidentPatientDays,
                        TotalBedsoperated = p.TotalBedsoperated,
                        OperatingCash = p.OperatingCash,
                        Investments = p.Investments,
                        ReserveForReplacementEscrowBalance = p.ReserveForReplacementEscrowBalance,
                        AccountsReceivable = p.AccountsReceivable,
                        CurrentAssets = p.CurrentAssets,
                        CurrentLiabilities = p.CurrentLiabilities,
                        TotalRevenues = p.TotalRevenues,
                        RentLeaseExpense = p.RentLeaseExpense,
                        DepreciationExpense = p.DepreciationExpense,
                        AmortizationExpense = p.AmortizationExpense,
                        TotalExpenses = p.TotalExpenses,
                        NetIncome = p.NetIncome,
                        FHAInsuredPrincipalPayment = p.FHAInsuredPrincipalPayment,
                        FHAInsuredInterestPayment = p.FHAInsuredInterestPayment,
                        MortgageInsurancePremium = p.MortgageInsurancePremium,
                        ReserveForReplacementDeposit = p.ReserveForReplacementDeposit,
                        DebtCoverageRatio = p.HasCalculated == false ? p.DerivedFinDict[DerivedFinancial.DebtCoverageRatio] : p.DebtCoverageRatio,// p.DebtCoverageRatio,
                        WorkingCapital = p.HasCalculated == false ? p.DerivedFinDict[DerivedFinancial.WorkingCapital] : p.WorkingCapital, //p.WorkingCapital,
                        DaysCashOnHand = p.HasCalculated == false ? p.DerivedFinDict[DerivedFinancial.DaysCashOnHand] : p.DaysCashOnHand, //p.DaysCashOnHand,
                        DaysInAcctReceivable = p.HasCalculated == false ? p.DerivedFinDict[DerivedFinancial.DaysInAccountReceivable] : p.DaysInAcctReceivable, //p.DaysInAcctReceivable,
                        AvgPaymentPeriod = p.HasCalculated == false ? p.DerivedFinDict[DerivedFinancial.AvgPaymentPeriod] : p.AvgPaymentPeriod, // p.AvgPaymentPeriod
                        ReserveForReplacementBalancePerUnit = p.HasCalculated == false ? p.DerivedFinDict[DerivedFinancial.ReserveForReplaceBalUnit] : p.ReserveForReplacementBalancePerUnit,
                        DebtCoverageRatioScore = p.HasCalculated == false ? p.DerivedScoreDict[DerivedFinancial.DebtCoverageRatio] : p.DebtCoverageRatioScore,
                        WorkingCapitalScore = p.HasCalculated == false ? p.DerivedScoreDict[DerivedFinancial.WorkingCapital] : p.WorkingCapitalScore,
                        DaysCashOnHandScore = p.HasCalculated == false ? p.DerivedScoreDict[DerivedFinancial.DaysCashOnHand] : p.DaysCashOnHandScore,
                        DaysInAcctReceivableScore = p.HasCalculated == false ? p.DerivedScoreDict[DerivedFinancial.DaysInAccountReceivable] : p.DaysCashOnHandScore,
                        AvgPaymentPeriodScore = p.HasCalculated == false ? p.DerivedScoreDict[DerivedFinancial.AvgPaymentPeriod] : p.AvgPaymentPeriodScore,
                        ScoreTotal = p.HasCalculated == false ? p.ScoreSum : p.ScoreTotal,
                        HasComment = p.HasComment
                    });
        }
       
        
        public IEnumerable<DerivedUploadData> GetUploadDataBySearch(int iUserId, string[] userRoles)
        {
            int? iLenderId = UserPrincipal.Current.LenderId;
            if ((userRoles.Contains("Lender") || RoleManager.IsCurrentUserLenderRoles()) && !iLenderId.HasValue)
            {
                throw new InvalidOperationException(string.Format("User id: {0} is a lender, but does not have corresponding lender id in UserLender table", UserPrincipal.Current.UserId));
            }
            if (RoleManager.IsSuperUserRole(UserPrincipal.Current.UserName))
            {
                return dataUploadIntermediateRepository.GetUploadedDataByRole(iLenderId, commentViewRepository, null);
            }
            if (RoleManager.IsCurrentOperatorAccountRepresentative())
            {
                var allowedFhas = GetAllowedFhaByLarUserId(UserPrincipal.Current.UserId);
                return dataUploadIntermediateRepository.GetUploadedDataByRole(iLenderId, commentViewRepository, allowedFhas);
            }
            if (RoleManager.IsAccountExecutiveOrWorkloadManagerRole(UserPrincipal.Current.UserName))
            {
                var allowedFhas = GetAllowedFhaByAeOrWlmUserId(UserPrincipal.Current.UserId);
                return dataUploadIntermediateRepository.GetUploadedDataByRole(null, commentViewRepository, allowedFhas);
            }
            return dataUploadIntermediateRepository.GetUploadedDataByRole(iLenderId, commentViewRepository, null);
        } 
        public PaginateSortModel<DerivedUploadData> GetUploadDataByRoleWithPageSort(int iUserId, string[] userRoles,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum)
        {
            //int? iLenderId = userLenderServicerRepository.GetLenderId(iUserId, userRoles);
            int? iLenderId = UserPrincipal.Current.LenderId;
            if ((userRoles.Contains("Lender") || RoleManager.IsCurrentUserLenderRoles()) && !iLenderId.HasValue)
            {
                throw new InvalidOperationException(string.Format("User id: {0} is a lender, but does not have corresponding lender id in UserLender table", UserPrincipal.Current.UserId));
            }

            
            PaginateSortModel<DerivedUploadData> resultsRaw;
            if (RoleManager.IsSuperUserRole(UserPrincipal.Current.UserName))
            {
                resultsRaw = dataUploadIntermediateRepository.GetUploadedDataWithPageSort(iLenderId, strSortBy,
                    sortOrder, pageSize, pageNum, commentViewRepository, null);
            }
            else if (RoleManager.IsCurrentOperatorAccountRepresentative())
            {
                var allowedFhas = GetAllowedFhaByLarUserId(UserPrincipal.Current.UserId);
                resultsRaw = dataUploadIntermediateRepository.GetUploadedDataWithPageSort(iLenderId, strSortBy,
                    sortOrder, pageSize, pageNum, commentViewRepository, allowedFhas);
            }
            else if (RoleManager.IsAccountExecutiveOrWorkloadManagerRole(UserPrincipal.Current.UserName))
            {
                var allowedFhas = GetAllowedFhaByAeOrWlmUserId(UserPrincipal.Current.UserId);
                resultsRaw = dataUploadIntermediateRepository.GetUploadedDataWithPageSort(null, strSortBy,
                    sortOrder, pageSize, pageNum, commentViewRepository, allowedFhas);
            }
            else
            {
                resultsRaw = dataUploadIntermediateRepository.GetUploadedDataWithPageSort(iLenderId, strSortBy,
                    sortOrder, pageSize, pageNum, commentViewRepository, null);
            }
            var results = PaginateSortModel<DerivedUploadData>.CreateInstance();
            results.TotalRows = resultsRaw.TotalRows;
            var entities = new List<DerivedUploadData>();

            if (resultsRaw.Entities.Any())
            {
                resultsRaw.Entities.ToList().ForEach(p =>
                    entities.Add(
                        new DerivedUploadData()
                        {
                            DataSource = DBSource.Intermediate,
                            LDI_ID = p.LDI_ID,
                            ProjectName = p.ProjectName,
                            ServiceName = p.ServiceName,
                            PeriodEnding = p.PeriodEnding,
                            OperatorOwner = p.OperatorOwner,
                            FinancialStatementType = p.FinancialStatementType,
                            UnitsInFacility = p.UnitsInFacility,
                            MonthsInPeriod = p.MonthsInPeriod,
                            FHANumber = p.FHANumber,
                            ResidentPatientDays = p.ResidentPatientDays,
                            TotalBedsoperated = p.TotalBedsoperated,
                            OperatingCash = p.OperatingCash,
                            Investments = p.Investments,
                            ReserveForReplacementEscrowBalance = p.ReserveForReplacementEscrowBalance,
                            AccountsReceivable = p.AccountsReceivable,
                            CurrentAssets = p.CurrentAssets,
                            CurrentLiabilities = p.CurrentLiabilities,
                            TotalRevenues = p.TotalRevenues,
                            RentLeaseExpense = p.RentLeaseExpense,
                            DepreciationExpense = p.DepreciationExpense,
                            AmortizationExpense = p.AmortizationExpense,
                            TotalExpenses = p.TotalExpenses,
                            NetIncome = p.NetIncome,
                            FHAInsuredPrincipalPayment = p.FHAInsuredPrincipalPayment,
                            FHAInsuredInterestPayment = p.FHAInsuredInterestPayment,
                            MortgageInsurancePremium = p.MortgageInsurancePremium,
                            ReserveForReplacementDeposit = p.ReserveForReplacementDeposit,
                            DebtCoverageRatio =
                                p.HasCalculated == false
                                    ? p.DerivedFinDict[DerivedFinancial.DebtCoverageRatio]
                                    : p.DebtCoverageRatio, // p.DebtCoverageRatio,
                            WorkingCapital =
                                p.HasCalculated == false
                                    ? p.DerivedFinDict[DerivedFinancial.WorkingCapital]
                                    : p.WorkingCapital, //p.WorkingCapital,
                            DaysCashOnHand =
                                p.HasCalculated == false
                                    ? p.DerivedFinDict[DerivedFinancial.DaysCashOnHand]
                                    : p.DaysCashOnHand, //p.DaysCashOnHand,
                            DaysInAcctReceivable =
                                p.HasCalculated == false
                                    ? p.DerivedFinDict[DerivedFinancial.DaysInAccountReceivable]
                                    : p.DaysInAcctReceivable, //p.DaysInAcctReceivable,
                            AvgPaymentPeriod =
                                p.HasCalculated == false
                                    ? p.DerivedFinDict[DerivedFinancial.AvgPaymentPeriod]
                                    : p.AvgPaymentPeriod, // p.AvgPaymentPeriod
                            ReserveForReplacementBalancePerUnit =
                                p.HasCalculated == false
                                    ? p.DerivedFinDict[DerivedFinancial.ReserveForReplaceBalUnit]
                                    : p.ReserveForReplacementBalancePerUnit,
                            DebtCoverageRatioScore =
                                p.HasCalculated == false
                                    ? p.DerivedScoreDict[DerivedFinancial.DebtCoverageRatio]
                                    : p.DebtCoverageRatioScore,
                            WorkingCapitalScore =
                                p.HasCalculated == false
                                    ? p.DerivedScoreDict[DerivedFinancial.WorkingCapital]
                                    : p.WorkingCapitalScore,
                            DaysCashOnHandScore =
                                p.HasCalculated == false
                                    ? p.DerivedScoreDict[DerivedFinancial.DaysCashOnHand]
                                    : p.DaysCashOnHandScore,
                            DaysInAcctReceivableScore =
                                p.HasCalculated == false
                                    ? p.DerivedScoreDict[DerivedFinancial.DaysInAccountReceivable]
                                    : p.DaysCashOnHandScore,
                            AvgPaymentPeriodScore =
                                p.HasCalculated == false
                                    ? p.DerivedScoreDict[DerivedFinancial.AvgPaymentPeriod]
                                    : p.AvgPaymentPeriodScore,
                            ScoreTotal = p.HasCalculated == false ? p.ScoreSum : p.ScoreTotal,
                            HasComment = p.HasComment
                        }));
            }
            results.Entities = entities;
            results.PageSize = resultsRaw.PageSize;
            return results;
        }

        public IEnumerable<DerivedUploadData> GetUploadDataByTime(DateTime myTimeInserted)
        {
            return dataUploadIntermediateRepository.GetUploadedDataByTime(TimezoneManager.GetUtcTimeFromPreferred(myTimeInserted));
        }


        public IEnumerable<DerivedUploadData> GetUploadDataByTimeForSearch(DateTime myTimeInserted)
        {
            return dataUploadIntermediateRepository.GetUploadedDataByTimeForSearch(TimezoneManager.GetUtcTimeFromPreferred(myTimeInserted),commentViewRepository);
        }

        public PaginateSortModel<DerivedUploadData> GetUploadedDataByTimePageSort(DateTime myTimeInserted, int userId,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum)
        {
            return dataUploadIntermediateRepository.GetUploadedDataByTimePageSort(TimezoneManager.GetUtcTimeFromPreferred(myTimeInserted), userId,
                strSortBy, sortOrder, pageSize, pageNum, commentViewRepository);
        }

        public IEnumerable<DerivedUploadData> GetUploadDataByCategory(string category)
        {
            decimal minVal = decimal.MinValue;
            decimal maxVal = decimal.MaxValue;
            // to do: change to enum
            switch (category)
            {
                case "Low Risk":
                    minVal = 0;
                    maxVal = 10;
                    break;
                case "Medium Risk":
                    minVal = 10;
                    maxVal = 20;
                    break;
                case "High Risk":
                    minVal = 20;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("{0} is out of expected range", category));
            }
            //return dataUploadIntermediateRepository.GetUploadedDataByScoreRange(minVal, maxVal);
            return dataUploadLiveRepository.GetUploadedDataByScoreRange(minVal, maxVal);
        }

        public PaginateSortModel<DerivedUploadData> GetUploadDataByCategoryForProjectReport(string category)
        {
            decimal minVal = decimal.MinValue;
            decimal maxVal = decimal.MaxValue;
            // to do: change to enum
            switch (category)
            {
                case "Low Risk":
                    minVal = 0;
                    maxVal = 10;
                    break;
                case "Medium Risk":
                    minVal = 10;
                    maxVal = 20;
                    break;
                case "High Risk":
                    minVal = 20;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("{0} is out of expected range", category));                    
            }
            //return dataUploadIntermediateRepository.GetUploadedDataByScoreRange(minVal, maxVal);
            return dataUploadLiveRepository.GetUploadedDataByScoreRangeForProjectReport(minVal, maxVal);
        }

        public IEnumerable<DerivedUploadData> GetUploadDataByCategoryAndQuarter(string category, string quarter)
        {
            decimal minVal = decimal.MinValue;
            decimal maxVal = decimal.MaxValue;
            // to do: change to enum
            switch (category)
            {
                case "Low Risk":
                    minVal = 0;
                    maxVal = 10;
                    break;
                case "Medium Risk":
                    minVal = 10;
                    maxVal = 20;
                    break;
                case "High Risk":
                    minVal = 20;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("{0} is out of expected range", category));
            }
            //return dataUploadIntermediateRepository.GetUploadedDataByScoreRange(minVal, maxVal);
            var result = quarter.Split(new[] { '/' });
            return dataUploadLiveRepository.GetUploadedDataByScoreRangeAndQuarter(minVal, maxVal, int.Parse(result[0]),int.Parse(result[1]),lenderFhaRepository);
        }

        public IEnumerable<DerivedUploadData> GetUploadDataByCategoryAndQuarterBySearch(string category,string quarter)
        {
            decimal minVal = decimal.MinValue;
            decimal maxVal = decimal.MaxValue;
            // to do: change to enum
            switch (category)
            {
                case "Low Risk":
                    minVal = 0;
                    maxVal = 10;
                    break;
                case "Medium Risk":
                    minVal = 10;
                    maxVal = 20;
                    break;
                case "High Risk":
                    minVal = 20;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("{0} is out of expected range", category));                    
            }
            //return dataUploadIntermediateRepository.GetUploadedDataByScoreRange(minVal, maxVal);
            var result = quarter.Split(new[] { '/' });
            return dataUploadLiveRepository.GetUploadedDataByScoreRangeAndQuarterBySearch(minVal, maxVal,commentRepository,int.Parse(result[0]),int.Parse(result[1]));
        }

        public PaginateSortModel<DerivedUploadData> GetUploadLiveDataByCategoryPageSort(string category, string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, int monthsInPeriod, int year)
        {
            
            int minVal = Int32.MinValue;
            int maxVal = Int32.MaxValue;
            // to do: change to enum
            switch (category)
            {
                case "Low Risk":
                    minVal = 0;
                    maxVal = 10;
                    break;
                case "Medium Risk":
                    minVal = 10;
                    maxVal = 20;
                    break;
                case "High Risk":
                    minVal = 20;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("{0} is out of expected range", category));
            }
            
            var result = dataUploadLiveRepository.GetUploadedDataByScoreRangeWithPageSort(minVal, maxVal,
                strSortBy, sortOrder, pageSize, pageNum, commentRepository, lenderFhaRepository, monthsInPeriod,year);
            IQueryable<DerivedUploadData> query =  result.Entities.AsQueryable();
            return PaginateSort.SortAndPaginate(query, strSortBy, sortOrder, pageSize, pageNum);
        }

        public PaginateSortModel<DerivedUploadData> GetUploadLiveDataByCategoryPageSort(string category, string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, string quarter)
        {
            DateTime selectedQtr = Convert.ToDateTime(quarter);
            int quarterNumber = (selectedQtr.Month - 1) / 3 + 1;
            decimal minVal = decimal.MinValue;
            decimal maxVal = decimal.MaxValue;
            // to do: change to enum
            switch (category)
            {
                case "Low Risk":
                    minVal = 0;
                    maxVal = 10;
                    break;
                case "Medium Risk":
                    minVal = 10;
                    maxVal = 20;
                    break;
                case "High Risk":
                    minVal = 20;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("{0} is out of expected range", category));
            }
            var result = dataUploadLiveRepository.GetUploadedDataByScoreRangeWithPageSort(minVal, maxVal,
                strSortBy, sortOrder, pageSize, pageNum, commentRepository, selectedQtr);
            return result;
        }

        public void CalcAndUpdateUploadedData()
        {
            System.Diagnostics.Debug.WriteLine(string.Format("Start load uncalculated intermediate at: {0}", DateTime.Now.ToLongTimeString()));
            var uncalculated = dataUploadIntermediateRepository.GetUnCalculated();
            System.Diagnostics.Debug.WriteLine(string.Format("End load uncalculated intermediate at: {0}", DateTime.Now.ToLongTimeString()));

            // you can apply different set of score algorithm with new IScoreManager and IScoreAgent
            // ScoreAgentV2, effective 09/04/2014
            // e.g. scoreManager = new ScoreManagerV1(new ScoreAgentV1)
            //scoreManager = new ScoreManager(new ScoreAgent());
            scoreManager = new ScoreManagerV3(new ScoreAgent());
            List<ExcelUploadView_Model> dataToUpdate = new List<Model.ExcelUploadView_Model>();
            System.Diagnostics.Debug.WriteLine(string.Format("Start to calculate ratio loop at: {0}", DateTime.Now.ToLongTimeString()));
            foreach (var item in uncalculated)
            {
                scoreManager.GetScores(item);
                item.DebtCoverageRatio = item.DerivedFinDict[DerivedFinancial.DebtCoverageRatio];
                item.DebtCoverageRatioScore = item.DerivedScoreDict[DerivedFinancial.DebtCoverageRatio];
                item.WorkingCapital = item.DerivedFinDict[DerivedFinancial.WorkingCapital];
                item.WorkingCapitalScore = item.DerivedScoreDict[DerivedFinancial.WorkingCapital];
                item.DaysCashOnHand = item.DerivedFinDict[DerivedFinancial.DaysCashOnHand];
                item.DaysCashOnHandScore = item.DerivedScoreDict[DerivedFinancial.DaysCashOnHand];
                item.DaysInAcctReceivable = item.DerivedFinDict[DerivedFinancial.DaysInAccountReceivable];
                item.DaysInAcctReceivableScore = item.DerivedScoreDict[DerivedFinancial.DaysInAccountReceivable];
                item.AvgPaymentPeriod = item.DerivedFinDict[DerivedFinancial.AvgPaymentPeriod];
                item.AvgPaymentPeriodScore = item.DerivedScoreDict[DerivedFinancial.AvgPaymentPeriod];
                item.ReserveForReplacementBalancePerUnit = item.DerivedFinDict[DerivedFinancial.ReserveForReplaceBalUnit];
                item.ScoreTotal = item.ScoreSum;
                item.HasCalculated = true;
                dataToUpdate.Add(item);
            }
            System.Diagnostics.Debug.WriteLine(string.Format("End calculate ratio loop at: {0}", DateTime.Now.ToLongTimeString()));
            dataUploadIntermediateRepository.UpdateData(dataToUpdate);
            unitOfWorkIntermediate.Save();
            System.Diagnostics.Debug.WriteLine(string.Format("End save calculated intermediate at: {0}", DateTime.Now.ToLongTimeString()));  
        }

        public IEnumerable<ExcelUploadView_Model> GetUploadedDataByQuarter(int monthsInPeriod, int year)
        {
            var uploadedData = dataUploadLiveRepository.GetUploadedData(monthsInPeriod,year);
            uploadedData = uploadedData.ToList();//.Where((p => int.Parse(p.MonthsInPeriod) == monthsInPeriod && Convert.ToDateTime(p.PeriodEnding).Date.Year == year));
            return uploadedData;
        }

        public IEnumerable<UploadStatusViewModel> GetUploadStatusByLenderId(int lenderId)
        {
            return uploadStatusRepository.GetUploadStatusByLenderId(lenderId);
        }

        public IEnumerable<UploadStatusDetailModel> GetUploadStatusDetailByLenderId(int lenderId, string selectedQtr)
        {
            if (!string.IsNullOrEmpty(selectedQtr))
            {
                var result = selectedQtr.Split(new[] { '/' });
                var username = UserPrincipal.Current.UserName;
                var usertype = GetUsertypeBasedOnRole(username);
                return uploadStatusDetailRepository.GetUploadStatusDetailByLenderId(username, usertype,
                    lenderId, int.Parse(result[0]), int.Parse(result[1]));
            }
           return null;
        }

        private static string GetUsertypeBasedOnRole(string username)
        {
            if (RoleManager.IsSuperUserRole(username))
            {
                return EnumType.EnumToValue(EnumType.Parse<HUDRole>("1"));
            }
            if (RoleManager.IsWorkloadManagerRole(username))
            {
                return EnumType.EnumToValue(EnumType.Parse<HUDRole>("4"));
            }
            if (RoleManager.IsAccountExecutiveRole(username))
            {
                return EnumType.EnumToValue(EnumType.Parse<HUDRole>("3"));
            }
            return EnumType.EnumToValue(EnumType.Parse<HUDRole>("0")); ;
        }

        public string GetErrorMsgForEmptyFields(int rowNumber, string columnName)
        {
            string errorMsg = "Check the value for '" + columnName + "' at row# " + rowNumber + ".";
            return errorMsg;
        }

        public string GetErrorMsgForInvalidFields(int rowNumber, string columnName, string dataType)
        {
            string errorMsg = "The '" + columnName + "' column accepts only  " + dataType + ".  Please correct the upload file and repeat the upload process.  Row#: " + rowNumber + ".";
            return errorMsg;
        }

        public List<string> GetCurrentQuarters()
        {
            var quarters = new List<string>();
            var latestQtr = Convert.ToDateTime(DateHelper.GetCurrentQuarter());
            var month = latestQtr.Month;
            var year = latestQtr.Year;
            var q1 = EnumType.GetEnumDescription(EnumType.Parse<FinancialQuarter>("1"));
            var q2 = EnumType.GetEnumDescription(EnumType.Parse<FinancialQuarter>("2"));
            var q3 = EnumType.GetEnumDescription(EnumType.Parse<FinancialQuarter>("3"));
            var q4 = EnumType.GetEnumDescription(EnumType.Parse<FinancialQuarter>("4"));
            if(month <= 3)
            {
                quarters.Add(q4 + "/" + Convert.ToString(year - 1));
                quarters.Add(q3 + "/" + Convert.ToString(year - 1));
                quarters.Add(q2 + "/" + Convert.ToString(year - 1));
                quarters.Add(q1+"/"+Convert.ToString(year-1));
            }
            else if(month <= 6)
            {
                quarters.Add(q1 + "/" + Convert.ToString(year));
                quarters.Add(q4 + "/" + Convert.ToString(year - 1));
                quarters.Add(q3 + "/" + Convert.ToString(year - 1));
                quarters.Add(q2 + "/" + Convert.ToString(year - 1));
            }
            else if(month <= 9)
            {
                quarters.Add(q2 + "/" + Convert.ToString(year));
                quarters.Add(q1 + "/" + Convert.ToString(year));
                quarters.Add(q4 + "/" + Convert.ToString(year - 1));
                quarters.Add(q3 + "/" + Convert.ToString(year - 1));
            }
            else if (month <= 12)
            {
                quarters.Add(q3 + "/" + Convert.ToString(year));
                quarters.Add(q2 + "/" + Convert.ToString(year));
                quarters.Add(q1 + "/" + Convert.ToString(year));
                quarters.Add(q4 + "/" + Convert.ToString(year - 1));
            }

            return quarters;

        }

      
        public IEnumerable<UploadStatusViewModel> GetUploadStatusDetailByQuarter(string quarter, int lenderId)
        {
            var username = UserPrincipal.Current.UserName;
            var usertype = GetUsertypeBasedOnRole(username);
            
            if (quarter != null)
            {
                var result = quarter.Split(new[] {'/'});
                var uploadStatusReportResults = uploadStatusRepository.GetUploadStatusDetailByQuarter(username,usertype,lenderId,
                    int.Parse(result[0]),
                    int.Parse(result[1])).OrderByDescending(p => p.Received);
                if (uploadStatusReportResults.Any())
                {
                    return uploadStatusReportResults;
                }
                return uploadStatusRepository.GetUploadStatusDetailByQuarter(username,usertype,lenderId, int.Parse(result[0]),
                    int.Parse(result[1])).OrderBy(p => p.LenderName);
            }
            return uploadStatusRepository.GetUploadStatusDetailByQuarter(username,usertype,lenderId, 0, 0).OrderBy(p => p.LenderName);
        }

        public IEnumerable<FhaInfoModel> GetFhasByLenderIds(string lenderIds)
        {
            return fhaRepository.GetFhasByLenderIds(lenderIds);
        }

        public void UpdateLenderPropertyInfo(int userId, int lenderId)
        {
            dataUploadLiveRepository.UpdateLenderPropertyInfo(userId, lenderId);
        }

        public int SaveFormUploadToIntermediateTbl(FormUploadModel model)
        {
            int ldiID = dataUploadIntermediateRepository.SaveFormUploadToIntermediateTbl(model);
            unitOfWorkIntermediate.Save();
            return ldiID;
        }

        public IEnumerable<ReportViewModel> GetMissingProjectsForLender(int lenderId, string selectedQtr)
        {
            if (!string.IsNullOrEmpty(selectedQtr))
            {
                var result = selectedQtr.Split(new[] { '/' });
                var username = UserPrincipal.Current.UserName;
                var usertype = GetUsertypeBasedOnRole(username);
                return dataUploadIntermediateRepository.GetMissingProjectsForLender(username, usertype, lenderId, int.Parse(result[0]), int.Parse(result[1]));
            }
            return null;
        }

        public IDictionary<string, string> GetQuartersList()
        {
            Tuple<DateTime,int> tuple;
            var quarters = new Dictionary<string, string>();
            if (RoleManager.IsCurrentUserLenderRoles())
            {
                var iLenderId = UserPrincipal.Current.LenderId;
                if ((UserPrincipal.Current.Roles.Contains("Lender") || RoleManager.IsCurrentUserLenderRoles()) &&
                    !iLenderId.HasValue)
                {
                    throw new InvalidOperationException(
                        string.Format(
                            "User id: {0} is a lender, but does not have corresponding lender id in UserLender table",
                            UserPrincipal.Current.UserId));
                }
                tuple = dataUploadIntermediateRepository.GetLatestPeriodEnding(iLenderId);
            }
            else
            {
                tuple = dataUploadIntermediateRepository.GetLatestPeriodEnding(null);
            }
            if (tuple == null) return quarters;
            var year = tuple.Item1.Year;
            var monthsInPeriod = tuple.Item2;
           
            if (monthsInPeriod == 3)
            {
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("12")), year - 1),"12/"+(year-1));
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("9")), year - 1), "9/" + (year - 1));
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("6")), year - 1), "6/" + (year - 1));
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("3")), year - 1), "3/" + (year - 1));
                
            }
            else if (monthsInPeriod == 6)
            {
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("3")), year), "3/"+year);
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("12")), year - 1), "12/" + (year - 1));
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("9")), year - 1), "9/" + (year - 1));
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("6")), year - 1), "6/" + (year - 1));
            }
            else if (monthsInPeriod == 9)
            {
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("6")), year), "6/" + year);
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("3")), year), "3/" + year);
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("12")), year - 1), "12/" + (year - 1));
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("9")), year - 1), "9/" + (year - 1));
            }
            else if (monthsInPeriod == 12)
            {
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("9")), year), "9/" + year);
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("6")), year), "6/" + year);
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("3")), year), "3/" + year);
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("12")), year - 1), "12/" + (year - 1));
            }

            return quarters;
        }

        public string GetLatestQuarter(string type)
        {
            Tuple<DateTime, int> tuple;
            if (RoleManager.IsCurrentUserLenderRoles())
            {
                var iLenderId = UserPrincipal.Current.LenderId;
                if ((UserPrincipal.Current.Roles.Contains("Lender") || RoleManager.IsCurrentUserLenderRoles()) &&
                    !iLenderId.HasValue)
                {
                    throw new InvalidOperationException(
                        string.Format(
                            "User id: {0} is a lender, but does not have corresponding lender id in UserLender table",
                            UserPrincipal.Current.UserId));
                }
                tuple = dataUploadIntermediateRepository.GetLatestPeriodEnding(iLenderId);
            }
            else
            {
                tuple = dataUploadIntermediateRepository.GetLatestPeriodEnding(null);
            }
            if (tuple != null)
            {
                var year = tuple.Item1.Year;
                var monthsInPeriod = tuple.Item2;
                var quarter = Enum.GetName(typeof(ReportingPeriod), monthsInPeriod);
                if (string.Equals(type,"key"))
                {
                    return quarter + " " + year;
                }
                if (string.Equals(type,"value"))
                {
                    return monthsInPeriod + "/" + year;
                }
            }
            return null;
        }
    }
}
