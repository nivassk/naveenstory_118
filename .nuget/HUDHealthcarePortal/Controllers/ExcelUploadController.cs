﻿using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using BusinessService.Interfaces;
using Core;
using Elmah;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Data.Common;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Filters;
using SpreadsheetGear;
using HUDHealthcarePortal;

namespace HUDHealthCarePortal.Controllers
{
    [Authorize]
    public class ExcelUploadController : Controller
    {
        private readonly bool _isAeEmailAlertEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsAEEmailAlertEnabled"]);
        private IUploadDataManager uploadDataMgr;
        private IEmailManager emailMgr;
        private IBackgroundJobMgr backgroundJobMgr;
        private static object syncRoot = new object();
        public bool agreed = false;
        private BizRuleManager bizRuleMgr = new BizRuleManager();

        // userid to rows copied dictionary
        private static IDictionary<int, long> ProcessStatus { get; set; }

        public ExcelUploadController() : this(new UploadDataManager(), new EmailManager(), new BackgroundJobMgr())
        {

        }

        public ExcelUploadController(IUploadDataManager uploadDataManager, IEmailManager emailManager,
            IBackgroundJobMgr backgroundMgr)
        {
            uploadDataMgr = uploadDataManager;
            emailMgr = emailManager;
            backgroundJobMgr = backgroundMgr;
            if (ProcessStatus == null)
            {
                ProcessStatus = new Dictionary<int, long>();
            }
        }

        /// <summary>
        /// Shows the agreement.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AccessDeniedAuthorize(
            Roles =
                "BackupAccountManager,HUDAdmin,AccountExecutive,Attorney,HUDDirector,WorkflowManager,LenderAccountManager,LenderAccountRepresentative,Administrator,SuperUser"
            )]
        public ActionResult Batch_File_Upload_Template()
        {
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "Batch_File_Upload_Template.xlsx");
            Response.WriteFile(Server.MapPath("../Content/AppFiles/Batch_File_Upload_Template.xlsx"));
            Response.End();
            return View();
        }

        [HttpGet]
        [AccessDeniedAuthorize(
            Roles =
                "BackupAccountManager,HUDAdmin,AccountExecutive,Attorney,HUDDirector,WorkflowManager,LenderAccountManager,LenderAccountRepresentative,Administrator,SuperUser"
                //"AccountExecutive,Administrator,Attorney,BackupAccountManager,HUDAdmin,HUDDirector,LenderAccountRepresentative,LenderAccountManager,LenderAdmin,Servicer,SuperUser,WorkflowManager"
            )]
        public ActionResult ExcelUploadView()
        {
            PopupHelper.ConfigPopup(TempData, 720, 300, "Disclaimer", false, false, true, false, false, true,
                "../ExcelUpload/PreUploadConfirm");
            if (TempData["PasswordExpires"] != null && (bool) TempData["PasswordExpires"])
                PopupHelper.ConfigPopup(TempData, 640, 340, "Change Password", false, false, true, false, false, true,
                    "../Account/ChangePassword");
            return View();
        }

        /// <summary>
        /// to make breadcrumb display properly on second level
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (RoleManager.IsCurrentOperatorAccountRepresentative())
                return RedirectToAction("Index", "FormUpload");
            return RedirectToAction("ExcelUploadView");
        }


        [HttpPost]
        public ActionResult PreUploadConfirm()
        {
            return PartialView("PreUploadConfirm");
        }

        [HttpPost]
        public ActionResult Importexcel()
        {
            OleDbConnection excelConnection = null;
            SqlConnection sqlConnection = null;
            Session["Confirmation"] = null;

            if (Request.Files["FileUpload1"].ContentLength > 0)
            {
                try
                {
                    lock (syncRoot)
                    {
                        string extension = Path.GetExtension(Request.Files["FileUpload1"].FileName);
                        //Debug.WriteLine(string.Format("Start to write to local file at: {0}",
                        //    DateTime.Now.ToLongTimeString()));
                        if ((extension == ".xlsx") || (extension == ".xls"))
                        {
                            string sPathUploadingFileLocation = Request.Files["FileUpload1"].FileName;
                            string sFileName = Path.GetFileName(Request.Files["FileUpload1"].FileName);
                            string sPathStoring = ConfigurationManager.AppSettings["UploadExcelBackup"];
                            sPathStoring = sPathStoring + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" +
                                           UserPrincipal.Current.UserId + "_" + sFileName;

                            if (!ProcessStatus.ContainsKey(UserPrincipal.Current.UserId))
                                ProcessStatus.Add(UserPrincipal.Current.UserId, 0);
                            else
                                ProcessStatus[UserPrincipal.Current.UserId] = 0;

                            long iLenderID = 0;
                            int iUserID;

                            if (System.IO.File.Exists(sPathStoring))
                                System.IO.File.Delete(sPathStoring);

                            //
                            ////////////////////////////////////////Initial Excel Upload///////////////////////////////////////
                            //

                            Request.Files["FileUpload1"].SaveAs(sPathStoring);
                            //Debug.WriteLine(string.Format("End write to local file at: {0}",
                            //    DateTime.Now.ToLongTimeString()));

                            string sqlConnectionString =
                                ConfigurationManager.ConnectionStrings[WebUiConstants.IntermediateConnectionName]
                                    .ConnectionString;
                            sqlConnection = new SqlConnection(sqlConnectionString);

                            //Create connection string to Excel work book
                            //string excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + sPathUploadingFileLocation + ";Extended Properties=Excel 12.0;Persist Security Info=True";
                            string excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                                           sPathStoring +
                                                           ";Extended Properties=Excel 12.0;Persist Security Info=True";

                            //Create Connection to Excel work book
                            excelConnection = new OleDbConnection(excelConnectionString);

                            //Create OleDbCommand to fetch data from Excel
                            //OleDbCommand cmd = new OleDbCommand("Select [Project Name],[Servicer Name],[Lender ID],[Period Ending],[Months In Period],[FHA Number],[Resident Patient Days],[Total beds operated],[Operating Cash],[Investments],[Reserve for Replacement Escrow Balance],[Accounts Receivable],[Current Assets],[Current Liabilities],[Total Revenues],[Rent/Lease Expense],[Depreciation Expense],[Amortization Expense],[Total Expenses],[Net Income],[FHA Insured Mortgage Principal Payment],[FHA Insured Mortgage Interest Expense],[Mortgage Insurance Premium (MIP)] from [Sheet1$]", excelConnection);
                            string templateWorksheetName = "Batch File Upload Template";
                            excelConnection.Open();

                            DataTable dbSchema = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            if (dbSchema == null || dbSchema.Rows.Count < 1)
                            {
                                Session["Confirmation"] = "Error: Could not find data in the spreadsheet.  Please make sure the file you are using is from the template at the link to Batch File Upload Template.";
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            string compareWorksheetName = dbSchema.Rows[0]["TABLE_NAME"].ToString();
                            //tool will add single quotes if the sheet name has spaces.
                            string singlequote = templateWorksheetName.IndexOf(" ",0) >= 0 ? "'" : "";
                            var myRowsList = dbSchema.Rows;
                            if (!(from row in dbSchema.AsEnumerable() select (row["TABLE_NAME"])).ToList().Contains(singlequote + templateWorksheetName + "$" + singlequote))
                            {
                                Session["Confirmation"] =
                                    "The file does not have the expected worksheet tab name: \"" + templateWorksheetName + "\". Please verify with the template from the link on this page.";
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            OleDbCommand cmd =
                                    new OleDbCommand(
                                        "Select [FHA Number],[Servicer ID],[Operator Owner],[Period Ending],[Months In Period],[Financial Statement Type],[Units in Facility],[Cash],[Investments],[Reserve for Replacement Escrow Balance],[Net Accounts Receivable],[Current Assets],[Current Liabilities],[Total Revenues],[Rent/Lease Expense],[Depreciation Expense],[Amortization Expense],[Total Expenses],[Net Income],[Reserve for Replacement Deposit],[FHA Insured Principal Payment],[FHA Insured Interest Payment],[FHA Mortgage Insurance Premium] from [" + templateWorksheetName + "$]",
                                        excelConnection);

                            OleDbDataAdapter adp = new OleDbDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            DataTable dtWithData = new DataTable();
                            adp.SelectCommand = cmd;
                            adp.FillSchema(dtWithData, SchemaType.Source);
                            adp.Fill(dtWithData);

                            // validate fha number/Servicer ID pair, if conflict with db data, stop uploading with error message
                            LenderFhaCol lenderFhaPairs = new LenderFhaCol();
                            int idx = 1;
                            foreach (DataRow row in dtWithData.Rows)
                            {
                                idx++;
                                string sLenderIDEmptyNull = Convert.ToString(row["Servicer ID"]);
                                if (string.IsNullOrEmpty(sLenderIDEmptyNull))
                                {
                                    if (string.IsNullOrEmpty(Convert.ToString(row["FHA Number"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Operator Owner"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Period Ending"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Months In Period"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Financial Statement Type"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Units in Facility"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Cash"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Investments"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Reserve for Replacement Escrow Balance"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Net Accounts Receivable"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Current Assets"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Current Liabilities"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Total Revenues"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Rent/Lease Expense"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Depreciation Expense"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Amortization Expense"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Total Expenses"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Net Income"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["Reserve for Replacement Deposit"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["FHA Insured Principal Payment"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["FHA Insured Interest Payment"])) &&
                                        string.IsNullOrEmpty(Convert.ToString(row["FHA Mortgage Insurance Premium"])))
                                    {
                                        continue;
                                    }

                                    Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(idx,
                                        "Servicer ID");
                                    return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                }
                                int intoutput;
                                if (!int.TryParse(sLenderIDEmptyNull, out intoutput))
                                {
                                    Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(idx, "Servicer ID", "Number");
                                    return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                }

                                string sFHANumberEmptyNull = Convert.ToString(row["FHA Number"]);
                                if (string.IsNullOrEmpty(sFHANumberEmptyNull))
                                {
                                    Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(idx,
                                        "FHA Number");
                                    return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                }
                                if (!(new Regex(@"\d{3}-\d{5}")).Match(sFHANumberEmptyNull).Success)
                                {
                                    Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(idx, "FHA Number", "###-#####");
                                    return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                }

                                //if (string.IsNullOrEmpty(Convert.ToString(row["Servicer ID"])))
                                //    break;
                                lenderFhaPairs.LenderFhaPairs.Add(
                                    new LenderFhaPairModel(int.Parse(row["Servicer ID"].ToString()),
                                        (string) row["FHA Number"]));
                            }
                            string lenderFhaPairsXml = XmlHelper.Serialize(lenderFhaPairs, typeof (LenderFhaCol),
                                Encoding.Unicode);

                            DataTable tblErrFhas = new DataTable();
                            string sConn_Live_db =
                                ConfigurationManager.ConnectionStrings[WebUiConstants.LiveConnectionName].ConnectionString;
                            using (SqlConnection conn = new SqlConnection(sConn_Live_db))
                            using (SqlCommand cmdVal = new SqlCommand("usp_HCP_Validate_LenderFhaPair", conn))
                            {
                                cmdVal.CommandType = CommandType.StoredProcedure;

                                cmdVal.Parameters.Add(new SqlParameter("@LenderFhaPairs", SqlDbType.Xml));
                                cmdVal.Parameters["@LenderFhaPairs"].Value = lenderFhaPairsXml;

                                SqlDataAdapter da = new SqlDataAdapter(cmdVal);

                                da.Fill(tblErrFhas);
                            }
                            if (tblErrFhas.Rows.Count > 0)
                            {
                                List<string> violationFhas = new List<string>();
                                foreach (DataRow row in tblErrFhas.Rows)
                                {
                                    violationFhas.Add(row[0].ToString());
                                }
                                Session["Confirmation"] =
                                    string.Format("Following FHA Numbers don't match corresponding Lenders:{0}{1}",
                                        Environment.NewLine, TextUtils.CommaDelimitedText(violationFhas));
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }

                            dtWithData.Columns.Add("UserID");
                            dtWithData.Columns.Add("ModifiedOn");
                            dtWithData.Columns.Add("ModifiedBy");
                            dtWithData.Columns.Add("IsLate");
                            string sOperatorOwnerNull,
                                sInvestments,
                                sMonthsinPeriodNull,
                                sFinancialStatementTypeNull,
                                sUnitsInFacility,
                                sOperatingCash,
                                sReserveForReplacementEscrowBalance,
                                sAccountsReceivable;
                            string sCurrentAssets,
                                sCurrentLiabilities,
                                sTotalRevenues,
                                sRentLeaseExpense,
                                sDepreciationExpense,
                                sAmortizationExpense,
                                sTotalExpenses,
                                sNetIncome;
                            string sReserveForReplacementDeposit,
                                sFHAInsuredPrincipalPayment,
                                sFHAInsuredInterestPayment,
                                sMortgageInsurancePremium;

                            dt = dtWithData.Clone();

                            string sFHANumberEmpty;
                            string sLenderIDEmpty;
                            int i = 1;
                            Double doubleOutputTest = 0.0;
                            int intOutputTest = 0;
                            string currentField = "";
                            
                            foreach (DataRow dRowData in dtWithData.Rows)
                            {
                                sLenderIDEmpty = Convert.ToString(dRowData["Servicer ID"]);
                                if (string.IsNullOrEmpty(sLenderIDEmpty))
                                    continue;
                                sFHANumberEmpty = Convert.ToString(dRowData["FHA Number"]);
                                i++;

                                if (!string.IsNullOrEmpty(sFHANumberEmpty) && (!string.IsNullOrEmpty(sLenderIDEmpty)))
                                {
                                    currentField = "Operator Owner";
                                    sOperatorOwnerNull = Convert.ToString(dRowData[currentField]).Trim();
                                    if (string.IsNullOrEmpty(sOperatorOwnerNull))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (sOperatorOwnerNull != "Operator" && sOperatorOwnerNull != "Owner-Operator")
                                    {
                                        if (sOperatorOwnerNull.ToLower() == "owner-operator")
                                        {
                                            sOperatorOwnerNull = "Owner-Operator";
                                        }
                                        else if (sOperatorOwnerNull.ToLower() == "operator")
                                        {
                                            sOperatorOwnerNull = "Operator";
                                        }
                                        else
                                        {
                                            Session["Confirmation"] = currentField + " must be either Owner-Operator or Operator.";
                                            return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                        }
                                    }

                                    currentField = "Investments";
                                    sInvestments = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sInvestments))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sInvestments, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "Months in Period";
                                    sMonthsinPeriodNull = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sMonthsinPeriodNull))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!int.TryParse(sMonthsinPeriodNull, out intOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }


                                    currentField = "Financial Statement Type";
                                    sFinancialStatementTypeNull = Convert.ToString(dRowData[currentField]).Trim();
                                    if (string.IsNullOrEmpty(sFinancialStatementTypeNull))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (sFinancialStatementTypeNull != "Certified" && sFinancialStatementTypeNull != "Audit")
                                    {
                                        if (sFinancialStatementTypeNull.ToLower() == "certified")
                                        {
                                            sFinancialStatementTypeNull = "Certified";
                                        }
                                        else if (sFinancialStatementTypeNull.ToLower() == "audit")
                                        {
                                            sFinancialStatementTypeNull = "Audit";
                                        }
                                        else
                                        {
                                            Session["Confirmation"] = currentField + " must be either Certified or Audit.";
                                            return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                        }
                                    }

                                    currentField = "Units in Facility";
                                    sUnitsInFacility = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sUnitsInFacility))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!int.TryParse(sUnitsInFacility, out intOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "Cash";
                                    sOperatingCash = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sOperatingCash))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sOperatingCash, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "Reserve for Replacement Escrow Balance";
                                    sReserveForReplacementEscrowBalance =
                                        Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sReserveForReplacementEscrowBalance))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sReserveForReplacementEscrowBalance, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "Net Accounts Receivable";
                                    sAccountsReceivable = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sAccountsReceivable))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sAccountsReceivable, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "Current Assets";
                                    sCurrentAssets = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sCurrentAssets))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sCurrentAssets, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "Current Liabilities";
                                    sCurrentLiabilities = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sCurrentLiabilities))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sCurrentLiabilities, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "Total Revenues";
                                    sTotalRevenues = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sTotalRevenues))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sTotalRevenues, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "Rent/Lease Expense";
                                    sRentLeaseExpense = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sRentLeaseExpense))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sRentLeaseExpense, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "Depreciation Expense";
                                    sDepreciationExpense = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sDepreciationExpense))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sDepreciationExpense, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "Amortization Expense";
                                    sAmortizationExpense = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sAmortizationExpense))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sAmortizationExpense, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "Total Expenses";
                                    sTotalExpenses = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sTotalExpenses))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sTotalExpenses, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "Net Income";
                                    sNetIncome = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sNetIncome))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sNetIncome, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "Reserve for Replacement Deposit";
                                    sReserveForReplacementDeposit = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sReserveForReplacementDeposit))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sReserveForReplacementDeposit, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "FHA Insured Principal Payment";
                                    sFHAInsuredPrincipalPayment = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sFHAInsuredPrincipalPayment))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sFHAInsuredPrincipalPayment, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "FHA Insured Interest Payment";
                                    sFHAInsuredInterestPayment = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sFHAInsuredInterestPayment))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sFHAInsuredInterestPayment, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    currentField = "FHA Mortgage Insurance Premium";
                                    sMortgageInsurancePremium = Convert.ToString(dRowData[currentField]);
                                    if (string.IsNullOrEmpty(sMortgageInsurancePremium))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForEmptyFields(i,
                                            currentField);
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    if (!Double.TryParse(sMortgageInsurancePremium, out doubleOutputTest))
                                    {
                                        Session["Confirmation"] = uploadDataMgr.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    //Validating Period Ending in right format
                                    try
                                    {
                                        var dPeriodEnding = Convert.ToDateTime(dRowData["Period Ending"]);
                                    }
                                    catch (Exception)
                                    {
                                        Session["Confirmation"] = "Period Ending is not in correct format:MM/DD/YYYY or is not a valid date.";
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    dt.Rows.Add(Convert.ToString(dRowData["FHA Number"]),
                                        Convert.ToString(dRowData["Servicer ID"]),
                                        sOperatorOwnerNull,
                                        Convert.ToDateTime(dRowData["Period Ending"]),
                                        Convert.ToString(dRowData["Months In Period"]),
                                        sFinancialStatementTypeNull,
                                        sUnitsInFacility, sOperatingCash, sInvestments,
                                        sReserveForReplacementEscrowBalance, sAccountsReceivable, sCurrentAssets,
                                        sCurrentLiabilities, sTotalRevenues, sRentLeaseExpense, sDepreciationExpense,
                                        sAmortizationExpense, sTotalExpenses, sNetIncome, sReserveForReplacementDeposit,
                                        sFHAInsuredPrincipalPayment, sFHAInsuredInterestPayment,
                                        sMortgageInsurancePremium);
                                }
                                else
                                {
                                    break;
                                }
                            }

                            //Error message to verify that Excel has data.[
                            if (dt.Rows.Count > 0)
                            {
                                DateTime dPeriodEnding;
                                string sFHANumber;
                                int iDuplicateFHANumber = 0;
                                int iMonthIn = 0;
                                string sFinancialStatementType;
                                int iCertified = 0;
                                int iAudited = 0;
                                bool bFinancialStatementType = false;

                                DateTime dCurrentDate;
                                TimeSpan tDateDifference;

                                // for Operator account rep, restrict fha's allowed to upload
                                if (RoleManager.IsCurrentOperatorAccountRepresentative())
                                {
                                    var allowedFhas =
                                        uploadDataMgr.GetAllowedFhaByLarUserId(UserPrincipal.Current.UserId);
                                    List<string> fhasToUpload = new List<string>();
                                    foreach (DataRow dRow in dt.Rows)
                                    {
                                        fhasToUpload.Add(Convert.ToString(dRow["FHA Number"]));
                                    }
                                    var violatingFhas = fhasToUpload.Where(p => !allowedFhas.Contains(p));
                                    if (violatingFhas != null && violatingFhas.Count() > 0)
                                    {
                                        Session["Confirmation"] =
                                            string.Format(
                                                "You are not allowed to upload FHA's:{0} that are not included in allowed FHA's:{1}.",
                                                TextUtils.CommaDelimitedText(violatingFhas),
                                                TextUtils.CommaDelimitedText(allowedFhas));
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                }

                                foreach (DataRow dRow in dt.Rows)
                                {
                                    //Gets the Servicer ID
                                    iLenderID = Convert.ToInt64(dRow["Servicer ID"]);

                                    if (RoleManager.IsCurrentUserLenderRoles() &&
                                        UserPrincipal.Current.LenderId != iLenderID)
                                    {
                                        Session["Confirmation"] =
                                            "You are not allowed to upload file for another lender.";
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }

                                    dPeriodEnding = Convert.ToDateTime(dRow["Period Ending"]);
                                    sFHANumber = Convert.ToString(dRow["FHA Number"]);
                                    iMonthIn = Convert.ToInt32(dRow["Months In Period"]);

                                    //validating quarterly and year-end financial rules
                                    //validating quarterly and year-end financial rules
                                    bool isLateSubmit = false;
                                    var uploadStatusFlag =
                                        bizRuleMgr.GetUploadStatusFlag(dRow["Months In Period"].ToString(),
                                            dRow["Period Ending"].ToString());
                                    switch (uploadStatusFlag)
                                    {
                                        case UploadStatusFlag.BadData:
                                            Session["Confirmation"] = "Wrong Period Ending or Months in Period.";
                                            return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                            break;
                                        case UploadStatusFlag.Late:
                                            isLateSubmit = true;
                                            break;
                                    }
                                    dCurrentDate = DateTime.Now;
                                    tDateDifference = dCurrentDate - dPeriodEnding;
                                    // cannot enter future date in period ending
                                    if (tDateDifference < new TimeSpan(0, 0, 0))
                                    {
                                        Session["Confirmation"] = "Date must not be in the future.";
                                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                    }
                                    dRow["IsLate"] = isLateSubmit;

                                    foreach (DataRow dRowFHANumber in dt.Rows)
                                    {
                                        sFinancialStatementType =
                                            Convert.ToString(dRowFHANumber["Financial Statement Type"]);

                                        if ((sFHANumber == Convert.ToString(dRowFHANumber["FHA Number"])) &&
                                            (dPeriodEnding == Convert.ToDateTime(dRowFHANumber["Period Ending"])) &&
                                            (iMonthIn == Convert.ToInt32(dRowFHANumber["Months In Period"])))
                                        {
                                            //Certified
                                            if (sFinancialStatementType == "Certified")
                                            {
                                                iCertified++;
                                            }

                                            //Audited
                                            if (sFinancialStatementType == "Audit")
                                            {
                                                iAudited++;
                                            }

                                            if (iCertified > 1 || iAudited > 1)
                                            {
                                                bFinancialStatementType = true;
                                            }

                                            iDuplicateFHANumber++;
                                        }

                                        //Duplicate rows exists
                                        if ((iDuplicateFHANumber > 1) && (bFinancialStatementType))
                                        {
                                            Session["Confirmation"] =
                                                "Excel upload file contains duplicate FHA Number.";
                                            return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                        }
                                    }

                                    iDuplicateFHANumber = 0;
                                    iCertified = 0;
                                    iAudited = 0;

                                    dRow["UserID"] = UserPrincipal.Current.UserId;
                                    dRow["ModifiedOn"] = DateTime.UtcNow;
                                    dRow["ModifiedBy"] = UserPrincipal.Current.UserId;

                                }

                                sqlConnection.Open();

                                //Debug.WriteLine(string.Format("Start bulk copy prepare columns at: {0}",
                                //    DateTime.Now.ToLongTimeString()));

                                using (SqlBulkCopy sqlBulk = new SqlBulkCopy(sqlConnection))
                                {
                                    // Set up the column mappings by ordinal.
                                    //FHANumber
                                    SqlBulkCopyColumnMapping column0 =
                                        new SqlBulkCopyColumnMapping(0, 2);
                                    sqlBulk.ColumnMappings.Add(column0);

                                    //Servicer ID
                                    SqlBulkCopyColumnMapping column1 =
                                        new SqlBulkCopyColumnMapping(1, 3);
                                    sqlBulk.ColumnMappings.Add(column1);

                                    //Operator Owner
                                    SqlBulkCopyColumnMapping column2 =
                                        new SqlBulkCopyColumnMapping(2, 4);
                                    sqlBulk.ColumnMappings.Add(column2);

                                    //Period Ending
                                    SqlBulkCopyColumnMapping column3 =
                                        new SqlBulkCopyColumnMapping(3, 5);
                                    sqlBulk.ColumnMappings.Add(column3);

                                    //Months In Period
                                    SqlBulkCopyColumnMapping column4 =
                                        new SqlBulkCopyColumnMapping(4, 6);
                                    sqlBulk.ColumnMappings.Add(column4);

                                    //Financial Statement Type
                                    SqlBulkCopyColumnMapping column5 =
                                        new SqlBulkCopyColumnMapping(5, 7);
                                    sqlBulk.ColumnMappings.Add(column5);

                                    //Units in Facility
                                    SqlBulkCopyColumnMapping column6 =
                                        new SqlBulkCopyColumnMapping(6, 8);
                                    sqlBulk.ColumnMappings.Add(column6);

                                    //Cash
                                    SqlBulkCopyColumnMapping column7 =
                                        new SqlBulkCopyColumnMapping(7, 9);
                                    sqlBulk.ColumnMappings.Add(column7);

                                    //Investments
                                    SqlBulkCopyColumnMapping column8 =
                                        new SqlBulkCopyColumnMapping(8, 10);
                                    sqlBulk.ColumnMappings.Add(column8);

                                    //ReserveForReplacementEscrowBalance
                                    SqlBulkCopyColumnMapping column9 =
                                        new SqlBulkCopyColumnMapping(9, 11);
                                    sqlBulk.ColumnMappings.Add(column9);

                                    //Net Accounts Receivable
                                    SqlBulkCopyColumnMapping column10 =
                                        new SqlBulkCopyColumnMapping(10, 12);
                                    sqlBulk.ColumnMappings.Add(column10);

                                    //CurrentAssets
                                    SqlBulkCopyColumnMapping column11 =
                                        new SqlBulkCopyColumnMapping(11, 13);
                                    sqlBulk.ColumnMappings.Add(column11);

                                    //Current Liabilities
                                    SqlBulkCopyColumnMapping column12 =
                                        new SqlBulkCopyColumnMapping(12, 14);
                                    sqlBulk.ColumnMappings.Add(column12);

                                    //Total Revenues
                                    SqlBulkCopyColumnMapping column13 =
                                        new SqlBulkCopyColumnMapping(13, 15);
                                    sqlBulk.ColumnMappings.Add(column13);

                                    //Rent/Lease Expense
                                    SqlBulkCopyColumnMapping column14 =
                                        new SqlBulkCopyColumnMapping(14, 16);
                                    sqlBulk.ColumnMappings.Add(column14);

                                    //Depreciation Expense
                                    SqlBulkCopyColumnMapping column15 =
                                        new SqlBulkCopyColumnMapping(15, 17);
                                    sqlBulk.ColumnMappings.Add(column15);

                                    //Amortization Expense
                                    SqlBulkCopyColumnMapping column16 =
                                        new SqlBulkCopyColumnMapping(16, 18);
                                    sqlBulk.ColumnMappings.Add(column16);

                                    //Total Expenses
                                    SqlBulkCopyColumnMapping column17 =
                                        new SqlBulkCopyColumnMapping(17, 19);
                                    sqlBulk.ColumnMappings.Add(column17);

                                    //NetIncome
                                    SqlBulkCopyColumnMapping column18 =
                                        new SqlBulkCopyColumnMapping(18, 20);
                                    sqlBulk.ColumnMappings.Add(column18);

                                    //Reserve for Replacement Deposit
                                    SqlBulkCopyColumnMapping column19 =
                                        new SqlBulkCopyColumnMapping(19, 21);
                                    sqlBulk.ColumnMappings.Add(column19);

                                    //FHA Insured Principal Payment
                                    SqlBulkCopyColumnMapping column20 =
                                        new SqlBulkCopyColumnMapping(20, 22);
                                    sqlBulk.ColumnMappings.Add(column20);

                                    //FHA Insured Interest Payment
                                    SqlBulkCopyColumnMapping column21 =
                                        new SqlBulkCopyColumnMapping(21, 23);
                                    sqlBulk.ColumnMappings.Add(column21);

                                    //FHA Mortgage Insurance Premium
                                    SqlBulkCopyColumnMapping column22 =
                                        new SqlBulkCopyColumnMapping(22, 24);
                                    sqlBulk.ColumnMappings.Add(column22);

                                    SqlBulkCopyColumnMapping column23 =
                                        new SqlBulkCopyColumnMapping(23, 42);
                                    sqlBulk.ColumnMappings.Add(column23);

                                    SqlBulkCopyColumnMapping column24 =
                                        new SqlBulkCopyColumnMapping(24, 44);
                                    sqlBulk.ColumnMappings.Add(column24);

                                    SqlBulkCopyColumnMapping column25 =
                                        new SqlBulkCopyColumnMapping(25, 40);
                                    sqlBulk.ColumnMappings.Add(column25);

                                    SqlBulkCopyColumnMapping column26 =
                                        new SqlBulkCopyColumnMapping(26, 46);
                                    sqlBulk.ColumnMappings.Add(column26);

                                    //Give your Destination table name
                                    sqlBulk.DestinationTableName = "Lender_DataUpload_Intermediate";
                                    //Debug.WriteLine(string.Format("Start bulk copy: {0}",
                                    //    DateTime.Now.ToLongTimeString()));

                                    sqlBulk.WriteToServerAsync(dt);

                                    //Debug.WriteLine(string.Format("End bulk copy at: {0}",
                                    //    DateTime.Now.ToLongTimeString()));
                                }

                                //
                                ////////////////////////////////////////Initial Excel Upload///////////////////////////////////////
                                //                
                                string sConnection_HCP_Live_db =
                                    ConfigurationManager.ConnectionStrings[WebUiConstants.LiveConnectionName].ConnectionString;
                                SqlConnection conn = new SqlConnection(sConnection_HCP_Live_db);
                                SqlCommand command1 = new SqlCommand();
                                iUserID = UserPrincipal.Current.UserId;
                                conn.Open();

                                Debug.WriteLine(string.Format("Start fill in property id at: {0}",
                                    DateTime.Now.ToLongTimeString()));
                                command1.Connection = conn;
                                command1.CommandText = "usp_HCP_Verify_PropertyID_iRems";
                                command1.CommandType = CommandType.StoredProcedure;

                                // Add the input parameter and set its properties.
                                SqlParameter parameter = new SqlParameter();

                                //Servicer ID/Lender ID
                                parameter.ParameterName = "@LenderID";
                                parameter.SqlDbType = SqlDbType.Int;
                                parameter.Direction = ParameterDirection.Input;

                                //Add the parameter to the Parameters collection - User Name.
                                command1.Parameters.AddWithValue("LenderID", iLenderID);

                                //User ID
                                parameter.ParameterName = "@UserID";
                                parameter.SqlDbType = SqlDbType.Int;
                                parameter.Direction = ParameterDirection.Input;

                                // Add the parameter to the Parameters collection - Password.
                                command1.Parameters.AddWithValue("UserID", iUserID);
                                command1.ExecuteNonQuery();
                                Debug.WriteLine(string.Format("End fill in property id at: {0}",
                                    DateTime.Now.ToLongTimeString()));

                                Debug.WriteLine(string.Format("Start ratio calculation at: {0}",
                                    DateTime.Now.ToLongTimeString()));

                                uploadDataMgr.CalcAndUpdateUploadedData();

                                excelConnection.Close();
                                Debug.WriteLine(string.Format("End ratio calculation: {0}",
                                    DateTime.Now.ToLongTimeString()));
                                Session["Confirmation"] = "File Uploaded Successfully (" + sFileName + ")";

                                Dictionary<string, string> dictfhaNumbers = new Dictionary<string, string>();
                                foreach (DataRow item in dt.Rows)
                                {
                                    // 0: fha number column
                                    var fha = item[0].ToString();
                                    if (!string.IsNullOrEmpty(fha) && !dictfhaNumbers.ContainsKey(fha))
                                        dictfhaNumbers.Add(fha, fha);
                                }

                                // use thread pool to finish sending email so not holding up UI
                                if (_isAeEmailAlertEnabled)
                                {
                                    backgroundJobMgr.SendUploadNotificationEmails(emailMgr, dictfhaNumbers.Keys);
                                }
                            }
                            else
                            {
                                Session["Confirmation"] =
                                    "Please make sure the excel file you are uploading has data in it.";
                            }
                        }
                        else
                        {
                            Session["Confirmation"] =
                                "The file is not in the expected format. Please make sure the file you are uploading is in excel format (.xls or .xlsx).";
                        }
                    }
                }
                catch (OleDbException dbEx)
                {
                    Session["Confirmation"] = "Excel file does not have correct format to upload, OleDbException: " +
                                              dbEx.Message;
                    ErrorSignal.FromCurrentContext().Raise(dbEx);
                }
                catch (Exception ex)
                {
                    Session["Confirmation"] =
                        "The file is not in the expected format. Please make sure the file you are uploading is in excel format (.xls or .xlsx).";
                    ErrorSignal.FromCurrentContext().Raise(ex);
                    //string errorMessage = ex.GetBaseException().Message;
                    //throw new InvalidOperationException("error while uploading", ex);
                    //RecreateIndexAfterInsert(sqlConnection);
                }
                finally
                {
                    if (excelConnection != null)
                    {
                        excelConnection.Close();
                        excelConnection.Dispose();
                    }
                    if (sqlConnection != null)
                    {
                        sqlConnection.Close();
                        sqlConnection.Dispose();
                    }
                    Debug.WriteLine(string.Format("Close all connections at: {0}", DateTime.Now.ToLongTimeString()));
                    //lock (syncRoot)
                    //{
                    //    if (ProcessStatus.ContainsKey(UserPrincipal.Current.UserId))
                    //        ProcessStatus.Remove(UserPrincipal.Current.UserId);
                    //}
                }
            }

            else
            {
                Session["Confirmation"] = "You must attach a file to upload, then click the SUBMIT button.";
            }

            return RedirectToAction("ExcelUploadView", "ExcelUpload");
        }

        //[HttpGet]
        public ContentResult GetCurrentProgress(string userId)
        {
            ControllerContext.HttpContext.Response.AddHeader("cache-control", "no-cache");
            //var currentProgress = longRunningClass.GetStatus(id).ToString();
            string currentProgress = "1";
            int UserId = int.Parse(userId);
            lock (syncRoot)
            {
                if (ProcessStatus.ContainsKey(UserId))
                    currentProgress = ProcessStatus[UserId].ToString();
            }
            return Content(currentProgress);
        }

        private void sqlBulk_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            lock (syncRoot)
            {
                if (ProcessStatus.ContainsKey(UserPrincipal.Current.UserId))
                    ProcessStatus[UserPrincipal.Current.UserId] = e.RowsCopied;
            }
        }

        private void DropIndexBeforeInsert(SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText =
                "ALTER INDEX [PK_Lender_DataUpload_Intermediate] ON [HCP_Intermediate_db].[dbo].[Lender_DataUpload_Intermediate] DISABLE";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;

            cmd.ExecuteNonQuery();
        }

        private void RecreateIndexAfterInsert(SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText =
                "ALTER INDEX [PK_Lender_DataUpload_Intermediate] ON [HCP_Intermediate_db].[dbo].[Lender_DataUpload_Intermediate] REBUILD";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;

            cmd.ExecuteNonQuery();
        }
    }
}