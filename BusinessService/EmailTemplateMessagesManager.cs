﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model;
using Repository;
using Repository.Interfaces;

namespace BusinessService
{
    public class EmailTemplateMessagesManager : IEmailTemplateMessagesManager
    {
        private readonly IEmailTemplateMessagesRepository _emailTemplateMessagesRepository;
        private readonly UnitOfWork unitOfWorkLive;

        public EmailTemplateMessagesManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            _emailTemplateMessagesRepository = new EmailTemplateMessagesRepository(unitOfWorkLive);
        }

        public EmailTemplateMessagesModel GetEmailTemplate(EmailType emailTypeID)
        {
            return _emailTemplateMessagesRepository.GetEmailTemplate(emailTypeID);
        }


        public EmailRecipientModel GetRecipentbyView(System.Guid Taskinstanceid, int viewid)
        {
            return _emailTemplateMessagesRepository.GetRecipentbyView(Taskinstanceid, viewid);
        }

        public System.Collections.Generic.IEnumerable<string> GetAeEmailbyInstanceid(System.Guid Taskinstanceid)
        {
            return _emailTemplateMessagesRepository.GetAeEmailbyInstanceid(Taskinstanceid);
        }
    }
}
