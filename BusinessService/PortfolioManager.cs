﻿using System.Linq;
using BusinessService;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using System.Collections.Generic;
using Repository.Interfaces;

namespace HUDHealthcarePortal.BusinessService
{
    public class PortfolioManager : IPortfolioManager
    {
        private readonly IPortfolioRepository portfolioRepository;

        public PortfolioManager()
        {
            UnitOfWork unitOfWorkLive = new UnitOfWork(DBSource.Live);
            portfolioRepository = new PortfolioRepository(unitOfWorkLive);
        }

        IEnumerable<DerivedUploadData> IPortfolioManager.GetPortfolios(string quarterToken)
        {
            var portfolios = portfolioRepository.GetPortfolios(quarterToken);
            // calculate non empty portfolio ratios
            var scoreManager = new ScoreManagerPortfolio(new ScoreAgent());
            var uploadDatas = portfolios as IList<DerivedUploadData> ?? portfolios.ToList();
            //foreach (var item in uploadDatas)
            //{
            //    if (item.IsPortfolioNoData)
            //        continue;

            //    scoreManager.GetScores(item);
            //    item.DebtCoverageRatio = item.DerivedFinDict[DerivedFinancial.DebtCoverageRatio];
            //    item.DebtCoverageRatioScore = item.DerivedScoreDict[DerivedFinancial.DebtCoverageRatio];
            //    item.WorkingCapital = item.DerivedFinDict[DerivedFinancial.WorkingCapital];
            //    item.WorkingCapitalScore = item.DerivedScoreDict[DerivedFinancial.WorkingCapital];
            //    item.DaysCashOnHand = item.DerivedFinDict[DerivedFinancial.DaysCashOnHand];
            //    item.DaysCashOnHandScore = item.DerivedScoreDict[DerivedFinancial.DaysCashOnHand];
            //    item.DaysInAcctReceivable = item.DerivedFinDict[DerivedFinancial.DaysInAccountReceivable];
            //    item.DaysInAcctReceivableScore = item.DerivedScoreDict[DerivedFinancial.DaysInAccountReceivable];
            //    item.AvgPaymentPeriod = item.DerivedFinDict[DerivedFinancial.AvgPaymentPeriod];
            //    item.AvgPaymentPeriodScore = item.DerivedScoreDict[DerivedFinancial.AvgPaymentPeriod];
            //    item.ReserveForReplacementBalancePerUnit = item.DerivedFinDict[DerivedFinancial.ReserveForReplaceBalUnit];
            //    item.ScoreTotal = item.ScoreSum;
            //    item.HasCalculated = true;
            //}
            return uploadDatas;
        }

        PaginateSortModel<DerivedUploadData> IPortfolioManager.GetPortfolios(int pageNum, int pageSize, string sort, SqlOrderByDirecton sortOrder, string quarterToken)
        {
            var portfolios = portfolioRepository.GetPortfolios(pageNum, pageSize, sort, sortOrder, quarterToken);
            // calculate non empty portfolio ratios
            var scoreManager = new ScoreManagerPortfolio(new ScoreAgent());
            //var uploadDatas = portfolios as IList<DerivedUploadData> ?? portfolios.ToList();
            //foreach (var item in uploadDatas)
            var derivedUploadDatas = portfolios as IList<DerivedUploadData> ?? portfolios.ToList();
            foreach (var item in derivedUploadDatas)
            {
                if (item.IsPortfolioNoData)
                    continue;

                scoreManager.GetScores(item);
                item.DebtCoverageRatio2 = item.DerivedFinDict[DerivedFinancial.DebtCoverageRatio2];
                item.AverageDailyRateRatio = item.DerivedFinDict[DerivedFinancial.AverageDailyRate];
                item.NOIRatio = item.DerivedFinDict[DerivedFinancial.NOI];
                item.HasCalculated = true;
            }
            return derivedUploadDatas.AsQueryable().SortAndPaginate(sort, sortOrder, pageSize, pageNum);
        }

        public PaginateSortModel<DerivedUploadData> GetPortfolioDetail(int pageNum, int pageSize, string sortBy, SqlOrderByDirecton sortOrder, string portfolioNumber, string quarterToken)
        {
            var results = portfolioRepository.GetPortfolioDetail(pageNum, pageSize, sortBy, sortOrder,portfolioNumber, quarterToken);
            return results.AsQueryable().SortAndPaginate(sortBy, sortOrder, pageSize, pageNum);
        }
    }
}
