﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using HUDHealthcarePortal.Repository;
using Model;
using Repository;
using Repository.Interfaces;
using System.Collections;
using System.Collections.Generic;
using System;
using HUDHealthcarePortal.Repository.AssetManagement;
using HUDHealthcarePortal.Repository.Interfaces.AssetManagement;

namespace BusinessService.AssetManagement
{
    public class NonCriticalRequestExtensionManager: INonCriticalRequestExtensionManager
    {
        private INonCriticalProjectInfoRepository projectInfoRepository;
        private INonCriticalRepairsRepository nonCriticalRepairsRepository;
        private INonCriticalRequestExtensionRepository nonCriticalRequestExtensionRepository;
        private UnitOfWork unitOfWorkLive;
        private LenderInfoRepository lenderInfoRepository;
        private IUserRepository userRepository;
        private ITaskRepository taskRepository;
        private IUserLenderServicerRepository userLenderServicerRepository;
        private ILenderFhaRepository lenderFhaRepository;

        public NonCriticalRequestExtensionManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            projectInfoRepository = new NonCriticalProjectInfoRepository(unitOfWorkLive);
            lenderInfoRepository = new LenderInfoRepository(unitOfWorkLive);
            nonCriticalRepairsRepository = new NonCriticalRepairsRepository(unitOfWorkLive);
            userRepository = new UserRepository(unitOfWorkLive);
            taskRepository = new TaskRepository();
            userLenderServicerRepository = new UserLenderServicerRepository(unitOfWorkLive);
            lenderFhaRepository = new LenderFhaRepository(unitOfWorkLive);
            nonCriticalRequestExtensionRepository = new NonCriticalRequestExtensionRepository(unitOfWorkLive);
        }
        public NonCriticalPropertyModel GetNonCriticalPropertyInfo(string fhaNumber)
        {
            return projectInfoRepository.GetNonCriticalPropertyInfo(fhaNumber);
        }

        public List<string> GetAllowedNonCriticalFhaByLenderUserId(int lenderUserId)
        {
           return  userLenderServicerRepository.GetAllowedNonCriticalFhaByLenderUserId(lenderUserId, lenderFhaRepository,
                projectInfoRepository);
        }
        public Guid SaveNonCriticalRequestExtension(NonCriticalRequestExtensionModel model)
        {
            var nonCriticalRequestExtensionId = nonCriticalRequestExtensionRepository.SaveNonCriticalRequestExtension(model);
            unitOfWorkLive.Save();
            return nonCriticalRequestExtensionId;
        }

        public void UpdateNonCriticalRequestExtension(NonCriticalRequestExtensionModel model)
        {
            nonCriticalRequestExtensionRepository.UpdateNonCriticalRequestExtension(model);
            unitOfWorkLive.Save();
        }

        public void UpdateTaskId(NonCriticalRequestExtensionModel model)
        {
            nonCriticalRequestExtensionRepository.UpdateTaskId(model);
            unitOfWorkLive.Save();
        }

        public TaskModel GetLatestTaskId(Guid taskInstanceId)
        {
            return taskRepository.GetLatestTaskByTaskInstanceId(taskInstanceId);
        }
   
    }
}