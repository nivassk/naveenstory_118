﻿using BusinessService.Interfaces;
using Core;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model;
using Repository.ManagementReport;

namespace BusinessService.ManagementReport
{
    public class MissingProjectReportManager : IMissingProjectReportManager
    {
        private MissingProjectReportRepository _missingProjectReportRepository;

        public MissingProjectReportManager()
        {
            //var unitOfWorkIntermediate = new UnitOfWork(DBSource.Intermediate);
            _missingProjectReportRepository = new MissingProjectReportRepository();
        }

        public ReportModel GetMissingProjectReport(HUDRole hudRole, string userName, int year)
        {
            var selectedYear = EnumType.GetEnumDescription(EnumType.Parse<ReportYear>(year.ToString())); 
            switch (hudRole)
            {
                case HUDRole.SuperUser:
                    return GetSuperUserMissingProjectReport(int.Parse(selectedYear));
                case HUDRole.WorkflowManager:
                    return GetWorkloadManagerMissingProjectReport(userName, int.Parse(selectedYear));
                case HUDRole.AccountExecutive:
                    return GetAccountExecutiveMissingProjectReport(userName, int.Parse(selectedYear));
                default:
                    return null;
            }
        }

        public ReportModel GetSuperUserMissingProjectReport(int year)
        {
            return _missingProjectReportRepository.GetMissingProjectReportForSuperUser(year);
        }

        public ReportModel GetSuperUserMissingProjectReportWithWLM(string userName, int year)
        {
            var selectedYear = EnumType.GetEnumDescription(EnumType.Parse<ReportYear>(year.ToString()));
            return _missingProjectReportRepository.GetMissingProjectReportForSuperUserWithWLM(userName, HUDRole.SuperUser.ToString(), int.Parse(selectedYear));
        }

        public ReportModel GetWorkloadManagerMissingProjectReport(string userName, int year)
        {
            var selectedYear = EnumType.GetEnumDescription(EnumType.Parse<ReportYear>(year.ToString()));
            return _missingProjectReportRepository.GetMissingProjectReportForWorkloadManager(userName, HUDRole.WorkflowManager.ToString(), int.Parse(selectedYear));
        }

        public ReportModel GetAccountExecutiveMissingProjectReport(string userName, int year)
        {
            var selectedYear = EnumType.GetEnumDescription(EnumType.Parse<ReportYear>(year.ToString()));
            return _missingProjectReportRepository.GetMissingProjectReportForAccountExecutive(userName, HUDRole.AccountExecutive.ToString(), int.Parse(selectedYear));
        }
    }
}
