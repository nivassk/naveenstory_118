﻿using System.Collections.Generic;
using HUDHealthcarePortal.Model;

namespace BusinessService.Interfaces
{
    public interface IPortfolioManager
    {
        IEnumerable<DerivedUploadData> GetPortfolios(string quarterToken);
        PaginateSortModel<DerivedUploadData> GetPortfolios(int pageNum, int pageSize, string sortBy, SqlOrderByDirecton sortOrder, string quarterToken);
        PaginateSortModel<DerivedUploadData> GetPortfolioDetail(int pageNum, int pageSize, string sortBy, SqlOrderByDirecton sortOrder, string portfolioNumber, string quarterToken);
    }
}
