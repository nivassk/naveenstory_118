﻿using System.Collections.Generic;
using Model;

namespace BusinessService.Interfaces
{
    public interface INonCriticalLateAlertsManager
    {
        List<NonCriticalLateAlertsModel> GetNonCriticalLateAlertsToSend(string date);
        void SaveNonCriticalLateAlerts(IList<NonCriticalLateAlertsModel> models);
    }
}