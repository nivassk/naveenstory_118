﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using Model;
using Repository.Interfaces;


namespace BusinessService.Interfaces
{
    public interface INonCriticalRepairsRequestManager
    {
        PropertyInfoModel GetPropertyInfo(string fhaNumber);
        string GetLenderName(int lenderId);
        int GetAeUserIdByFhaNumber(string fhaNumber);
        string GetAeEmailByFhaNumber(string fhaNumber);
        Guid SaveNonCriticalRepairsRequest(NonCriticalRepairsViewModel model);
        void UpdateNonCriticalRepairsRequest(NonCriticalRepairsViewModel model);
        UserViewModel GetUserById(int userId);
        void UpdateTaskId(NonCriticalRepairsViewModel model);
        TaskModel GetLatestTaskId(Guid taskInstanceId);
        List<string> GetAllowedNonCriticalFhaByLenderUserId(int userid);
        NonCriticalPropertyModel GetNonCriticalPropertyInfo(string fhanumber);
        NonCriticalPropertyModel GetNonCriticalStatusAndDrawInfo(NonCriticalPropertyModel model, string fhanumber);
        IList<NonCriticalRulesChecklistModel> GetNonCrticalRulesCheckList(FormType formType);
        IList<NonCriticalReferReasonModel> GetNonCrticalReferReasons(Guid ncrId);
        NonCriticalPropertyModel GetNonCrticalNonCrticalTransactions(int propertyId, int lenderId, string fhaNumber);
        bool GetTransactionLedgerStatus(string fhanumber);
        string GetDisclaimerMsgByPageType(string pageTypeName); // User Story 1901 
    }
}
