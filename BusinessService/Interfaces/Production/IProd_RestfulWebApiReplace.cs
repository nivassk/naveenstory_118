﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BusinessService.Interfaces.Production
{
   public interface IProd_RestfulWebApiReplace
    {
       RestfulWebApiResultModel ReplaceDocument(RestfulWebApiUpdateModel fileInfo, string token, HttpPostedFileBase myFile);
    }
}
