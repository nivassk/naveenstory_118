﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces.Production
{
   public  interface IProductionPamReportManager
    {
       ProductionPAMReportModel GetProductionPAMReportSummary(string appType, string programType, string prodUserIds, DateTime? fromDate, DateTime? toDate, string status, string Level, Guid? grouptaskinstanceid, string Mode = "Other");
    }
}
