﻿using Model.Production;
using System.Collections.Generic;

namespace BusinessService.Production
{
    public interface IProd_AdhocDataManager
    {
        IList<Prod_AdhocDataModel> GetProdDataForAdhocDataDownload();
    }
        
}