﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces.Production
{
    public interface IProd_LoanCommitteeManager
    {
        Prod_LoanCommitteeViewModel GetLoanCommitteeDetails(Guid applicationTaskInstanceId);
        Guid SaveLoanCommitteeDetails(Prod_LoanCommitteeViewModel lcModel);
        int GetLCDateCount(DateTime lcDate);
        IList<string> GetLoanCommitteeDatesToDisable();
        IList<Prod_LoanCommitteeViewModel> GetLCFiltersValues();
        IList<Prod_LoanCommitteeViewModel> UpdateLCGridResults(IList<Prod_LoanCommitteeViewModel> Model);
        bool IsUserProductionWlm(int userId);    
        IList<string> AllProductionUsers();
		Prod_LoanCommitteeViewModel GetLoanCommitteeDetailByFha(string pFHANumber);
        List<Prod_LoanCommitteeViewModel> GetSubmitedDates(Guid applicationTaskInstanceId);
        Prod_LoanCommitteeViewModel GetLoanCommitteeSelctedDateInfo(Guid taskInstanceId, string SelectedDate);
    }
}
