﻿using HUDHealthcarePortal.Core;

namespace BusinessService.Interfaces
{
    public interface IScoreManager
    {
        void GetScores(FundamentalFinancial financial);
    }
}
