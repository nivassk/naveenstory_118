﻿using Model.AssetManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces
{
    public interface IRFORRANDNCR_GroupTasksManager
    {
        RFORRANDNCR_GroupTaskModel GetGroupTaskAByTaskInstanceId(Guid guid);
        Guid AddR4RGroupTasks(RFORRANDNCR_GroupTaskModel model);
        void DeleteGroupTask(Guid TaskInstanceId,string FHANumber);
    }
}
