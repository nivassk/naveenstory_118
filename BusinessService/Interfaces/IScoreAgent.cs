﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Core;

namespace BusinessService.Interfaces
{
    public interface IScoreAgent
    {
        void InitializeScoreMatrix();
        Dictionary<DerivedFinancial, Func<decimal?, decimal?>> ScoreMatrix {get;}
    }
}
