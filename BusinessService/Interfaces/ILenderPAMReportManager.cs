﻿using System;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Core;
using Model;
using HUDHealthcarePortal.Model.AssetManagement;
using System.Collections;
using System.Collections.Generic;
// User Story 1904
namespace BusinessService.Interfaces
{
    public interface ILenderPAMReportManager
    {
        IEnumerable<UserInfoModel> GetLenderUsersByRoleLenderId(int lenderId, string roleName);

        PAMReportModel GetLenderPAMSummary(string lamIds, string bamIds, string larIds, string roleIds, DateTime? fromDate, DateTime? toDate, int? status, string projectAction, int page, string sort, SqlOrderByDirecton sortdir);

        PAMReportModel GetSearchCriteriaDetails(ref PAMReportModel reportModel, string lamIds, string bamIds, string larIds, string roleIds, int? status,
           string projectAction, DateTime? fromDate, DateTime? toDate, string userRole, int lenderId);

        string GetLenderName(int lenderId);
    }
}
