﻿using System.Collections.Generic;
using HUDHealthcarePortal.Model;

namespace BusinessService.Interfaces
{
    public interface ICommentManager
    {
        void AddCommentForProject(CommentModel comment);
        IEnumerable<CommentModel> GetCommentsByProject(string fhaNumber);
        IEnumerable<CommentModel> GetCommentsByLdiID(int ldiID);
        void UpdateCommentForProject(CommentModel comment);
        UserViewModel GetUploaderByLdiID(int ldiID);
    }
}
