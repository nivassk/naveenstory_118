﻿using System.Collections.Generic;
using HUDHealthcarePortal.Model;

namespace BusinessService.Interfaces
{
    public interface ILookupManager
    {
        IEnumerable<SecurityQuestionLookup> GetAllSecurityQuestions();
        IEnumerable<EmailTypeLookup> GetAllEmailTypes();
        IEnumerable<KeyValuePair<int, string>> GetAllLenders();
    }
}
