﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;

namespace BusinessService.Interfaces
{
    public interface IReserveForReplacementManager
    {
        PropertyInfoModel GetPropertyInfo(string fhaNumber);
        int GetAeUserIdByFhaNumber(string fhaNumber);
        string GetAeEmailByFhaNumber(string fhaNumber);
        IEnumerable<ReserveForReplacementFormModel> GetR4RFormByPropertyId(int propertyId);
        Guid SaveR4RForm(ReserveForReplacementFormModel model);
        void UpdateR4RForm(ReserveForReplacementFormModel model);
        TaskModel GetLatestTaskId(Guid taskInstanceId);
        void UpdateTaskId(ReserveForReplacementFormModel model);
        string GetDisclaimerMsgByPageType(string pageTypeName);
        List<string> GetAllFHANumbersListForR4R();
        ReserveForReplacementFormModel GetApprovedAmountByFHANumber(string fhaNumber);
        void UpdateApprovedAmount(decimal changeApprovedAmount, string FHANumber, string HudRemarks);
        List<String> GetSubmitedFHANumbers(int lenderID);
        ReserveForReplacementFormModel GetR4RbyTaskInstanceId(Guid taskInstanceId);
		ReserveForReplacementFormModel GetR4RFormByFHANumber(string fhaNumber);

	}
}
