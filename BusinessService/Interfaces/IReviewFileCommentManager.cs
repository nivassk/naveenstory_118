﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;

namespace BusinessService.Interfaces
{
    public interface IReviewFileCommentManager
    {
        void SaveReviewFileComment(IList<ReviewFileCommentModel> reviewCommentList);
    }
}
