﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using Model;
using Model.AssetManagement;

namespace BusinessService.Interfaces
{
    public interface IReviewerTaskAssignmentManager
    {
         int AssinTaskToReviewer(ReviewerTaskAssigmentViewModel reviewerTaskAssigmentViewModel);
         TaskModel GetLatestTaskId(Guid taskInstanceId);
    }
}
