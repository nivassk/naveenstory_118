﻿using BusinessService.Interfaces.UserLogin;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model;
using Repository.UserLogin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.UserLogin
{
    public class UserLoginManager : IUserLoginManager
    {


         private UnitOfWork unitOfWorkLive;
        private UserLoginRepository userloginRepository;

        public UserLoginManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            userloginRepository = new UserLoginRepository(unitOfWorkLive);
        }


        public Guid SaveUserLogin(UserLoginModel model)
        {
            model.UserLoginID = Guid.NewGuid();
            var pk = userloginRepository.SaveUserLogin(model);
            unitOfWorkLive.Save();
            return pk;
        }

        public void UpdatUserLogin(UserLoginModel opaViewModel)
        {
            userloginRepository.UpdatUserLogin(opaViewModel);
            unitOfWorkLive.Save();
        }


        public List<UserLoginModel> GetLoginHistory(int userid)
        {
            return userloginRepository.GetLoginHistory(userid);
        }
    }
}
