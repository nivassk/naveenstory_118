﻿using BusinessService.Interfaces.Production;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Production
{
    public class ProductionPamReportManager : IProductionPamReportManager
    {
         private UnitOfWork unitOfWorkTask;
         private IProductionPAMReportRepository prodpamRepository;


        public ProductionPamReportManager()
        {
            unitOfWorkTask = new UnitOfWork(DBSource.Task);

            prodpamRepository = new ProductionPamReportRepository(unitOfWorkTask);
            
        }

        public ProductionPAMReportModel GetProductionPAMReportSummary(string appType, string programType, string prodUserIds, DateTime? fromDate, DateTime? toDate, string status, string Level, Guid? grouptaskinstanceid, string Mode = "Other")
        {
            return prodpamRepository.GetProductionPAMReportSummary(appType, programType, prodUserIds, fromDate, toDate, status, Level, grouptaskinstanceid, Mode);
        }
    }
}
