﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService
{
    public class RequestAdditionalInfoFilesManager:IRequestAdditionalInfoFileManager
    {
        private IRequestAdditionalInfoFilesRepository requestAdditionalInfoFilesRepository;
        private UnitOfWork unitOfWork;

        public RequestAdditionalInfoFilesManager()
        {
            unitOfWork = new UnitOfWork(DBSource.Task);
            requestAdditionalInfoFilesRepository = new RequestAdditionalInfoFilesRepository(unitOfWork);
        }

        public void SaveRequestAdditionalInfoFiles(IList<RequestAdditionalInfoFileModel> fileList)
        {
            requestAdditionalInfoFilesRepository.SaveRequestAdditionalInfoFiles(fileList);
            unitOfWork.Save();
        }
    }
}
