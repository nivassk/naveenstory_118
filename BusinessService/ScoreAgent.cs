﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using System;
using System.Collections.Generic;

namespace HUDHealthcarePortal.BusinessService
{
    public class ScoreAgent : IScoreAgent
    {
        public Dictionary<DerivedFinancial, Func<decimal?, decimal?>> scoreMatrix = new Dictionary<DerivedFinancial, Func<decimal?, decimal?>>();
        public ScoreAgent()
        {
        }

        public Dictionary<DerivedFinancial, Func<decimal?, decimal?>> ScoreMatrix
        {
            get { return scoreMatrix; }
        }

        public void InitializeScoreMatrix()
        {
            InitializeMatrix();
        }

        private void InitializeMatrix()
        {
            //scoreMatrix.Add(DerivedFinancial.DebtCoverageRatio, GetDebtCoverageRatioFunc());
            //scoreMatrix.Add(DerivedFinancial.WorkingCapital, GetWorkingCapitalFunc());
            //scoreMatrix.Add(DerivedFinancial.DaysCashOnHand, GetDaysCashOnHandFunc());
            //scoreMatrix.Add(DerivedFinancial.DaysInAccountReceivable, GetDaysInAcctReceivableFunc());
            //scoreMatrix.Add(DerivedFinancial.AvgPaymentPeriod, GetAvgPaymentPeriodFunc());

            scoreMatrix.Add(DerivedFinancial.DebtCoverageRatio2, GetDebtCoverageRatio2Func());            
            scoreMatrix.Add(DerivedFinancial.AverageDailyRate, GetAverageDailyRateFunc());
            scoreMatrix.Add(DerivedFinancial.NOI, GetNOIFunc());
        }

        private Func<decimal?, decimal?> GetDebtCoverageRatio2Func()
        {
            return null;
        }

        private Func<decimal?, decimal?> GetAverageDailyRateFunc()
        {
            return null;
        }

        private Func<decimal?, decimal?> GetNOIFunc()
        {
            return null;
        }

        private Func<decimal?, decimal?> GetDebtCoverageRatioFunc()
        {
            return x =>
            {
                if (x >= 1.45m)
                    return 0.0m;
                else if (x >= 1.3m && x < 1.45m)
                    //return 1.0m;
                    return 3.0m;
                else if (x >= 1.2m && x < 1.3m)
                    //return 2.0m;
                    return 6.0m;
                else if (x >= 1.1m && x < 1.2m)
                    //return 3.0m;
                    return 9.0m;
                else if (x < 1.1m)
                    //return 4.0m;
                    return 12.0m;
                else
                    return null;
            };
        }

        private Func<decimal?, decimal?> GetWorkingCapitalFunc()
        {
            return x =>
            {
                if (x >= 1.3m)
                    return 0.0m;
                else if (x >= 1.2m && x < 1.3m)
                    return 1.0m;
                else if (x >= 1.1m && x < 1.2m)
                    return 2.0m;
                else if (x >= 1.0m && x < 1.1m)
                    return 3.0m;
                else if (x < 1.0m)
                    return 4.0m;
                else
                    return null;
            };
        }

        private Func<decimal?, decimal?> GetDaysCashOnHandFunc()
        {
            return x =>
            {
                if (x >= 19m)
                    return 0.0m;
                else if (x >= 15m && x < 19m)
                    return 1.0m;
                else if (x >= 11m && x < 15m)
                    return 2.0m;
                else if (x >= 7m && x < 11m)
                    return 3.0m;
                else if (x < 7m)
                    return 4.0m;
                else
                    return null;
            };
        }


        private Func<decimal?, decimal?> GetDaysInAcctReceivableFunc()
        {
            return x =>
            {
                if (x <= 45m)
                    return 0.0m;
                else if (x > 45m && x <= 60m)
                    return 1.0m;
                else if (x > 60m && x <= 70m)
                    return 2.0m;
                else if (x > 70m && x <= 80m)
                    return 3.0m;
                else if (x > 80m)
                    return 4.0m;
                else
                    return null;
            };
        }

        private Func<decimal?, decimal?> GetAvgPaymentPeriodFunc()
        {
            return x =>
            {
                if (x <= 45m)
                    return 0.0m;
                else if (x > 45m && x <= 60m)
                    return 1.0m;
                else if (x > 60m && x <= 70m)
                    return 2.0m;
                else if (x > 70m && x <= 80m)
                    return 3.0m;
                else if (x > 80m)
                    return 4.0m;
                else
                    return null;
            };
        }
    }
}