﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace HUDHealthcarePortal.BusinessService
{
    public class ExcelFormatter
    {
        public string FriendlyHeaderName { get; set; }
        public string DataFormatString { get; set; }
        public HorizontalAlign HeaderHorizAlign { get; set; }
        public HorizontalAlign ItemHorizAlign { get; set; }

        public ExcelFormatter()
        {
            HeaderHorizAlign = HorizontalAlign.Center;
            ItemHorizAlign = HorizontalAlign.Right;
        }

        public ExcelFormatter(string friendlyHeaderName) : this()
        {
            FriendlyHeaderName = friendlyHeaderName;
        }

        public ExcelFormatter(string friendlyHeaderName, string dataFormatString) : this(friendlyHeaderName)
        {
            DataFormatString = dataFormatString;
        }

        public ExcelFormatter(string friendlyHeaderName, string dataFormatString, HorizontalAlign headerHorizAlign, HorizontalAlign itemHorizAlign)
        {
            FriendlyHeaderName = friendlyHeaderName;
            DataFormatString = dataFormatString;
            HeaderHorizAlign = headerHorizAlign;
            ItemHorizAlign = itemHorizAlign;
        }
    }

    public class ExcelManager<T> where T : class
    {
        // key is T property to show, value is Header name (show user friendly names, not camel case)
        private Dictionary<string, ExcelFormatter> _dictColumnHeadersToShow { get; set; }

        public ExcelManager()
        {
            // build default dictionary with all properties of T
            var properties = typeof(T).GetProperties(System.Reflection.BindingFlags.Public);
            _dictColumnHeadersToShow = new Dictionary<string, ExcelFormatter>();
            properties.ToList().ForEach(p => _dictColumnHeadersToShow.Add(p.Name, new ExcelFormatter(p.Name)));
        }
        public ExcelManager(Dictionary<string, ExcelFormatter> dictColumnHeadersToShow)
        {
            _dictColumnHeadersToShow = dictColumnHeadersToShow;
        }

        public Dictionary<string, ExcelFormatter> GetHeaderDict()
        {
            return _dictColumnHeadersToShow;
        }
    }

    public static class CsvManager
    {
        public static string ToCsv<T>(this IEnumerable<T> items)  where T : class
        {
            var csvBuilder = new StringBuilder();
            var properties = typeof(T).GetProperties();
            //Header row
            foreach (var prop in properties)
            {
                csvBuilder.Append(prop.Name.ToCsvValue() + ",");
            }
            csvBuilder.AppendLine("");//Add line break
            //Body
            foreach (T item in items)
            {
                string line = string.Join(",", properties.Select(p => p.GetValue(item, null).ToCsvValue()).ToArray());
                csvBuilder.AppendLine(line);
            }
            return csvBuilder.ToString();
        }
        /// <summary>
        /// Helper method for dealing with nulls and escape characters
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string ToCsvValue<T>(this T item)
        {
            if (item == null) return "\"\"";
            if (item is string)
            {
                return string.Format("\"{0}\"", item.ToString().Replace("\"", "\\\""));
            }
            double dummy;
            if (double.TryParse(item.ToString(), out dummy))
            {
                return string.Format("{0}", item);
            }
            return string.Format("\"{0}\"", item);
        }
    }
}
