using System;
using System.Web;

namespace HUDHealthcarePortal.BusinessService
{
    public class SessionHelper
    {
        public const string SESSION_KEY_NEW_ROLES = "new_roles_key";
        public const string SESSION_KEY_REMOVE_ROLES = "remove_roles_key";
        public const string SESSION_KEY_NEW_LENDERS = "new_lenders_key";
        public const string SESSION_KEY_MANAGING_OAR = "managing_oar_key";
        public const string SESSION_KEY_MANAGING_OAR_FHA_LIST = "managing_oar_fha_list_key";
        public const string SESSION_KEY_LAR_FHA_LINKS = "lar_fha_link_key";
        public const string SESSION_KEY_PORTFOLIO_SUMMARY_LINK_DICT = "portfolio_summary_link_key";
        public const string SESSION_KEY_PROJECT_REPORT_LINK_DICT = "project_report_link_key";
        public const string SESSION_KEY_UPLOAD_STATUS_LINK_DICT = "upload_status_link_key";
        public const string SESSION_KEY_MISSING_PROJECT_LINK_DICT = "missing_project_link_key";
        public const string SESSION_KEY_PROJECTS_REPORT_LINK_DICT = "projects_report_link_key";
        public const string SESSION_KEY_LARGE_LOAN_REPORT_LINK_DICT = "large_loan_report_link_key";
        public const string SESSION_KEY_RATIO_EXCEPTIONS_REPORT_LINK_DICT = "ratio_exceptions_report_link_key";
        public const string SESSION_KEY_PAM_REPORT_LINK_DICT = "pam_report_link_key";
        public const string SESSION_KEY_LENDER_PAM_REPORT_LINK_DICT = "lender_pam_report_link_key";//user story 1904
        public const string SESSION_KEY_MANGE_USER_REPORT_LINK_DICT = "mange_user_report_link_key";
        public const string SESSION_KEY_MY_TASKS_LINK_DICT = "my_tasks_link_key";
        public const string SESSION_KEY_GROUP_TASKS_LINK_DICT = "group_tasks_link_key";
        public const string SESSION_KEY_TASK_REASSIGNMENT_DICT = "task_reassignment_link_key";
        public const string SESSION_KEY_PROJECT_DETAILREPORTLINK_DICT = "detail_project_report_link_key";
        public const string SESSION_KEY_PROJECT_DETAIL_ERROR_REPORTLINK_DICT = "detail_project_Error_report_link_key";
        public const string SESSION_KEY_PROJECT_CURQRTR_AE_REPORTLINK_DICT = "cur_qrtr_report_link_key";
        public const string SESSION_KEY_PROJECT_CURQRTR_WLM_REPORTLINK_DICT = "cur_qrtr_wlm_report_link_key";
        public const string SESSION_KEY_PROJECT_CURQRTR_ADMIN_REPORTLINK_DICT = "cur_qrtr_admin_report_link_key";

        public const string SESSION_KEY_PROJECT_PTREPORTLINK_DICT = "detail_PT_report_link_key";
        public const string SESSION_KEY_PROJECT_WLM_PTREPORTLINK_DICT = "detail_PT_wlm_report_link_key";
        public const string SESSION_KEY_PROJECT_AE_PTREPORTLINK_DICT = "detail_PT_ae_report_link_key";

        public const string SESSION_KEY_IC_FHA_LINKS = "lc_fha_link_key";
        public const string SESSION_KEY_WORKLOADMANAGERID = "WorkLoadManager_Id";
        public const string SESSION_KEY_LOGINUSERROLE = "login_Role_Name";
        public const string SESSION_KEY_PRODPAM_FILTERS = "prod_pam_filters";

        #region Session helper methods

        /// <summary>
        /// Gets value in Session and removes it
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T SessionExtract<T>(string key)
        {
            return SessionExtract<T>(key, default(T));
        }

        /// <summary>
        /// Gets value in Session and removes it, if there is no such a value or type missmatch, returns default value
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="key">Session key</param>
        /// <param name="defaultValue">Default return value</param>
        /// <returns></returns>
        public static T SessionExtract<T>(string key, T defaultValue)
        {
            try
            {
                return SessionGet(key, defaultValue);
            }
            finally
            {
                HttpContext.Current.Session.Remove(key);
            }
        }

        /// <summary>
        /// Gets value from session without removing
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="key">Session key</param>
        /// <returns></returns>
        public static T SessionGet<T>(string key)
        {
            return SessionGet(key, default(T));
        }

        /// <summary>
        /// Gets value from session without removing,  if there is no such a value or type missmatch, returns default value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static T SessionGet<T>(string key, T defaultValue)
        {
            if (!string.IsNullOrEmpty(key))
            {
                try
                {
                    Object obj = HttpContext.Current.Session[key];
                    if (obj != null)
                    {
                        return (T) obj;
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Format("Cannot get \"{0}\" from session", key), ex);
                }
            }
            return defaultValue;
        }

        #endregion
    }
}
