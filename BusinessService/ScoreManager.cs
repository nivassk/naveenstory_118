﻿using BusinessService.Interfaces;
using Core.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HUDHealthcarePortal.BusinessService
{
    public class ScoreManager : IScoreManager
    {
        private IScoreAgent scoreAgent;
        //private IEnumerable<Type> interfacesInTheAssemble = Assembly.GetExecutingAssembly().GetTypes().ToList()
        //    .Where(p => p.GetInterface(typeof(IUploadedLoanVisitor).ToString()) != null);
        
        public ScoreManager(IScoreAgent agent)
        {
            scoreAgent = agent;
            scoreAgent.InitializeScoreMatrix();
        }
        
        public void GetScores(FundamentalFinancial financial)
        {
            IEnumerable<Type> interfacesInTheClass = this.GetType().GetNestedTypes()
                .Where(p => p.GetInterface(typeof(IUploadedLoanVisitor).ToString()) != null);
            Type[] types = this.GetType().GetNestedTypes();
            var ta = false;
            int iiscore = 1;
            iiscore = iiscore + 1;
            foreach (KeyValuePair<DerivedFinancial, Func<decimal?, decimal?>> item in scoreAgent.ScoreMatrix)
            {
                if (!financial.DerivedFinDict.ContainsKey(item.Key))
                {
                    // use reflection to create the right interface
                    foreach (Type type in interfacesInTheClass)
                    {
                        MemberInfo info = type.GetField("VisitorType") as MemberInfo ??
                                    type.GetProperty("VisitorType") as MemberInfo;
                        if(info != null)
                        {
                            var visitor = Activator.CreateInstance(type);
                            //FillDerived((IUploadedLoanVisitor)visitor, financial);
                            financial.FillDerived((IUploadedLoanVisitor)visitor);
                        }
                    }                    
                }
            }

            foreach (KeyValuePair<DerivedFinancial, Func<decimal?, decimal?>> item in scoreAgent.ScoreMatrix)
            {
                decimal? derivedVal = financial.DerivedFinDict[item.Key];
                decimal? score = null;
                if (financial.DerivedScoreDict.ContainsKey(item.Key))
                {
                    score = financial.DerivedScoreDict[item.Key];
                }
                else
                {
                    score = item.Value(derivedVal);
                    financial.DerivedScoreDict.Add(item.Key, score);
                }
                
                // need to revist this, count null as 0???
                //ScoreSum += item.Value(derivedVal);
                financial.ScoreSum += score;                
                //ScoreSum += score;
            }
        }

        public class DebtCoverageRatioVisitor : IUploadedLoanVisitor
        {
            private decimal? _derivedVal;
            public void Visit(FundamentalFinancial fundamentalFin)
            {
                var uploadedData = fundamentalFin as ExcelUploadView_Model;
                if (uploadedData != null)
                {
                    decimal denominator = (uploadedData.FHAInsuredPrincipalPayment ?? 0) + (uploadedData.FHAInsuredInterestPayment ?? 0) + (uploadedData.MortgageInsurancePremium ?? 0);
                    if (denominator != 0)
                        _derivedVal = uploadedData.NetIncome / denominator;
                }
                fundamentalFin.DerivedFinDict.Add(this.VisitorType, _derivedVal);
            }


            public DerivedFinancial VisitorType
            {
                get { return DerivedFinancial.DebtCoverageRatio; }
            }
        }

        public class WorkingCapitalVisitor : IUploadedLoanVisitor
        {
            private decimal? _derivedVal;
            public void Visit(FundamentalFinancial fundamentalFin)
            {
                var uploadedData = fundamentalFin as ExcelUploadView_Model;
                if (uploadedData != null && uploadedData.CurrentLiabilities != 0)
                    _derivedVal = uploadedData.CurrentAssets / uploadedData.CurrentLiabilities;
                fundamentalFin.DerivedFinDict.Add(this.VisitorType, _derivedVal);
            }

            public DerivedFinancial VisitorType
            {
                get { return DerivedFinancial.WorkingCapital; }
            }
        }

        public class DaysCashOnHandVisitor : IUploadedLoanVisitor
        {
            private decimal? _derivedVal;
            public void Visit(FundamentalFinancial fundamentalFin)
            {
                var uploadedData = fundamentalFin as ExcelUploadView_Model;
                if (uploadedData != null)
                {
                    decimal denominator = (uploadedData.TotalExpenses ?? 0) - (uploadedData.DepreciationExpense ?? 0) - (uploadedData.AmortizationExpense ?? 0);
                    DateTime periodEnd;
                    int monthsInPeriod;
                    DateTime.TryParse(uploadedData.PeriodEnding, out periodEnd);
                    int.TryParse(uploadedData.MonthsInPeriod, out monthsInPeriod);
                    if (denominator != 0)
                        _derivedVal = ((uploadedData.OperatingCash + uploadedData.Investments) / denominator) * DateHelper.DaysFromBeginningOfYearTill(periodEnd);
                }
                fundamentalFin.DerivedFinDict.Add(this.VisitorType, _derivedVal);
            }

            public DerivedFinancial VisitorType
            {
                get { return DerivedFinancial.DaysCashOnHand; }
            }
        }

        public class DaysInAccountReceivableVisitor : IUploadedLoanVisitor
        {
            private decimal? _derivedVal;
            public void Visit(FundamentalFinancial fundamentalFin)
            {
                var uploadedData = fundamentalFin as ExcelUploadView_Model;
                if (uploadedData != null)
                {
                    decimal denominator = uploadedData.TotalRevenues ?? 0;
                    DateTime periodEnd;
                    DateTime.TryParse(uploadedData.PeriodEnding, out periodEnd);
                    if (denominator != 0)
                        _derivedVal = (uploadedData.AccountsReceivable / denominator) * DateHelper.DaysFromBeginningOfYearTill(periodEnd);
                }
                fundamentalFin.DerivedFinDict.Add(this.VisitorType, _derivedVal);
            }

            public DerivedFinancial VisitorType
            {
                get { return DerivedFinancial.DaysInAccountReceivable; }
            }
        }

        public class AvgPaymentPeriodVisitor : IUploadedLoanVisitor
        {
            private decimal? _derivedVal;
            public void Visit(FundamentalFinancial fundamentalFin)
            {
                var uploadedData = fundamentalFin as ExcelUploadView_Model;
                if (uploadedData != null)
                {
                    decimal denominator = (uploadedData.TotalExpenses ?? 0) - (uploadedData.DepreciationExpense ?? 0) - (uploadedData.AmortizationExpense ?? 0);
                    DateTime periodEnd;
                    DateTime.TryParse(uploadedData.PeriodEnding, out periodEnd);
                    if (denominator != 0)
                        _derivedVal = (uploadedData.CurrentLiabilities / denominator) * DateHelper.DaysFromBeginningOfYearTill(periodEnd);
                }
                fundamentalFin.DerivedFinDict.Add(this.VisitorType, _derivedVal);
            }

            public DerivedFinancial VisitorType
            {
                get { return DerivedFinancial.AvgPaymentPeriod; }
            }
        }
    }
}
