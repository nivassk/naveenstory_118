﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Interfaces;

namespace HUDHealthcarePortal.BusinessService
{
    public class CommentManager : ICommentManager
    {
        private ICommentRepository commentRepository;
        private IUserRepository userRepository;
        private UnitOfWork unitOfWork;

        public CommentManager() 
        {
            unitOfWork = new UnitOfWork(DBSource.Live);
            commentRepository = new CommentRepository(unitOfWork);
            userRepository = new UserRepository(unitOfWork);
        }

        [Obsolete("Use GetCommentsByLdiID instead")]
        public IEnumerable<CommentModel> GetCommentsByProject(string fhaNumber)
        {
            var rawComment = commentRepository.GetCommentsByProject(fhaNumber);
            rawComment.ToList().ForEach(p => PopulateCommentRecursive(p));
            //PopulateCommentRecursive(rawComment);
            return rawComment;
        }

        public IEnumerable<CommentModel> GetCommentsByLdiID(int ldiID)
        {
            var rawComment = commentRepository.GetCommentsByLdiID(ldiID);
            rawComment.ToList().ForEach(p => PopulateCommentRecursive(p));
            //PopulateCommentRecursive(rawComment);
            return rawComment;
        }

        public void AddCommentForProject(CommentModel comment)
        {
            commentRepository.AddCommentForProject(comment);
            unitOfWork.Save();
        }

        public void UpdateCommentForProject(CommentModel comment)
        {
            commentRepository.UpdateCommentForProject(comment);
            unitOfWork.Save();
        }

        public UserViewModel GetUploaderByLdiID(int ldiID)
        {
            return userRepository.GetUploadUserByLdiID(ldiID);
        }

        private void PopulateCommentRecursive(CommentModel rawComment)
        {
            if (rawComment == null)
                return;
            if(!string.IsNullOrEmpty(rawComment.Comments))
            {
                var samelvlComment = XmlHelper.Deserialize(typeof(CommentModel), rawComment.Comments, System.Text.Encoding.Unicode) as CommentModel;
                if (samelvlComment != null)
                {
                    rawComment.Comment = samelvlComment.Comment;
                    rawComment.ChildComments = samelvlComment.ChildComments;
                    rawComment.CommentUtcTime = samelvlComment.CommentUtcTime;
                    rawComment.IsCommentPublic = samelvlComment.IsCommentPublic;
                    rawComment.NodeId = samelvlComment.NodeId;
                    rawComment.Username = samelvlComment.Username;
                    rawComment.UserRole = samelvlComment.UserRole;
                }
                if (rawComment.ChildComments != null && rawComment.ChildComments.Count() > 0)
                    rawComment.ChildComments.ForEach(p => PopulateCommentRecursive(p));
            }
        }
    }
}
