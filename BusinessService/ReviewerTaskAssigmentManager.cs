﻿using BusinessService.Interfaces;
using Core.Utilities;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.ExceptionHandling;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Security;
using Repository;
using Repository.Interfaces;
using WebMatrix.WebData;


namespace BusinessService
{

    public class ReviewerTaskAssigmentManager : IReviewerTaskAssignmentManager
    {
      
        private UnitOfWork unitOfWorkTask;
        private IReviewerTaskAssignmentRepository reviewerTaskAssignmentRepository;
        private ITaskRepository taskRepository;

       public  ReviewerTaskAssigmentManager()
        {
          
            unitOfWorkTask = new UnitOfWork(DBSource.Live);
            reviewerTaskAssignmentRepository = new ReviewerTaskAssignmentRepository();
            taskRepository = new TaskRepository();
        }

        public int AssinTaskToReviewer(ReviewerTaskAssigmentViewModel reviewerTaskAssigmentViewModel)
        {
            var reviewTaskAssignmentId = reviewerTaskAssignmentRepository.InsertReviewerTaskAssignment(reviewerTaskAssigmentViewModel);
           unitOfWorkTask.Save();
            return reviewTaskAssignmentId;
        }


        public TaskModel GetLatestTaskId(Guid taskInstanceId)
        {
            return taskRepository.GetLatestTaskByTaskInstanceId(taskInstanceId);
        }
    }

  
}
