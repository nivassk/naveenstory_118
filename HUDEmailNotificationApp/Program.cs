﻿using System;
using System.Configuration;
using System.Net.Mail;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDEmailNotificationApp.Classes;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Model;
using JobLogger;
using Model;

namespace HUDEmailNotificationApp
{
    class Program
    {
        public static string C3SupportEmail = ConfigurationManager.AppSettings["C3SupportEmailAddress"];

        static void Main(string[] args)
        {
            int i = -1;
            try
            {
                int count;
              
                string date = args.Length == 0 ? string.Empty : args[0];

                CreateAutoMappers();
                NonCriticalLateNotification nonCriticalLateNotification = new NonCriticalLateNotification();
                count = nonCriticalLateNotification.NotifyUsers(date);
                i++;
                Logger.WriteJobResult("Late NCR Email Notification Application", i, count);
                Logger.SendJobEmail(new EmailManager());

                Console.WriteLine(count + " email notifications are sent.");
                //Console.WriteLine("");
                //Console.WriteLine("Press enter to exit");
                //Console.ReadLine();
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                Logger.WriteJobResult("Late NCR Email Notification Application", i, -1);
                Logger.SendJobEmail(new EmailManager());
            }
        }

        static void SendFailureNotification(string ex)
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtpServer = new SmtpClient();
            mail.From = new MailAddress(C3SupportEmail);
            string body = ex;

            mail.Subject = "NCR Alert Notification App Failure";
            mail.Body = body;
            mail.IsBodyHtml = false;
            mail.To.Add("cpark@c3-systems.com");

            smtpServer.Send(mail);
        }

        static void CreateAutoMappers()
        {
            Mapper.CreateMap<HCP_Authentication, UserViewModel>()
                .ForMember(dest => dest.SelectedTimezoneId, map => map.MapFrom(src => src.PreferredTimeZone))
                .ForMember(dest => dest.MiddleName, map => map.MapFrom(src => src.UserMInitial));

            Mapper.CreateMap<Address, AddressModel>();

            Mapper.CreateMap<HCP_EmailTemplateMessages, EmailTemplateMessagesModel>();

            Mapper.CreateMap<EmailModel, HCP_Email>();

            Mapper.CreateMap<NonCriticalLateAlertsModel, NonCriticalLateAlerts>()
                .ForMember(dest => dest.PropertyID, map => map.MapFrom(src => src.PropertyID))
                .ForMember(dest => dest.LenderID, map => map.MapFrom(src => src.LenderID))
                .ForMember(dest => dest.FHANumber, map => map.MapFrom(src => src.FHANumber))
                .ForMember(dest => dest.TimeSpanId, map => map.MapFrom(src => src.TimeSpanId))
                .ForMember(dest => dest.HCP_EmailId, map => map.MapFrom(src => src.HCP_EmailId))
                .ForMember(dest => dest.RecipientIds, map => map.MapFrom(src => src.RecipientIds));

            Mapper.CreateMap<HUD_Project_Manager, HUDManagerModel>()
                .ForMember(dest => dest.Manager_Name, map => map.MapFrom(src => src.HUD_Project_Manager_Name))
                .ForMember(dest => dest.Manager_ID, map => map.MapFrom(src => src.HUD_Project_Manager_ID));
        }
    }
}
