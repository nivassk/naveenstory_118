﻿using System.Collections.Generic;
using HUDHealthcarePortal.Core;

namespace HUDEmailNotificationApp.Classes
{
    public interface IEmailNotification
    {
        int SendEmails(EmailType emailType, string keywords, string userIds);
    }
}