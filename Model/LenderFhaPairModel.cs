﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace HUDHealthcarePortal.Model
{
    [Serializable]
    public class LenderFhaCol
    {
        public List<LenderFhaPairModel> LenderFhaPairs;

        public LenderFhaCol()
        {
            this.LenderFhaPairs = new List<LenderFhaPairModel>();
        }
    }
    [Serializable]
    public class LenderFhaPairModel
    {
        [XmlAttribute]
        public int LenderId { get; set; }
        [XmlAttribute]
        public string FhaNumber { get; set; } 
 
        public LenderFhaPairModel()
        {

        }
      
        public LenderFhaPairModel(int lenderId, string fhaNumber)
        {
            this.LenderId = lenderId;
            this.FhaNumber = fhaNumber;
        }
    }
}
