﻿using System;
using HUDHealthcarePortal.Core;

namespace HUDHealthcarePortal.Model
{
    public class HUDManagerModel
    {
        public int Manager_ID { get; set; }
        public string Manager_Name { get; set; }
        public Nullable<int> AddressID { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public Nullable<int> OnBehalfOfBy { get; set; }
        public Nullable<bool> Deleted_Ind { get; set; }

        public AddressModel AddressModel { get; set; }
        public HUDManagerType ManagerType { get; set; }
        public int ISOU_ID { get; set; }
    }
}
