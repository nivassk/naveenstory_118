﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Model
{
	public class Prod_FormAmendmentTaskModel
	{
		//private readonly List<FirmCommitmentAmendmentTypeModel> firmCommitmentAmendmentType;
		public string TaskxrefIdUI { get; set; }
		public string TaskInstanceIdUI { get; set; }
		public int TaskId { get; set; }

		public Guid? TaskInstanceId { get; set; }
		public Guid? TaskXrefid { get; set; }
		public int AmendmentTemplateID { get; set; }
		[Display(Name = "FHA#")]

		public string FHANumber { get; set; }
		public int PageTypeId { get; set; }
		[Display(Name = "Amendment Issue Date")]
		public DateTime? FirmAmendmentIssueDate { get; set; }
		[Display(Name = "Lender Addressee")]
		[StringLength(100)]
		public string LenderAddressee { get; set; }
		[Display(Name = "Addressee's Title")]
		[StringLength(80)]
		public string AddresseeTitle { get; set; }
		[Display(Name = "Lender Company Name")]
		[StringLength(100)]
		public string LenderCompanyName { get; set; }

		[Display(Name = "Which Firm Amendment ?")]
		public int FirmAmendmentNum { get; set; }
		public List<FirmCommitmentAmendmentTypeModel> FirmCommitmentAmendmentTypes { get; set; }

		[Display(Name = "Project Name")]
		[StringLength(100)]
		public string ProjectName { get; set; }
		[Display(Name = "Project Address")]
		[StringLength(80)]
		public string ProjectAddress { get; set; }
		[Display(Name = "Borrower")]
		[StringLength(80)]
		public string Borrower { get; set; }

		[Display(Name = "Saluatation")]
		public int Saluatation { get; set; }

		public List<SaluatationTypeModel> SaluatationTypes { get; set; }
		//[Display(Name = "First Name")]
		//[StringLength(50)]
		//public string FirstName { get; set; }

		[Display(Name = "Last Name")]
		[StringLength(50)]
		public string LastName { get; set; }
		[Display(Name = "Firm Commitment Signed")]
		public DateTime? FirmCommitmentSignedDate { get; set; }

		[Display(Name = "Party or Property Name")]
		public int Party_Property_Id { get; set; }

		public List<PartyTypeModel> PartyPropertyNameTypes { get; set; }
		public string Party_Propert_Name { get; set; }
		[Display(Name = "Party/Property Address")]
		public int Party_Property_Address_Id { get; set; }


		public List<PartyTypeModel> PartyPropertyAddressTypes { get; set; }

		public string Party_Property_Address_Name { get; set; }
		[Display(Name = "Loan Amount")]
		public decimal LoanAmount { get; set; }

		[Display(Name = "Previous Loan Amount")]
		public decimal PrevLoanAmount { get; set; }
		[Display(Name = "Interest Rate")]
		public decimal InterestRate { get; set; }
		[Display(Name = "Monthly P&I Payment")]
		public decimal MonthlyPayment { get; set; }
		[Display(Name = "Change in Monthly P&I Payment")]
		public decimal DifferentMonthlyPayment { get; set; }
		[Display(Name = "Years #")]

		public int DueYear { get; set; }
		[Display(Name = "Months #")]
		public int DueMonth { get; set; }
		[Display(Name = "Extension")]
		public DateTime? CommitmentTerminationDate { get; set; }
		//[Display(Name = "Additional or Initial Deposit")]
		[Display(Name = "Deposit")]
		public int AdditionalInitialDeposit { get; set; }

		public List<DepositTypeModel> AdditionalInitialDepositTypes { get; set; }

		[Display(Name = "R4R Amount")]
		public decimal R4RAmount { get; set; }
		[Display(Name = "Critical Repair Cost")]
		public decimal CriticalRepairCost { get; set; }
		//[Display(Name = "Remaining Non-Critical and Borrower-elective Repair Cost")]
		[Display(Name = "Remaining Repair Cost")]
		public decimal RemainingRepairCostExihibitC { get; set; }
		[Display(Name = "Escrow Estimate")]
		public int EscrowEstimate { get; set; }

		public List<EscrowEstimateTypeModel> EscrowEstimateTypes { get; set; }

		[Display(Name = "Repair Cost Estimate")]
		public int RepairCostEstimate { get; set; }
		public List<RepairCostEstimateTypeModel> RepairCostEstimateTypes { get; set; }

		[Display(Name = "Special Condition")]
		public int SpecialCondition { get; set; }
		public List<SpecialConditionTypeModel> SpecialConditionTypes { get; set; }

		[Display(Name = "Annual Lease Payment")]
		public decimal AnnualLeasePayment { get; set; }

		[Display(Name = "Exhibit Letter")]
		public int ExhibitLetter { get; set; }
		public List<ExhibitLetterTypeModel> ExhibitLetterTypes { get; set; }

		[Display(Name = "Other Changes")]
		[DataType(DataType.MultilineText)]
		[StringLength(100)]
		public string Other { get; set; }

		[Display(Name = "CC's Name")]
		[StringLength(50)]
		public string CC_Name { get; set; }
		[Display(Name = "CC's Contact Details")]
		[StringLength(100)]
		public string CC_ContactDetails { get; set; }
		[Display(Name = "Agent Name")]
		public string AuthorizedAgent { get; set; }

		public int AuthorizedAgentSignatureId { get; set; }
		[Display(Name = "WLM Name")]
		public string WLM { get; set; }
		public int WLMSignatureId { get; set; }
		[Display(Name = "Attachments")]
		public bool AttachExhibitA { get; set; }
		public bool AttachExhibitB { get; set; }
		public bool AttachExhibitC { get; set; }
		public bool AttachHUD92264aORCF { get; set; }

		[UIHint("SignaturePad")]
		public byte[] MySignature { get; set; }

		[UIHint("SignaturePad")]
		public byte[] CurrentSignature { get; set; }

		[Display(Name = "Do you need to update Agent's Signature?")]
		public bool IsNewSignatureRequired { get; set; }


		[UIHint("SignaturePad")]
		public byte[] WLMSignature { get; set; }

		[UIHint("SignaturePad")]
		public byte[] WLMCurrentSignature { get; set; }

		[Display(Name = "Do you need to update WLM's Signature?")]
		public bool IsWLMNewSignatureRequired { get; set; }

		public string AgentPictureLocation { get; set; }
		public string WlmPictureLocation { get; set; }

		public string SaluatationType { get; set; }
		public string PartyPropertyName { get; set; }
		public string PartyPropertyAddress { get; set; }
		public string AdditionalInitialDepositType { get; set; }
		public string EscrowEstimateType { get; set; }
		public string ExhibitLetterType { get; set; }
		public string RepairCostEstimateType { get; set; }
		public string SpecialConditionType { get; set; }
		public string AgentSignatureFileName { get; set; }
		public string WLMSignatureFileName { get; set; }

		public bool Party_Propert_Name_cb { get; set; }
		public bool Party_Property_Address_Name_cb { get; set; }
		public bool LoanAmount_cb { get; set; }
		public bool InterestRate_cb { get; set; }
		public bool MonthlyPayment_cb { get; set; }
		public bool DifferentMonthlyPayment_cb { get; set; }
		public bool CommitmentTerminationDate_cb { get; set; }
		public bool R4RAmount_cb { get; set; }
		public bool CriticalRepairCost_cb { get; set; }
		public bool RemainingRepairCostExihibitC_cb { get; set; }
		public bool SpecialCondition_cb { get; set; }
		public bool AnnualLeasePayment_cb { get; set; }
		public bool Other_cb { get; set; }
		public bool ExhibitLetter_cb { get; set; }

		public DateTime? DAPCompletedDate { get; set; }

		public int PropertyId { get; set; }
		public string RoleType { get; set; }
		public int CreatedBy { get; set; }
		public int ModifiedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }

	}


	public class Prod_FormAmendmentTaskModelSP
	{

		public int AmendmentNumber { get; set; }
		public string AmendmentTemplate { get; set; }
		public string AmendmentType { get; set; }
		public string SubmittedBy { get; set; }
		public DateTime SubmittedOn { get; set; }
		public string AssignedUWCloser { get; set; }
		public DateTime? DateAssignedUWCloser { get; set; }
		public string AssignedWLM { get; set; }
		public DateTime? DateAssignedWLM { get; set; }
		public DateTime? DAPCompletedByCloser { get; set; }
		public DateTime DAPCompletedByCloser1 { get; set; }

		public string UWCloserDts { get; set; }
		public string WLMDts { get; set; }
		public string DAPCloserDts { get; set; }

		public string TaskxrefIdUI { get; set; }
		public string TaskInstanceIdUI { get; set; }
		public int TaskId { get; set; }

		public Guid? TaskInstanceId { get; set; }
		public Guid? TaskXrefid { get; set; }
		public int AmendmentTemplateID { get; set; }
		public string FHANumber { get; set; }

	}
}

