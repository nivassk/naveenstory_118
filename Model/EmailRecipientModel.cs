﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class EmailRecipientModel
    {
       
        public string Fullname { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string LenderEmail { get; set; }
        
    }
}
