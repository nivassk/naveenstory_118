﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
   public class ReviewerTitlesModel
    {
        public int Reviewer_Id { get; set; }
        public string Reviewer_Name { get; set; }
        public int? TitleTypeId { get; set; }
    }
}
