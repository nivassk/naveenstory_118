﻿using System;

namespace Model
{
    public class TransactionModel
    {
        public Guid TransactionId { get; set; }
        public Guid FormId { get; set; }
        public int FormType { get; set; }
        public decimal? Amount { get; set; }
        public decimal? CurrentBalance { get; set; }
        public int TransactionType { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public int? Status { get; set; } 
    }
}