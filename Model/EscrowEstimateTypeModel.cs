﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
	public class EscrowEstimateTypeModel
	{
		public int EscrowEstimateTypeId { get; set; }
		public string EscrowEstimate { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }

	}
}

