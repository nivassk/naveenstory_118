﻿using System.ComponentModel.DataAnnotations;

namespace HUDHealthcarePortal.Model
{
    public class RequestModel 
    {
        [Required(ErrorMessage="Your Name is required")]
        [Display(Name = "Your Name")]
        public string ClientName { get; set; }
        [Required(ErrorMessage = "Your Email is required")]
        [EmailAddress(ErrorMessage = "Not a valid email address")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Your Email")]
        public string ClientEmail { get; set; }
        [Required(ErrorMessage = "Your Contact Number is required")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Contact Number")]
        public string ClientPhone { get; set; }
        [Required(ErrorMessage = "Subject is required")]
        [Display(Name = "Subject")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "Message is required")]
        [Display(Name = "Message")]
        public string ClientMessage { get; set; }

        public RequestModel()
        {
        }     
    }
}
