﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class PageTypeViewModel
    {
        public int PageTypeId { get; set; }
        public string PageTypeDescription { get; set; }
        public int ModuleId   { get; set; }
    }
}
