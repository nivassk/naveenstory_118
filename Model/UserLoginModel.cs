﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
   public class UserLoginModel
    {
        public Guid UserLoginID { get; set; }
        public DateTime Login_Time { get; set; }
        public DateTime? Logout_Time { get; set; }
        public string RoleName { get; set; }
        public string Session_Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public int? User_id { get; set; }
    }
}
