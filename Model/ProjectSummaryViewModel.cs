﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Model;

namespace HUDHealthcarePortal.Model
{
    [Serializable]
    public class ProjectSummaryViewModel : ReportGridModel
    {
        public string WorkloadManager { get; set; }
        public int? WorkloadManagerId { get; set; }
        public string AccountExecutive { get; set; }
        public int? AccountExecutiveId { get; set; }
        public string LenderName { get; set; }
        public int? LenderId { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public string PeriodEndingDate { get; set; }
        public string MonthsInPeriod { get; set; }
        public bool IsHighRisk { get; set; }
        public int? TotalUnpaidPrincipalBalance { get; set; }

        public ProjectSummaryViewModel()
        {

        }
    }
}
