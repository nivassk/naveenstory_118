﻿namespace HUDHealthcarePortal.Model
{
    public class PropertyInfoModel
    {
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public string ReacScore { get; set; }
        public string TroubledCode { get; set; }
        public string ActiveDecCaseInd { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public string InitialEndorsementDate { get; set; }
        //Production
        public string ProjectType { get; set; }
        public int ProjectTypeID { get; set; }
        public int AddressId { get; set;}
        public bool? IsAddressChange { get; set; }
    }
    public class AdditionalPropertyInfo:PropertyInfoModel
    {
        public int LenderID { get; set; }
        public string LenderOrg { get; set; }
        public int AssignedAEId { get; set; }
        public string AssignedAeName { get; set; }
        public int AssignedWLMId { get; set; }
        public string AssignedWLMName { get; set; }
    }

	public class AmendmentPropertyInfo : PropertyInfoModel
	{
		public bool IsCloserAssigned { get; set; }
	}
}
