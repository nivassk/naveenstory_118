﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
	public class ExhibitLetterTypeModel
	{
		public int ExhibitLetterTypeId { get; set; }
		public string ExhibitLetter { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }

	}
}
