﻿using System.ComponentModel.DataAnnotations;

namespace HUDHealthcarePortal.Model
{
    public class RequiredExceptForAttribute : ValidationAttribute
    {
        public bool ExceptForCondition { get; set; }

        public override bool IsValid(object value)
        {
            return ExceptForCondition || (value != null && !string.IsNullOrEmpty((string)value));
        }
    }
}
