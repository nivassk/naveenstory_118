﻿namespace HUDHealthcarePortal.Model
{
    public class MultiLoanModel
    {
        public int PropertyID { get; set; }
        public string ProjectName { get; set; }
        public int? FhaCount { get; set; }
        public string OwnerCompanyType { get; set; }
        public string OwnerContact { get; set; }
        public string OwnerPhone { get; set; }
    }
}
