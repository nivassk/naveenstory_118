﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
    public class ReviewerTaskAssigmentViewModel : UserViewModel 
    {
        public int ReviewerTaskAssignmentId { get; set; }
        public Guid TaskInstanceId { get; set; }
        public string FHANumber { get; set; }
        public int ReviwerUserId { get; set; }
        public int Status { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public string PropertyName { get; set; }
        public int id { get; set; }
       public  bool fromTaskReAssignment { get; set; }
       public string reAssignedTo { get; set; }
    }
}
