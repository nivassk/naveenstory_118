﻿using System.Collections.Generic;
using System.Web.Mvc;
using HUDHealthcarePortal.Core;
using System;
using Core;
using HUDHealthcarePortal.Core.Utilities;
using System.Text;
using Model;

namespace HUDHealthcarePortal.Model
{
     [Serializable]
    public class TaskReAssignmentViewModel :TaskModel
    {
        public int TaskReAssignId { get; set; }
        public Guid TaskInstanceId { get; set; }
        public string FromAE { get; set; }
        public string defaultwlm { get; set; }
        public string ReAssignedTo { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public bool? Deleted_Ind { get; set; }
        public DateTime? AssignedDate { get; set; }
        public MultiSelectList WLMListItems { get; set; }
        public IList<SelectListItem> AEListItems { get; set; }
        public IList<SelectListItem> ISOUListItems { get; set; }
        public IList<SelectListItem> ReassignAEListItems { get; set; }
        public IList<SelectListItem> ReassignISOUListItems { get; set; }
        public MultiSelectList ProjectAction { get; set; }
        public PamProjectActionTypeViewModel PamProjectActionTypes { get; set; }
        public IList<string> SelectedWLMNameList { get; set; }
        public string SelectedAE { get; set; }
        public string SelectedReassignAE { get; set; }
        public string SelectedReassignISOU { get; set; }
        public string SelectedAEEmail { get; set; }
        public string SelectedReassignAEEmail { get; set; }
        public string SelectedWLMEmail { get; set; }
        public string SelectedReassignedWLMEmail { get; set; }
        public IList<SelectListItem> TaskAgeList { get; set; }
        public string SelectedTaskAge { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public PaginateSortModel<TaskDetailPerAeModel> TaskDetailPerAeList { get; set; }
        public bool IsDisplayConfirmationPopup { get; set; }
        public bool IsSelectAll { get; set; }
        public bool IsAssignToInternalSpecialOptionUser { get; set; }

        public TaskReAssignmentViewModel()
        {
            TaskDetailPerAeList = new PaginateSortModel<TaskDetailPerAeModel>()
            {
                Entities = new List<TaskDetailPerAeModel>(),
                TotalRows = 0,
                PageSize = 10
            };
        }
        public string ReAssignedByName { get; set; }
    }
}
