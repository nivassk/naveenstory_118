﻿using HUDHealthcarePortal.Core;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Model
{
    public class ReportModel
    {
        public ReportModel(ReportType reportType)
        {
            ReportHeaderModel = new ReportHeaderModel();
            ReportHeaderModel.ReportType = reportType;
            ReportGridModelList = new List<ReportGridModel>();
        }

        public ReportHeaderModel ReportHeaderModel { get; set; }
        public IList<ReportGridModel> ReportGridModelList { get; set; }

        public MultiSelectList PeriodEndingListItems { get; set; }
        public MultiSelectList QuarterListItems { get; set; }
        public MultiSelectList PortFolioNumberListItems { get; set; }
        public MultiSelectList PortFolioNameListItems { get; set; }
        public MultiSelectList PEQtrListItems { get; set; }
        public MultiSelectList PFNOPEListItems { get; set; }
        public MultiSelectList PROPIDListItems { get; set; }
        public MultiSelectList FHANOListItems { get; set; }
        public bool IsShowExceptionsOnly { get; set; }

    }    
}
