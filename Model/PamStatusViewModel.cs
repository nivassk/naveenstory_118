﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class PamStatusViewModel
    {
        public int PamStatusId { get; set; }
        public string StatusDescription { get; set; }
        public int ModuelId { get; set; }
        public int displayorder { get; set; }
    }
}
