﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
   public class PDRColumnViewModel
    {
       public int ColumnID { get; set; }
        public string ColumnName { get; set; }
        public bool Visibility { get; set; }
    }

    public class PDRUserPreferenceViewModel
    {
        public int PDRUserPreferenceID { get; set; }

        public int UserID { get; set; }

        public int PDRColumnID { get; set; }

        public bool Visibility { get; set; }
    }
}
