﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;
using HUDHealthcarePortal.Model;

namespace HUDHealthcarePortal.Model.AssetManagement
{
    [Serializable]
    public class NonCriticalRequestExtensionModel
    {
        [XmlIgnore]
        public IList<String> AvailableFHANumbersList { get; set; }
        public string LenderName { get; set; }
        public string PropertyName { get; set; }
        public AddressModel PropertyAddress { get; set; }

        public Guid NonCriticalRequestExtensionId { get; set; }
        public int PropertyID { get; set; }
        public string FHANumber { get; set; }
        public int? LenderID { get; set; }
        public DateTime? ClosingDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? ExtensionPeriod { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int ExtensionRequestStatus { get; set; }
        public string Comments { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public int? TaskId { get; set; }
        public bool IsDisplayAcceptPopup { get; set; }
        // storing the serialized data, files, concurrency details
        public Guid? TaskGuid { get; set; }
        public int? SequenceId { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedTo { get; set; }
        public TaskOpenStatusModel TaskOpenStatus { get; set; }
        public byte[] Concurrency { get; set; }
        public bool? IsConcurrentUserUpdateFound { get; set; }
        public bool IsPAMReport { get; set; }
        public string ManageMode { get; set; }
        public string ViewName { get; set; }
        public bool? IsApprove { get; set; }
        public DateTime? CalcExtenDate{ get; set; }
        public bool IsDefaultExention { get; set; }
        public bool IsAnyPendingExtensionRequestExist { get; set; }
        public bool? IsReassigned { get; set; }
        public bool IsUpdateiREMS { get; set; }
        //User story 1904
        public bool IsLenderPAMReport { get; set; }
    }
}
