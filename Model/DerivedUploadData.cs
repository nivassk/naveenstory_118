﻿using Model;

namespace HUDHealthcarePortal.Model
{
    public class DerivedUploadData : ExcelUploadView_Model
    {
        public bool HasComment { get; set; }
        public string Portfolio_Number { get; set; }
        public string Portfolio_Name { get; set; }
        public int? Portfolio_Count { get; set; }
        public decimal? NetOperatingIncome { get; set; }
        public decimal MonthsInPeriodAvg { get; set; }

        // if portfolio has no any upload yet, use Units in facility as surrogate to test
        public bool IsPortfolioNoData
        {
            get { return UnitsInFacility == 0; }
        }

        public YearQuarterModel YearQuarterModel { get; set; }
    }
}
