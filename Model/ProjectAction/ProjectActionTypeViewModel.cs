﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model.ProjectAction
{
    public class ProjectActionTypeViewModel
    {
        public int ProjectActionID { get; set; }
      
        public string ProjectActionName { get; set; }
    }
}
