﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.ProjectAction
{
    [Serializable]
    public class GroupTaskModel
    {

        public int TaskId { get; set; }
        public Guid TaskInstanceId { get; set;}
        public int RequestStatus { get; set;}
        public int InUse { get; set;}
        public string FhaNumber { get; set; }
        public string PropertyName { get; set;}
        public DateTime? RequestDate { get; set;}
        public string RequesterName { get; set;}
        public DateTime? ServicerSubmissionDate { get; set; }
        public DateTime? ProjectActionStartDate { get; set;}
        public int ProjectActionTypeId { get; set; }
        public bool IsDisclimerAccepted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string ProjectActionName { get; set; }
        public string UserRole { get; set; }
        public string UserNameInUse { get; set; }
        public string Status { get; set; }
        public bool IsEditMode { get; set; }
        public byte[] Concurrency { get; set; }
        public string ServicerComments { get; set; }

        public bool IsAddressChanged { get; set; }
    }
}
