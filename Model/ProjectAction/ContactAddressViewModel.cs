﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model.ProjectAction
{
    public class ContactAddressViewModel
    {
        public string FHANumber { get; set; }
        public string PropertyName { get; set; }
        public AddressModel ServicerAddress { get; set; }
        public AddressModel OperatorAddress { get; set; }
        public AddressModel LenderAddress { get; set; }
        public AddressModel ManagementAgentAddress { get; set; }
        public AddressModel OwnerAddress { get; set; }

        public ContactAddressViewModel()
        {
            ServicerAddress = new AddressModel();
            OperatorAddress = new AddressModel();
            LenderAddress = new AddressModel();
            ManagementAgentAddress = new AddressModel();
            OwnerAddress = new AddressModel();
        }
    }
}
