﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
   public class ProductionComments
    {
        public LenderComment LenderComment { get; set; }
        public List<ProductionUserComments> ProductionUserComments { get; set; }
    }

    public class LenderComment
    {
        public string CommentText { get; set; }
        public string LenderName { get; set; }
    }

    public class ProductionUserComments
    {
        public string CommentType { get; set; }
        public string CommentText { get; set; }
        public string ProductionUserName { get; set; }
        
    }
}
