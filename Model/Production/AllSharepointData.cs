﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
  public class AllSharepointData
    {
        public int ScreenId { get; set; }
        public bool? IsLongTermHold { get; set; }
        public DateTime? DateOfHold { get; set; }
        public string ReasonForHold { get; set; }
        public bool? IsLoanCommitteApproved { get; set; }
        public DateTime? LoanCommitteApprovedDate { get; set; }
        public bool? IsFirmCommitmentIssued { get; set; }
        public DateTime? FirmCommitmentIssuedDate { get; set; }
        public bool? IsEarlyCommencementRequested { get; set; }
        public DateTime? EarlyCommencementRequestedDate { get; set; }
        public DateTime? EarlyCommencementIssuedDate { get; set; }
        public bool? IsLDLIssued { get; set; }
        public DateTime? LDLIssuedDate { get; set; }
        public DateTime? LDLCompleteResponseReceived { get; set; }
        public string MiscellaneousComments { get; set; }
        public string OtherQueueWorkComments { get; set; }
        public int SelectedAEId { get; set; }
        public bool? IsIremsCompleted { get; set; }
        public bool? IsAppsCompleted { get; set; }
        public bool? IsIremsPrintoutSaved { get; set; }
        public bool? IsMirandaCompletedCMSListCheck { get; set; }
        public decimal ContractorContractPrice { get; set; }
        public decimal ContractorSecondaryAmtPaid { get; set; }
        public decimal ContractorAmountPaid { get; set; }
        public string OGCComment { get; set; }
        public string AppraisalComment { get; set; }
        public string EnvironmentalComment { get; set; }
        public string SurveyComment { get; set; }
        public string ContractorComment { get; set; }
        public string ClosingComment { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedON { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Guid TaskinstanceId { get; set; }
        public string FHANumber { get; set; }

        //Closing information
        public bool IsCloserContractor { get; set; }
        public string ContractorContactName { get; set; }
        public int ClosingProgramSpecialistId { get; set; }
        public bool IsCostCertRecieved { get; set; }
        public DateTime? CostCertRecievedDate { get; set; }
        public bool IsCostCertCompletedAndIssued { get; set; }
        public DateTime? CostCertIssueDate { get; set; }
        public bool Is290Completed { get; set; }
        public DateTime? Two90CompletedDate { get; set; }
        public bool IsActiveNCRE { get; set; }
        public DateTime? NCREDueDate { get; set; }
        public decimal NCREInitalBalance { get; set; }
        public DateTime? ClosingPackageRecievedDate { get; set; }

        public DateTime? InitialClosingDate { get; set; }
        public int ClosingAEId { get; set; }
        public int ClosingCloserId { get; set; }
        public int BackupOgcAddressId { get; set; }
        public int OgcAddressId { get; set; }
    }
}
