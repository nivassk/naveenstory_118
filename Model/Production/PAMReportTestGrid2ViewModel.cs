﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class PAMReportTestGrid2ViewModel
    {
        public string ProjectStage { get; set; }
        public string ProjectStatus { get; set; }
        public string AppraiserStatus { get; set; }
        public string EnvironmentalistStatus { get; set; }
        public string TitleSurveyStatus { get; set; }
        public bool IsContractorAssigned { get; set; }
        public Guid CurrentTaskInstanceId { get; set; }
    }
}
