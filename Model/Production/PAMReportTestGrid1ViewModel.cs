﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class PAMReportTestGrid1ViewModel
    {
        public string ProjectName { get; set; }
        public string FhaNumber { get; set; }
        public string LoanType { get; set; }
        public DateTime ProjectStartDate { get; set; }
        public int TotalDays { get; set; }
        public string Status { get; set; }
        public decimal LoanAmount { get; set; }
        public Guid CurrentTaskInstanceId { get; set; }

    }
}
