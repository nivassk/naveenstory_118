﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using HUDHealthcarePortal.Model;

namespace Model.Production
{
   public class ProdApplicationSharepointData
    {
        public int PageTypeId { get; set; }
        public string ProdAppName { get; set; }
        public List<ContractorInfo> ReviewersList { get; set; }
        public ApplicationDetailViewModel ApplicationDetails { get; set; }
        public GeneralInformationViewModel GeneralInformation { get; set; }
        public MiscellaneousInformationViewModel MiscellaneousInformation { get; set; }
        public ClosingInfo ClosingInfo { get; set; }
        public Prod_LoanCommitteeViewModel LoanCommittee { get; set; }
		public Prod_FormAmendmentTaskModel AmendmentInfo { get; set; }

		public List<AmendmentsModel> AmendmentList { get; set; }
		//public int SharePointSection { get; set; }
	}

	public class ReviewerInfo
   {
       public Guid TaskXrefId { get; set; }
       public string Reviewertype { get; set; }
       public int ReviewerTypeId { get; set; }
       public string ContractorRole { get; set; }
       public bool IsAssigned { get; set; }

       [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
       public DateTime DateAssigned { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
       public DateTime ? DateCompleted { get; set; }
       public string AssignedTo { get; set; }
       public int StatusId { get; set; }
       public string Status { get; set; }
       public string Comments { get; set; }

       public AddressModel Address { get; set; }

   }
   public class ContractorInfo : ReviewerInfo
   {
       [DisplayFormat(DataFormatString = "{0:n0}")]
       public decimal ContractPrice { get; set; }
       [DisplayFormat(DataFormatString = "{0:n0}")]
       public decimal SecondaryAmountPaid { get; set; }
       [DisplayFormat(DataFormatString = "{0:n0}")]
       public decimal AmountPaid { get; set; }
   }
   public class ClosingInfo : ReviewerInfo
   {

       public Guid TaskInstanceId { get; set; }
       public bool IsCloserContractor { get; set; }
       public string ContractorContactName { get; set; }
       public int ClosingProgramSpecialistId { get; set; }
       public string ClosingProgramSpecialistName { get; set; }
       public bool IsCostCertRecieved { get; set; }

       [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
       public DateTime? CostCertRecievedDate { get; set; }
       
       public bool IsCostCertCompletedAndIssued { get; set; }

       [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
       public DateTime? CostCertIssueDate { get; set; }
       public bool Is290Completed { get; set; }

       [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
       public DateTime? Two90CompletedDate { get; set; }
       public bool IsActiveNCRE { get; set; }

       [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
       public DateTime? NCREDueDate { get; set; }
       public decimal NCREInitalBalance { get; set; }

       [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
       public DateTime? ClosingPackageRecievedDate { get; set; }
       public string ClosingComment { get; set; }

       [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
       public DateTime? InitialClosingDate { get; set; }
       public int ClosingAEId { get; set; }
       public string ClosingAEName { get; set; }
       public int ClosingCloserId { get; set; }
       public string ClosingCloserName { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? ClosingDateCreatedOn { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime?  ClosingDateModifiedOn { get; set; }


    }

    public class ApplicationDetailViewModel
    {
        public string FHANumber { get; set; }
        public Guid TaskInstanceId { get; set; }
        public int PropertyId { get; set; }
        public string ProjectName { get; set; }
        public int LenderId { get; set; }
        public string LenderName { get; set; }
        public string LenderContactEmail { get; set; }
        public int PropertyAddressId { get; set; }
        public string PropertyStreetAddress { get; set; }
        public string PropertyCity { get; set; }
        public string PropertyState { get; set; }
        public string PropertyZip { get; set; }
        public decimal LoanAmount { get; set; }
    }

    public class GeneralInformationViewModel
    {
        public string FHANumber { get; set; }
        public Guid TaskInstanceId { get; set; }
        public DateTime? FirmReviewStartDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime QueueEntryDate { get; set; }
         [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ProgramSpecialistAssignedDate { get; set; }
         [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ProgramSpecialistCompletedDate { get; set; }
        public bool IsTaxCreditProperty { get; set; }
        public bool IsContractorProcessedLoan { get; set; }
        public bool IsTwoStageSubmittal { get; set; }
        public string WLMName { get; set; }
        public string UnderwriterName { get; set; }
        public string ProjectStatus { get; set; }
         [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ProjectStatusAsofDate { get; set; }        
        public bool? IsLongTermHold { get; set; }
         [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfHold { get; set; }
        public string ReasonForHold { get; set; }
        public bool IsApplicationRejected { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ApplicationRejectionDate { get; set; }
        public bool? IsLoanCommitteApproved { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LoanCommitteApprovedDate { get; set; }
        public bool IsFirmCommitmentIssued { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FirmCommitmentIssuedDate { get; set; }
    }

	public class AmendmentSPViewModel
	{
		public string FHANumber { get; set; }
		public Guid TaskInstanceId { get; set; }
		public IList<String> AmendmentSubmitted { get; set; }
		public string AmendmentSubmittedData { get; set; }
		public IList<TaskFileModel> TaskFileList { get; set; }
		public string AssignedUWCloser { get; set; }
		public string AssignedWLM { get; set; }
		public DateTime AmendmentDate { get; set; }
		public bool DAPcompleted { get; set; }
		public DateTime DAPCompletedDate { get; set; }
		public decimal LoanAmount { get; set; }
		public decimal AmendedLoanAmount { get; set; }

	}

	public class AmendmentsModel : AmendmentSPViewModel
	{
		public int AmendmentNumber { get; set; }
		public string AmendmentTemplate { get; set; }
		public string AmendmentType { get; set; }
		public DateTime? DAPCompletedByCloser { get; set; }
		public string DAPCloserDts { get; set; }
		public string UWCloserDts { get; set; }
		public string WLMDts { get; set; }
		public string SubmittedBy { get; set; }
		public DateTime SubmittedOn { get; set; }

	}
	public class MiscellaneousInformationViewModel
    {
        public string FHANumber { get; set; }
        public Guid TaskInstanceId { get; set; }
        public bool? IsEarlyCommencementRequested { get; set; }
         [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EarlyCommencementRequestedDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EarlyCommencementIssuedDate { get; set; }
        public bool? IsLDLIssued { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LDLIssuedDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LDLCompleteResponseReceived { get; set; }
        public string MiscellaneousComments { get; set; }
        public string DisplayMiscellaneousComments { get; set; }
        public bool IsUnderwritingContractorAssigned { get; set; }
        public string UWContractorName { get; set; }
        public string UWContractorEmailAddress { get; set; }
        public string UWContractorPhoneNumber { get; set; }
        public string OtherQueueWorkComments { get; set; }
        public string DisplayOtherQueueWorkComments { get; set; }
        public IList<SelectListItem> AEList { get; set; }
        public int SelectedAeId { get; set; }
        public string SelectedAeName { get; set; }
        public bool? IsIremsCompleted { get; set; }
        public bool? IsAppsCompleted { get; set; }
        public bool? IsIremsPrintoutSaved { get; set; }
        public bool? IsMirandaCompletedCMSListCheck { get; set; }

        public MiscellaneousInformationViewModel()
        {
            AEList = new List<SelectListItem>();
        }
    }

    public class AttachFilesViewModel
    {
        public string FHANumber { get; set; }
        public Guid TaskInstanceId { get; set; }
        public IList<TaskFileModel> TaskFileList { get; set; }
        public string SelectedSectionName { get; set; }
        public bool TransAccessStatus { get; set; }
    }

    public class SharepointAttachmentsModel
    {
        public string FileName { get; set; }
        public double FileSize { get; set; }
        public string RoleName { get; set; }
        public string UploadedBy { get; set; }
        public DateTime UploadedOn { get; set; }
        public string SectionName { get; set; }
        public int FileId { get; set; }
    }
}
