﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
  public class SharePointCommentsModel
    {
        public Guid TaskInstanceId { get; set; }
        public string FHANumber { get; set; }
        public string OGCComments { get; set; }
        public string EnvironmentalComment { get; set; }
        public string SurveyComment { get; set; }
        public string ContractorComment { get; set; }
        public string ClosingComment { get; set; }
        public string AppraisalComment { get; set; }

        // Contractors contract price
        public decimal ContractorContractPrice { get; set; }
        public decimal ContractorSecondaryAmtPaid { get; set; }
        public decimal ContractorAmountPaid { get; set; }
    }

  public class SharePointClosingInfoModel
  {
      public Guid TaskInstanceId { get; set; }
      public string FHANumber { get; set; }
      public bool IsCloserContractor { get; set; }
      public string ContractorContactName { get; set; }
      public int ClosingProgramSpecialistId { get; set; }
      public bool IsCostCertRecieved { get; set; }
      public DateTime? CostCertRecievedDate { get; set; }
      public bool IsCostCertCompletedAndIssued { get; set; }
      public DateTime? CostCertIssueDate { get; set; }
      public bool Is290Completed { get; set; }
      public DateTime? Two90CompletedDate { get; set; }
      public bool IsActiveNCRE { get; set; }
      public DateTime? NCREDueDate { get; set; }
      public decimal NCREInitalBalance { get; set; }
      public DateTime? ClosingPackageRecievedDate { get; set; } 
      public string ClosingComment { get; set; }

      public DateTime? InitialClosingDate { get; set; }
      public int ClosingAEId { get; set; }
      public int ClosingCloserId { get; set; }
        public DateTime? ClosingDateCreatedOn { get; set; }
        public DateTime?  ClosingDateModifiedOn { get; set; }
    }
}
