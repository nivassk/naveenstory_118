﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using HUDHealthcarePortal.Core;
using Model;
using System.Web.Mvc;
using System.Linq;
using System.Collections;
using HUDHealthcarePortal.Model;

namespace Model.Production
{
    public class HUDPamReportFiltersModel
    {

        public DateTime? FromDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}",ApplyFormatInEditMode =true)]        
        public DateTime? ToDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]

        public List<SelectListItem> AllProjectName { get; set; }
        public List<SelectListItem> AllFHANumber { get; set; }
        public List<SelectListItem> AllLoanType { get; set; }
        public IList<SelectListItem> AllStatus { get; set; }
        public IList<SelectListItem> AllFHANumberRequest { get; set; }
        public IList<SelectListItem> AllCorporateCreditReview { get; set; }
        public IList<SelectListItem> AllPortfolioNumberAssignment { get; set; }
        public IList<SelectListItem> AllMasterLeaseNumberAssignment { get; set; }
        public IList<SelectListItem> AllUnderwriter { get; set; }
        public IList<SelectListItem> AllAppraiser { get; set; }
        public IList<SelectListItem> AllEnvironmentalist { get; set; }
        public IList<SelectListItem> AllTitle_Survey { get; set; }
        public IList<SelectListItem> AllExecutedDocuments { get; set; }
		public IList<SelectListItem> AllProjectStage { get; set; }
        //public IList<SelectListItem> Title_Survey { get; set; }
        public string UserRole { get; set; }
        public string ProjectName { get; set; }
        public string FHANumber { get; set; }
        public string LoanType { get; set; }
        public string Status { get; set; }
        public string FHANumberRequest { get; set; }
        public string CorporateCreditReview { get; set; }
        public string PortfolioNumberAssignment { get; set; }
        public string MasterLeaseNumberAssignment { get; set; }
        public string Underwriter { get; set; }
        public string Appraiser { get; set; }
        public string Environmentalist { get; set; }
        public string Title_Survey { get; set; }
        public string ExecutedDocuments { get; set; }
		public string ProjectStage { get; set; }
		public int CreatedBy { get; set; }
        public DateTime CreatedON { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime ModifiedON { get; set; }
        public int FilterID { get; set; }
    }
}
