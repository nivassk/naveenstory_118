﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class Prod_FhaAgingReportViewModel
    {
        public string FHANumber { get; set; }
        public string ProjectTypeName { get; set; }
        public string Lender_Name { get; set; }
        public string LenderUserName { get; set; }
        public string NotificationType { get; set; }
        public bool IsNotificationSent { get; set; }
        public DateTime FhaRequestCompletionDate { get; set; }
    }
}
