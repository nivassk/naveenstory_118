﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class ProjectTypeModel
    {
        public int ProjectTypeId { get; set; }
        public string ProjectTypeName { get; set; }
        public bool IsotherloanType { get; set; }
        public int PageTypeId { get; set; }
    }
}
