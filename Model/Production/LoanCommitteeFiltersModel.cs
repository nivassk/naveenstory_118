﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using HUDHealthcarePortal.Core;
using Model;
using System.Web.Mvc;
using System.Linq;
using System.Collections;
using HUDHealthcarePortal.Model;

namespace Model.Production
{
    public class LoanCommitteeFiltersModel
    {
        public List<SelectListItem> AllLoanCommitteeDate { get; set; }
        public List<SelectListItem> AllUnderwriter { get; set; }
        public List<SelectListItem> AllWorkloadManager { get; set; }
        public List<SelectListItem> AllSequenceNumber { get; set; }
        public List<SelectListItem> AllDay { get; set; }
        public List<SelectListItem> AllProjectType { get; set; }
        public List<SelectListItem> AllProjectTypeNames { get; set; }
        public List<SelectListItem> AllOGCAttorney { get; set; }
        public List<SelectListItem> AllOHPAppraisalReviewer { get; set; }
        public List<SelectListItem> AllLender { get; set; }
        public DateTime? LoanCommitteeDate { get; set; }
        public string Underwriter { get; set; }
        public string WorkloadManager { get; set; }
        public string SequenceNumber { get; set; }
        public string Day { get; set; }
       // public string ProjectType { get; set; }
        public string OGCAttorney { get; set; }
        public string OHPAppraisalReviewer { get; set; }
        public string Lender { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime CreatedON { get; set; }
        public Guid LoanCommitteeId { get; set; }
        public int SequenceId { get; set; }
        public string FHANumber { get; set; }
        public Guid ApplicationTaskinstanceId { get; set; }
        public int LenderId { get; set; }
        public int? ProjectScheduledForLC { get; set; }
        public DateTime? LoanCommitteDate { get; set; }
        public string LCRecommendation { get; set; }
        public int? A7Presentation { get; set; }
        public int? TwoStageSubmittal { get; set; }

        public string LCComments { get; set; }
        public string ProjectName { get; set; }
        public int? ProjectType { get; set; }
        public string ProjectTypeName { get; set; }
        public string NonProfit { get; set; }
        public decimal? LoanAmount { get; set; }
        //public string Underwriter { get; set; }
        public string Wlm { get; set; }
        public string Ogc { get; set; }
        public string Appraiser { get; set; }
        //public string Lender { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public bool Deleted_Ind { get; set; }
    }
}
