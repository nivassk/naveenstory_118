﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{

   
    public class Prod_TaskXrefModel
    {
        public Prod_TaskXrefModel()
        {

        }
        public Guid TaskXrefid { get; set; }
        public int TaskId { get; set; }
        public Guid TaskInstanceId { get; set; }
        public Guid groupTaskID { get; set; }
        public int ? AssignedBy { get; set; }
        public int ? AssignedTo { get; set; }
        public string Comments { get; set; }
        public bool IsReviewer { get; set; }
        public int ViewId { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ? ModifiedBy { get; set; }
        public int Status { get; set; }
        public DateTime ? CompletedOn { get; set; }
        public DateTime? AssignedDate { get; set; }
    }


    //#84 
    public class Prod_Taskassigned
    {
       public int ViewId { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime? CompletedOn { get; set; }
        public int? TaskId { get; set; }
        public Guid TaskXrefid { get; set; }
        public Guid TaskInstanceId { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedTo { get; set; }
        public string Status { get; set; }
        public string FhaType { get; set; }
    }
}
