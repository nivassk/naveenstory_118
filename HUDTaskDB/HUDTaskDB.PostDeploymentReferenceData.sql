﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[Prod_View] WHERE ViewName = 'DAP')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[Prod_View]([ViewName],[ViewTypeId],[ModifiedOn])
	VALUES ('DAP',4,GetDate())
	END
   
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[Prod_View] WHERE ViewName = 'WLM')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[Prod_View]([ViewName],[ViewTypeId],[ModifiedOn])
	VALUES ('WLM',1,GetDate())
	END
   
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[Prod_ViewType] WHERE ViewTypeId = 4)
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[Prod_ViewType]([ViewTypeId],[ViewType],[ModifiedOn])
	VALUES (4,'HudProdWlmAmendment',GetDate())
	END   
END

BEGIN
IF EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[Prod_View] WHERE ViewName = 'WLM')
	BEGIN
		update [$(DatabaseName)].[dbo].[Prod_View] set ViewTypeId=4 where  ViewName = 'WLM'
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[Prod_FolderStructure] WHERE FolderKey = '38')
	BEGIN
	INSERT INTO [dbo].[Prod_FolderStructure]
           ([FolderKey]
           ,[FolderName]
           ,[ParentKey]
           ,[level]
           ,[ViewTypeId]
           ,[FolderSortingNumber]
           ,[SubfolderSequence])
     VALUES
           (38
           ,'AmendmentTemplate'
           ,NULL
           ,0
           ,2
           ,NULL
           ,NULL)
	END
END
BEGIN 
--delete Prod_FolderStructure where FolderKey in (33,34,35,36,37)
update Prod_FolderStructure set FolderName='Amendment',SubfolderSequence=NULL where FolderKey=32
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '1')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('1',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '2')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('2',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '3')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('3',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '4')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('4',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '5')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('5',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '6')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('6',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '7')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('7',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '8')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('8',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '9')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('9',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '10')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('10',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '11')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('11',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '12')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('12',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '13')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('13',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '14')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('14',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType] WHERE FirmCommitmentAmendment = '15')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[FirmCommitmentAmendmentType]([FirmCommitmentAmendment],[CreatedOn],[ModifiedOn])
	VALUES ('15',GetDate(),NULL)
	END
END



BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SaluatationType] WHERE Saluatation = 'Ms.')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SaluatationType]([Saluatation],[CreatedOn],[ModifiedOn])
	VALUES ('Ms.',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SaluatationType] WHERE Saluatation = 'Mr.')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SaluatationType]([Saluatation],[CreatedOn],[ModifiedOn])
	VALUES ('Mr.',GetDate(),NULL)
	END
END


BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[PartyType] WHERE Party = 'Lender''s')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[PartyType]([Party],[CreatedOn],[ModifiedOn])
	VALUES ('Lender''s',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[PartyType] WHERE Party = 'Borrower''s')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[PartyType]([Party],[CreatedOn],[ModifiedOn])
	VALUES ('Borrower''s',GetDate(),NULL)
	END
END



BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[PartyType] WHERE Party = 'Project''s')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[PartyType]([Party],[CreatedOn],[ModifiedOn])
	VALUES ('Project''s',GetDate(),NULL)
	END
END



BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[DepositType] WHERE Deposit = 'Additional')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[DepositType]([Deposit],[CreatedOn],[ModifiedOn])
	VALUES ('Additional',GetDate(),NULL)
	END
END

BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[DepositType] WHERE Deposit = 'Initial')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[DepositType]([Deposit],[CreatedOn],[ModifiedOn])
	VALUES ('Initial',GetDate(),NULL)
	END
END



BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[EscrowEstimateType] WHERE EscrowEstimate = '110')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[EscrowEstimateType]([EscrowEstimate],[CreatedOn],[ModifiedOn])
	VALUES ('110',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[EscrowEstimateType] WHERE EscrowEstimate = '120')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[EscrowEstimateType]([EscrowEstimate],[CreatedOn],[ModifiedOn])
	VALUES ('120',GetDate(),NULL)
	END
END


BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[RepairCostEstimateType] WHERE RepairCostEstimate = '10')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[RepairCostEstimateType]([RepairCostEstimate],[CreatedOn],[ModifiedOn])
	VALUES ('10',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[RepairCostEstimateType] WHERE RepairCostEstimate = '20')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[RepairCostEstimateType]([RepairCostEstimate],[CreatedOn],[ModifiedOn])
	VALUES ('20',GetDate(),NULL)
	END
END


BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '1')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('1',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '2')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('2',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '3')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('3',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '4')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('4',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '5')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('5',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '6')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('6',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '7')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('7',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '8')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('8',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '9')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('9',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '10')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('10',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '11')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('11',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '12')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('12',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '13')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('13',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '14')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('14',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '15')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('15',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '16')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('16',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '17')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('17',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '18')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('18',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '19')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('19',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[SpecialConditionType] WHERE SpecialCondition = '20')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[SpecialConditionType]([SpecialCondition],[CreatedOn],[ModifiedOn])
	VALUES ('20',GetDate(),NULL)
	END
END


BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[ExhibitLetterType] WHERE ExhibitLetter = 'A')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[ExhibitLetterType]([ExhibitLetter],[CreatedOn],[ModifiedOn])
	VALUES ('A',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[ExhibitLetterType] WHERE ExhibitLetter = 'B')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[ExhibitLetterType]([ExhibitLetter],[CreatedOn],[ModifiedOn])
	VALUES ('B',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[ExhibitLetterType] WHERE ExhibitLetter = 'C')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[ExhibitLetterType]([ExhibitLetter],[CreatedOn],[ModifiedOn])
	VALUES ('C',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[ExhibitLetterType] WHERE ExhibitLetter = 'D')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[ExhibitLetterType]([ExhibitLetter],[CreatedOn],[ModifiedOn])
	VALUES ('D',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[ExhibitLetterType] WHERE ExhibitLetter = 'E')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[ExhibitLetterType]([ExhibitLetter],[CreatedOn],[ModifiedOn])
	VALUES ('E',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[ExhibitLetterType] WHERE ExhibitLetter = 'F')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[ExhibitLetterType]([ExhibitLetter],[CreatedOn],[ModifiedOn])
	VALUES ('F',GetDate(),NULL)
	END
END
BEGIN
	IF NOT EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[ExhibitLetterType] WHERE ExhibitLetter = 'G')
	BEGIN
	INSERT INTO [$(DatabaseName)].[dbo].[ExhibitLetterType]([ExhibitLetter],[CreatedOn],[ModifiedOn])
	VALUES ('G',GetDate(),NULL)
	END
END

/*  Amendments Folder Insert -- Not valid as per Userstory 3962 */

--insert into [$(DatabaseName)].[dbo].[Prod_FolderStructure] 
--select 32,'Section Exhibits A',null,0,4,23,'A'
--where not exists (select * from [$(DatabaseName)].[dbo].[Prod_FolderStructure]  where FolderKey = 32);
--insert into [$(DatabaseName)].[dbo].[Prod_FolderStructure] 
--select 33,'Section Exhibits B',null,0,4,24,'A'
--where not exists (select * from [$(DatabaseName)].[dbo].[Prod_FolderStructure]  where FolderKey = 33);
--insert into [$(DatabaseName)].[dbo].[Prod_FolderStructure]  
--select 34,'Section Exhibits C',null,0,4,25,'A'
--where not exists (select * from [$(DatabaseName)].[dbo].[Prod_FolderStructure]  where FolderKey = 34);
--insert into [$(DatabaseName)].[dbo].[Prod_FolderStructure]  
--select 35,'92264A ORCF',null,0,4,26,'A'
--where not exists (select * from [$(DatabaseName)].[dbo].[Prod_FolderStructure]  where FolderKey = 35);
--insert into [$(DatabaseName)].[dbo].[Prod_FolderStructure]  
--select 36,'Amendment Letter',null,0,4,27,'A'
--where not exists (select * from [$(DatabaseName)].[dbo].[Prod_FolderStructure]  where FolderKey = 36);
--insert into [$(DatabaseName)].[dbo].[Prod_FolderStructure]  
--select 37,'Other',null,0,4,28,'A'
--where not exists (select * from [$(DatabaseName)].[dbo].[Prod_FolderStructure]  where FolderKey = 37);


--Task 3630: Add Production Indexes for Performance Issues--
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K16_K17_K2' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K16_K17_K2] ON [dbo].[Task]
(
	[PageTypeId] ASC,
	[FhaNumber] ASC,
	[TaskInstanceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K16_K17_K9_K2' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K16_K17_K9_K2] ON [dbo].[Task]
(
	[PageTypeId] ASC,
	[FhaNumber] ASC,
	[TaskStepId] ASC,
	[TaskInstanceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K16_K17_K9_K2_1_3' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K16_K17_K9_K2_1_3] ON [dbo].[Task]
(
	[PageTypeId] ASC,
	[FhaNumber] ASC,
	[TaskStepId] ASC,
	[TaskInstanceId] ASC
)
INCLUDE ( 	[TaskId],
	[SequenceId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K16D_2_17' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K16D_2_17] ON [dbo].[Task]
(
	[PageTypeId] DESC
)
INCLUDE ( 	[TaskInstanceId],
	[FhaNumber]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K16D_K17_2' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K16D_K17_2] ON [dbo].[Task]
(
	[PageTypeId] DESC,
	[FhaNumber] ASC
)
INCLUDE ( 	[TaskInstanceId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K17_K16_2' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K17_K16_2] ON [dbo].[Task]
(
	[FhaNumber] ASC,
	[PageTypeId] ASC
)
INCLUDE ( 	[TaskInstanceId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K17_K16D_3_7' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K17_K16D_3_7] ON [dbo].[Task]
(
	[FhaNumber] ASC,
	[PageTypeId] DESC
)
INCLUDE ( 	[SequenceId],
	[StartTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K2_K1' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K2_K1] ON [dbo].[Task]
(
	[TaskInstanceId] ASC,
	[TaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K2_K1_7_9_16' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K2_K1_7_9_16] ON [dbo].[Task]
(
	[TaskInstanceId] ASC,
	[TaskId] ASC
)
INCLUDE ( 	[StartTime],
	[TaskStepId],
	[PageTypeId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K2_K1_K3' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K2_K1_K3] ON [dbo].[Task]
(
	[TaskInstanceId] ASC,
	[TaskId] ASC,
	[SequenceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K2_K1_K3_K17_K4_K5_K15_6_7_8_9_10_11_12_13_14_16' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K2_K1_K3_K17_K4_K5_K15_6_7_8_9_10_11_12_13_14_16] ON [dbo].[Task]
(
	[TaskInstanceId] ASC,
	[TaskId] ASC,
	[SequenceId] ASC,
	[FhaNumber] ASC,
	[AssignedBy] ASC,
	[AssignedTo] ASC,
	[IsReassigned] ASC
)
INCLUDE ( 	[DueDate],
	[StartTime],
	[Notes],
	[TaskStepId],
	[DataKey1],
	[DataKey2],
	[DataStore1],
	[DataStore2],
	[TaskOpenStatus],
	[PageTypeId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K2_K3_16_17' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K2_K3_16_17] ON [dbo].[Task]
(
	[TaskInstanceId] ASC,
	[SequenceId] ASC
)
INCLUDE ( 	[PageTypeId],
	[FhaNumber]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K2_K3_7' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K2_K3_7] ON [dbo].[Task]
(
	[TaskInstanceId] ASC,
	[SequenceId] ASC
)
INCLUDE ( 	[StartTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K2_K3_9' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K2_K3_9] ON [dbo].[Task]
(
	[TaskInstanceId] ASC,
	[SequenceId] ASC
)
INCLUDE ( 	[TaskStepId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K2_K3_K7' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K2_K3_K7] ON [dbo].[Task]
(
	[TaskInstanceId] ASC,
	[SequenceId] ASC,
	[StartTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K2_K3D_K16_K5_K9_K1_7' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K2_K3D_K16_K5_K9_K1_7] ON [dbo].[Task]
(
	[TaskInstanceId] ASC,
	[SequenceId] DESC,
	[PageTypeId] ASC,
	[AssignedTo] ASC,
	[TaskStepId] ASC,
	[TaskId] ASC
)
INCLUDE ( 	[StartTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K2_K3D_K16_K9_K5_K1_7' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K2_K3D_K16_K9_K5_K1_7] ON [dbo].[Task]
(
	[TaskInstanceId] ASC,
	[SequenceId] DESC,
	[PageTypeId] ASC,
	[TaskStepId] ASC,
	[AssignedTo] ASC,
	[TaskId] ASC
)
INCLUDE ( 	[StartTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K2_K4_K5_K9_K1_K16_K3_K7_8' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K2_K4_K5_K9_K1_K16_K3_K7_8] ON [dbo].[Task]
(
	[TaskInstanceId] ASC,
	[AssignedBy] ASC,
	[AssignedTo] ASC,
	[TaskStepId] ASC,
	[TaskId] ASC,
	[PageTypeId] ASC,
	[SequenceId] ASC,
	[StartTime] ASC
)
INCLUDE ( 	[Notes]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K2_K5_K9_K16_K1_7' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K2_K5_K9_K16_K1_7] ON [dbo].[Task]
(
	[TaskInstanceId] ASC,
	[AssignedTo] ASC,
	[TaskStepId] ASC,
	[PageTypeId] ASC,
	[TaskId] ASC
)
INCLUDE ( 	[StartTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K2_K5_K9_K16_K4_K3_K1_7' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K2_K5_K9_K16_K4_K3_K1_7] ON [dbo].[Task]
(
	[TaskInstanceId] ASC,
	[AssignedTo] ASC,
	[TaskStepId] ASC,
	[PageTypeId] ASC,
	[AssignedBy] ASC,
	[SequenceId] ASC,
	[TaskId] ASC
)
INCLUDE ( 	[StartTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K2_K9_K7' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K2_K9_K7] ON [dbo].[Task]
(
	[TaskInstanceId] ASC,
	[TaskStepId] ASC,
	[StartTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K3_K16_K17_7' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K3_K16_K17_7] ON [dbo].[Task]
(
	[SequenceId] ASC,
	[PageTypeId] ASC,
	[FhaNumber] ASC
)
INCLUDE ( 	[StartTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K4' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K4] ON [dbo].[Task]
(
	[AssignedBy] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K4_K2' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K4_K2] ON [dbo].[Task]
(
	[AssignedBy] ASC,
	[TaskInstanceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K5' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K5] ON [dbo].[Task]
(
	[AssignedTo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K5_K2' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K5_K2] ON [dbo].[Task]
(
	[AssignedTo] ASC,
	[TaskInstanceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K9_K2_K3D_K4_K16_K5_K1_7' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K9_K2_K3D_K4_K16_K5_K1_7] ON [dbo].[Task]
(
	[TaskStepId] ASC,
	[TaskInstanceId] ASC,
	[SequenceId] DESC,
	[AssignedBy] ASC,
	[PageTypeId] ASC,
	[AssignedTo] ASC,
	[TaskId] ASC
)
INCLUDE ( 	[StartTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Task_11_597577167__K9_K2_K5_K4_K16_K3_K1_7' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Task_11_597577167__K9_K2_K5_K4_K16_K3_K1_7] ON [dbo].[Task]
(
	[TaskStepId] ASC,
	[TaskInstanceId] ASC,
	[AssignedTo] ASC,
	[AssignedBy] ASC,
	[PageTypeId] ASC,
	[SequenceId] ASC,
	[TaskId] ASC
)
INCLUDE ( 	[StartTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
