﻿CREATE PROCEDURE [dbo].[GetMailRecipients](
   @Taskinstanceid uniqueidentifier,
   
   @Viewid int

)
AS
    BEGIN
	declare @Lenderemail nvarchar(max)

	select top 1 @Lenderemail= assignedby  from [$(DatabaseName)].dbo.task  order by starttime asc

	    		
 select Auth.firstname + ' ' + Auth.Lastname as FullName ,ad.addressline1 + ' ,' + isnull(ad.addressline2,'')  + ' ,' + ad.City + ' ,' + + ad.statecode  +' ,' + ad.zip  as Address,Auth.username as Email,@Lenderemail as LenderEmail
from [$(LiveDB)].dbo.hcp_authentication Auth
inner join [$(LiveDB)].dbo.Address Ad on Auth.addressid=ad.addressid
inner join [$(DatabaseName)].dbo.Prod_TaskXref xref on xref.assignedto=auth.userid

where xref.taskinstanceid=@Taskinstanceid  and xref.viewid=@Viewid
  
  
  end


