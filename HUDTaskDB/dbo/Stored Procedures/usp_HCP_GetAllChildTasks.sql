﻿CREATE PROCEDURE usp_HCP_GetAllChildTasks 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
       SELECT OP.ProjectActionFormId,
       op.FhaNumber,
       op.PropertyName,
       op.RequestDate,
       op.ServicerSubmissionDate,
       op.ProjectActionStartDate,
       op.ProjectActionTypeId,
       op.RequestStatus,
       op.MytaskId,
       op.CreatedOn,
       op.ModifiedOn,
       op.CreatedBy,
       op.ModifiedBy,
       op.ServicerComments,
       op.AEComments,
       op.TaskInstanceId,
       op.RequesterName,
       op.IsAddressChange
       FROM dbo.ParentChildTask PT
       JOIN [$(LiveDB)].dbo.OPAForm OP
       ON PT.ParentTaskInstanceId = OP.TaskInstanceId
END
GO

