﻿CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetPAMGridTwoResults]
	(@TaskInstanceId uniqueidentifier)
AS
BEGIN
	Declare @FHANumber varchar(9)
	set @FHANumber = (select top 1 FHANumber from [$(LiveDB)].dbo.Prod_FHANumberRequest 
	where TaskinstanceId = @TaskInstanceId)
	
	Declare @PageTypeId int

	Declare @ProjectStatus varchar(50)

	Declare @AppraiserStatus varchar(50)	

	Declare @EnvStatus varchar(50)	

	Declare @TnSStatus varchar(50)
	
	Declare @IsContractorAssigned bit
	
	Declare @TaskStatus int	

	Declare @CurrentTaskInstanceId uniqueidentifier					

	IF(@FHANumber is not null)
		Begin
			set @PageTypeId = (select top 1 PageTypeId from Task where FhaNumber = @FHANumber order by PageTypeId desc)
			set @CurrentTaskInstanceId = (select top 1 TaskInstanceId from Task where FhaNumber = @FHANumber and PageTypeId = @PageTypeId)
			IF(@PageTypeId > 4)
				Begin 
					If exists (select top 1 * from Task tk join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId
								where TaskStepId = 17 and FhaNumber = @FHANumber and PageTypeId = @PageTypeId)
						Begin
							set @ProjectStatus = case when exists (select top 1 * from Task tk join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId
																	where TaskStepId = 17 and FhaNumber = @FHANumber and PageTypeId = @PageTypeId and ViewId = 1)
													  then (select case when [Status] = 15 then 'Complete' 
																 when  [Status] = 18 then 'In-Process' else 'Unassigned' end
															from Task tk
															join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId
															where FhaNumber = @FHANumber and PageTypeId = @PageTypeId 
															and Viewid = 1)
													  else 'Unassigned' end

							set @AppraiserStatus = case when exists (select top 1 * from Task tk join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId
																	where TaskStepId = 17 and FhaNumber = @FHANumber and PageTypeId = @PageTypeId and ViewId = 12)
													  then (select case when [Status] = 15 then 'Complete' 
																 when  [Status] = 18 then 'In-Process' else 'Unassigned' end
															from Task tk
															join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId
															where FhaNumber = @FHANumber and PageTypeId = @PageTypeId and Viewid = 12)
													  else 'Unassigned' end
												
							set @EnvStatus = case when exists (select top 1 * from Task tk join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId
																	where TaskStepId = 17 and FhaNumber = @FHANumber and PageTypeId = @PageTypeId and ViewId = 3)
													  then (select case when [Status] = 15 then 'Complete' 
																 when  [Status] = 18 then 'In-Process' else 'Unassigned' end
															from Task tk
															join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId
															where FhaNumber = @FHANumber and PageTypeId = @PageTypeId and Viewid = 3)
													  else 'Unassigned' end
													  										
							set @TnSStatus = case when exists (select top 1 * from Task tk join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId
																	where TaskStepId = 17 and FhaNumber = @FHANumber and PageTypeId = @PageTypeId and ViewId = 2)
													  then (select case when [Status] = 15 then 'Complete' 
																 when  [Status] = 18 then 'In-Process' else 'Unassigned' end
															from Task tk
															join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId
															where FhaNumber = @FHANumber and PageTypeId = @PageTypeId and Viewid = 2)
													  else 'Unassigned' end
							set @IsContractorAssigned = case when exists (select top 1 TaskXrefid from Task tk
														join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId
														where FhaNumber = @FHANumber and PageTypeId = @PageTypeId and Viewid in (6,7))
														then Convert(bit,1) else Convert(bit,0) end							
						End
					Else 
						Begin
							set @ProjectStatus = 'Unassigned'
							set @AppraiserStatus = 'Unassigned'
							set @EnvStatus = 'Unassigned'
							set @TnSStatus = 'Unassigned'
							set @IsContractorAssigned = Convert(bit,0)
						End
					select 
						(case when @PageTypeId = 5 then 'Application'
							when @PageTypeId = 10 then 'Draft Closing' else 'Executed Closing' end) as ProjectStage,
						@ProjectStatus as ProjectStatus,
						@AppraiserStatus as AppraiserStatus,
						@EnvStatus as EnvironmentalistStatus,
						@TnsStatus as TitleSurveyStatus,
						@IsContractorAssigned as IsContractorAssigned,
						@CurrentTaskInstanceId as CurrentTaskInstanceId		 
				End
			ELSE
				Begin
				
					set @TaskStatus = (select top 1 TaskStepId from Task where taskinstanceid = @TaskInstanceId order by SequenceId desc)
					set @ProjectStatus = (select case when @TaskStatus = 17 then 'Unassigned'
													  when @TaskStatus = 18 then 'In-Process'
													  when @TaskStatus = 19 then 'Complete' end)
					select 'FHA Number Request' as ProjectStage,
						@ProjectStatus as ProjectStatus,
						'N/A' as ProjectStatus,
						'N/A' as AppraiserStatus,
						'N/A' as EnvironmentalistStatus,
						'N/A' as TitleSurveyStatus,
						Convert(bit,0) as IsContractorAssigned,
						@CurrentTaskInstanceId as CurrentTaskInstanceId	
				End				
		End
	ELSE 
		Begin
			set @CurrentTaskInstanceId = @TaskInstanceId
			set @TaskStatus = (select top 1 TaskStepId from Task where taskinstanceid = @TaskInstanceId order by SequenceId desc)
			set @ProjectStatus = (select case when @TaskStatus = 17 then 'Unassigned'
											  when @TaskStatus = 18 then 'In-Process'
											  when @TaskStatus = 19 then 'Complete' end)
			select 'FHA Number Request' as ProjectStage,
				@ProjectStatus as ProjectStatus,
				'N/A' as ProjectStatus,
				'N/A' as AppraiserStatus,
				'N/A' as EnvironmentalistStatus,
				'N/A' as TitleSurveyStatus,
				Convert(bit,0) as IsContractorAssigned,
				@CurrentTaskInstanceId as CurrentTaskInstanceId	
		End

    
END
