﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetProdLenderPAMExcel]
(
       @RoleList varchar(MAX),
       @LAMList varchar(MAX),
       @BAMList varchar(MAX),
       @LARList varchar(MAX),
       @StartDate datetime,
       @EndDate datetime,
       @Status nvarchar(50),
       @UserId int,
       @UserLenderId int,
       @AppType nvarchar(50),
       @ProgramType nvarchar(50)
)
AS
BEGIN
       DECLARE @NewLineChar AS CHAR(2) = CHAR(13) + CHAR(10) 
       Declare @SelectedLendersList table (LenderUserId int)

       --get role name for user
       Declare @UserRoleName nvarchar(256)
       select @UserRoleName = webRoles.[RoleName] from [$(LiveDB)].[dbo].[webpages_UsersInRoles] webRolesUser 
       inner join [$(LiveDB)].[dbo].[webpages_Roles] webRoles 
       on webRolesUser.RoleId = webRoles.RoleId
       where webRolesUser.UserId = @UserId

       --if user role name is lar, then insert userid to lenders list table. 
       if (@UserRoleName ='LenderAccountRepresentative')
              insert into @SelectedLendersList values (@UserId)
       else if (@UserRoleName ='LenderAccountManager' or @UserRoleName ='BackupAccountManager')
       begin

              if(@RoleList IS NULL OR @RoleList = '')
              begin
                     INSERT INTO @SelectedLendersList  select userMaster.[UserID] from [$(LiveDB)].[dbo].[HCP_Authentication] userMaster
                                                                                  inner join [$(LiveDB)].[dbo].[User_Lender] userLender
                                                                                  on userLender.[User_ID] = userMaster.UserID
                                                                                  inner join [$(LiveDB)].[dbo].[webpages_UsersInRoles] userRoles
                                                                                   on userRoles.UserId = userMaster.UserID
                                                                                  where userLender.Lender_ID = @UserLenderId  and userRoles.RoleId in(13,14,15)
              end
              else
              begin

                     
              if CHARINDEX('1',@RoleList) > 0
              begin
                     --IF @LAMList IS NULL OR @LAMList = ''
                     --     INSERT INTO @SelectedLendersList    select userMaster.[UserID] from [dbo].[HCP_Authentication] userMaster
                     --                                                                   inner join [dbo].[User_Lender] userLender
                     --                                                                   on userLender.[User_ID] = userMaster.UserID
                     --                                                                   inner join [dbo].[webpages_UsersInRoles] userRoles
                     --                                                                   on userRoles.UserId = userMaster.UserID
                     --                                                                   where userLender.Lender_ID = @UserLenderId  and userRoles.RoleId =15
                     --ELSE 
                           INSERT INTO @SelectedLendersList SELECT * FROM [$(LiveDB)].dbo.fn_StringSplitterToInt(@LAMList)
              end

                     
              if CHARINDEX('2',@RoleList) > 0
              begin
                     --IF @BAMList IS NULL OR @BAMList = ''
                     --     INSERT INTO @SelectedLendersList   select userMaster.[UserID] from [dbo].[HCP_Authentication] userMaster
                     --                                                                   inner join [dbo].[User_Lender] userLender
                     --                                                                   on userLender.[User_ID] = userMaster.UserID
                     --                                                                   inner join [dbo].[webpages_UsersInRoles] userRoles
                     --                                                                   on userRoles.UserId = userMaster.UserID
                     --                                                                   where userLender.Lender_ID = @UserLenderId  and userRoles.RoleId =14
                     --ELSE 
                           INSERT INTO @SelectedLendersList SELECT * FROM [$(LiveDB)].dbo.fn_StringSplitterToInt(@BAMList)
              end

                     
              if CHARINDEX('3',@RoleList) > 0
              begin
                     --IF @LARList IS NULL OR @LARList = ''
                     --     INSERT INTO @SelectedLendersList    select userMaster.[UserID] from [dbo].[HCP_Authentication] userMaster
                     --                                                                   inner join [dbo].[User_Lender] userLender
                     --                                                                   on userLender.[User_ID] = userMaster.UserID
                     --                                                                   inner join [dbo].[webpages_UsersInRoles] userRoles
                     --                                                                   on userRoles.UserId = userMaster.UserID
                     --                                                                   where userLender.Lender_ID = @UserLenderId  and userRoles.RoleId =13
                     --ELSE 
                           INSERT INTO @SelectedLendersList SELECT * FROM [$(LiveDB)].dbo.fn_StringSplitterToInt(@LARList)
              end

                     
              if CHARINDEX('0',@RoleList) > 0
              begin
                     IF NOT EXISTS(SELECT 1 FROM @SelectedLendersList  WHERE LenderUserId = @UserId)
                     insert into @SelectedLendersList values (@UserId)
              end
              else
              begin
                     IF EXISTS(SELECT 1 FROM @SelectedLendersList  WHERE LenderUserId = @UserId)
                           delete from  @SelectedLendersList  WHERE LenderUserId = @UserId
              end
              end
       end

       if (@StartDate is not null and @EndDate is null)    set @EndDate =   GETUTCDATE() 
       if (@StartDate is null and @EndDate is not null)    set @StartDate = '01-01-2015'

       DECLARE @SelectedPageType TABLE (PagetypeId INT)              
       IF @AppType IS NULL OR @AppType = ''
              INSERT INTO @SelectedPageType  SELECT PagetypeId FROM  [$(LiveDB)].dbo.HCP_PageType where moduleid = 2 
       ELSE 
              INSERT INTO @SelectedPageType SELECT * FROM [$(LiveDB)].dbo.fn_StringSplitterToInt(@AppType)

    DECLARE @SelectedProjectType TABLE (ProjectTypeId INT)           
       IF @ProgramType IS NULL OR @ProgramType = ''
              INSERT INTO @SelectedProjectType  SELECT ProjectTypeId FROM  [$(LiveDB)].dbo.[Prod_ProjectType] 
       ELSE 
              INSERT INTO @SelectedProjectType SELECT * FROM [$(LiveDB)].dbo.fn_StringSplitterToInt(@ProgramType)

     DECLARE @SelectedStatus TABLE (TaskStepId INT)    
       IF @Status IS NULL OR @Status = ''
       begin
              INSERT INTO @SelectedStatus values (14)
              INSERT INTO @SelectedStatus values (15)
              INSERT INTO @SelectedStatus values (17)
              INSERT INTO @SelectedStatus values (18)
              INSERT INTO @SelectedStatus values (19)
       end
       ELSE 
       begin
              if(@Status like '%1%')
              begin
                     INSERT INTO @SelectedStatus values (17)
              end
              if(@Status like '%2%')
              begin
                     INSERT INTO @SelectedStatus  values(14)
                     INSERT INTO @SelectedStatus  values(18)
              end
              if(@Status like '%3%')
              begin
                     INSERT INTO @SelectedStatus values (15)
                     INSERT INTO @SelectedStatus values (19)
              end
       end

              


       CREATE TABLE #LenderPAMReport
       (
              taskInstanceId uniqueidentifier,
              FHANumber NVARCHAR(15), 
              ProjectName NVARCHAR(100), 
              LoanType NVARCHAR(100),
              ProductionTaskType NVARCHAR(100),
              StartDate DATETIME,
              EndDate DATETIME,
              DaysActive int,
              LoanAmount decimal,
              LenderName NVARCHAR(100),
              LenderRole varchar(50),
              IsTaskReassigned bit,
              StatusId int,
              ProcessedBy NVARCHAR(100),
              ViewName  varchar(50),
              IsRAI bit

       );
       
       --Gets Main Task Application Process
       INSERT INTO #LenderPAMReport
       Select t.TaskInstanceId as taskInstanceId,
                 t.[FhaNumber],
                 fr.ProjectName,
                 pt.ProjectTypeName,
                 pg.PageTypeDescription as ProductionTaskType,
                 t.StartTime as StartDate,
                 (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 15) THEN tend.StartTime ELSE null END) as  enddate,
                 (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 15) THEN DATEDIFF(DAY,t.StartTime,tend.StartTime) ELSE DATEDIFF(DAY,t.StartTime,GETUTCDATE()) END) as DaysActive,             
                 fr.LoanAmount,
                 t.AssignedBy as LenderName,
                 r.RoleName as LenderRole,
                 (CASE WHEN t.IsReassigned is null THEN 0 ELSE t.IsReassigned END) as IsTaskReassigned , 
                 ts.TaskStepId as StatusId,
                 t.AssignedTo as ProcessedBy,
                 (CASE WHEN t.PageTypeId = 4 THEN 'InsertFha'
                     ELSE 'UnderWriter' END) as ViewName,
                     0 as IsRAI

       from [$(DatabaseName)].dbo.Task t
       inner join [$(LiveDB)].dbo.[Prod_FHANumberRequest] fr on fr.FHANumber = t.FHANumber
       inner join [$(LiveDB)].[dbo].[Prod_ProjectType] pt on pt.ProjectTypeId = fr.ProjectTypeId
       inner join  [$(LiveDB)].[dbo].HCP_PageType pg on pg.PageTypeId = t.PageTypeId
       left join(select tfinal.* from [$(DatabaseName)].dbo.Task tfinal
                       inner join (select t1.TaskInstanceId,max(starttime) as taskstarttime
                                         from  [$(DatabaseName)].dbo.Task t1 group by t1.TaskInstanceId ) tmaxdate on tmaxdate.TaskInstanceId = tfinal.TaskInstanceId and tmaxdate.taskstarttime = tfinal.StartTime
                                         )tend on tend.TaskInstanceId= t.TaskInstanceId 
       inner join [$(LiveDB)].dbo.HCP_Authentication a on a.UserName = t.AssignedBy
       inner join [$(LiveDB)].dbo.webpages_UsersInRoles ur on ur.UserId = a.UserID
       INNER JOIN @SelectedLendersList lenders ON lenders.LenderUserId = ur.UserId
       inner join @SelectedPageType spt on spt.PageTypeId = t.PageTypeId
       inner join @SelectedProjectType sproj on sproj.ProjectTypeId = pt.ProjectTypeId
       inner join @SelectedStatus sStat on sStat.TaskStepId = t.TaskStepId
       inner join [$(LiveDB)].dbo.webpages_Roles r on r.RoleId = ur.RoleId
       inner join [$(DatabaseName)].dbo.TaskStep ts on ts.TaskStepId = tend.TaskStepId
       left join [$(DatabaseName)].dbo.ParentChildTask pc on pc.ChildTaskInstanceId = t.TaskInstanceId
       
       where t.SequenceId = 0  and pc.ChildTaskInstanceId is null and
              --R4R.ServicerSubmissionDate BETWEEN @StartDate AND @EndDate
         (  (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND   t.StartTime>= @StartDate and t.StartTime < DATEADD(day,1,@EndDate))
                     OR
                     ((@StartDate IS NULL AND @EndDate IS NULL) AND t.StartTime <= GETUTCDATE()) ) 

       --Gets Miain Task of FHA Request  
    INSERT INTO #LenderPAMReport
       Select t.TaskInstanceId as taskInstanceId,
                (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 19) THEN tend.FhaNumber ELSE null END) as FhaNumber,
                 fr.ProjectName,
                 pt.ProjectTypeName,
                 pg.PageTypeDescription as ProductionTaskType,
                 t.StartTime as StartDate,
                 (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 19) THEN tend.StartTime ELSE null END) as  enddate,
                 (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 19) THEN DATEDIFF(DAY,t.StartTime,tend.StartTime) ELSE DATEDIFF(DAY,t.StartTime,GETUTCDATE()) END) as DaysActive,             
                 fr.LoanAmount,
                 t.AssignedBy as LenderName,
                 r.RoleName as LenderRole,
                 (CASE WHEN t.IsReassigned is null THEN 0 ELSE t.IsReassigned END) as IsTaskReassigned , 
                 ts.TaskStepId as StatusId,
                 t.AssignedTo as ProcessedBy,
                 (CASE WHEN t.PageTypeId = 4 THEN 'InsertFha'
                     ELSE 'UnderWriter' END) as ViewName,
                     0 as IsRAI

       from [$(DatabaseName)].dbo.Task t
       inner join [$(LiveDB)].dbo.[Prod_FHANumberRequest] fr on fr.TaskinstanceId = t.TaskInstanceId
       inner join [$(LiveDB)].[dbo].[Prod_ProjectType] pt on pt.ProjectTypeId = fr.ProjectTypeId
       inner join  [$(LiveDB)].[dbo].HCP_PageType pg on pg.PageTypeId = t.PageTypeId
       left join(select tfinal.* from [$(DatabaseName)].dbo.Task tfinal
                       inner join (select t1.TaskInstanceId,max(starttime) as taskstarttime
                                         from  [$(DatabaseName)].dbo.Task t1 group by t1.TaskInstanceId ) tmaxdate on tmaxdate.TaskInstanceId = tfinal.TaskInstanceId and tmaxdate.taskstarttime = tfinal.StartTime
                                         )tend on tend.TaskInstanceId= t.TaskInstanceId 
       inner join [$(LiveDB)].dbo.HCP_Authentication a on a.UserName = t.AssignedBy
       inner join [$(LiveDB)].dbo.webpages_UsersInRoles ur on ur.UserId = a.UserID
       INNER JOIN @SelectedLendersList lenders ON lenders.LenderUserId = ur.UserId
       inner join @SelectedPageType spt on spt.PageTypeId = t.PageTypeId
       inner join @SelectedProjectType sproj on sproj.ProjectTypeId = pt.ProjectTypeId
       inner join @SelectedStatus sStat on sStat.TaskStepId = t.TaskStepId
       inner join [$(LiveDB)].dbo.webpages_Roles r on r.RoleId = ur.RoleId
       inner join [$(DatabaseName)].dbo.TaskStep ts on ts.TaskStepId = tend.TaskStepId
       left join [$(DatabaseName)].dbo.ParentChildTask pc on pc.ChildTaskInstanceId = t.TaskInstanceId
       
       where t.SequenceId = 0  and pc.ChildTaskInstanceId is null and
              --R4R.ServicerSubmissionDate BETWEEN @StartDate AND @EndDate
         (  (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND   t.StartTime>= @StartDate and t.StartTime < DATEADD(day,1,@EndDate))
                     OR
                     ((@StartDate IS NULL AND @EndDate IS NULL) AND t.StartTime <= GETUTCDATE()) ) 

    CREATE TABLE #TaskInstanceList
       (
              taskInstanceId uniqueidentifier
       )
       -- Gets the list of parent taskids
       insert into #TaskInstanceList
       select distinct taskInstanceId from #LenderPAMReport

       -- Gets Xref Tasks for Application Req Types
       INSERT INTO #LenderPAMReport
       Select t.TaskInstanceId as taskInstanceId,
                 t.[FhaNumber],
                 fr.ProjectName,
                 pt.ProjectTypeName,
                 pg.PageTypeDescription as ProductionTaskType,
                 t.StartTime as StartDate,
                 (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 15) THEN tend.StartTime ELSE null END) as  enddate,
                 (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 15) THEN DATEDIFF(DAY,t.StartTime,tend.StartTime) ELSE DATEDIFF(DAY,t.StartTime,GETUTCDATE()) END) as DaysActive,             
                 fr.LoanAmount,
                 t.AssignedBy as LenderName,
                 r.RoleName as LenderRole,
                 (CASE WHEN t.IsReassigned is null THEN 0 ELSE t.IsReassigned END) as IsTaskReassigned , 
                 ts.TaskStepId as StatusId,
                 xrefAut.FirstName + ' ' + xrefAut.LastName as ProcessedBy,
                 pv.ViewName as ViewName,
                 (CASE WHEN pc.ChildTaskInstanceId is null THEN 0 ELSE 1 END) as IsRAI 

       from [$(DatabaseName)].dbo.Prod_TaskXref txref
       inner join #TaskInstanceList tl on tl.taskInstanceId = txref.TaskInstanceId
       inner join [$(LiveDB)].dbo.HCP_Authentication xrefAut on xrefAut.UserID = txref.AssignedTo
       inner join [$(DatabaseName)].dbo.Prod_View pv on pv.ViewId = txref.ViewId
       inner join [$(DatabaseName)].dbo.Task t on t.TaskInstanceId = txref.TaskInstanceId and t.SequenceId = 0
       inner join [$(LiveDB)].dbo.[Prod_FHANumberRequest] fr on fr.FHANumber = t.FHANumber
       inner join [$(LiveDB)].[dbo].[Prod_ProjectType] pt on pt.ProjectTypeId = fr.ProjectTypeId
       inner join  [$(LiveDB)].[dbo].HCP_PageType pg on pg.PageTypeId = t.PageTypeId
       left join(select tfinal.* from [$(DatabaseName)].dbo.Task tfinal
                       inner join (select t1.TaskInstanceId,max(starttime) as taskstarttime
                                         from  [$(DatabaseName)].dbo.Task t1 group by t1.TaskInstanceId ) tmaxdate on tmaxdate.TaskInstanceId = tfinal.TaskInstanceId and tmaxdate.taskstarttime = tfinal.StartTime
                                         )tend on tend.TaskInstanceId= t.TaskInstanceId 
       inner join [$(LiveDB)].dbo.HCP_Authentication a on a.UserName = t.AssignedBy
       inner join [$(LiveDB)].dbo.webpages_UsersInRoles ur on ur.UserId = a.UserID
       inner join [$(LiveDB)].dbo.webpages_Roles r on r.RoleId = ur.RoleId
       inner join [$(DatabaseName)].dbo.TaskStep ts on ts.TaskStepId = tend.TaskStepId
       left join [$(DatabaseName)].dbo.ParentChildTask pc on pc.ChildTaskInstanceId = t.TaskInstanceId
       
       -- Gets Child Tasks for Application Req Types in Task Table
       INSERT INTO #LenderPAMReport     
       Select xref.TaskInstanceId as taskInstanceId,
                 t.[FhaNumber],
                 fr.ProjectName,
                 pt.ProjectTypeName,
                 pg.PageTypeDescription as ProductionTaskType,
                 t.StartTime as StartDate,
                 (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 15) THEN tend.StartTime ELSE null END) as  enddate,
                 (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 15) THEN DATEDIFF(DAY,t.StartTime,tend.StartTime) ELSE DATEDIFF(DAY,t.StartTime,GETUTCDATE()) END) as DaysActive,             
                 fr.LoanAmount,
                 t.AssignedTo as LenderName,
                 r.RoleName as LenderRole,
                 (CASE WHEN t.IsReassigned is null THEN 0 ELSE t.IsReassigned END) as IsTaskReassigned , 
                 ts.TaskStepId as StatusId,
                 t.AssignedBy as ProcessedBy,
                 'RAI' as ViewName,
                 (CASE WHEN pc.ChildTaskInstanceId is null THEN 0 ELSE 1 END) as IsRAI 


       from [$(DatabaseName)].dbo.Task t
       inner join [$(LiveDB)].dbo.[Prod_FHANumberRequest] fr on fr.FHANumber = t.FHANumber
       inner join [$(LiveDB)].[dbo].[Prod_ProjectType] pt on pt.ProjectTypeId = fr.ProjectTypeId
       inner join  [$(LiveDB)].[dbo].HCP_PageType pg on pg.PageTypeId = t.PageTypeId
       left join(select tfinal.* from [$(DatabaseName)].dbo.Task tfinal
                       inner join (select t1.TaskInstanceId,max(starttime) as taskstarttime
                                         from  [$(DatabaseName)].dbo.Task t1 group by t1.TaskInstanceId ) tmaxdate on tmaxdate.TaskInstanceId = tfinal.TaskInstanceId and tmaxdate.taskstarttime = tfinal.StartTime
                                         )tend on tend.TaskInstanceId= t.TaskInstanceId 
       inner join [$(LiveDB)].dbo.HCP_Authentication a on a.UserName = t.AssignedBy
       inner join [$(LiveDB)].dbo.webpages_UsersInRoles ur on ur.UserId = a.UserID
       
       inner join [$(LiveDB)].dbo.webpages_Roles r on r.RoleId = ur.RoleId
       inner join [$(DatabaseName)].dbo.TaskStep ts on ts.TaskStepId = tend.TaskStepId
       left join [$(DatabaseName)].dbo.ParentChildTask pc on pc.ChildTaskInstanceId = t.TaskInstanceId
       left join [$(DatabaseName)].dbo.Prod_TaskXref xref on xref.TaskXrefid = pc.ParentTaskInstanceId
       
       where t.SequenceId = 0   and
              t.TaskInstanceId in( (select pc.ChildTaskInstanceId from [$(DatabaseName)].dbo.ParentChildTask pc 
                                                                                          where pc.ParentTaskInstanceId in (select xref.TaskXrefid from [$(DatabaseName)].dbo.Prod_TaskXref xref
                                                                                                                                                       inner join #TaskInstanceList tl on tl.taskInstanceId = xref.TaskInstanceId )))
    
       -- Gets Xref Tasks for Insert FHA
       INSERT INTO #LenderPAMReport
       Select t.TaskInstanceId as taskInstanceId,
                 t.[FhaNumber],
                 fr.ProjectName,
                 pt.ProjectTypeName,
                 pg.PageTypeDescription as ProductionTaskType,
                 t.StartTime as StartDate,
                 (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 19) THEN tend.StartTime ELSE null END) as  enddate,
                 (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 19) THEN DATEDIFF(DAY,t.StartTime,tend.StartTime) ELSE DATEDIFF(DAY,t.StartTime,GETUTCDATE()) END) as DaysActive,             
                 fr.LoanAmount,
                 t.AssignedBy as LenderName,
                 r.RoleName as LenderRole,
                 (CASE WHEN t.IsReassigned is null THEN 0 ELSE t.IsReassigned END) as IsTaskReassigned , 
                 ts.TaskStepId as StatusId,
                  xrefAut.FirstName + ' ' + xrefAut.LastName as ProcessedBy,
                 pv.ViewName as ViewName,
                 (CASE WHEN pc.ChildTaskInstanceId is null THEN 0 ELSE 1 END) as IsRAI 

       from [$(DatabaseName)].dbo.Prod_TaskXref txref
       inner join #TaskInstanceList tl on tl.taskInstanceId = txref.TaskInstanceId
       inner join [$(LiveDB)].dbo.HCP_Authentication xrefAut on xrefAut.UserID = txref.AssignedTo
       inner join [$(DatabaseName)].dbo.Prod_View pv on pv.ViewId = txref.ViewId
       inner join [$(DatabaseName)].dbo.Task t on t.TaskInstanceId = txref.TaskInstanceId and t.SequenceId = 0
       inner join [$(LiveDB)].dbo.[Prod_FHANumberRequest] fr on fr.TaskinstanceId = t.TaskInstanceId
       inner join [$(LiveDB)].[dbo].[Prod_ProjectType] pt on pt.ProjectTypeId = fr.ProjectTypeId
       inner join  [$(LiveDB)].[dbo].HCP_PageType pg on pg.PageTypeId = t.PageTypeId
       left join(select tfinal.* from [$(DatabaseName)].dbo.Task tfinal
                       inner join (select t1.TaskInstanceId,max(starttime) as taskstarttime
                                         from  [$(DatabaseName)].dbo.Task t1 group by t1.TaskInstanceId ) tmaxdate on tmaxdate.TaskInstanceId = tfinal.TaskInstanceId and tmaxdate.taskstarttime = tfinal.StartTime
                                         )tend on tend.TaskInstanceId= t.TaskInstanceId 
       inner join [$(LiveDB)].dbo.HCP_Authentication a on a.UserName = t.AssignedBy
       inner join [$(LiveDB)].dbo.webpages_UsersInRoles ur on ur.UserId = a.UserID
       inner join [$(LiveDB)].dbo.webpages_Roles r on r.RoleId = ur.RoleId
       inner join [$(DatabaseName)].dbo.TaskStep ts on ts.TaskStepId = tend.TaskStepId
       left join [$(DatabaseName)].dbo.ParentChildTask pc on pc.ChildTaskInstanceId = t.TaskInstanceId
       
       SELECT DISTINCT taskInstanceId ,
              FHANumber , 
              ProjectName , 
              LoanType ,
              ProductionTaskType ,
              StartDate ,
              EndDate ,
              DaysActive ,
              LoanAmount ,
              LenderName ,
              LenderRole ,
              IsTaskReassigned ,
              StatusId ,
              ProcessedBy ,
              ViewName ,
              IsRAI 

              into #ProjectCompletedDateNull
       FROM #LenderPAMReport 
       where  EndDate is null
       ORDER BY taskInstanceId,StartDate asc
       
       SELECT DISTINCT taskInstanceId ,
              FHANumber , 
              ProjectName , 
              LoanType ,
              ProductionTaskType ,
              StartDate ,
              EndDate ,
              DaysActive ,
              LoanAmount ,
              LenderName ,
              LenderRole ,
              IsTaskReassigned ,
              StatusId ,
              ProcessedBy ,
              ViewName  ,
              IsRai
  
              into #ProjectCompletedDateNotNull
       FROM #LenderPAMReport 
       where EndDate is not null
       ORDER BY taskInstanceId,EndDate desc;

       WITH cte AS 
              (
              select * from #ProjectCompletedDateNull
              ),

       cte2 AS 
       (
       select * from #ProjectCompletedDateNotNull
       )

       SELECT ROW_NUMBER() OVER (ORDER BY cte.taskInstanceId,cte.StartDate) AS ID, * FROM cte
       UNION ALL
       SELECT ROW_NUMBER() OVER (ORDER BY cte2.taskInstanceId,cte2.EndDate desc) AS ID,* FROM cte2 

END


