﻿
CREATE  PROCEDURE [dbo].[USP_HCP_Prod_GetApplicationAndClosingType](
 @TaskinstanceId uniqueidentifier
)
AS
BEGIN

CREATE TABLE #AppFHANumber(
 loanType varchar(50),
 fhaNumber varchar(50)
)

 INSERT INTO #AppFHANumber 
   select top 1 projectType.ProjectTypeName as loanType,opa.FhaNumber
  FROM [$(DatabaseName)].[dbo].[Task]
  join [$(LiveDB)].dbo.OPAForm opa on opa.TaskInstanceId = task.TaskInstanceId
  join  [$(LiveDB)].dbo.Prod_ProjectType projectType on projectType.ProjectTypeId = opa.ProjectActionTypeId
  where task.TaskInstanceId= @TaskinstanceId



 select   null as PageTypeDescription,task.TaskInstanceId as TaskInstanceId ,task.PageTypeId,task.FhaNumber,task.TaskStepId
  from [$(DatabaseName)].dbo.task task 
 --join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
 inner join (  select TaskInstanceId,max([SequenceId]) as SequenceId
                from  [$(DatabaseName)].dbo.task task2
                where  task2.PageTypeId > 4 and task2.TaskInstanceId not in (select ChildTaskInstanceId from ParentChildTask ) and task2.[FhaNumber] =(select fhaNumber from #AppFHANumber)
                group by [TaskInstanceId] 
			)task2 on task2.TaskInstanceId = task.TaskInstanceId and task2.SequenceId = task.SequenceId
  where task.PageTypeId <> 9  --except construction management page type
  order by task.PageTypeId 
 
END

