﻿
CREATE PROCEDURE [dbo].[usp_Prod_UpdatesForTaskReassignment] 
       @TaskInstanceId uniqueidentifier,
       @ViewId int,
       @ReassignedUserId int,
       @UserId int
       AS
/*
Date		 changed by		Reason
====		===========		======
10/03/2018		Skumar		PM Task reassigment for FHA

*/
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       --SET NOCOUNT ON;

       BEGIN TRANSACTION;
       BEGIN TRY
              
              DECLARE @FromUserId int
              DECLARE @FromUserName nvarchar(50)
			  DECLARE @ReassignedUserName nvarchar(50)

			  select @ReassignedUserName = username 
			  from [$(LiveDB)].dbo.HCP_Authentication a
			  where a.UserID = @ReassignedUserId

              if exists (select top 1 (TaskXrefid) from Prod_TaskXref where TaskXrefid = @TaskInstanceId)
              BEGIN
              select  @FromUserName = username ,@FromUserId=UserID  from [$(LiveDB)].dbo.HCP_Authentication a
                                       inner join Prod_TaskXref x on a.UserID = x.AssignedTo
                                         where x.TaskXrefid = @TaskInstanceId and ViewId = @ViewId 
                end
                     else
                     begin
                       select  @FromUserName = username ,@FromUserId=UserID  from [$(LiveDB)].dbo.HCP_Authentication a
                                       inner join Task t on a.UserName = t.AssignedTo
                                         where t.TaskInstanceId = @TaskInstanceId 
                     end



              INSERT INTO [dbo].[Prod_TaskReAssignments]
                              ([TaskInstanceId]
                              ,[FromAE]
                              ,[ReAssignedTo]
                              ,[CreatedOn]
                              ,[CreatedBy]
                              ,[Deleted_Ind]
                              ,[ViewId])
                           VALUES
                                     (@TaskInstanceId
                                     ,(select top 1 username from [$(LiveDB)].dbo.HCP_Authentication a
                                       where a.UserID = @FromUserId)
                                     ,@ReassignedUserName
                                     ,GETUTCDATE()
                                     ,@UserId
                                     ,0
                                     ,@ViewId)

              if(@ViewId = 1)
              begin

                     update [dbo].[Prod_TaskXref] 
                     set [AssignedTo]= @ReassignedUserId,
                           [ModifiedBy] = @UserId,
                           [ModifiedOn] = GETUTCDATE(),
                           [IsReassigned] = 1
                     where TaskXrefid = @TaskInstanceId 
                             and [ViewId] = @ViewId
                             and [AssignedTo]= @FromUserId
                     
                     update [dbo].[Task]
                     set [AssignedTo] = @ReassignedUserName
                     where TaskInstanceId =(select TaskInstanceId from Prod_TaskXref where TaskXrefid = @TaskInstanceId)

              end
              else if (@ViewId = 8)
              begin
                     update [dbo].[Task]
                     set [AssignedTo] = @ReassignedUserName
                     where TaskInstanceId = @TaskInstanceId
                     and [AssignedTo] = @FromUserName
                     
              end
              else 
              begin
                     update [dbo].[Prod_TaskXref] 
                     set [AssignedTo]= @ReassignedUserId,
                           [ModifiedBy] = @UserId,
                           [ModifiedOn] = GETUTCDATE(),
                           [IsReassigned] = 1
                     where TaskXrefid = @TaskInstanceId 
                             and [ViewId] = @ViewId
                             and [AssignedTo]= @FromUserId
              end    

					 update [dbo].[Task]
                     set [AssignedTo] = @ReassignedUserName
                     where TaskInstanceId = @TaskInstanceId

                     update [dbo].[Task]
                     set [AssignedBy] = @ReassignedUserName,
                           [IsReassigned] = 1
                     where [AssignedBy] = @FromUserName
                     and TaskInstanceId in (select [ChildTaskInstanceId] from [dbo].[ParentChildTask] pc
                                                                     where [ParentTaskInstanceId] = @TaskInstanceId)

                     update [dbo].[ReviewFileComment]
                     set [CreatedBy] = @ReassignedUserId
                     where ReviewerProdViewId = @ViewId
                     and FileTaskId in (select taskfileid 
                                                   from [dbo].[TaskFile] tf
                                                   inner join Prod_TaskXref tx on tx.TaskInstanceId = tf.TaskInstanceId)
                     and [CreatedBy] = @FromUserId



                     update [dbo].[NewFileRequest]
                     set [RequestedBy] = @ReassignedUserId
                     where ReviewerProdViewId = @ViewId
                     and ChildTaskInstanceId in (select [ChildTaskInstanceId] from [dbo].[ParentChildTask] pc
                                                                     where [ParentTaskInstanceId] = @TaskInstanceId)
                     and [RequestedBy] = @FromUserId

            update [dbo].[ParentChildTask] 
                     set CreatedBy = @ReassignedUserId
                     where ParentTaskInstanceId = @TaskInstanceId
                     and [CreatedBy] = @FromUserId

                     update [dbo].[ReviewFileStatus]
                     set [ReviewerUserId] = @ReassignedUserId,
                         [ModifiedBy] = @UserId,
                         [ModifiedOn] = GETUTCDATE()
                           where ReviewerProdViewId = @ViewId
                           and [ReviewerUserId] = @FromUserId
                           and  [TaskFileId] in (select taskfileid 
                                                   from [dbo].[TaskFile] tf
                                                   inner join Prod_TaskXref tx on tx.TaskInstanceId = tf.TaskInstanceId)

                     
                           
       
       COMMIT TRANSACTION;
       END TRY
       BEGIN CATCH
       print 'error ocurred'
       ROLLBACK TRANSACTION;
       END CATCH

END

