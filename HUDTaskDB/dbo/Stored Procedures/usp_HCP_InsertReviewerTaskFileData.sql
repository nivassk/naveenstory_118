﻿
CREATE PROCEDURE [dbo].[usp_HCP_InsertReviewerTaskFileData]
(
@TaskInstanceId uniqueidentifier,
@ReviewerUserId int,
@UserId int,
@ReviewerProdViewId int
)
AS 
-- Returns the first name, last name, job title, and contact type for the specified contact.
BEGIN

select tf.* into #MyTempTaskFile from TaskFile tf 
inner join ReviewFileStatus  rfs on rfs.TaskFileId = tf.TaskFileId
where rfs.ReviewerProdViewId = @ReviewerProdViewId  and tf.TaskInstanceId =  @TaskInstanceId

IF EXISTS (SELECT * FROM #MyTempTaskFile)
begin

INSERT INTO [dbo].[ReviewFileStatus]
           (ReviewFileStatusId,
		   [TaskFileId]
           ,[ReviewerUserId]
           ,[Status]
           ,[ModifiedOn]
		   ,ReviewerProdViewId
           ,[ModifiedBy]
		   )
   ( select NEWID(),
			tf.[TaskFileId],
			@ReviewerUserId,
			1,
			GETDATE(),
			@ReviewerProdViewId,
			@UserId
     from [dbo].[TaskFile] tf
     where [TaskInstanceId] = @TaskInstanceId )  

UPDATE [$(DatabaseName)].[dbo].[ReviewFileStatus]
   SET
       [ReviewerUserId] =  @ReviewerUserId
      ,[Status] =  1
      ,[ModifiedOn] = GETDATE()
      ,[ModifiedBy] =  @UserId
where [TaskFileId] in (select TaskFileId from #MyTempTaskFile) 
      and ReviewerProdViewId = @ReviewerProdViewId 
        

end
else
begin

	INSERT INTO [dbo].[ReviewFileStatus]
           (ReviewFileStatusId,
		   [TaskFileId]
           ,[ReviewerUserId]
           ,[Status]
           ,[ModifiedOn]
		   ,ReviewerProdViewId
           ,[ModifiedBy]
		   )
   ( select  NEWID(),
				[TaskFileId],
                     @ReviewerUserId,
                     1,
                     GETDATE(),
					 @ReviewerProdViewId,
                     @UserId
     from [dbo].[TaskFile] 
        where [TaskInstanceId] = @TaskInstanceId )  
        

end

END

