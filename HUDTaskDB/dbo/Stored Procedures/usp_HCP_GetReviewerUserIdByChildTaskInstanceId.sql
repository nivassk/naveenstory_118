﻿CREATE PROCEDURE [dbo].[usp_HCP_GetReviewerUserIdByChildTaskInstanceId]
( @ChildTaskInstanceId uniqueidentifier)
AS
BEGIN
	


	select au.userid from TaskFile tf 
	join Task t 
	on tf.TaskInstanceId = t.TaskInstanceId
	join [$(LiveDB)].dbo.HCP_Authentication au
	on t.AssignedBy = au.Username
	where t.SequenceId = 0 and t.taskinstanceid = @ChildTaskInstanceId
END

