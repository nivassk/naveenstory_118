﻿CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetProdHUDPAMSubReportV3] 
@CurrentTaskInstanceId uniqueidentifier ,
@FhaNumber [nvarchar](max)  
AS    
/*    
Date				Changed by		 Reason    
====				==========		 ======    
06/04/2018			skumar		Created to show sub tasks
08/09/2018			skumar		Production PAM report: FHA stage order incorrect in PAM report grid
10/01/2018			skumar		bug 4133: TC Production PAM report Grid: '+' sign not expanding on Executed Closing

*/ 
    
BEGIN  
   
 Declare @PAMReport table   
 (  
	RowId int,    
	ProjectName varchar(500),  
	FhaNumber varchar(9),  
	LoanType varchar(100),  
	ProjectStartDate datetime,  
	TotalDays int,  
	Status varchar(100),  
	LoanAmount decimal(19,2),  
	CurrentTaskInstanceId uniqueidentifier,  
	ProjectStage varchar(100),  
	ProjectStatus varchar(100),  
	AppraiserStatus varchar(100),  
	EnvironmentalistStatus varchar(100),  
	TitleSurveyStatus varchar(100), 
	ReviewerAppraiserStatus varchar(100),  
	ReviewerEnvironmentalistStatus varchar(100),  
	ReviewerTitleSurveyStatus varchar(100), 
	IsContractorAssigned bit,  
	IsUnderWriterAssigned bit,
	UnderWriterAssigned varchar(100),  		   
	CreditReviewStatus varchar(100),  
	PortfolioStatus varchar(100)  ,
	OrderByPageTypeId int      
 );  
 Declare @StartTime datetime  
 Declare @TaskInstanceId uniqueidentifier  
 Declare @PageTypeId int  
 Declare @ProjectStatus varchar(50)  
 Declare @AppraiserStatus varchar(50)  
 Declare @EnvStatus varchar(50)   
 Declare @TnSStatus varchar(50) 
 Declare @ReviewerAppraiserStatus varchar(50)  
 Declare @ReviewerEnvStatus varchar(50)   
 Declare @ReviewerTnSStatus varchar(50) 
 Declare @IsContractorAssigned bit   
 Declare @IsUnderWriterAssigned bit 
 Declare @UnderWriterAssigned   varchar(50) 
 Declare @TaskStatus int   
 Declare @CreditReviewStatus varchar(100)  
 Declare @PortfolioStatus varchar(100)  
   
 DECLARE @RowsToProcess  int  
 DECLARE @CurrentRow     int  
 DECLARE @EmptyGuid uniqueidentifier = (select cast(cast(0 as binary) as uniqueidentifier))  
 DECLARE @NonIRRCount int  
 DECLARE @OrderByPageTypeId int
 

 --Report for all requests other than IRR--  
 DECLARE @FHARequest TABLE (RowID int not null primary key identity(1,1), TaskInstanceId uniqueidentifier,FhaNumber varchar(9) )      
   
 INSERT INTO @FHARequest (TaskInstanceId,FhaNumber) 
 SELECT CompletedPgTaskinstanceid,FhaNumber FROM  [$(LiveDB)].[dbo].[Prod_NextStage] WHERE FHANumber=@FhaNumber and  CompletedPgTaskinstanceid != @CurrentTaskInstanceId order by ModifiedOn desc

  
 SET @RowsToProcess=@@ROWCOUNT  
  
 SET @CurrentRow=0  
 WHILE @CurrentRow<@RowsToProcess  
	BEGIN  
		SET @CurrentRow=@CurrentRow+1  
		SELECT @TaskInstanceId=TaskInstanceId FROM @FHARequest WHERE RowID=@CurrentRow  
		SELECT @FHANumber=FhaNumber FROM @FHARequest WHERE RowID=@CurrentRow  
		set @StartTime = (select top 1 StartTime	from [$(LiveDB)].dbo.Prod_FHANumberRequest fr   
													join task tk on fr.FHANumber = tk.FhaNumber  
													where	fr.TaskinstanceId = @TaskInstanceId and   
															tk.PageTypeId = 17 and tk.SequenceId = 1)  
    
		set @CurrentTaskInstanceId = @TaskInstanceId
		
		Insert into @PAMReport (RowId,ProjectName,FhaNumber,LoanType,ProjectStartDate,TotalDays,Status,LoanAmount,CurrentTaskInstanceId)  
					select @CurrentRow as RowId,  
						   fr.ProjectName as ProjectName,  
						   ISNULL(fr.FHANumber,'N/A') as FhaNumber,  
						   pt.ProjectTypeName as LoanType,  
						   fr.CreatedOn as ProjectStartDate,  
  
						    --  (select ISNULL(DATEDIFF(dd,  
							--						   (select top 1 StartTime from task    
							--						   where TaskInstanceId = @TaskInstanceId  
							--						   order by SequenceId),  
							--							(case when @StartTime is null then GETDATE() else  @StartTime end)
							--					   ),
							--					   0
							--				)
							--)as TotalDays,  
							(select ISNULL(DATEDIFF(dd, 
									 (select top 1 StartTime from task      
										where TaskInstanceId = @TaskInstanceId  and SequenceId=0 
										order by SequenceId),
										--(case when @StartTime is null
										--	  then GETDATE() 
										--	  else  (select top 1 StartTime from task      
										--			where TaskInstanceId = @TaskInstanceId  and SequenceId=1  
										--			order by SequenceId)
										--	 end 
										--)  
									   (select top 1 StartTime from task      
										where TaskInstanceId = @TaskInstanceId  and SequenceId=1  
										order by SequenceId) 
								  ),
								  0  
						 )  
		  )as TotalDays, 
  
							case when ((select top 1 taskid from task   
											where FhaNumber = @FHANumber and PageTypeId = 17 and SequenceId = 0 and TaskStepId = 15) is null)  
								 then 'Open' else 'Complete' end as Status,  
							fr.LoanAmount as LoanAmount,  
							@CurrentTaskInstanceId as CurrentTaskInstanceId  
  
		from [$(LiveDB)].dbo.Prod_FHANumberRequest fr  
		join [$(LiveDB)].dbo.Prod_ProjectType pt on fr.ProjectTypeId = pt.ProjectTypeId  
		left join [$(LiveDB)].dbo.Prod_NextStage n on fr.TaskinstanceId = n.CompletedPgTaskInstanceId   
		where fr.FHANumber = @FHANumber
  
		IF(@FHANumber is not null)  
		   Begin  
			set @PageTypeId = (select top 1 PageTypeId from Task where FhaNumber = @FHANumber  and TaskInstanceId = @CurrentTaskInstanceId order by PageTypeId desc)  
			set @OrderByPageTypeId = (select Orderby from [$(LiveDB)].dbo.HCP_PageType where PageTypeId=@PageTypeId)
			set @CurrentTaskInstanceId = (select top 1 TaskInstanceId from Task where FhaNumber = @FHANumber and PageTypeId = @PageTypeId)  
			IF(@PageTypeId > 4)  
				Begin   
					If @PageTypeId = 16 
						Begin
							Set @PageTypeId = (	select top 1 CompletedPgTypeId from [$(LiveDB)].dbo.Prod_NextStage where fhanumber = @FHANumber order by CompletedPgTypeId desc) 
							set @OrderByPageTypeId = (select Orderby from [$(LiveDB)].dbo.HCP_PageType where PageTypeId=@PageTypeId)
							set @CurrentTaskInstanceId = (select top 1 TaskInstanceId from Task where FhaNumber = @FHANumber and PageTypeId = @PageTypeId) 
						End	
					If exists (select top 1 * from Task tk join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId  
								where TaskStepId in (15,17,18) and FhaNumber = @FHANumber and PageTypeId = @PageTypeId)  
						Begin  
							set @ProjectStatus = case when exists (select top 1 * from Task tk join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId  
												  where TaskStepId in (15,17,18) and FhaNumber = @FHANumber and PageTypeId = @PageTypeId and ViewId = 1)  
												then (select top 1 case when [Status] = 15 then 'Complete'   
												  when  [Status] = 18 then 'In-Process' else 'Unassigned' end  
												from Task tk  
												join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId  
												where FhaNumber = @FHANumber and PageTypeId = @PageTypeId   
												and Viewid = 1)  
												else 'Unassigned' end  
  
							set @AppraiserStatus = case when exists (select top 1 * from Task tk join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId  
													  where TaskStepId in (15,17,18) and FhaNumber = @FHANumber and PageTypeId = @PageTypeId and ViewId = 12)  
													then (select top 1 case when [Status] = 15 then 'Complete'   
													  when  [Status] = 18 then 'In-Process' else 'Unassigned' end  
													from Task tk  
													join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId  
													where FhaNumber = @FHANumber and PageTypeId = @PageTypeId and Viewid = 12)  
													else 'Unassigned' end
							set @EnvStatus = case when exists (select top 1 * from Task tk join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId  
											  where TaskStepId in (15,17,18) and FhaNumber = @FHANumber and PageTypeId = @PageTypeId and ViewId = 3)  
											then (select top 1 case when [Status] = 15 then 'Complete'   
											  when  [Status] = 18 then 'In-Process' else 'Unassigned' end  
											from Task tk  
											join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId  
											where FhaNumber = @FHANumber and PageTypeId = @PageTypeId  and Viewid = 3)  
											else 'Unassigned' end  
                           
							set @TnSStatus = case when exists (select top 1 * from Task tk join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId  
											  where TaskStepId in (15,17,18) and FhaNumber = @FHANumber and PageTypeId = @PageTypeId and ViewId = 2)  
											then (select top 1 case when [Status] = 15 then 'Complete'   
											  when  [Status] = 18 then 'In-Process' else 'Unassigned' end  
											from Task tk  
											join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId  
											where FhaNumber = @FHANumber and PageTypeId = @PageTypeId and Viewid = 2)  
											else 'Unassigned' end    
							set @ReviewerAppraiserStatus = case when exists (select top 1 * from Task tk join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId  
																	where TaskStepId in (15,17,18) and FhaNumber = @FHANumber and PageTypeId = @PageTypeId and ViewId = 12)  
													then (select top 1 case when ([Status] = 15 or [Status] = 18) then upper(au.FirstName) + ' ' + upper(au.LastName)  
																			--when  [Status] = 18 then upper(concat(au.FirstName, '  ', au.LastName))
																			else 'Unassigned' end  
															from Task tk  
															join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId 
															join [$(LiveDB)].dbo.HCP_Authentication au on tx.AssignedTo = au.UserID 
															where FhaNumber = @FHANumber and PageTypeId = @PageTypeId and Viewid = 12) 
													else 'Unassigned' 
													end     
							set @ReviewerEnvStatus = case when exists (select top 1 * from Task tk 
															join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId  
															where TaskStepId in (15,17,18) and FhaNumber = @FHANumber and PageTypeId = @PageTypeId and ViewId = 3)  
											  then (select top 1 case when ([Status] = 15 or [Status] = 18) then upper(au.FirstName) + ' ' + upper(au.LastName)  
																	 else 'Unassigned' end  
													from Task tk  
													join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId 
													join [$(LiveDB)].dbo.HCP_Authentication au on tx.AssignedTo = au.UserID
													where FhaNumber = @FHANumber and PageTypeId = @PageTypeId and Viewid = 3)  
											  else 'Unassigned' 
											  end  
                           
							set @ReviewerTnSStatus = case when exists (select top 1 * from Task tk 
															join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId  
															where TaskStepId in (15,17,18) and FhaNumber = @FHANumber and PageTypeId = @PageTypeId and ViewId = 2)  
											  then (select top 1 case when ([Status] = 15 or [Status] = 18) then upper(au.FirstName) + ' ' + upper(au.LastName)  
																	 else 'Unassigned' end   
													from Task tk  
													join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId  
													join [$(LiveDB)].dbo.HCP_Authentication au on tx.AssignedTo = au.UserID
													where FhaNumber = @FHANumber and PageTypeId = @PageTypeId and Viewid = 2)  
											  else 'Unassigned' 
											  end  
							set @IsContractorAssigned = case when exists (select top 1 TaskXrefid from Task tk  
														   join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId  
														   where FhaNumber = @FHANumber and PageTypeId = @PageTypeId and Viewid in (6,7))  
														   then Convert(bit,1) else Convert(bit,0) end    
							set @IsUnderWriterAssigned = case when exists (select top 1 TaskXrefid 
																		from Task tk  
																		join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId 
																		where FhaNumber = @FHANumber and PageTypeId = @PageTypeId 
																				and Viewid = 1)  
														  then Convert(bit,1) 
														  else Convert(bit,0) 
														  end  
							
							set @UnderWriterAssigned = case when exists (select top 1 TaskXrefid 
																		from Task tk  
																		join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId 
																		where FhaNumber = @FHANumber and PageTypeId = @PageTypeId 
																				and Viewid = 1)  
														  then  (select top 1 case when ([Status] = 15 or [Status] = 18) 
																				   then upper(au.FirstName) + ' ' + upper(au.LastName)  
																					else 'Unassigned' 
																					end  
																from Task tk  
																join Prod_TaskXref tx on tk.TaskInstanceId = tx.TaskInstanceId 
																join [$(LiveDB)].dbo.HCP_Authentication au on tx.AssignedTo = au.Userid
																where FhaNumber = @FHANumber and PageTypeId = @PageTypeId and Viewid = 1)  
														else 'Unassigned' 
														end      
							set @CreditReviewStatus = 'N/A'   
							set @PortfolioStatus = 'N/A'        
						End  
					Else   
						Begin  
							set @ProjectStatus = 'Unassigned'  
							set @AppraiserStatus = 'Unassigned'  
							set @EnvStatus = 'Unassigned'  
							set @TnSStatus = 'Unassigned'  
							set @IsContractorAssigned = Convert(bit,0)  
							set @CreditReviewStatus = 'N/A'   
							set @PortfolioStatus = 'N/A'  
						End  
					update @PAMReport 
					set   
						   ProjectStage = (case when @PageTypeId = 4 then 'FHA Number Request'
												when @PageTypeId = 5 then 'Application' 
												when @PageTypeId = 6 then 'Construction Single Stage' 
												when @PageTypeId = 7 then 'Construction Two Stage Initial' 
												when @PageTypeId = 8 then 'Construction Two Stage Final' 
												when @PageTypeId = 9 then 'Construction Management' 
												when @PageTypeId = 10 then 'Draft Closing' 
												when @PageTypeId = 11 then 'Closing Two Stage Initial' 
												when @PageTypeId = 12 then 'Closing Two Stage Final' 
												when @PageTypeId = 17 then 'Executed Closing'  end),  
						   CurrentTaskInstanceId = @CurrentTaskInstanceId,  
						   ProjectStatus=@ProjectStatus,  
						   AppraiserStatus=(case when @PageTypeId = 17 then 'N/A'else @AppraiserStatus end),  
						   EnvironmentalistStatus=(case when @PageTypeId = 17 then 'N/A'else @EnvStatus end),  
						   TitleSurveyStatus=(case when @PageTypeId = 17 then 'N/A'else @TnsStatus end), 
						   ReviewerAppraiserStatus=(case when @PageTypeId = 17 then 'N/A'else @ReviewerAppraiserStatus end),  
						   ReviewerEnvironmentalistStatus=(case when @PageTypeId = 17 then 'N/A'else @ReviewerEnvStatus end),  
						   ReviewerTitleSurveyStatus=(case when @PageTypeId = 17 then 'N/A'else @ReviewerTnsStatus end), 
						   IsContractorAssigned=ISNULL(@IsContractorAssigned, Convert(bit,0)),  
						   IsUnderWriterAssigned=ISNULL(@IsUnderWriterAssigned, Convert(bit,0)),
						   UnderWriterAssigned=(case when @PageTypeId = 17 then 'N/A'else @UnderWriterAssigned end),  	     
						   CreditReviewStatus=@CreditReviewStatus,   
						   PortfolioStatus=@PortfolioStatus,
						   Status=(case when  @ProjectStatus = 'Complete' then 'Close' else 'Open' end)  ,
							OrderByPageTypeId =@OrderByPageTypeId 
					where RowId = @CurrentRow    
				End 
			 ELSE --@PageTypeId < 4 
				Begin  
      
					  set @TaskStatus = (select top 1 TaskStepId from Task where taskinstanceid = @TaskInstanceId order by SequenceId desc)  
					  set @ProjectStatus = (select case when @TaskStatus = 17 then 'Unassigned'  
								when @TaskStatus = 18 then 'In-Process'  
								when @TaskStatus = 19 then 'Complete' end)  
					  set @ProjectStatus = ISNULL(@ProjectStatus,'N/A')  
					  set @CreditReviewStatus = (select case when (IsCreditReviewRequired = 1 and tx.ViewId = 10 and tx.AssignedTo > 0 and tx.[Status] = 19) then 'Complete'  
						   when (IsCreditReviewRequired = 1 and tx.ViewId = 10 and tx.AssignedTo > 0 and tx.[Status] = 18) then 'In-Process'  
						   when (IsCreditReviewRequired = 1 and tx.ViewId = 10 and tx.AssignedTo <= 0) then 'Unassigned'   
						   when (IsCreditReviewRequired = 0) then 'N/A' end  
							from  [$(LiveDB)].dbo.Prod_FHANumberRequest fr  
						   left join Prod_TaskXref tx on fr.TaskinstanceId = tx.TaskInstanceId   
						   where FHANumber = @FHANumber and tx.ViewId = 10)  
					  set @PortfolioStatus = (select case when (IsPortfolioRequired = 1 and tx.ViewId = 9 and tx.AssignedTo > 0 and tx.[Status] = 19) then 'Complete'  
							   when (IsPortfolioRequired = 1 and tx.ViewId = 9 and tx.AssignedTo > 0 and tx.[Status] = 18) then 'In-Process'  
							   when (IsPortfolioRequired = 1 and tx.ViewId = 9 and tx.AssignedTo <= 0) then 'Unassigned'   
							   when (IsPortfolioRequired = 0) then 'N/A' end  
								from  [$(LiveDB)].dbo.Prod_FHANumberRequest fr  
							   left join Prod_TaskXref tx on fr.TaskinstanceId = tx.TaskInstanceId   
							   where FHANumber = @FHANumber  and tx.ViewId = 9)  
  
					  update @PAMReport set   
					   ProjectStage = 'FHA Number Request',  
					   ProjectStatus= @ProjectStatus,  
					   AppraiserStatus= 'N/A',  
					   EnvironmentalistStatus= 'N/A',  
					   TitleSurveyStatus= 'N/A',  
					   ReviewerAppraiserStatus='N/A', 
					   ReviewerEnvironmentalistStatus='N/A', 
					   ReviewerTitleSurveyStatus= 'N/A', 
					   IsContractorAssigned=Convert(bit,0),  
					   IsUnderWriterAssigned=Convert(bit,0),  
					   CreditReviewStatus=ISNULL(@CreditReviewStatus,'N/A'),   
					   PortfolioStatus=ISNULL(@PortfolioStatus,'N/A') ,
					   Status=(case when  @ProjectStatus = 'Complete' then 'Close' else 'Open' end) ,
						OrderByPageTypeId =@OrderByPageTypeId 
					   where RowId = @CurrentRow  
       
				  End      
		  End  --@FHANumber is not null 
   END  
 
 --Sort order- Open projects: Oldest to Newest & Closed projects: Newest to Oldest--    
 ;WITH cte AS   
 (  
  select * from @PAMReport where Status = 'Open'   
 ),  
 cte2 AS   
 (  
  select * from @PAMReport where Status = 'Close'   
 )  
  
     
 SELECT ROW_NUMBER() OVER (ORDER BY cte.ProjectStartDate) AS ID, * FROM cte  
 UNION ALL  
 SELECT ROW_NUMBER() OVER (ORDER BY cte2.ProjectStartDate desc) AS ID,* FROM cte2   
   
      
END


