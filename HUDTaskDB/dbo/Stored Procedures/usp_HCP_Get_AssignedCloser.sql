﻿
CREATE PROCEDURE [dbo].[usp_HCP_Get_AssignedCloser]
(
    @FhaNumber nvarchar(20)
   
)
AS
SELECT distinct tx.AssignedTo, aut.FirstName, aut.LastName
FROM [dbo].[Task] t
join [Prod_TaskXref] tx on t.TaskInstanceId = tx.TaskInstanceId
join [$(LiveDB)].[dbo].[HCP_Authentication] aut on tx.AssignedTo = aut.UserID
 where t.PageTypeId in (10,11,12) and t.FhaNumber = @FhaNumber and tx.ViewId = 1


