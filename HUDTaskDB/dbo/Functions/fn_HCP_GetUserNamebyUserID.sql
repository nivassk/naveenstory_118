﻿CREATE FUNCTION [dbo].[fn_HCP_GetUserNamebyUserID]
(
	@userid int
)
RETURNS nVARCHAR(100)
AS
BEGIN

Return (Select top 1 Firstname + ' ' + Lastname  from   [$(LiveDB)].dbo.hcp_authentication where userid=@userid
				  )

END

