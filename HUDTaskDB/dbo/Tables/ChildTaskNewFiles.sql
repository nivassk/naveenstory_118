﻿CREATE TABLE [dbo].[ChildTaskNewFiles](
	[ChildTaskNewFileId] [uniqueidentifier] NOT NULL,
	[ChildTaskInstanceId] [uniqueidentifier] NOT NULL,
	[TaskFileInstanceId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_ChildTaskNewFiles] PRIMARY KEY CLUSTERED 
(
	[ChildTaskNewFileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

