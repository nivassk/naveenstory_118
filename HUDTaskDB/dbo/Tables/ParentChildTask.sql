﻿
CREATE TABLE [dbo].[ParentChildTask](
	[ParentChildTaskId] [uniqueidentifier] NOT NULL,
	[ChildTaskInstanceId] [uniqueidentifier] NOT NULL,
	[ParentTaskInstanceId] [uniqueidentifier] NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[CreatedBy] [int] NOT NULL,
 CONSTRAINT [PK_ParentChildTask] PRIMARY KEY CLUSTERED 
(
	[ParentChildTaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]