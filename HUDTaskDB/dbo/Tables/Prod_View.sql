﻿
CREATE TABLE [dbo].[Prod_View](
	[ViewId] [int] IDENTITY(1,1) NOT NULL,
	[ViewName] [nvarchar](50) NULL,
	[ViewTypeId] [int] NULL,
	[Modifiedon] [datetime] NULL,
 CONSTRAINT [PK_Prod_View] PRIMARY KEY CLUSTERED 
(
	[ViewId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Prod_View]  WITH CHECK ADD  CONSTRAINT [FK_Prod_View_Prod_View] FOREIGN KEY([ViewTypeId])
REFERENCES [dbo].[Prod_ViewType] ([ViewTypeId])
GO

ALTER TABLE [dbo].[Prod_View] CHECK CONSTRAINT [FK_Prod_View_Prod_View]
GO

