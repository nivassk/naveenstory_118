﻿CREATE TABLE [dbo].[Prod_Form290Task]
(
	[Form290TaskID] [int] IDENTITY(1,1) NOT NULL,
	[ClosingTaskInstanceID] [uniqueidentifier] NOT NULL,
	[TaskInstanceID] [uniqueidentifier] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedByUserName] [varchar](50) NOT NULL,
	[Status] [int] NOT NULL,
 [DateFirstReminderSent] DATETIME NULL, 
    [DateFollowingReminderSent] DATETIME NULL, 
    [ReminderEmailsCount] INT NULL, 
    CONSTRAINT [PK_Form290Task] PRIMARY KEY CLUSTERED 
(
	[Form290TaskID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
