﻿
CREATE TABLE [dbo].[TaskAges](
	[TaskAgeId] [int] IDENTITY(1,1) NOT NULL,
	[TaskAgeInterval] [varchar](50) NULL,
	[TaskAgeIntervalText] [varchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[DeletedInd] [bit] NOT NULL CONSTRAINT [DF_TaskAge_DeletedInd]  DEFAULT ((0)),
 CONSTRAINT [PK_TaskAges] PRIMARY KEY CLUSTERED 
(
	[TaskAgeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]