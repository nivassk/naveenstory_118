﻿
CREATE TABLE [dbo].[TaskFlow](
	[TaskFlowId] [int] IDENTITY(1,1) NOT NULL,
	[TaskStepId] [int] NOT NULL,
	[NxtTaskStepId] [int] NOT NULL,
	[UserRole] [nvarchar](128) NULL,
	[NxtStepRole] [nvarchar](128) NULL,
 CONSTRAINT [PK_TaskFlow] PRIMARY KEY CLUSTERED 
(
	[TaskFlowId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[TaskFlow]  WITH CHECK ADD  CONSTRAINT [FK_TaskFlow_TaskStep] FOREIGN KEY([TaskStepId])
REFERENCES [dbo].[TaskStep] ([TaskStepId])
GO

ALTER TABLE [dbo].[TaskFlow] CHECK CONSTRAINT [FK_TaskFlow_TaskStep]
GO

ALTER TABLE [dbo].[TaskFlow]  WITH CHECK ADD  CONSTRAINT [FK_TaskFlow_TaskStep1] FOREIGN KEY([NxtTaskStepId])
REFERENCES [dbo].[TaskStep] ([TaskStepId])
GO

ALTER TABLE [dbo].[TaskFlow] CHECK CONSTRAINT [FK_TaskFlow_TaskStep1]
GO

