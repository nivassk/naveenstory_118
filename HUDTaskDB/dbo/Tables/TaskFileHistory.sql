﻿
CREATE TABLE [dbo].[TaskFileHistory](
	[TaskFileHistoryId] [uniqueidentifier] NOT NULL,
	[ParentTaskFileId] [uniqueidentifier] NOT NULL,
	[ChildTaskFileId] [uniqueidentifier] NOT NULL,
	[ChildTaskInstanceId] [uniqueidentifier] NOT NULL,
	[TransactionId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
 CONSTRAINT [PK_TaskFileHistory] PRIMARY KEY CLUSTERED 
(
	[TaskFileHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

