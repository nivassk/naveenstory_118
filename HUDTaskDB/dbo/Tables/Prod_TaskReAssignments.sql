﻿
CREATE TABLE [dbo].[Prod_TaskReAssignments](
	[TaskReAssignId] [int] IDENTITY(1,1) NOT NULL,
	[TaskInstanceId] [uniqueidentifier] NULL,
	[FromAE] [nvarchar](150) NOT NULL,
	[ReAssignedTo] [nvarchar](150) NOT NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[Deleted_Ind] [bit] NULL,
	[ViewId] [int] NULL,
 CONSTRAINT [PK__Prod_Tas__B9BA2173AE971B07] PRIMARY KEY CLUSTERED 
(
	[TaskReAssignId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

