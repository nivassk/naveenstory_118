﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Alter
       [dbo].[Prod_TaskXref] (Table)
       [dbo].[USP_GetProductionTasksByUserName] (Procedure)
       [dbo].[usp_HCP_Prod_GetPAMReportStatusGrid] (Procedure)
       [dbo].[usp_HCP_Prod_GetProdHUDPAMReportV3] (Procedure)
       [dbo].[USP_GetGroupTasksbyUser] (Procedure)
       [dbo].[USP_GetOPAFilesByTaskId] (Procedure)
       [dbo].[usp_HCP_GetProdLenderPAMReportSubGridChildData] (Procedure)
       [dbo].[usp_HCP_GetReAssignedTasksByUserName] (Procedure)
       [dbo].[usp_HCP_GetTaskDetailsForAE] (Procedure)
       [dbo].[usp_HCP_Prod_FileNameByFolderKeyForDropDown] (Procedure)
       [dbo].[usp_HCP_Prod_GetOpaHistory_FolderList] (Procedure)
     Create
       [dbo].[DepositType] (Table)
       [dbo].[EscrowEstimateType] (Table)
       [dbo].[ExhibitLetterType] (Table)
       [dbo].[FirmCommitmentAmendmentType] (Table)
       [dbo].[PartyType] (Table)
       [dbo].[Prod_FormAmendmentTask] (Table)
       [dbo].[RepairCostEstimateType] (Table)
       [dbo].[SaluatationType] (Table)
       [dbo].[SpecialConditionType] (Table)
       [dbo].[usp_HCP_Prod_GetProdHUDPAMSubReportV3] (Procedure)
     Refresh
       [dbo].[usp_HCP_Get_AssignedCloser] (Procedure)

** Supporting actions
     Refresh
       [dbo].[USP_GetProductionPamReportExcl] (Procedure)
       [dbo].[USP_GetProductionPamReport] (Procedure)
       [dbo].[USP_GetProdReassignmentTasks] (Procedure)
       [dbo].[USP_GetFilteredProductionTasks] (Procedure)
       [dbo].[usp_HCP_Prod_GetPAMGridTwoResults] (Procedure)
       [dbo].[usp_Prod_UpdateFHARequestAndTaskXref] (Procedure)
       [dbo].[usp_Prod_UpdatesForTaskReassignment] (Procedure)
