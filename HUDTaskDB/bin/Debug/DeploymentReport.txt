﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Drop
       [dbo].[Task].[_dta_stat_597577167_7_2] (Statistic)
       [dbo].[Task].[_dta_index_Task_20_597577167__K2_K3_7] (Index)
     Alter
       [HUDHealthCareDevUser] (User)
       [dbo].[DocumentTypes] (Table)
       [dbo].[Prod_SubFolderStructure] (Table)
       [dbo].[Prod_TaskXref] (Table)
       [dbo].[TaskFile] (Table)
       [dbo].[USP_GetProductionPamReportExcl] (Procedure)
       [dbo].[USP_GetProductionPamReport] (Procedure)
       [dbo].[USP_GetProdReassignmentTasks] (Procedure)
       [dbo].[USP_GetProductionTasksByUserName] (Procedure)
       [dbo].[usp_HCP_GetReviewerUserIdByChildTaskInstanceId] (Procedure)
       [dbo].[usp_HCP_Prod_GetPendingRAIByTaskInstanceId] (Procedure)
       [dbo].[GetAelistforMail] (Procedure)
       [dbo].[GetMailRecipients] (Procedure)
       [dbo].[USP_GetGroupTasksbyUser] (Procedure)
       [dbo].[usp_HCP_GetProdLenderPAMReportSubGridChildData] (Procedure)
       [dbo].[usp_HCP_GetTaskDetailsForAE] (Procedure)
       [dbo].[usp_HCP_Prod_FileNameByFolderKeyForDropDown] (Procedure)
       [dbo].[USP_HCP_Prod_GetApplicationAndClosingType] (Procedure)
       [dbo].[usp_HCP_Prod_GetOpaHistory_FolderList] (Procedure)
     Create
       [dbo].[Prod_Form290Task] (Table)
       [dbo].[fn_IsFederalHoliday] (Function)
       [dbo].[usp_HCP_Prod_GetPAMGridOneResults] (Procedure)
       [dbo].[usp_HCP_Prod_GetPAMGridTwoResults] (Procedure)
       [dbo].[usp_HCP_Prod_GetPAMReportStatusGrid] (Procedure)
       [dbo].[usp_HCP_Prod_GetProdHUDPAMReportV3] (Procedure)
       [dbo].[usp_HCP_Prod_SendExecutedClosingReminderEmail] (Procedure)
     Refresh
       [dbo].[USP_GetFilteredProductionTasks] (Procedure)
       [dbo].[usp_HCP_Get_AssignedCloser] (Procedure)
       [dbo].[usp_Prod_UpdateFHARequestAndTaskXref] (Procedure)
       [dbo].[usp_Prod_UpdatesForTaskReassignment] (Procedure)
       [dbo].[usp_HCP_InsertReviewerTaskFileData] (Procedure)
       [dbo].[usp_HCP_Prod_GetAllReviewersStatusByTaskInstanceId] (Procedure)

** Supporting actions

If this deployment is executed, changes to [HUDHealthCareDevUser] might introduce run-time errors in [db_datareader].
If this deployment is executed, changes to [HUDHealthCareDevUser] might introduce run-time errors in [db_datawriter].

