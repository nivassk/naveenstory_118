﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Timers;
using System.IO;
using System.Web;
using Model;
using Model.Production;
using BusinessService.Production;
using BusinessService.Interfaces.Production;


namespace TransAccessWinService
{
    public partial class ScheduledTasks : ServiceBase
    {
        private Timer timer = null;

        public ScheduledTasks()
        {
            InitializeComponent();
            int timerInterval = Convert.ToInt32(ConfigurationManager.AppSettings["TimerInterval"]); //in minutes
            timer = new Timer();
            //timer.Interval = timerInterval * 60 * 1000;
            timer.Interval = 10 * 1000;
            //timer.Elapsed += new System.Timers.ElapsedEventHandler(TimerInvoke);
            TimerInvoke(null, null);
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        private void TimerInvoke(object sender, ElapsedEventArgs e)
        {
            byte[] fileContent;
            string fhaNumber, fileTypeId, instanceId, propertyId, fileType;
            //string fileFolder = AppDomain.CurrentDomain.BaseDirectory;
            string fileFolder = @"C:\HUDRoot\HUDHealthcarePortal\ServicePack\2.0SP11\HUDHealthcarePortal\Uploads";
            DirectoryInfo d = new DirectoryInfo(fileFolder);
            FileInfo[] Files = d.GetFiles("*");
            HCP_TaskEntities taskContext = new HCP_TaskEntities();
            HCP_Live_dbEntities liveContext = new HCP_Live_dbEntities();
            IProd_RestfulWebApiDocumentUpload webApiDocumentUpload = new Prod_RestfulWebApiDocumentUpload();
            IProd_RestfulWebApiTokenRequest webApiTokenRequest = new Prod_RestfulWebApiTokenRequest();
            RestfulWebApiTokenResultModel request = webApiTokenRequest.RequestToken();
            RestfulWebApiUploadModel uploadmodel;
            RestfulWebApiResultModel webApiUploadResult;
            foreach (FileInfo file in Files)
            {
                try
                {
                    //1. Prepare data
                    string[] fileNameFields = file.Name.Split("_".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    fhaNumber = fileNameFields[0];
                    fileTypeId = fileNameFields[1];
                    instanceId = fileNameFields[2];
                    var live = (from x in liveContext.ProjectInfoes.AsEnumerable()
                                join y in liveContext.Lender_FHANumber.AsEnumerable()
                                on x.FHANumber equals y.FHANumber
                                where x.FHANumber.Equals(fhaNumber)
                                select new
                                {
                                    propertyId = x.PropertyID.ToString()
                                }).SingleOrDefault();
                    propertyId = live == null ? null : live.propertyId;
                    var task = (from x in taskContext.TaskFiles
                                where x.SystemFileName == file.Name
                                select x).SingleOrDefault();
                    fileType = task == null ? null : task.FileName;

                    //2. Upload file 
                    uploadmodel = new RestfulWebApiUploadModel();
                    uploadmodel.documentType = "550";
                    uploadmodel.propertyID = "7892";
                    uploadmodel.indexType = "1";
                    uploadmodel.indexValue = "10021";
                    uploadmodel.pdfConvertableValue = "false";
                    fileContent = FileToByteArray(fileFolder + "\\" + file.Name);
                    HttpPostedFileBase objFile = (HttpPostedFileBase)new MemoryPostedFile(fileContent);
                    webApiUploadResult = webApiDocumentUpload.UploadDocumentUsingWebApi(uploadmodel, request.access_token, objFile);

                    //3. Delete file 
                    if(webApiUploadResult.status == "success" )
                        File.Delete(fileFolder + "\\" + file.Name);

                    //4. Update TaskFile table
                    task = (from f in taskContext.TaskFiles
                            where f.SystemFileName == file.Name
                            select f).Single();
                    task.TransAccessOn = DateTime.UtcNow;
                    taskContext.SaveChanges();
                }
                catch (Exception e1) { }
            }
        }

        public static byte[] FileToByteArray(string fileName)
        {
            byte[] fileData = null;
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                var binaryReader = new BinaryReader(fs);
                fileData = binaryReader.ReadBytes((int)fs.Length);
            }
            return fileData;
        }

        protected override void OnStart(string[] args)
        {
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
            timer.Enabled = false;
        }
    }
}
