﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace TransAccessWinService
{
    static class Program
    {
        static void Main()
        {
#if (!DEBUG)
           	System.ServiceProcess.ServiceBase[] ServicesToRun;
           	ServicesToRun = new System.ServiceProcess.ServiceBase[] { new ScheduledTasks() };
           	System.ServiceProcess.ServiceBase.Run(ServicesToRun);
#else
            ScheduledTasks service = new ScheduledTasks();
            System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
#endif
        }
    }
}
