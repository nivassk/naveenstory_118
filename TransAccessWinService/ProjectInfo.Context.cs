﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TransAccessWinService
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class HCP_Live_dbEntities : DbContext
    {
        public HCP_Live_dbEntities()
            : base("name=HCP_Live_dbEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Lender_FHANumber> Lender_FHANumber { get; set; }
        public virtual DbSet<ProjectInfo> ProjectInfoes { get; set; }
    }
}
