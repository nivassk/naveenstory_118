﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.IO;

namespace TransAccessWinService
{
    public class MemoryPostedFile : HttpPostedFileBase
    {
        private readonly byte[] fileBytes;
        private string fileName;
        private MemoryStream inputStream;

        public MemoryPostedFile(byte[] fileBytes, string fileName = null)
        {
            this.fileBytes = fileBytes;
            this.fileName = fileName;
            this.inputStream = new MemoryStream(fileBytes);
        }

        public override int ContentLength { get { return fileBytes.Length; } }

        public override string FileName { get { return fileName; } }

        public override Stream InputStream { get { return inputStream; } }
    }
}
