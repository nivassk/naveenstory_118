﻿using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace HUDHealthcarePortal.Core
{
    // don't want to expose dbcontext to business service layer, workaround to put FundamentalFinancial and Lender_UploadData_Intermediate
    // to Core assembly
    [Serializable]
    public abstract class FundamentalFinancial
    {   
        [XmlIgnore]
        public Dictionary<DerivedFinancial, decimal?> DerivedFinDict = new Dictionary<DerivedFinancial, decimal?>();
        [XmlIgnore]
        public Dictionary<DerivedFinancial, decimal?> DerivedScoreDict = new Dictionary<DerivedFinancial, decimal?>();
        [XmlIgnore]
        public decimal? ScoreSum = 0;
        
        public void FillDerived(IUploadedLoanVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
