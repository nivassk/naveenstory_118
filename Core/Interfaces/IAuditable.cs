﻿using System;

namespace Core.Interfaces
{
    interface IAuditable
    {
        Nullable<int> ModifiedBy { get; set; }
        Nullable<int> OnBehalfOfBy { get; set; }
        Nullable<DateTime> ModifiedOn { get; set; }
    }
}
