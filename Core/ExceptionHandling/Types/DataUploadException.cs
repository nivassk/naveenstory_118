﻿using System;
using Core.ExceptionHandling;
using HUDHealthcarePortal.Core.ExceptionHandling;

namespace Core.ExceptionHandling.Types
{
    public class DataUploadException : ExceptionBase
    {
        public DataUploadException(ExceptionCategory category, string message)
            : base(category, message)
        {
        }
    }
}
