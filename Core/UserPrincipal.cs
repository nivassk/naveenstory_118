﻿using System;
using System.Security.Principal;
using Core;
using Core.Interfaces;

namespace HUDHealthcarePortal.Core
{
    // use IUserPrincipal is to facilitate unit tests
    public class UserPrincipal : System.Security.Principal.GenericPrincipal, IUserPrincipal
    {
        public const string ANONYMOUS_NAME = "Anonymous";
        
        private UserData _mOUserData;

        public UserPrincipal(IIdentity id, string[] roles)
            : base(id, roles)
        {
        }

        /// <summary>
        /// Get current existing UserPrincipal object.
        /// Create a new UserPrincipal object If it has not been set yet.
        /// </summary>
        public new static UserPrincipal Current
        {
            get
            {
                UserPrincipal oPrincipal = System.Threading.Thread.CurrentPrincipal as UserPrincipal;
                //Check if CurrentPrincipal is already set
                if (oPrincipal != null)
                {
                    return oPrincipal;
                }

                return CreateNewPrincipal();
            }
        }

        /// <summary>
        /// Get current existing UserPrincipal object.
        /// Return null if there is no UserPrincipal initialized
        /// </summary>
        /// <returns></returns>
        public static UserPrincipal ExistingCurrentPrincipal
        {
            get
            {
                UserPrincipal oPrincipal = System.Threading.Thread.CurrentPrincipal as UserPrincipal;
                //Check if CurrentPrincipal is already set
                if (oPrincipal != null)
                {
                    return oPrincipal;
                }
                return null;
            }
        }

        public int UserId
        {
            get { return _mOUserData.UserId; }
        }

        public int? LenderId
        {
            get
            {
                //if(!_mOUserData.LenderId.HasValue)
                //    _mOUserData.Initialize(UserName);
                return _mOUserData.LenderId;
            }
        }

        public int? ServicerId
        {
            get { return _mOUserData.ServicerId; }
        }

        public string UserName
        {
            get { return _mOUserData.UserName; }
        }

        public string FullName
        {
            get { return _mOUserData.FirstName + ' ' +  _mOUserData.LastName; }
        }

        public string Timezone
        {
            get 
            {
                if (!string.IsNullOrEmpty(_mOUserData.Timezone))
                {
                    var enumTimezone = EnumType.Parse<HUDTimeZone>(_mOUserData.Timezone);
                    return EnumType.GetEnumDescription(enumTimezone);
                }
                else
                    return null;
            }
        }

        /// <summary>
        /// Gets the user role.
        /// </summary>
        /// <value>
        /// The user role.
        /// </value>
        public string UserRole
        {
            get { return _mOUserData.UserRole; }
            set { _mOUserData.UserRole = value; }
        }
        /// <summary>
        /// Roles are populated after user login in _ViewStart
        /// </summary>
        public string[] Roles
        {
            get { return _mOUserData.Roles; }
            set { _mOUserData.Roles = value; }
        }


        public UserData UserData
        {
            get { return _mOUserData; }
        }

        public bool IsAuthenticated
        {
            get { return System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated;  }
        }

        public override bool IsInRole(string role)
        {
            return UserRole == role;
        }

        /// <summary>
        /// Initialization routine called to allow the principal object to cache and retrieve the internal UserData object
        /// </summary>
        public void Initialize()
        {
            string sUserName = this.Identity.Name;
            if (string.IsNullOrEmpty(sUserName))
                sUserName = ANONYMOUS_NAME;
            if (UserDataCache.Contains(sUserName))
            {
                Debugger.OutputDebugString("UserPrincipal::Initialize. Get UserData [{0}] from cache.", sUserName);
                _mOUserData = UserDataCache.GetUserData(sUserName);
                if (_mOUserData.IsExpireFromCache())
                {
                    Debugger.OutputDebugString("UserData is expired. Reload from database again.", sUserName);
                    _mOUserData.Initialize(sUserName);
                    UserDataCache.AddUserData(sUserName, _mOUserData);
                }
            }
            else
            {
                Debugger.OutputDebugString("UserPrincipal::Initialize. Create a new UserData [{0}] and add into cache.", sUserName);
                _mOUserData = new UserData(sUserName);
                UserDataCache.AddUserData(sUserName, _mOUserData);
            }
        }

        public static void RemoveUserDataCache(string userName)
        {
            UserDataCache.RemoveFromCache(userName);
        }

        #region Private Methods

        /// <summary>
        /// Create a new UserPrincipal from current WindowsIdentity
        /// and assign it to System.Threading.Thread.CurrentPrincipal
        /// </summary>
        /// <returns></returns>
        protected static UserPrincipal CreateNewPrincipal()
        {
            //CurrentPrincipal is not set, so create new UserPrincipal
            //UserPrincipal newPrincipal = new UserPrincipal(WindowsIdentity.GetCurrent());
            Debugger.OutputDebugString("Create a new user principal from current identity");
            UserPrincipal newPrincipal = new UserPrincipal(System.Threading.Thread.CurrentPrincipal.Identity, new string[] { });
            //Set CurrentPrincipal if the user is IsAuthenticated
            //if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                Debugger.OutputDebugString("Set Thread.CurrentPrincipal to the new UserPrincipal.");
                newPrincipal.Initialize();

                //if (newPrincipal.UserData.DeletedInd.GetValueOrDefault(false))
                //    throw new ApplicationException(
                //        string.Format("User {0} marked as deleted/inactive", newPrincipal.UserName));

                System.Threading.Thread.CurrentPrincipal = newPrincipal;
            }
            return newPrincipal;
        }

        #endregion
    }
}
