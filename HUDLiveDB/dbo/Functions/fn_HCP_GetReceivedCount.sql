﻿
CREATE FUNCTION [dbo].[fn_HCP_GetReceivedCount]
(
   @Year INT,
   @WLMId INT,
   @AEId INT
)
RETURNS INT
AS
BEGIN
	DECLARE @PeriodEnding DATETIME
	DECLARE @MonthsInPeriod INT
	DECLARE @ReceivedCount INT
	DECLARE @ReceivedSumCount INT
	DECLARE @FHANumber NVARCHAR(MAX)
	SET @ReceivedSumCount = 0
	DECLARE @StartDate DATETIME
	DECLARE @EndDate DATETIME
	DECLARE @YearDiff INT
	IF(@Year = YEAR(GETDATE()))
		BEGIN
			SET @StartDate = DATEADD(YYYY, DATEDIFF(YYYY,0,getdate()), 0) 
			SET @EndDate = GETDATE() - 1
		END
	ELSE
		BEGIN
			SET @YearDiff = CONVERT(INT,(YEAR(GETDATE()) - @Year))
			SET @StartDate = DATEADD(yy, DATEDIFF(yy,0,getdate()) - @YearDiff, 0) 
			SET @EndDate = DATEADD(YYYY, DATEDIFF(YYYY,0,getdate()) - @YearDiff + 1, -1)
		END	
	DECLARE @ExpectedCount_cursor CURSOR   
	IF @WLMId > 0
	 BEGIN
		SET @ExpectedCount_cursor = CURSOR FOR
			SELECT DISTINCT LDL.FHANumber
			FROM Lender_DataUpload_Live LDL INNER JOIN ProjectInfo PF
			ON LDL.FHANumber = PF.FHANumber AND LDL.FHANumber IN (SELECT DISTINCT FHANumber FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL)
			WHERE LDL.MonthsInPeriod IN (3,6,9,12) AND
			PF.HUD_WorkLoad_Manager_ID IS NOT NULL AND PF.HUD_WorkLoad_Manager_ID = @WLMId
	 END
	ELSE IF @AEId > 0
	 BEGIN
		SET @ExpectedCount_cursor = CURSOR FOR
			SELECT DISTINCT LDL.FHANumber
			FROM Lender_DataUpload_Live LDL INNER JOIN ProjectInfo PF
			ON LDL.FHANumber = PF.FHANumber AND LDL.FHANumber IN (SELECT DISTINCT FHANumber FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL)
			WHERE LDL.MonthsInPeriod IN (3,6,9,12) AND
			PF.HUD_Project_Manager_ID IS NOT NULL AND PF.HUD_Project_Manager_ID = @AEId
	 END
	ELSE 
	 BEGIN 
		SET @ExpectedCount_cursor = CURSOR FOR
			SELECT DISTINCT LDL.FHANumber
			FROM Lender_DataUpload_Live LDL INNER JOIN ProjectInfo PF
			ON LDL.FHANumber = PF.FHANumber AND LDL.FHANumber IN (SELECT DISTINCT FHANumber FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL)
			WHERE LDL.MonthsInPeriod IN (3,6,9,12) 
	 END
	 
	OPEN @ExpectedCount_cursor   
	FETCH NEXT FROM @ExpectedCount_cursor INTO @FHANumber   

	WHILE @@FETCH_STATUS = 0   
	BEGIN  
		SET @MonthsInPeriod	=	(SELECT MAX(MonthsInPeriod) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL 
								INNER JOIN [$(DatabaseName)].dbo.Lender_FHANumber LF 
								ON LDL.FHANumber = LF.FHANumber
								AND LDL.LenderID = LF.LenderID
								WHERE FHA_EndDate IS NULL 
								AND  MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber AND YEAR(PeriodEnding) = @Year) 

		SET @PeriodEnding =		(SELECT MAX(PeriodEnding) FROM Lender_DataUpload_Live LDL
								INNER JOIN Lender_FHANumber LF
								ON LDL.FHANumber = LF.FHANumber
								AND LDL.LenderID = LF.LenderID
								WHERE FHA_EndDate IS NULL
								AND  MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber
								AND  MonthsInPeriod = @MonthsInPeriod AND YEAR(PeriodEnding) = @Year) 
		
		IF @MonthsInPeriod = 12
			BEGIN	
				IF @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 			 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END		
				SET @MonthsInPeriod = @MonthsInPeriod - 3
				SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL INNER JOIN
									 [$(DatabaseName)].dbo.Lender_FHANumber LF
									 ON LDL.FHANumber = LF.FHANumber AND LDL.LenderID = LF.LenderID WHERE FHA_EndDate IS NULL AND 
									 MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod)  
									 AND YEAR(PeriodEnding) = @Year)

				SET @PeriodEnding = (SELECT MAX(PeriodEnding) FROM Lender_DataUpload_Live LDL
									INNER JOIN Lender_FHANumber LF
									ON LDL.FHANumber = LF.FHANumber
									AND LDL.LenderID = LF.LenderID
									WHERE FHA_EndDate IS NULL
									AND  MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber
									AND  MonthsInPeriod = @MonthsInPeriod AND YEAR(PeriodEnding) = @Year) 
			
				IF @ReceivedCount <> 0 AND @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END
				SET @MonthsInPeriod = @MonthsInPeriod - 3
				SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL INNER JOIN
									 [$(DatabaseName)].dbo.Lender_FHANumber LF
									 ON LDL.FHANumber = LF.FHANumber AND LDL.LenderID = LF.LenderID WHERE FHA_EndDate IS NULL AND 
									 MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod)  
									 AND YEAR(PeriodEnding) = @Year)

				SET @PeriodEnding = (SELECT MAX(PeriodEnding) FROM Lender_DataUpload_Live LDL
									INNER JOIN Lender_FHANumber LF
									ON LDL.FHANumber = LF.FHANumber
									AND LDL.LenderID = LF.LenderID
									WHERE FHA_EndDate IS NULL
									AND  MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber
									AND  MonthsInPeriod = @MonthsInPeriod AND YEAR(PeriodEnding) = @Year)  
			
				IF @ReceivedCount <> 0 AND @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END
				SET @MonthsInPeriod = @MonthsInPeriod - 3
				SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL INNER JOIN
									 [$(DatabaseName)].dbo.Lender_FHANumber LF
									 ON LDL.FHANumber = LF.FHANumber AND LDL.LenderID = LF.LenderID WHERE FHA_EndDate IS NULL AND 
									 MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod)  
									 AND YEAR(PeriodEnding) = @Year)

				SET @PeriodEnding = (SELECT MAX(PeriodEnding) FROM Lender_DataUpload_Live LDL
									INNER JOIN Lender_FHANumber LF
									ON LDL.FHANumber = LF.FHANumber
									AND LDL.LenderID = LF.LenderID
									WHERE FHA_EndDate IS NULL
									AND  MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber
									AND  MonthsInPeriod = @MonthsInPeriod AND YEAR(PeriodEnding) = @Year) 
			
				IF @ReceivedCount <> 0 AND @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END
			END

		ELSE IF @MonthsInPeriod = 9
			BEGIN	
				IF @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 			 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END		
				SET @MonthsInPeriod = @MonthsInPeriod + 3
				SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL INNER JOIN
									 [$(DatabaseName)].dbo.Lender_FHANumber LF
									 ON LDL.FHANumber = LF.FHANumber AND LDL.LenderID = LF.LenderID WHERE FHA_EndDate IS NULL AND 
									 MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod)  
									 AND YEAR(PeriodEnding) = @Year)

				SET @PeriodEnding = (SELECT MAX(PeriodEnding) FROM Lender_DataUpload_Live LDL
									INNER JOIN Lender_FHANumber LF
									ON LDL.FHANumber = LF.FHANumber
									AND LDL.LenderID = LF.LenderID
									WHERE FHA_EndDate IS NULL
									AND  MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber
									AND  MonthsInPeriod = @MonthsInPeriod AND YEAR(PeriodEnding) = @Year) 
			
				IF @ReceivedCount <> 0 AND @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END
				SET @MonthsInPeriod = @MonthsInPeriod - 6
				SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL INNER JOIN
									 [$(DatabaseName)].dbo.Lender_FHANumber LF
									 ON LDL.FHANumber = LF.FHANumber AND LDL.LenderID = LF.LenderID WHERE FHA_EndDate IS NULL AND 
									 MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod)  
									 AND YEAR(PeriodEnding) = @Year)

				SET @PeriodEnding = (SELECT MAX(PeriodEnding) FROM Lender_DataUpload_Live LDL
									INNER JOIN Lender_FHANumber LF
									ON LDL.FHANumber = LF.FHANumber
									AND LDL.LenderID = LF.LenderID
									WHERE FHA_EndDate IS NULL
									AND  MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber
									AND  MonthsInPeriod = @MonthsInPeriod AND YEAR(PeriodEnding) = @Year) 
			
				IF @ReceivedCount <> 0 AND @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END
				SET @MonthsInPeriod = @MonthsInPeriod - 3
				SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL INNER JOIN
									 [$(DatabaseName)].dbo.Lender_FHANumber LF
									 ON LDL.FHANumber = LF.FHANumber AND LDL.LenderID = LF.LenderID WHERE FHA_EndDate IS NULL AND 
									 MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod)  
									 AND YEAR(PeriodEnding) = @Year)

				SET @PeriodEnding = (SELECT MAX(PeriodEnding) FROM Lender_DataUpload_Live LDL
									INNER JOIN Lender_FHANumber LF
									ON LDL.FHANumber = LF.FHANumber
									AND LDL.LenderID = LF.LenderID
									WHERE FHA_EndDate IS NULL
									AND  MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber
									AND  MonthsInPeriod = @MonthsInPeriod AND YEAR(PeriodEnding) = @Year)  
			
				IF @ReceivedCount <> 0 AND @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END
			END
			ELSE IF @MonthsInPeriod = 6
			BEGIN	
				IF @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 			 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END		
				SET @MonthsInPeriod = @MonthsInPeriod + 6
				SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL INNER JOIN
									 [$(DatabaseName)].dbo.Lender_FHANumber LF
									 ON LDL.FHANumber = LF.FHANumber AND LDL.LenderID = LF.LenderID WHERE FHA_EndDate IS NULL AND 
									 MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod)  
									 AND YEAR(PeriodEnding) = @Year)

				SET @PeriodEnding = (SELECT MAX(PeriodEnding) FROM Lender_DataUpload_Live LDL
									INNER JOIN Lender_FHANumber LF
									ON LDL.FHANumber = LF.FHANumber
									AND LDL.LenderID = LF.LenderID
									WHERE FHA_EndDate IS NULL
									AND  MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber
									AND  MonthsInPeriod = @MonthsInPeriod AND YEAR(PeriodEnding) = @Year) 
			
				IF @ReceivedCount <> 0 AND @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END
				SET @MonthsInPeriod = @MonthsInPeriod - 3
				SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL INNER JOIN
									 [$(DatabaseName)].dbo.Lender_FHANumber LF
									 ON LDL.FHANumber = LF.FHANumber AND LDL.LenderID = LF.LenderID WHERE FHA_EndDate IS NULL AND 
									 MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod)  
									 AND YEAR(PeriodEnding) = @Year)

				SET @PeriodEnding = (SELECT MAX(PeriodEnding) FROM Lender_DataUpload_Live LDL
									INNER JOIN Lender_FHANumber LF
									ON LDL.FHANumber = LF.FHANumber
									AND LDL.LenderID = LF.LenderID
									WHERE FHA_EndDate IS NULL
									AND  MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber
									AND  MonthsInPeriod = @MonthsInPeriod AND YEAR(PeriodEnding) = @Year) 
			
				IF @ReceivedCount <> 0 AND @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END
				SET @MonthsInPeriod = @MonthsInPeriod - 6
				SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL INNER JOIN
									 [$(DatabaseName)].dbo.Lender_FHANumber LF
									 ON LDL.FHANumber = LF.FHANumber AND LDL.LenderID = LF.LenderID WHERE FHA_EndDate IS NULL AND 
									 MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod)  
									 AND YEAR(PeriodEnding) = @Year)

				SET @PeriodEnding = (SELECT MAX(PeriodEnding) FROM Lender_DataUpload_Live LDL
									INNER JOIN Lender_FHANumber LF
									ON LDL.FHANumber = LF.FHANumber
									AND LDL.LenderID = LF.LenderID
									WHERE FHA_EndDate IS NULL
									AND  MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber
									AND  MonthsInPeriod = @MonthsInPeriod AND YEAR(PeriodEnding) = @Year) 
				IF @ReceivedCount <> 0 AND @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END
			END
			ELSE IF @MonthsInPeriod = 3
			BEGIN	
				IF @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 			 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END		
				SET @MonthsInPeriod = @MonthsInPeriod + 3
				SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL INNER JOIN
									 [$(DatabaseName)].dbo.Lender_FHANumber LF
									 ON LDL.FHANumber = LF.FHANumber AND LDL.LenderID = LF.LenderID WHERE FHA_EndDate IS NULL AND 
									 MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod)  
									 AND YEAR(PeriodEnding) = @Year)

				SET @PeriodEnding = (SELECT MAX(PeriodEnding) FROM Lender_DataUpload_Live LDL
									INNER JOIN Lender_FHANumber LF
									ON LDL.FHANumber = LF.FHANumber
									AND LDL.LenderID = LF.LenderID
									WHERE FHA_EndDate IS NULL
									AND  MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber
									AND  MonthsInPeriod = @MonthsInPeriod AND YEAR(PeriodEnding) = @Year) 
			
				IF @ReceivedCount <> 0 AND @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END
				SET @MonthsInPeriod = @MonthsInPeriod + 3
				SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL INNER JOIN
									 [$(DatabaseName)].dbo.Lender_FHANumber LF
									 ON LDL.FHANumber = LF.FHANumber AND LDL.LenderID = LF.LenderID WHERE FHA_EndDate IS NULL AND 
									 MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod)  
									 AND YEAR(PeriodEnding) = @Year)

				SET @PeriodEnding = (SELECT MAX(PeriodEnding) FROM Lender_DataUpload_Live LDL
									INNER JOIN Lender_FHANumber LF
									ON LDL.FHANumber = LF.FHANumber
									AND LDL.LenderID = LF.LenderID
									WHERE FHA_EndDate IS NULL
									AND  MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber
									AND  MonthsInPeriod = @MonthsInPeriod AND YEAR(PeriodEnding) = @Year) 
			
				IF @ReceivedCount <> 0 AND @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END
				SET @MonthsInPeriod = @MonthsInPeriod + 3
				SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL INNER JOIN
									 [$(DatabaseName)].dbo.Lender_FHANumber LF
									 ON LDL.FHANumber = LF.FHANumber AND LDL.LenderID = LF.LenderID WHERE FHA_EndDate IS NULL AND 
									 MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod)  
									 AND YEAR(PeriodEnding) = @Year)

				SET @PeriodEnding = (SELECT MAX(PeriodEnding) FROM Lender_DataUpload_Live LDL
									INNER JOIN Lender_FHANumber LF
									ON LDL.FHANumber = LF.FHANumber
									AND LDL.LenderID = LF.LenderID
									WHERE FHA_EndDate IS NULL
									AND  MonthsInPeriod IN (3,6,9,12) AND LDL.FHANumber = @FHANumber
									AND  MonthsInPeriod = @MonthsInPeriod AND YEAR(PeriodEnding) = @Year) 
			
				IF @ReceivedCount <> 0 AND @PeriodEnding BETWEEN @StartDate AND @EndDate
					BEGIN 
						SET @ReceivedSumCount = @ReceivedSumCount + 1	
					END
			END

		FETCH NEXT FROM @ExpectedCount_cursor INTO @FHANumber   
	END  
	CLOSE @ExpectedCount_cursor   
	DEALLOCATE @ExpectedCount_cursor
	
	
RETURN @ReceivedSumCount
END

