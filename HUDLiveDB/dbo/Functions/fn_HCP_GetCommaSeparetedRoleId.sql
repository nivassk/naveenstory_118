﻿
CREATE FUNCTION [dbo].[fn_HCP_GetCommaSeparetedRoleId]
(
	@UserId INT
)
RETURNS nVARCHAR(100)
AS
BEGIN
	DECLARE @RoleIds nVARCHAR(100)
	SELECT  @RoleIds = STUFF((SELECT ', ' + CAST(RoleId AS VARCHAR(512)) [text()]
							FROM dbo.webpages_UsersInRoles 
							WHERE UserId = @UserId
							FOR XML PATH(''), TYPE
							)
							.value('.','NVARCHAR(MAX)'),1,2,' '
										) 
	FROM dbo.webpages_UsersInRoles wr
	where wr.UserId = @UserId

	return @RoleIds

END

