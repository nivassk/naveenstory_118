﻿
CREATE FUNCTION [dbo].[fn_HCP_GetLateNCRs]
(
	@TimeSpanId int,
	@DaysLate int,
	@dtNow datetime
)
RETURNS TABLE
AS
RETURN
(
	SELECT Distinct np.PropertyId, np.LenderId, np.FhaNumber, pri.ProjectName, li.Lender_Name,
			pri.HUD_Project_Manager_ID, @TimeSpanId AS 'TimeSpanId', np.CurrentBalance / np.InitialNCREBalance as 'bal'
	FROM NonCritical_ProjectInfo np 
	LEFT JOIN NonCriticalLateAlerts na ON np.PropertyId = na.PropertyId AND np.LenderId = na.LenderId AND np.FhaNumber = na.FhaNumber, ProjectInfo pri 
	INNER join LenderInfo li on li.LenderID = pri.LenderID
	WHERE pri.FHANumber = np.FhaNumber 
	AND pri.PropertyID = np.PropertyId 
	AND pri.LenderID = np.LenderId
	AND ((np.ExtensionApprovalDate IS NULL 
	AND DATEDIFF(DAY, np.ClosingDate, @dtNow) >= @DaysLate) OR (DATEDIFF(DAY, np.ExtensionApprovalDate, @dtNow) >= @DaysLate)) 
	AND ((na.PropertyId IS NULL AND na.LenderId IS NULL AND na.FhaNumber IS NULL AND na.TimeSpanId IS NULL) OR (na.PropertyId IS NOT NULL AND na.LenderId IS NOT NULL AND na.FhaNumber IS NOT NULL 
	AND ((SELECT MAX(TimeSpanId) FROM NonCriticalLateAlerts WHERE PropertyId = na.PropertyId
	AND LenderId = na.LenderId AND FhaNumber = na.FhaNumber) < CASE WHEN @TimeSpanId = 3 THEN 2 ELSE @TimeSpanId END)))
	AND np.CurrentBalance IS NOT NULL AND np.InitialNCREBalance IS NOT NULL AND np.InitialNCREBalance <> 0.0
	AND np.CurrentBalance / np.InitialNCREBalance >= CASE WHEN @TimeSpanId <> 3 THEN 1.0 ELSE 0.5 END 
	AND np.CurrentBalance <> CASE WHEN @TimeSpanId = 3 THEN np.InitialNCREBalance ELSE 0.0 END
);

