
CREATE FUNCTION [dbo].[fn_HCP_GetAEEmailbyId]
(
	@PMId INT
)
RETURNS nVARCHAR(100)
AS
BEGIN
	DECLARE @AddressID INT
    DECLARE @Email nVARCHAR(100)
	SELECT @AddressID= AddressID FROM [$(DatabaseName)].[dbo].HUD_Project_Manager WHERE HUD_Project_Manager_ID=@PMId
	
   SELECT 	@Email= Email FROM  [$(DatabaseName)].[dbo].[Address] WHERE AddressID=@AddressID

    RETURN @Email
END