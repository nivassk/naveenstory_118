﻿
CREATE FUNCTION [dbo].[fn_HCP_GetMissingQuarters]
(
   @Year INT,
   @FHANumber NVARCHAR(10)

)
RETURNS @QuarterTable TABLE
(
	FHANumber NVARCHAR(MAX),
	QuarterMissed NVARCHAR(10),
	QuarterEndDate NVARCHAR(MAX)
)
AS
BEGIN
	DECLARE @PeriodEnding DATETIME
	DECLARE @MonthsInPeriod INT
	DECLARE @ReceivedCount INT
	DECLARE @QuarterMissed NVARCHAR(10)
	DECLARE @QuarterDate DATETIME
	DECLARE @QuarterEndDate NVARCHAR(MAX)
	DECLARE @MIP INT
	DECLARE @StartDate DATETIME
	DECLARE @EndDate DATETIME
	DECLARE @YearDiff INT
	DECLARE @YearCount INT
	DECLARE @NtCurrYear BIT
	IF(@Year = YEAR(GETDATE()))
		BEGIN
			SET @StartDate = DATEADD(YYYY, DATEDIFF(YYYY,0,getdate()), 0) 
			SET @EndDate = GETDATE() - 1
		END
	ELSE
		BEGIN
			SET @YearDiff = CONVERT(INT,(YEAR(GETDATE()) - @Year))
			SET @StartDate = DATEADD(yy, DATEDIFF(yy,0,getdate()) - @YearDiff, 0) 
			SET @EndDate = DATEADD(YYYY, DATEDIFF(YYYY,0,getdate()) - @YearDiff + 1, -1)
		END	
	SET @MonthsInPeriod = (SELECT MAX(MonthsInPeriod) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber  IN 
							(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL) AND 
								MonthsInPeriod IN (3,6,9,12) AND FHANumber = @FHANumber)

	SET @PeriodEnding = (SELECT MAX(PeriodEnding) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber  IN 
							(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL) AND 
								MonthsInPeriod IN (3,6,9,12) AND FHANumber = @FHANumber AND MonthsInPeriod = @MonthsInPeriod) 
	IF(YEAR(@PeriodEnding) <> YEAR(GETDATE()))
	BEGIN
		SET @YearCount = CONVERT(INT,(@Year - YEAR(@PeriodEnding)))
		SET @PeriodEnding = DATEADD(YY, @YearCount,@PeriodEnding) 
	END
	ELSE IF(YEAR(@PeriodEnding) = YEAR(GETDATE()))
	BEGIN
		IF(YEAR(@PeriodEnding) <> @Year)
		BEGIN
			SET @YearCount = CONVERT(INT,(@Year - YEAR(@PeriodEnding)))
			SET @PeriodEnding = DATEADD(YY, @YearCount,@PeriodEnding)
		END
	END
	IF(@Year = YEAR(GETDATE()))
		SET @NtCurrYear = 1
	ELSE
		SET @NtCurrYear = 0
	IF DATEDIFF(DAY,@StartDate,DATEADD(month, -9, @PeriodEnding)) >= 0
	BEGIN 
		SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber  IN 
					(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL) AND 
					MonthsInPeriod IN (3,6,9,12) AND FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod - 9)
					AND YEAR(PeriodEnding) = @Year)
		
		IF @ReceivedCount = 0
		BEGIN				
			IF @NtCurrYear = 0
			BEGIN			 
				SET @QuarterMissed = dbo.fn_HCP_GetQuarter(@MonthsInPeriod - 9)
				IF @QuarterMissed = '??'
					SET @QuarterMissed = dbo.fn_HCP_GetQuarterForPreviousYear(@MonthsInPeriod, 9)
				SET @QuarterEndDate = CONVERT(CHAR(10),  DATEADD(month, -9, @PeriodEnding), 120)
				INSERT INTO @QuarterTable 
					SELECT @FHANumber,@QuarterMissed, @QuarterEndDate
			END
					
		END
	END
	IF DATEDIFF(DAY,@StartDate,DATEADD(month, -6, @PeriodEnding)) >= 0
	BEGIN 				
		SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber  IN 
					(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL) AND 
					MonthsInPeriod IN (3,6,9,12) AND FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod - 6)
					AND YEAR(PeriodEnding) = @Year)
		
		IF @ReceivedCount = 0
		BEGIN				
			IF @NtCurrYear = 0
			BEGIN				 
				SET @QuarterMissed = dbo.fn_HCP_GetQuarter(@MonthsInPeriod - 6)
				IF @QuarterMissed = '??'
					SET @QuarterMissed = dbo.fn_HCP_GetQuarterForPreviousYear(@MonthsInPeriod, 6)
				SET @QuarterEndDate = CONVERT(CHAR(10),  DATEADD(month, -6, @PeriodEnding), 120)
				INSERT INTO @QuarterTable 
					SELECT @FHANumber,@QuarterMissed, @QuarterEndDate
			END		
		END
	END
	IF DATEDIFF(DAY,@StartDate,DATEADD(month, -3, @PeriodEnding)) >= 0
	BEGIN 
		SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber  IN 
					(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL) AND 
					MonthsInPeriod IN (3,6,9,12) AND FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod - 3)
					AND YEAR(PeriodEnding) = @Year)
		
		IF @ReceivedCount = 0
		BEGIN				
			IF @NtCurrYear = 0
			BEGIN				 
				SET @QuarterMissed = dbo.fn_HCP_GetQuarter(@MonthsInPeriod - 3)
				IF @QuarterMissed = '??'
					SET @QuarterMissed = dbo.fn_HCP_GetQuarterForPreviousYear(@MonthsInPeriod, 3)
				SET @QuarterEndDate = CONVERT(CHAR(10),  DATEADD(month, -3, @PeriodEnding), 120)
				INSERT INTO @QuarterTable 
					SELECT @FHANumber,@QuarterMissed, @QuarterEndDate
			END		
		END
	END
	IF DATEDIFF(DAY,@PeriodEnding, @EndDate) >= 0
	BEGIN 
		SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber  IN 
					(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL) AND 
					MonthsInPeriod IN (3,6,9,12) AND FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod)
					AND YEAR(PeriodEnding) = @Year)
		
		IF @ReceivedCount = 0
		BEGIN				
			IF @NtCurrYear = 0
			BEGIN				 
				SET @QuarterMissed = dbo.fn_HCP_GetQuarter(@MonthsInPeriod)
				--IF @QuarterMissed = '??'
				--	SET @QuarterMissed = dbo.fn_HCP_GetQuarterForPreviousYear(@MonthsInPeriod, 3)
				SET @QuarterEndDate = CONVERT(CHAR(10),  DATEADD(month, 0, @PeriodEnding), 120)
				INSERT INTO @QuarterTable 
					SELECT @FHANumber,@QuarterMissed, @QuarterEndDate
			END		
		END
	END
	IF DATEDIFF(DAY,DATEADD(month, 3, @PeriodEnding),@EndDate) >= 0
	BEGIN 
		IF (@MonthsInPeriod + 3) > 12
		BEGIN
			SET @MIP = (@MonthsInPeriod + 3) % 12
			SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber  IN 
				(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL) AND 
				MonthsInPeriod IN (3,6,9,12) AND FHANumber = @FHANumber AND MonthsInPeriod = (@MIP)
				AND YEAR(PeriodEnding) = @Year)
		
			IF @ReceivedCount = 0
			BEGIN				
						 
					SET @QuarterMissed = dbo.fn_HCP_GetQuarter(@MIP)
					SET @QuarterEndDate = CONVERT(CHAR(10),  DATEADD(month, 3, @PeriodEnding), 120)
					INSERT INTO @QuarterTable 
						SELECT @FHANumber,@QuarterMissed, @QuarterEndDate
					
			END
		END
		ELSE
		BEGIN
			SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber  IN 
						(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL) AND 
						MonthsInPeriod IN (3,6,9,12) AND FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod + 3)
						AND YEAR(PeriodEnding) = @Year)
		
			IF @ReceivedCount = 0
			BEGIN				
						 
					SET @QuarterMissed = dbo.fn_HCP_GetQuarter(@MonthsInPeriod + 3)
					SET @QuarterEndDate = CONVERT(CHAR(10),  DATEADD(month, 3, @PeriodEnding), 120)
					INSERT INTO @QuarterTable 
						SELECT @FHANumber,@QuarterMissed, @QuarterEndDate
					
			END
		END
	END
	IF DATEDIFF(DAY,DATEADD(month, 6, @PeriodEnding),@EndDate) >= 0
	BEGIN 
		IF (@MonthsInPeriod + 6) > 12
		BEGIN
			SET @MIP = (@MonthsInPeriod + 6) % 12
			SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber  IN 
				(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL) AND 
				MonthsInPeriod IN (3,6,9,12) AND FHANumber = @FHANumber AND MonthsInPeriod = (@MIP)
				AND YEAR(PeriodEnding) = @Year)
		
			IF @ReceivedCount = 0
			BEGIN				
						 
					SET @QuarterMissed = dbo.fn_HCP_GetQuarter(@MIP)
					SET @QuarterEndDate = CONVERT(CHAR(10),  DATEADD(month, 6, @PeriodEnding), 120)
					INSERT INTO @QuarterTable 
						SELECT @FHANumber,@QuarterMissed, @QuarterEndDate
					
			END
		END
		ELSE
		BEGIN
			SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber  IN 
						(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL) AND 
						MonthsInPeriod IN (3,6,9,12) AND FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod + 6)
						AND YEAR(PeriodEnding) = @Year)
		
			IF @ReceivedCount = 0
			BEGIN				
						 
					SET @QuarterMissed = dbo.fn_HCP_GetQuarter(@MonthsInPeriod + 6)
					SET @QuarterEndDate = CONVERT(CHAR(10),  DATEADD(month, 6, @PeriodEnding), 120)
					INSERT INTO @QuarterTable 
						SELECT @FHANumber,@QuarterMissed, @QuarterEndDate
					
			END
		END
	END
	IF DATEDIFF(DAY,DATEADD(month, 9, @PeriodEnding),@EndDate) >= 0
	BEGIN
		IF (@MonthsInPeriod + 9) > 12
		BEGIN
			SET @MIP = (@MonthsInPeriod + 9) % 12
			SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber  IN 
				(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL) AND 
				MonthsInPeriod IN (3,6,9,12) AND FHANumber = @FHANumber AND MonthsInPeriod = (@MIP)
				AND YEAR(PeriodEnding) = @Year)
		
			IF @ReceivedCount = 0
			BEGIN				
						 
					SET @QuarterMissed = dbo.fn_HCP_GetQuarter(@MIP)
					SET @QuarterEndDate = CONVERT(CHAR(10),  DATEADD(month, 9, @PeriodEnding), 120)
					INSERT INTO @QuarterTable 
						SELECT @FHANumber,@QuarterMissed, @QuarterEndDate
					
			END
		END
		ELSE
		BEGIN 
			SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber  IN 
						(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL) AND 
						MonthsInPeriod IN (3,6,9,12) AND FHANumber = @FHANumber AND MonthsInPeriod = (@MonthsInPeriod + 9)
						AND YEAR(PeriodEnding) = @Year)
		
			IF @ReceivedCount = 0
			BEGIN				
						 
					SET @QuarterMissed = dbo.fn_HCP_GetQuarter(@MonthsInPeriod + 9)
					SET @QuarterEndDate = CONVERT(CHAR(10),  DATEADD(month, 9, @PeriodEnding), 120)
					INSERT INTO @QuarterTable 
						SELECT @FHANumber,@QuarterMissed, @QuarterEndDate
					
			END
		END
	END			
	IF DATEDIFF(DAY,DATEADD(month, 12, @PeriodEnding),@EndDate) >= 0 
	BEGIN 
		IF (@MonthsInPeriod + 12) > 12
		BEGIN
			IF ((@MonthsInPeriod + 12) % 12) = 0
				SET @MIP = 12						
			ELSE 
				SET @MIP = (@MonthsInPeriod + 12) % 12
			SET @ReceivedCount = (SELECT COUNT(*) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber  IN 
				(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL) AND 
				MonthsInPeriod IN (3,6,9,12) AND FHANumber = @FHANumber AND MonthsInPeriod = (@MIP) 
				AND YEAR(PeriodEnding) = @Year)
		
			IF @ReceivedCount = 0
			BEGIN				
						 
					SET @QuarterMissed = dbo.fn_HCP_GetQuarter(@MIP)
					SET @QuarterEndDate = CONVERT(CHAR(10),  DATEADD(month, 12, @PeriodEnding), 120)
					INSERT INTO @QuarterTable 
						SELECT @FHANumber,@QuarterMissed, @QuarterEndDate
					
			END
		END
	END
	RETURN 
END

