﻿
CREATE TABLE [dbo].[HUD_WorkLoad_Manager_Portfolio_Manager](
	[HUD_WorkLoad_Manager_ID] [int] NOT NULL,
	[HUD_Project_Manager_ID] [int] NOT NULL,
 CONSTRAINT [PK_HUD_Manager_ID_Project_Manager_ID] PRIMARY KEY CLUSTERED 
(
	[HUD_WorkLoad_Manager_ID] ASC,
	[HUD_Project_Manager_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[HUD_WorkLoad_Manager_Portfolio_Manager]  WITH CHECK ADD  CONSTRAINT [FK_HUD_WorkLoad_Manager_Portfolio_Manager_HUD_Project_Manager] FOREIGN KEY([HUD_Project_Manager_ID])
REFERENCES [dbo].[HUD_Project_Manager] ([HUD_Project_Manager_ID])
GO

ALTER TABLE [dbo].[HUD_WorkLoad_Manager_Portfolio_Manager] CHECK CONSTRAINT [FK_HUD_WorkLoad_Manager_Portfolio_Manager_HUD_Project_Manager]
GO

ALTER TABLE [dbo].[HUD_WorkLoad_Manager_Portfolio_Manager]  WITH CHECK ADD  CONSTRAINT [FK_HUD_WorkLoad_Manager_Portfolio_Manager_HUD_WorkLoad_Manager] FOREIGN KEY([HUD_WorkLoad_Manager_ID])
REFERENCES [dbo].[HUD_WorkLoad_Manager] ([HUD_WorkLoad_Manager_ID])
GO

ALTER TABLE [dbo].[HUD_WorkLoad_Manager_Portfolio_Manager] CHECK CONSTRAINT [FK_HUD_WorkLoad_Manager_Portfolio_Manager_HUD_WorkLoad_Manager]
GO

