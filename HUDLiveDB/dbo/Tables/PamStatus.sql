﻿
CREATE TABLE [dbo].[PamStatus](
	[PamStatusId] [int] NOT NULL,
	[StatusDescription] [varchar](100) NULL,
	[ModuleId] [int] NULL,
	[displayorder] [int] NULL,
 CONSTRAINT [PK__PamStatu__61770CA6E562D270] PRIMARY KEY CLUSTERED 
(
	[PamStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
