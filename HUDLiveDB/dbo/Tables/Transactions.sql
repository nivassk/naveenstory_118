﻿
CREATE TABLE [dbo].[Transactions](
	[TransactionId] [uniqueidentifier] NOT NULL,
	[FormType] [int] NOT NULL,
	[FormId] [uniqueidentifier] NOT NULL,
	[Amount] [decimal](10, 2) NULL,
	[CurrentBalance] [decimal](10, 2) NULL,
	[TransactionType] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[Status] [int] NULL,
	[modifiedby] [int] NULL,
 CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD  CONSTRAINT [fk_Transactions_HCP_Authentication_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[Transactions] CHECK CONSTRAINT [fk_Transactions_HCP_Authentication_CreatedBy]
GO

