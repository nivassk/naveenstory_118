﻿CREATE TABLE [dbo].[HCP_OutGoingMail] (
    [OutGoingMailID] INT             IDENTITY (1, 1) NOT NULL,
    [Emails_To]      NVARCHAR (2000) NOT NULL,
    [Emails_CC]      NVARCHAR (2000) NULL,
    [Emails_BCC]     NVARCHAR (2000) NULL,
    [Subject]        NVARCHAR (200)  NULL,
    [Message]        NVARCHAR (2000) NULL,
    [ModifiedOn]     DATETIME        NOT NULL,
    [ModifiedBy]     INT             NOT NULL,
    [OnBehalfOfBy]   INT             NULL,
    [Deleted_Ind]    BIT             NULL,
    CONSTRAINT [PK_OutGoingMailID] PRIMARY KEY CLUSTERED ([OutGoingMailID] ASC)
);

