﻿
CREATE TABLE [dbo].[HCP_Email](
	[EmailId] [int] IDENTITY(1,1) NOT NULL,
	[EmailTo] [nvarchar](max) NOT NULL,
	[EmailFrom] [nvarchar](128) NOT NULL,
	[EmailCC] [nvarchar](max) NULL,
	[EmailBCC] [nvarchar](max) NULL,
	[Subject] [nvarchar](256) NOT NULL,
	[ContentText] [nvarchar](max) NULL,
	[ContentHtml] [nvarchar](max) NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
	[Deleted_Ind] [bit] NULL,
	[IsSent] [bit] NULL,
	[MailTypeId] [tinyint] NOT NULL,
 CONSTRAINT [PK_EmailID] PRIMARY KEY CLUSTERED 
(
	[EmailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

