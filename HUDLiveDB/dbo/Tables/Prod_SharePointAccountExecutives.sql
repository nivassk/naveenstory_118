﻿
CREATE TABLE [dbo].[Prod_SharePointAccountExecutives](
	[SharePointAEId] [int] IDENTITY(1,1) NOT NULL,
	[SharePointAEName] [varchar](255) NOT NULL,
	[Email] [nvarchar](max) NULL,
 CONSTRAINT [PK_SharePointAccountExecutives] PRIMARY KEY CLUSTERED 
(
	[SharePointAEId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

