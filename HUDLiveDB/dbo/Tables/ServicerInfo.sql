﻿
CREATE TABLE [dbo].[ServicerInfo](
	[ServicerID] [int] NOT NULL,
	[ServicerName] [nvarchar](100) NOT NULL,
	[AddressID] [int] NOT NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[OnBehalfOfBy] [int] NULL,
	[Deleted_Ind] [bit] NULL,
 CONSTRAINT [PK_ServicerID] PRIMARY KEY CLUSTERED 
(
	[ServicerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

