﻿
CREATE TABLE [dbo].[PageType_DisclaimerText](
	[DisclaimerTextId] [int] NOT NULL,
	[PageTypeId] [tinyint] NOT NULL,
 CONSTRAINT [PK__PageType__PageTypeId_DisclosureTextId] PRIMARY KEY CLUSTERED 
(
	[DisclaimerTextId] ASC,
	[PageTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PageType_DisclaimerText]  WITH CHECK ADD  CONSTRAINT [FK_PageType_Disclosure_HCP_DisclosureText] FOREIGN KEY([DisclaimerTextId])
REFERENCES [dbo].[HCP_DisclaimerText] ([DisclaimerTextId])
GO

ALTER TABLE [dbo].[PageType_DisclaimerText] CHECK CONSTRAINT [FK_PageType_Disclosure_HCP_DisclosureText]
GO

ALTER TABLE [dbo].[PageType_DisclaimerText]  WITH CHECK ADD  CONSTRAINT [FK_PageType_Disclosure_HCP_PageType] FOREIGN KEY([PageTypeId])
REFERENCES [dbo].[HCP_PageType] ([PageTypeId])
GO

ALTER TABLE [dbo].[PageType_DisclaimerText] CHECK CONSTRAINT [FK_PageType_Disclosure_HCP_PageType]
GO

