﻿
CREATE TABLE [dbo].[User_WLM_PM](
	[UserID] [int] NOT NULL,
	[HUD_WorkLoad_Manager_ID] [int] NULL,
	[HUD_Project_Manager_ID] [int] NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[Deleted_Ind] [bit] NULL,
	[OnBehalfOfBy] [int] NULL,
 CONSTRAINT [PK_User_WLM_PM] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[User_WLM_PM]  WITH CHECK ADD  CONSTRAINT [FK_User_WLM_PM_HCP_Authentication] FOREIGN KEY([UserID])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[User_WLM_PM] CHECK CONSTRAINT [FK_User_WLM_PM_HCP_Authentication]
GO

ALTER TABLE [dbo].[User_WLM_PM]  WITH CHECK ADD  CONSTRAINT [FK_User_WLM_PM_HUD_Project_Manager] FOREIGN KEY([HUD_Project_Manager_ID])
REFERENCES [dbo].[HUD_Project_Manager] ([HUD_Project_Manager_ID])
GO

ALTER TABLE [dbo].[User_WLM_PM] CHECK CONSTRAINT [FK_User_WLM_PM_HUD_Project_Manager]
GO

ALTER TABLE [dbo].[User_WLM_PM]  WITH CHECK ADD  CONSTRAINT [FK_User_WLM_PM_HUD_WorkLoad_Manager] FOREIGN KEY([HUD_WorkLoad_Manager_ID])
REFERENCES [dbo].[HUD_WorkLoad_Manager] ([HUD_WorkLoad_Manager_ID])
GO

ALTER TABLE [dbo].[User_WLM_PM] CHECK CONSTRAINT [FK_User_WLM_PM_HUD_WorkLoad_Manager]
GO

