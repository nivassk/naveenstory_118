﻿
CREATE TABLE [dbo].[HCP_User_FunctionPoint](
	[HCP_User_FunctionPoint_ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[HCP_FunctionPoint_ID] [int] NOT NULL,
	[Deleted_Ind] [bit] NOT NULL,
	[ModifiedOn] [timestamp] NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
 CONSTRAINT [PK_HCP_User_FunctionPoint] PRIMARY KEY CLUSTERED 
(
	[HCP_User_FunctionPoint_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[HCP_User_FunctionPoint]  WITH CHECK ADD  CONSTRAINT [FK_HCP_User_FunctionPoint_HCP_Authentication] FOREIGN KEY([UserID])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[HCP_User_FunctionPoint] CHECK CONSTRAINT [FK_HCP_User_FunctionPoint_HCP_Authentication]
GO

ALTER TABLE [dbo].[HCP_User_FunctionPoint]  WITH CHECK ADD  CONSTRAINT [FK_HCP_User_FunctionPoint_HCP_FunctionPoint_Ref] FOREIGN KEY([HCP_FunctionPoint_ID])
REFERENCES [dbo].[HCP_FunctionPoint_Ref] ([HCP_FunctionPoint_ID])
GO

ALTER TABLE [dbo].[HCP_User_FunctionPoint] CHECK CONSTRAINT [FK_HCP_User_FunctionPoint_HCP_FunctionPoint_Ref]
GO

