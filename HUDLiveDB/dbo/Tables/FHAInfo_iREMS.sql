﻿
CREATE TABLE [dbo].[FHAInfo_iREMS](
	[FHANumber] [nvarchar](15) NOT NULL,
	[FHADescription] [nvarchar](100) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
	[Deleted_Ind] [bit] NULL,
 CONSTRAINT [PK_FHANumber] PRIMARY KEY CLUSTERED 
(
	[FHANumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

