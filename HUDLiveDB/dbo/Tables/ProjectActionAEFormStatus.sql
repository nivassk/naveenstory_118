﻿
CREATE TABLE [dbo].[ProjectActionAEFormStatus](
	[ProjectActionAEFormStatusId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectActionFormId] [uniqueidentifier] NOT NULL,
	[CheckListId] [uniqueidentifier] NOT NULL,
	[RequestStatus] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_ProjectActionAEFormStatus] PRIMARY KEY CLUSTERED 
(
	[ProjectActionAEFormStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProjectActionAEFormStatus]  WITH CHECK ADD  CONSTRAINT [FK_ProjectActionAEFormStatus_CheckListQuestions] FOREIGN KEY([CheckListId])
REFERENCES [dbo].[CheckListQuestions] ([CheckListId])
GO

ALTER TABLE [dbo].[ProjectActionAEFormStatus] CHECK CONSTRAINT [FK_ProjectActionAEFormStatus_CheckListQuestions]
GO

ALTER TABLE [dbo].[ProjectActionAEFormStatus]  WITH CHECK ADD  CONSTRAINT [FK_ProjectActionAEFormStatus_ProjectActionForms] FOREIGN KEY([ProjectActionFormId])
REFERENCES [dbo].[ProjectActionForms] ([ProjectActionFormId])
GO

ALTER TABLE [dbo].[ProjectActionAEFormStatus] CHECK CONSTRAINT [FK_ProjectActionAEFormStatus_ProjectActionForms]
GO

