﻿
CREATE TABLE [dbo].[Prod_SelectedAdditionalFinancingResources](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FHANumberRequestId] [uniqueidentifier] NOT NULL,
	[ResourceId] [int] NOT NULL,
 CONSTRAINT [PK_Prod_SelectedAdditionalFinancingResources] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Prod_SelectedAdditionalFinancingResources]  WITH CHECK ADD  CONSTRAINT [FK_Prod_SelectedAdditionalFinancingResources_Prod_AdditionalFinancingResources] FOREIGN KEY([ResourceId])
REFERENCES [dbo].[Prod_AdditionalFinancingResources] ([ResourceId])
GO

ALTER TABLE [dbo].[Prod_SelectedAdditionalFinancingResources] CHECK CONSTRAINT [FK_Prod_SelectedAdditionalFinancingResources_Prod_AdditionalFinancingResources]
GO

ALTER TABLE [dbo].[Prod_SelectedAdditionalFinancingResources]  WITH CHECK ADD  CONSTRAINT [FK_Prod_SelectedAdditionalFinancingResources_Prod_FHANumberRequest] FOREIGN KEY([FHANumberRequestId])
REFERENCES [dbo].[Prod_FHANumberRequest] ([FHANumberRequestId])
GO

ALTER TABLE [dbo].[Prod_SelectedAdditionalFinancingResources] CHECK CONSTRAINT [FK_Prod_SelectedAdditionalFinancingResources_Prod_FHANumberRequest]
GO

