﻿
CREATE TABLE [dbo].[SecurityQuestion](
	[SecurityQuestionID] [int] NOT NULL,
	[SecurityQuestionDescription] [nvarchar](100) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[Deleted_Ind] [bit] NULL,
 CONSTRAINT [PK_SecurityQuestionID] PRIMARY KEY CLUSTERED 
(
	[SecurityQuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

