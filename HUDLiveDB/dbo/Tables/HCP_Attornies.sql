﻿
CREATE TABLE [dbo].[HCP_Attornies](
	[AttorneyID] [int] IDENTITY(1,1) NOT NULL,
	[AttorneyName] [varchar](100) NOT NULL,
	[AttorneyTitle] [varchar](100) NULL,
	[Office] [varchar](50) NULL,
	[Region] [varchar](50) NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
	[Deleted_Ind] [bit] NULL,
 CONSTRAINT [PK_AttorneyID] PRIMARY KEY CLUSTERED 
(
	[AttorneyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

