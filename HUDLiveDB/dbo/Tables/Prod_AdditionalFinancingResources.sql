﻿
CREATE TABLE [dbo].[Prod_AdditionalFinancingResources](
	[ResourceId] [int] NOT NULL,
	[ResourceName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Prod_AdditionalFinancingResources] PRIMARY KEY CLUSTERED 
(
	[ResourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

