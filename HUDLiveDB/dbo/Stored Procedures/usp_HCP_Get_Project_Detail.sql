﻿
-- =============================================
-- Author:		David Coddington
-- Create date: 6/5/2015
-- Description:	Returns Detail information for Project Report
-- =============================================
CREATE PROCEDURE [dbo].[usp_HCP_Get_Project_Detail]
(
    @ReportType nvarchar(100),
    @WlmId NVARCHAR(100),
    @AeId NVARCHAR(100),
    @LenderId NVARCHAR(100),
    @MinUploadDate DATETIME,
    @MaxUploadDate DATETIME,
	@IsHighLoan BIT,
	@IsDebtCoverageRatio BIT,
	@IsWorkingCapital BIT,
	@IsDaysCashOnHand BIT,
	@IsDaysInAcctReceivable BIT,
	@IsAvgPaymentPeriod BIT
)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	DECLARE @RatioExceptionsReport nvarchar(21)
	DECLARE @allRatioExceptionsReport BIT
	SET @RatioExceptionsReport = 'RatioExceptionsReport'

	--User types are: 'AccountExecutive', 'WorkflowManager', 'SuperUser'
	DECLARE @ReportHeader nvarchar(10)
	IF @MinUploadDate IS NULL
		SET @MinUploadDate = '01/01/1753'
	IF @MaxUploadDate IS NULL
		SET @MaxUploadDate = '12/31/9999'

	IF @IsHighLoan IS NULL
		SET @IsHighLoan = 0
	IF @IsDebtCoverageRatio IS NULL
		SET @IsDebtCoverageRatio = 0
	IF @IsWorkingCapital IS NULL
		SET @IsWorkingCapital = 0
	IF @IsDaysCashOnHand IS NULL
		SET @IsDaysCashOnHand = 0
	IF @IsDaysInAcctReceivable IS NULL
		SET @IsDaysInAcctReceivable = 0
	IF @IsAvgPaymentPeriod IS NULL
		SET @IsAvgPaymentPeriod = 0

    IF     @ReportType = @RatioExceptionsReport 
	   AND @IsDebtCoverageRatio = 0
	   AND @IsWorkingCapital = 0
	   AND @IsDaysCashOnHand = 0
	   AND @IsDaysInAcctReceivable = 0
	   AND @IsAvgPaymentPeriod = 0
		SET @allRatioExceptionsReport = 1
	ELSE
		SET @allRatioExceptionsReport = 0

	SELECT ld.FHANumber							"FhaNumber"
		  ,ld.ProjectName						"ProjectName"
		  ,ld.ServiceName						"LenderName"
		  ,ld.DataInserted						"UploadDate"
		  ,pri.Amortized_Unpaid_Principal_Bal	"UnpaidPrincipalBalance"
		  ,CASE
	       WHEN pri.Amortized_Unpaid_Principal_Bal  >= 25000000
		   THEN
		       CAST(1 AS BIT)
		   ELSE
		       CAST(0 AS BIT)
		   END									"IsHighRisk"
		  ,ld.PeriodEnding						"PeriodEndingDate"
		  ,  convert(int, ld.MonthsInPeriod)					"MonthsinPeriod"
		  ,ld.DataInserted						"DataInserted"
		  ,isnull(ld.DebtCoverageRatio,0)					"DebtCoverageRatio"
		  ,CASE
	       WHEN isnull(ld.DebtCoverageRatio,0)  < 1.2
		   THEN
		       CAST(1 AS BIT)
		   ELSE
		       CAST(0 AS BIT)
		   END									"IsDebtCoverageRatioException"
		  ,isnull(ld.DaysCashOnHand,0)					"DaysCashOnHand"
		  ,CASE
	       WHEN isnull(ld.DaysCashOnHand,0)  < 14
		   THEN
		       CAST(1 AS BIT)
		   ELSE
		       CAST(0 AS BIT)
		   END									"IsDaysCashOnHandException"
		  ,isnull(ld.AvgPaymentPeriod,0)					"AvgPaymentPeriod"
		  ,CASE
	       WHEN isnull(ld.AvgPaymentPeriod,9999)  > 70
		   THEN
		       CAST(1 AS BIT)
		   ELSE
		       CAST(0 AS BIT)
		   END									"IsAvgPaymentPeriodException"
		  ,isnull(ld.WorkingCapital,0)					"WorkingCapital"
		  ,CASE
	       WHEN isnull(ld.WorkingCapital,0)  < 1.2
		   THEN
		       CAST(1 AS BIT)
		   ELSE
		       CAST(0 AS BIT)
		   END									"IsWorkingCapitalScoreException"
		  ,isnull(ld.DaysInAcctReceivable,0)				"DaysInAcctReceivable"
		  ,CASE
	       WHEN isnull(ld.DaysInAcctReceivable,9999)  > 70
		   THEN
		       CAST(1 AS BIT)
		   ELSE
		       CAST(0 AS BIT)
		   END									"IsDaysInAcctReceivableException"
		  ,wlmi.HUD_WorkLoad_Manager_Name		"HudWorkloadManagerName"
		  ,pm.HUD_Project_Manager_Name			"HudProjectManagerName"
	
	  FROM (SELECT * FROM [$(IntermDB)].[dbo].[Lender_DataUpload_Intermediate]
			WHERE ldi_id in (SELECT MAX(ldi.ldi_id) ldi_id	
							 FROM [$(IntermDB)].[dbo].[Lender_DataUpload_Intermediate] ldi,
								  [dbo].fn_HCP_GetPropertyList(@WlmId, @AeId, @LenderId) fp
							 WHERE ldi.PropertyID =fp.PropertyID
							 GROUP BY ldi.FHANumber, ldi.PropertyID)
			AND	  CONVERT(DATE, DataInserted) >= @MinUploadDate
			AND   CONVERT(DATE, DataInserted) <= @MaxUploadDate
			AND   ((@allRatioExceptionsReport = 0
    	   			AND   (@IsDebtCoverageRatio = 0 OR DebtCoverageRatio < 1.2)
					AND   (@IsDaysCashOnHand = 0 OR DaysCashOnHand < 14)
					AND   (@IsAvgPaymentPeriod = 0 OR AvgPaymentPeriod > 70)
					AND   (@IsWorkingCapital = 0 OR WorkingCapital < 1.2)
					AND   (@IsDaysInAcctReceivable = 0 OR DaysInAcctReceivable > 70)
				   ) OR 
				   (@allRatioExceptionsReport = 1 AND
				    (     DebtCoverageRatio < 1.2
					 OR   DaysCashOnHand < 14
					 OR   AvgPaymentPeriod > 70
					 OR   WorkingCapital < 1.2
					 OR   DaysInAcctReceivable > 70
				    )
				   )
				  )
		   ) ld
		   INNER JOIN [dbo].[ProjectInfo] pri
		   ON  pri.PropertyID = ld.PropertyID
		   AND   pri.FHANumber = ld.FHANumber
		   AND (@IsHighLoan = 0 OR pri.Amortized_Unpaid_Principal_Bal >= 25000000)
		   left outer join [dbo].[HUD_WorkLoad_Manager] wlmi
		   on wlmi.HUD_WorkLoad_Manager_ID = pri.HUD_WorkLoad_Manager_ID
		   left outer join [dbo].[HUD_Project_Manager] pm
		   on pm.HUD_Project_Manager_ID = pri.HUD_Project_Manager_ID
		   left outer join [dbo].[LenderInfo] li
		   on li.LenderId = pri.LenderId
		ORDER BY LenderName, MonthsinPeriod, DataInserted, FhaNumber

