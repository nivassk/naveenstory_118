﻿
CREATE PROCEDURE [dbo].[usp_HCP_Get_NonCriticalLateAlertInfo]
(
	@Date nvarchar(20)
)
AS
BEGIN
	SET NOCOUNT ON
	IF 0=1 SET FMTONLY OFF
	DECLARE @dtNow DATETIME
	
	IF @Date IS NOT NULL AND LEN(@Date) > 0
		SET @dtNow = CONVERT(DATETIME, @Date)
	ELSE
		SET @dtNow = GETDATE()

	CREATE TABLE #TempNCRAlertInfo
	(
		PropertyId int not null,
		LenderId int not null,
		FhaNumber nvarchar(15) not null,
		PropertyName nvarchar(100) not null,
		LenderName nvarchar(100) not null,
		RecipientIds nvarchar(100) not null,
		TimeSpanId int not null,
		PRIMARY KEY (PropertyId, LenderId, FhaNumber, TimeSpanId)
	);

	-- Get records where NCR has not been submitted after 90 days
	INSERT INTO #TempNCRAlertInfo 
	SELECT * FROM dbo.fn_HCP_GetLateNCRs(0, 90, @dtNow)

	-- Get records where NCR has not been submitted after 180 days
	INSERT INTO #TempNCRAlertInfo 
	SELECT * FROM dbo.fn_HCP_GetLateNCRs(1, 180, @dtNow)

	-- Get records where NCR has not been submitted after 270 days
	INSERT INTO #TempNCRAlertInfo 
	SELECT * FROM dbo.fn_HCP_GetLateNCRs(2, 270, @dtNow)

	-- Get records where NCR has more than 50% of its balance after 270 days
	INSERT INTO #TempNCRAlertInfo 
	SELECT * FROM dbo.fn_HCP_GetLateNCRs(3, 270, @dtNow)

	-- If FHA record meets multiple alert criterias, delete all records
	-- except one with max TimeSpanId in order to send only one notification per FHA #
	DELETE tempRaw FROM #TempNCRAlertInfo tempRaw LEFT JOIN
	(
		SELECT temp.PropertyId, temp.LenderId, temp.FhaNumber, temp.PropertyName, temp.LenderName, temp.RecipientIds, temp.TimeSpanId
		FROM #TempNCRAlertInfo temp INNER JOIN 
		(SELECT PropertyId, LenderId, FhaNumber, MAX(TimeSpanId) AS MaxTimeSpanId
		FROM #TempNCRAlertInfo
		GROUP BY PropertyId, LenderId, FhaNumber) groupedTemp
		ON temp.PropertyId = groupedTemp.PropertyId AND temp.LenderId = groupedTemp.LenderId 
		AND temp.FhaNumber = groupedTemp.FhaNumber AND temp.TimeSpanId = groupedTemp.MaxTimeSpanId
	) tempCleaned
	ON tempRaw.PropertyId = tempCleaned.PropertyId AND tempRaw.LenderId = tempCleaned.LenderId
	AND tempRaw.FhaNumber = tempCleaned.FhaNumber AND tempRaw.TimeSpanId = tempCleaned.TimeSpanId
	WHERE tempCleaned.TimeSpanId IS NULL

	DECLARE @LenderId int
	DECLARE @FhaNumber nvarchar(15)
	DECLARE @RecipientId nvarchar(10)
	DECLARE @UserRoles nvarchar(15)
	SET @UserRoles = '11, 12, 13'	-- LAR, BAM, LAM (AE(8) is excluded since AE ID is already stored)

	-- Iterate through the temp table and update the RecipientIds fields 
	-- with other Lender User IDs as a single nvarchar value
	DECLARE cursorNCR CURSOR FOR SELECT LenderId, FhaNumber, RecipientIds FROM #TempNCRALertInfo
	OPEN cursorNCR
		FETCH NEXT FROM cursorNCR INTO @LenderId, @FhaNumber, @RecipientId

		WHILE @@FETCH_STATUS = 0 BEGIN
			DECLARE @UserIds nvarchar(100)
			SET @UserIds = @RecipientId
			SELECT @UserIds = @UserIds + ', ' + CONVERT(nvarchar(10), User_ID) FROM dbo.fn_HCP_GetUserIdsForLender(@FhaNumber, @LenderId, @UserRoles)
			UPDATE #TempNCRALertInfo SET RecipientIds = @UserIds WHERE LenderId = @LenderId AND FhaNumber = @FhaNumber
			
			FETCH NEXT FROM cursorNCR INTO @LenderId, @FhaNumber, @RecipientId
		END
	CLOSE cursorNCR
	DEALLOCATE cursorNCR

	SELECT PropertyId, LenderId, FhaNumber, PropertyName, LenderName, RecipientIds, TimeSpanId
	FROM #TempNCRAlertInfo
	ORDER BY PropertyId, LenderId, FhaNumber
END

