﻿CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetWPUpload]
(
@TaskInstanceId uniqueidentifier,
@UserId int,
@Status int,
@PageTypeId int
)
AS
/*
Date		Changed by		Reason
====		=========		======
20180223	skumar		    Task#1267. Sow form290 documents on the executed closing grid

*/
BEGIN

Declare @Form290TaskinstanceId uniqueidentifier
Declare @FHANumber varchar(10)
					IF(@PageTypeId = 17)
					Begin
						Set @FHANumber = (select top 1 FhaNumber from [$(TaskDB)].dbo.Task where TaskinstanceId = @TaskInstanceId)
						Set @Form290TaskinstanceId = (Select top 1 TaskinstanceId from [$(TaskDB)].dbo.Task where FhaNumber = @FHANumber and PageTypeId = 16)
						
						Select convert(nvarchar(max),fs.[FolderKey] )as id,
						 fs.[FolderName] as folderNodeName,
						 fs.[level] as [level],
						 convert(nvarchar(max),fs.[ParentKey] )as parent,
						 cast(0 as bit) as isLeaf,
						 cast(1 as bit) as expanded,
						 cast(1 as bit) as loaded,
						 null as fileId,
						 null as fileName,
						 null as fileSize,
						 null as uploadDate,
						 null as DocTypeID
						 ,@status as Status
                     
						 from [$(TaskDB)].dbo.Prod_FolderStructure fs  where fs.[FolderKey] not in (1,2) and fs.viewtypeid=2
                     
						 union

						 Select convert(nvarchar(max),tf.[FolderKey] ) +'_' +convert(nvarchar(max), tfile.[FileId] )as id,
						 tfile.[Filename] as folderNodeName,
						 cast(1 as bit) as [level],
						 convert(nvarchar(max),tf.[FolderKey] )as parent,
						 cast(1 as bit) as isLeaf,
						 cast(1 as bit) as expanded,
						 cast(1 as bit) as loaded,
						 tfile.[fileId] as fileId,
						 tfile.[fileName] as fileName,
						 tfile.[fileSize] as fileSize,
						 tfile.[uploadTime] as uploadDate,
						 tfile.[DocTypeID] as DocTypeID,
						 @status as Status
						 from [$(TaskDB)].dbo.Prod_FolderStructure fs 
						 inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on fs.[FolderKey] = tf.[FolderKey]
						 inner join [$(TaskDB)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
						 where tfile.TaskInstanceId = @TaskInstanceId  and fs.viewtypeid=2
                     
						-- and tfile.CreatedBy=@UserId

						union

						Select convert(nvarchar(max),tf.[FolderKey] ) +'_' +convert(nvarchar(max), tfile.[FileId] )as id,
						 tfile.[Filename] as folderNodeName,
						 cast(1 as bit) as [level],
						 convert(nvarchar(max),tf.[FolderKey] )as parent,
						 cast(1 as bit) as isLeaf,
						 cast(1 as bit) as expanded,
						 cast(1 as bit) as loaded,
						 tfile.[fileId] as fileId,
						 tfile.[fileName] as fileName,
						 tfile.[fileSize] as fileSize,
						 tfile.[uploadTime] as uploadDate,
						 tfile.[DocTypeID] as DocTypeID,
						 @status as Status
						 from [$(TaskDB)].dbo.Prod_FolderStructure fs 
						 inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on fs.[FolderKey] = tf.[FolderKey]
						 inner join [$(TaskDB)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
						 where tfile.TaskInstanceId = @Form290TaskinstanceId  and fs.viewtypeid=2
					End
					Else
					Begin
						Select convert(nvarchar(max),fs.[FolderKey] )as id,
						 fs.[FolderName] as folderNodeName,
						 fs.[level] as [level],
						 convert(nvarchar(max),fs.[ParentKey] )as parent,
						 cast(0 as bit) as isLeaf,
						 cast(1 as bit) as expanded,
						 cast(1 as bit) as loaded,
						 null as fileId,
						 null as fileName,
						 null as fileSize,
						 null as uploadDate,
						 null as DocTypeID
						 ,@status as Status
                     
						 from [$(TaskDB)].dbo.Prod_FolderStructure fs  where fs.[FolderKey] not in (1,2) and fs.viewtypeid=2
                     
						 union

						 Select convert(nvarchar(max),tf.[FolderKey] ) +'_' +convert(nvarchar(max), tfile.[FileId] )as id,
						 tfile.[Filename] as folderNodeName,
						 cast(1 as bit) as [level],
						 convert(nvarchar(max),tf.[FolderKey] )as parent,
						 cast(1 as bit) as isLeaf,
						 cast(1 as bit) as expanded,
						 cast(1 as bit) as loaded,
						 tfile.[fileId] as fileId,
						 tfile.[fileName] as fileName,
						 tfile.[fileSize] as fileSize,
						 tfile.[uploadTime] as uploadDate,
						 tfile.[DocTypeID] as DocTypeID,
						 @status as Status
						 from [$(TaskDB)].dbo.Prod_FolderStructure fs 
						 inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on fs.[FolderKey] = tf.[FolderKey]
						 inner join [$(TaskDB)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
						 where tfile.TaskInstanceId = @TaskInstanceId  and fs.viewtypeid=2
                     
                    -- and tfile.CreatedBy=@UserId
					End

                           
                       

              
end