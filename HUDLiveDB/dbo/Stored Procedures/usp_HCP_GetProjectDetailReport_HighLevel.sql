
CREATE PROCEDURE [dbo].[usp_HCP_GetProjectDetailReport_HighLevel]
@UserType NVARCHAR(100),
@Username  NVARCHAR(100),
@SortOrder NVARCHAR(10),
@QueryDate NVARCHAR(50)
AS 

BEGIN


declare @LenderIntermediate TABLE (

LDI_ID INT,
FHANumber nvarchar(15)  NULL,
ProjectName nvarchar(100)  NULL,
ServiceName nvarchar(100) NULL,
UnitsInFacility INT NULL,
PeriodEnding NVARCHAR(25) NULL,
MonthsInPeriod NVARCHAR(5),
NOICumulative DECIMAL(19,2) NULL,
QuaterlyNoi DECIMAL(19,2) NULL,
NOIQuaterbyQuater DECIMAL(19,2) NULL,
NOIPercentageChange DECIMAL(19,2) NULL,
CumulativeRevenue DECIMAL(19,2) NULL,
CumulativeExpenses DECIMAL(19,2) NULL,
QuaterlyOperatingRevenue  DECIMAL(19,2) NULL,
QuarterlyOperatingExpense DECIMAL(19,2) NULL,
RevenueQuarterbyQuarter DECIMAL(19,2) NULL,
RevenuePercentageChange DECIMAL(19,2) NULL,
DSCRDifferencePerQuarter DECIMAL(19,2) NULL,
DSCRPercentageChange DECIMAL(19,2) NULL,
CumulativeResidentDays DECIMAL(19,2) NULL,
QuarterlyResidentDays DECIMAL(19,2) NULL,
ADRperQuarter DECIMAL(19,2) NULL,
ADRDifferencePerQuarter DECIMAL(19,2) NULL,
ADRPercentageChange DECIMAL(19,2) NULL,
TotalErrors int
)
declare  @Summary_WLM table
(
RowNum INT,
HudWorkLoadManagerName NVARCHAR(100) NULL,
WLMEmail NVARCHAR(100) NULL,
ProjectCntWLM INT NULL,
ErrorCountWLM INT NULL,
WLMiD INT NULL
)

declare  @Summary_PM table
(

HUD_Project_Manager_Name NVARCHAR(100) NULL,
AEEmail NVARCHAR(100) NULL,
--HudWorkLoadManagerName NVARCHAR(100) NULL,
ProjectCntAE INT NULL,
ErrorCountAE INT NULL,
WLMiD INT NULL
)

declare  @Summary_PM_Filtered table
(

HUD_Project_Manager_Name NVARCHAR(100) NULL,
AEEmail NVARCHAR(100) NULL,
--HudWorkLoadManagerName NVARCHAR(100) NULL,
ProjectCntAE INT NULL,
ErrorCountAE INT NULL,
WLMiD INT NULL
)

--Table holding final output--
declare  @Summary_Final table
(

WLMName NVARCHAR(max) NULL,
Email NVARCHAR(100) NULL,
ProjectCountWLM INT NULL,
ErrorCountWLM INT NULL,
ProjectCountAE INT NULL,
ErrorCountAE INT NULL,
IsWLM INT NULL,
RowNum INT

)

DECLARE @Summary_Temp table
(
   FHANumber NVARCHAR(100) NULL,
    ProjectName NVARCHAR(100) NULL,
	ServiceName NVARCHAR(100) NULL,
	HudWorkLoadManagerName NVARCHAR(100) NULL,
	WLMEmail NVARCHAR(100) NULL,
	HUD_Project_Manager_Name NVARCHAR(100) NULL,
	AEEmail NVARCHAR(100) NULL,
	PMId int NULL,
	TotalErrors INT,
	WLMiD int
)


DECLARE @Summary_Temp_NoDuplicate table
(
   FHANumber NVARCHAR(100) NULL,
    ProjectName NVARCHAR(100) NULL,
	ServiceName NVARCHAR(100) NULL,
	HudWorkLoadManagerName NVARCHAR(100) NULL,
	WLMEmail NVARCHAR(100) NULL,
	HUD_Project_Manager_Name NVARCHAR(100) NULL,
	AEEmail NVARCHAR(100) NULL,
	PMId int NULL,
	TotalErrors INT,
	WLMiD int
)

declare @dt  Datetime
set @dt=convert(datetime, @QueryDate, 101)

INSERT INTO @LenderIntermediate SELECT DISTINCT a.LDI_ID, FHANumber ,ProjectName,ServiceName,   UnitsInFacility,PeriodEnding ,MonthsInPeriod ,NOICumulative , QuaterlyNoi ,NOIQuaterbyQuater ,NOIPercentageChange  , CumulativeRevenue,CumulativeExpenses ,QuaterlyOperatingRevenue ,QuarterlyOperatingExpense,RevenueQuarterbyQuarter ,RevenuePercentageChange , DSCRDifferencePerQuarter,DSCRPercentageChange ,CumulativeResidentDays,QuarterlyResidentDays,ADRperQuarter,ADRDifferencePerQuarter,ADRPercentageChange,a.IsProjectError            
from [$(IntermDB)].dbo. Lender_DataUpload_Intermediate a   join
(select FHANumber as FHA  , MonthsInPeriod as MIP , max (LDI_ID) as ldi_id_new,YEAR( CONVERT ( DATETIME, PeriodEnding )) as YR
from [$(IntermDB)].dbo.[Lender_DataUpload_Intermediate]
group by FHANumber,MonthsInPeriod,YEAR( CONVERT ( DATETIME, PeriodEnding ))) b
 on a .FHANumber = b.FHA and a. LDI_ID = b .ldi_id_new   and    CONVERT(VARCHAR(10),cast(a.datainserted as date), 101)>@dt

 Union
 SELECT DISTINCT a.LDI_ID, FHANumber ,ProjectName,ServiceName,   UnitsInFacility,PeriodEnding ,MonthsInPeriod ,NOICumulative , QuaterlyNoi ,NOIQuaterbyQuater ,NOIPercentageChange  , CumulativeRevenue,CumulativeExpenses ,QuaterlyOperatingRevenue ,QuarterlyOperatingExpense,RevenueQuarterbyQuarter ,RevenuePercentageChange , DSCRDifferencePerQuarter,DSCRPercentageChange ,CumulativeResidentDays,QuarterlyResidentDays,ADRperQuarter,ADRDifferencePerQuarter,ADRPercentageChange,a.IsProjectError            
from [$(IntermDB)].dbo. Lender_DataUpload_Intermediate_InActive a   join
(select FHANumber as FHA  , MonthsInPeriod as MIP , max (LDI_ID) as ldi_id_new,YEAR( CONVERT ( DATETIME, PeriodEnding )) as YR
from [$(IntermDB)].dbo.[Lender_DataUpload_Intermediate_InActive]
group by FHANumber,MonthsInPeriod,YEAR( CONVERT ( DATETIME, PeriodEnding ))) b
 on a .FHANumber = b.FHA and a. LDI_ID = b .ldi_id_new   and    CONVERT(VARCHAR(10),cast(a.datainserted as date), 101)>@dt

 --SELECT * FROM  @LenderIntermediate
--SET @UserType='AccountExecutive'
--SET @Username ='EDWARD.E.CHLYSTEK@hud.gov'



 IF (@UserType ='AccountExecutive')
BEGIN
INSERT INTO @Summary_Temp SELECT   LDI.FHANumber , LDI.ProjectName, LDI.ServiceName, wlm.HUD_WorkLoad_Manager_Name,NULL AS WLMEmail,ae.HUD_Project_Manager_Name,@Username AS AEEmail,ae.HUD_Project_Manager_ID,TotalErrors,wlm.HUD_WorkLoad_Manager_ID
FROM  @LenderIntermediate LDI
LEFT JOIN dbo.ProjectInfo p ON
LDI.FHANumber = p.FHANumber
JOIN dbo.HUD_Project_Manager ae ON p.HUD_Project_Manager_ID = ae.HUD_Project_Manager_ID
JOIN address a ON a.AddressID = ae.AddressID
JOIN dbo.HUD_WorkLoad_Manager wlm ON p.HUD_WorkLoad_Manager_ID = wlm.HUD_WorkLoad_Manager_ID 
WHERE  a.email =  @Username ORDER BY LDI.FHANumber,CONVERT ( DATETIME, LDI.PeriodEnding )
END

ELSE IF (@UserType = 'WorkflowManager')
BEGIN
INSERT INTO @Summary_Temp SELECT  LDI.FHANumber , LDI.ProjectName, LDI.ServiceName, wlm.HUD_WorkLoad_Manager_Name,@Username AS PMEmail,ae.HUD_Project_Manager_Name,a.Email AS  AEEmail,ae.HUD_Project_Manager_ID,TotalErrors,wlm.HUD_WorkLoad_Manager_ID
FROM  @LenderIntermediate LDI
LEFT JOIN dbo.ProjectInfo p ON
LDI.FHANumber = p.FHANumber
JOIN dbo.HUD_Project_Manager ae ON p.HUD_Project_Manager_ID = ae.HUD_Project_Manager_ID
JOIN address a ON a.AddressID = ae.AddressID
JOIN dbo.HUD_WorkLoad_Manager wlm ON p.HUD_WorkLoad_Manager_ID = wlm.HUD_WorkLoad_Manager_ID
AND P.HUD_WorkLoad_Manager_ID = (SELECT HUD_WorkLoad_Manager_ID FROM [$(DatabaseName)].dbo.HUD_WorkLoad_Manager WHERE AddressID =(SELECT AddressID FROM [$(DatabaseName)].dbo.Address WHERE Email = @Username)) ORDER BY LDI.FHANumber,CONVERT ( DATETIME, LDI.PeriodEnding )
END 

ELSE IF (@UserType = 'HUDAdmin' OR @UserType = 'SuperUser' OR @UserType = 'HUDDirector')
BEGIN
INSERT INTO @Summary_Temp SELECT  LDI.FHANumber , LDI.ProjectName, LDI.ServiceName, wlm.HUD_WorkLoad_Manager_Name,a.Email AS PMEmail,ae.HUD_Project_Manager_Name,[dbo].[fn_HCP_GetAEEmailbyId](ae.HUD_Project_Manager_ID) AS AEEmail,ae.HUD_Project_Manager_ID,TotalErrors,wlm.HUD_WorkLoad_Manager_ID
FROM  @LenderIntermediate LDI
LEFT JOIN dbo.ProjectInfo p ON
LDI.FHANumber = p.FHANumber
JOIN dbo.HUD_Project_Manager ae ON p.HUD_Project_Manager_ID = ae.HUD_Project_Manager_ID

JOIN dbo.HUD_WorkLoad_Manager wlm ON p.HUD_WorkLoad_Manager_ID = wlm.HUD_WorkLoad_Manager_ID 
JOIN address a ON a.AddressID = wlm.AddressID
ORDER BY LDI.FHANumber,CONVERT ( DATETIME, LDI.PeriodEnding )
END


--SELECT * FROM @Summary_Temp 

INSERT INTO  @Summary_Temp_NoDuplicate SELECT DISTINCT * FROM @Summary_Temp  where TotalErrors=1 or TotalErrors is null

--SELECT * FROM  @Summary_Temp_NoDuplicate 

IF (@SortOrder ='ASC')
BEGIN
INSERT INTO @Summary_WLM  SELECT DISTINCT  DENSE_RANK() OVER ( ORDER BY   p.HudWorkLoadManagerName asc) AS RowNum ,p. HudWorkLoadManagerName,p.WLMEmail,p.ProjectCntWLM,ISNULL(e.ErrorCountWLM,0) AS ErrorCountWLM,p. WLMiD FROM( SELECT  WLMiD, COUNT (DISTINCT fhanumber ) as ProjectCntWLM,HudWorkLoadManagerName,WLMEmail FROM @Summary_Temp_NoDuplicate GROUP BY HudWorkLoadManagerName,WLMiD,WLMEmail ) p

LEFT JOIN 

(SELECT WLMiD,COUNT (DISTINCT fhanumber ) as ErrorCountWLM,HudWorkLoadManagerName FROM @Summary_Temp_NoDuplicate WHERE TotalErrors = 1 GROUP BY HudWorkLoadManagerName,WLMiD  ) e

ON p.WLMiD=e.WLMiD
END
ELSE IF (@SortOrder = 'DESC')
BEGIN
INSERT INTO @Summary_WLM  SELECT DISTINCT  DENSE_RANK() OVER ( ORDER BY   p.HudWorkLoadManagerName desc) AS RowNum ,p. HudWorkLoadManagerName,p.WLMEmail,p.ProjectCntWLM,ISNULL(e.ErrorCountWLM,0) AS ErrorCountWLM,p. WLMiD FROM( SELECT  WLMiD, COUNT (DISTINCT fhanumber ) as ProjectCntWLM,HudWorkLoadManagerName,WLMEmail FROM @Summary_Temp_NoDuplicate GROUP BY HudWorkLoadManagerName,WLMiD,WLMEmail ) p

LEFT JOIN 

(SELECT WLMiD,COUNT (DISTINCT fhanumber ) as ErrorCountWLM,HudWorkLoadManagerName FROM @Summary_Temp_NoDuplicate WHERE TotalErrors = 1 GROUP BY HudWorkLoadManagerName,WLMiD  ) e

ON p.WLMiD=e.WLMiD
END



--SELECT * FROM @Summary_WLM 
 
 INSERT INTO @Summary_PM SELECT  distinct HUD_Project_Manager_Name, AEEmail ,COUNT (DISTINCT fhanumber ) as ProjectCntAE, (CASE WHEN totalerrors = 1   then count(distinct fhanumber) ELSE 0 END) as ErrorCountAE  , WLMiD FROM @Summary_Temp_NoDuplicate GROUP BY HUD_Project_Manager_Name,WLMiD,pmid,wlmid,totalerrors,AEEmail
--INSERT INTO @Summary_PM SELECT  distinct t1.HUD_Project_Manager_Name, t1.AEEmail ,t1 .ProjectCntAE, ISNULL(t2.ErrorCountAE,0) AS ErrorCountAE ,t1. WLMiD
--FROM (
--SELECT distinct  WLMiD, COUNT (DISTINCT fhanumber ) as ProjectCntAE,HUD_Project_Manager_Name,AEEmail,convert(int,pmid+wlmid) as AEPMID FROM @Summary_Temp_NoDuplicate GROUP BY HUD_Project_Manager_Name,WLMiD,AEEmail,pmid,wlmid,totalerrors
--)t1
--INNER JOIN
--(
--SELECT distinct WLMiD,(CASE WHEN totalerrors = 1 or  totalerrors=2   then count(distinct fhanumber) ELSE 0 END) as ErrorCountAE,HUD_Project_Manager_Name,convert(int,pmid+wlmid) as AEPMID FROM @Summary_Temp_NoDuplicate GROUP BY HUD_Project_Manager_Name,WLMiD,pmid,wlmid,totalerrors

--)t2
--ON t2.AEPMID=t1.AEPMID

--insert into @Summary_PM_Filtered SELECT A.HUD_Project_Manager_Name, A.AEEmail, A.ProjectCntAE, A.ErrorCountAE,A.WLMiD
--FROM @Summary_PM A
--INNER JOIN (
--    SELECT wlmid, MAX(ProjectCntAE) as maxprojcnt, MAX(ErrorCountAE) as maxerrcnt
--    FROM @Summary_PM
--    GROUP BY wlmid
--) B ON a.wlmid = b.wlmid AND a.ProjectCntAE = b.maxprojcnt and a.ErrorCountAE = b.maxerrcnt

insert into @Summary_PM_Filtered SELECT 
t1.HUD_Project_Manager_Name,t1.AEEmail,t1.ProjectCntAE,t2.ErrorCountAE,t1.WLMiD
FROM
(select ROW_NUMBER()  OVER (ORDER BY HUD_Project_Manager_Name) AS ID,HUD_Project_Manager_Name,AEEmail,sum(ProjectCntAE) as ProjectCntAE, wlmid from @Summary_PM group by HUD_Project_Manager_Name,AEEmail,wlmid) t1 
INNER JOIN
(select ROW_NUMBER()  OVER (ORDER BY HUD_Project_Manager_Name) AS ID,HUD_Project_Manager_Name,AEEmail,sum(ErrorCountAE) as ErrorCountAE, wlmid from @Summary_PM group by HUD_Project_Manager_Name,AEEmail,wlmid) t2
ON t1.ID = t2.ID

delete from @Summary_PM

insert into @Summary_PM select * from @Summary_PM_Filtered 

--SELECT * FROM @Summary_PM
DECLARE  @rcnt INT ,@rownum int
SET @rownum=0


select @rcnt = min( RowNum ) from @Summary_WLM

while @rcnt is not null
BEGIN
DECLARE @wlmid INT
INSERT INTO @Summary_Final SELECT '<b>' +HudWorkLoadManagerName + '</b>',WLMEmail ,ProjectCntWLM,ErrorCountWLM,NULL,null,1,@rownum AS RowNum FROM @Summary_WLM  WHERE RowNum=@rcnt
SET @rownum=@rownum+1
SELECT @wlmid= WLMiD FROM @Summary_WLM WHERE RowNum=@rcnt
INSERT INTO @Summary_Final  SELECT '<div style="color: blue; float: right;">'+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '+ HUD_Project_Manager_Name +'</div>',AEEmail,NULL,NULL,ProjectCntAE,ErrorCountAE,0,@rownum AS RowNum FROM @Summary_PM WHERE wlmid=@wlmid 

SET @rownum=@rownum+1
select @rcnt = min ( RowNum ) from @Summary_WLM where RowNum > @rcnt
END
SELECT * FROM @Summary_Final ORDER BY RowNum 


END


GO

