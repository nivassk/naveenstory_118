﻿
-- =============================================
-- Author:		<Rashmi Ganapathy>
-- Create date: <3/21/2017>
-- Description:	<Get Production Application details for Sharepoint screen>
-- =============================================
/*
5/17/2018   Rashmi Ganapathy			Task #2869 - Fix for UW Name and Production WLM
*/
CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetGeneralInfoDetailsForSharePointScreen] 
(@TaskinstanceId uniqueidentifier)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	CREATE TABLE #GeneralInfo
	(
		FHANumber varchar(50),
		TaskInstanceId uniqueidentifier,
		FirmReviewStartDate datetime,
		QueueEntryDate datetime,
		ProgramSpecialistAssignedDate datetime,
		ProgramSpecialistCompletedDate datetime,
		IsTaxCreditProperty bit,
		IsContractorProcessedLoan bit,
		IsTwoStageSubmittal bit,
		WLMName varchar(50),
		UnderwriterName varchar(50),
		ProjectStatus varchar(50),
		ProjectStatusAsofDate datetime,
		IsApplicationRejected bit,	
		ApplicationRejectionDate datetime
	);
	
	Insert into #GeneralInfo
	select op.FhaNumber as FHANumber,
	@TaskinstanceId as TaskInstanceId,
	null as FirmReviewStartDate,
	ServicerSubmissionDate as QueueEntryDate,
	null as ProgramSpecialistAssignedDate,
	null as ProgramSpecialistCompletedDate,
	IsLIHTC as IsTaxCreditProperty,
	Convert(bit,0) as IsContractorProcessedLoan,
	Case When fr.ConstructionStageTypeId = 2 Then Convert(bit,1) else Convert(bit,0) End as IsTwoStageSubmittal,	
	null as WLMName,
	null as UnderwriterName,
	Case When tk.TaskStepId = 17 Then 'In Queue' 
		 When tk.TaskStepId = 18 Then 'In Process'
		 When (tk.TaskStepId = 19 OR tk.TaskStepId=15) Then 'Complete' End as ProjectStatus,
	op.ModifiedOn as ProjectStatusAsofDate,
	Case When op.RequestStatus = 5 Then Convert(bit,1) else Convert(bit,0) End as IsApplicationRejected,
	Case When op.RequestStatus = 5 Then op.ModifiedOn else null End as ApplicationRejectionDate
	from OPAForm op
	join Prod_FHANumberRequest fr
	on op.FhaNumber = fr.FHANumber
	join [$(TaskDB)].dbo.Task tk
	on op.TaskInstanceId = tk.TaskInstanceId
	where op.TaskInstanceId = @TaskinstanceId

	update #GeneralInfo
	set ProgramSpecialistAssignedDate =tx.ModifiedOn,
	ProgramSpecialistCompletedDate = tx.CompletedOn,
	UnderwriterName = Concat(au.FirstName,' ',au.LastName)
	from HCP_Authentication au		
	join [$(TaskDB)].dbo.Prod_TaskXref tx
	on au.userid = tx.assignedTo 
	where tx.TaskInstanceId = @TaskinstanceId and 
	tx.ViewId = 1

	update #GeneralInfo
	set WLMName = Concat(au.FirstName,' ',au.LastName)
	from [$(TaskDB)].dbo.Prod_TaskXref tx
	join dbo.HCP_Authentication au
	on tx.AssignedBy = au.userid
	join dbo.webpages_Usersinroles ur
	on au.userid = ur.userid
	join dbo.webpages_roles rl
	on ur.roleid = rl.roleid
	where tx.TaskInstanceId = @TaskinstanceId
	and tx.ViewId = 1
	and rl.rolename = 'ProductionWlm'

	update #GeneralInfo
	set IsContractorProcessedLoan = Case When tx.AssignedTo <> null then Convert(bit,1) else Convert(bit,0) end
	from [$(TaskDB)].dbo.Prod_TaskXref tx
	join [$(TaskDB)].dbo.Task tk
	on tx.Taskinstanceid = tk.taskinstanceid
	where ViewId in (6,7) and tk.TaskInstanceId = @TaskinstanceId

	--update #GeneralInfo
	--set FirmReviewStartDate = ctk.StartTime
	--from [$(TaskDB)].dbo.Task tk
	--join [$(TaskDB)].dbo.Prod_TaskXref tx
	--on tx.TaskInstanceId = tk.TaskInstanceId
	--join [$(TaskDB)].dbo.ParentChildTask pc
	--on tx.TaskXrefid = pc.ParentTaskInstanceId
	--join [$(TaskDB)].dbo.task ctk 
	--on pc.ChildTaskInstanceId = ctk.TaskInstanceId
	--where tx.viewid = 1 and ctk.TaskStepId = 20 and tk.TaskInstanceId = @TaskinstanceId

	update #GeneralInfo
	set FirmReviewStartDate =(SELECT TOP 1 tx.AssignedDate
	from [$(TaskDB)].dbo.Task tk
	join [$(TaskDB)].dbo.Prod_TaskXref tx
	on tx.TaskInstanceId = tk.TaskInstanceId
	where tx.viewid = 1 and tk.TaskInstanceId = @TaskinstanceId)

	Select gi.*, 
	Case When sp.IsLongTermHold is null Then Convert(bit,0) else sp.IsLongTermHold End as IsLongTermHold,
	sp.DateOfHold as DateOfHold,
	sp.ReasonForHold as ReasonForHold,
	Case When sp.IsLoanCommitteApproved is null Then Convert(bit,0) else sp.IsLoanCommitteApproved End as IsLoanCommitteApproved,
	sp.LoanCommitteApprovedDate as LoanCommitteApprovedDate,
	Case When sp.IsFirmCommitmentIssued is null Then Convert(bit,0) else sp.IsFirmCommitmentIssued End as IsFirmCommitmentIssued,
	sp.FirmCommitmentIssuedDate as FirmCommitmentIssuedDate
	from #GeneralInfo gi 
	left join [dbo].[Prod_SharepointScreen] sp
	on gi.TaskInstanceId = sp.TaskInstanceId
	where gi.TaskInstanceId = @TaskinstanceId
END


GO

