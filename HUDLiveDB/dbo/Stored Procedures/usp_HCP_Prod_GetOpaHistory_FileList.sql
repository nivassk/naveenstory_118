﻿CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetOpaHistory_FileList]
(
@TaskInstanceId uniqueidentifier,
@UserId int,
@ViewId int,
@FolderKey int
)
AS
/*  
Date		Changed by		Reason  
====		===========     ======   
03/28/2018  Rashmi			Filter the results by production roles
			Venkatesh
			skumar    
04/02/2018  skumar			HUD user that’s not assigned to the task goes to the PAM report and clicks on the form but can’t see the documents uploaded there.  
04/05/2018  skumar			Lender is seeing Multiple HUD comments under Folder name   
05/21/2018	skumar			show uploaded Lender Documents (before and after hud user assigment)  under review grid
07/03/2018	skumar			Executing Closing a documents are not showing up in the read only task.
07/05/2018	skumar			Closing Showing documents in duplication(taskfileids)
*/ 
BEGIN

DECLARE @LenderId int 
 select @LenderId = count (au.UserID) from [dbo].[HCP_Authentication] au
  inner join [dbo].[webpages_UsersInRoles] wu on wu.UserId = au.UserID
  inner join [dbo].[webpages_Roles] r on r.RoleId = wu.RoleId
   where r.RoleName in ('Lender','LenderAccountManager','LenderAccountRepresentative','BackupAccountManager','InspectionContractor')
  --where r.RoleName in ('Lender','LenderAccountManager','LenderAccountRepresentative','BackupAccountManager','InspectionContractor','HUDAdmin')
  and au.UserID = @UserId


if (@FolderKey = 1)
begin

select  distinct
			case 
			when cnf.[ChildTaskNewFileId] is null then 'New File Request(files not uploaded)'
			else 'New File Request(New Files uploaded)'  end as actionTaken,

			
			 (A.FirstName + ' ' + A.LastName)  as name,

			r.RoleName +'('+rv.[ViewName] +')'  as userRole,

			nf.[Comments] as comment,
			
			nf.[RequestedOn]   as submitDate,

			null as isFileHistory,
		    null as isFileComment,
			null as taskFileId,
			null as fileId ,
			null as [downloadFileId] ,

			null as [fileName],
              		
			null as fileRename,

			null as fileExt,

			null  as fileSize,

			null as isApprove,
				
			null as raiComments

from [$(TaskDB)].[dbo].[NewFileRequest] nf
--tables join to get user info
left join HCP_Authentication a on a.[UserID] = nf.[RequestedBy]
left join webpages_UsersInRoles l on l.UserId = a.userid
left join webpages_Roles r on r.roleid = l.roleid
inner join [$(TaskDB)].[dbo].[Prod_View] rv on rv.[ViewId] = nf.[ReviewerProdViewId]
-- table to get status of the request
left join [$(TaskDB)].[dbo].[ChildTaskNewFiles] cnf on cnf.[ChildTaskInstanceId] = nf.[ChildTaskInstanceId]
left join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.[ChildTaskInstanceId] = nf.[ChildTaskInstanceId]
left join [$(TaskDB)].[dbo].[Prod_TaskXref] xref on xref.[TaskXrefid] = pc.ParentTaskInstanceId
where xref.[TaskInstanceId]= @TaskInstanceId


end
else if (@FolderKey = 2)
begin
select 
			submitresult.actionTaken as actionTaken,			
			submitresult.name  as name,
			submitresult.userRole   as userRole,
			submitresult.comment as comment,			
			submitresult.submitDate   as submitDate,
			null as isFileHistory,
		    null as isFileComment,
			null as taskFileId,
			null as fileId ,
			null as [downloadFileId] ,
			null as [fileName],              		
			null as fileRename,
			null as fileExt,
			null  as fileSize,
			null as isApprove,				
			null as raiComments
			from (
				select distinct
							pcc.[ChildTaskInstanceId] ,
							ts.[TaskDesc] as actionTaken,

			
							(A.FirstName + ' ' + A.LastName)  as name,
							r.RoleName   as userRole,
							t.[Notes]as comment,			
							t.[StartTime]   as submitDate,
							null as isFileHistory,
							null as isFileComment,
							null as taskFileId,
							null as fileId ,
							null as [downloadFileId] ,
							null as [fileName],              		
							null as fileRename,
							null as fileExt,
							null  as fileSize,
							null as isApprove,				
							null as raiComments
				from [$(TaskDB)].[dbo].[Task]  t
				inner join [$(TaskDB)].[dbo].[TaskStep] ts on ts.[TaskStepId] = t.[TaskStepId]
				left join [$(TaskDB)].[dbo].[ParentChildTask] pcc on pcc.[ChildTaskInstanceId] = t.[TaskInstanceId]
				--tables join to get lender info
				left join HCP_Authentication a on a.[UserName] = t.[AssignedBy]
				left join webpages_UsersInRoles l on l.UserId = a.userid
				left join webpages_Roles r on r.roleid = l.roleid
				left join [$(TaskDB)].[dbo].[Prod_TaskXref] xref on xref.[TaskXrefid] = pcc.ParentTaskInstanceId
				--where xref.[TaskInstanceId]= @TaskInstanceId or t.TaskInstanceId =  @TaskInstanceId
				 --or pcc.ParentTaskInstanceId =  @TaskInstanceId
				 where (xref.[TaskInstanceId]= @TaskInstanceId or t.TaskInstanceId =  @TaskInstanceId
				 or pcc.ParentTaskInstanceId =  @TaskInstanceId) and  r.RoleName in ('ProductionUser', 'ProductionWlm','Reviewer','LenderAccountManager')
				 group by pcc.[ChildTaskInstanceId],TaskDesc,(A.FirstName + ' ' + A.LastName),r.RoleName,t.[Notes],t.[StartTime]
				 ) submitresult


end

else if (@LenderId > 0)
begin


select  distinct
			CASE 
			
			WHEn  rfs.[StatusId] = 4 then  rfs.[StatusName] 
			else 'File Upload' end as actionTaken,

			CASE WHEN rfs.[StatusId] =4 then  (ra.FirstName + ' ' + ra.LastName) 
			else (A.FirstName + ' ' + A.LastName)end  as name,

			CASE WHEN rfs.[StatusId] =4 then  rr.RoleName +'('+rv.[ViewName] +')' 
			else r.RoleName end  as userRole,

			null as comment,

			CASE WHEN rfs.[StatusId] =4 then  fileStatus.ModifiedOn 
			else  COALESCE(tfchild.UploadTime, tfparent.UploadTime) end   as submitDate,

			CAST(
				CASE 
					WHEN tfchild.TaskFileId is null 
						THEN 0 
					ELSE 1
				END as bit) as isFileHistory,

		     CAST(
				CASE 
					WHEN rfcHistory.Comment is null 
						THEN 0 
					ELSE 1
				END as bit) as isFileComment,

	   tfparent.TaskFileId as taskFileId,
	   tfparent.FileId as fileId ,
	    COALESCE(tfchild.[FileId], tfparent.[FileId]) as [downloadFileId] ,

		 COALESCE(tfchild.[FileName], tfparent.[FileName])  as [fileName],
              
		
		reverse(Substring(reverse(tfparent.[FileName]), Charindex('.', reverse(tfparent.[FileName]))+1,Len(tfparent.[FileName])))as fileRename,

	  reverse(left(reverse(tfparent.[FileName]), charindex('.', reverse(tfparent.[FileName])) -1)) as fileExt,

	COALESCE(tfchild.fileSize, tfparent.fileSize)  as fileSize,

	  null as isReqAddInfo,
				null as isApprove,
				
	null as 	raiComments,
	fileStatus.[Status] as fileReviewStatusId
		
from [$(TaskDB)].dbo.TaskFile tfparent

-- join tables task and xref table
inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
left join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskInstanceId = t.TaskInstanceId
--table join to folder structure
inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on tf.[TaskFileId] = tfparent.TaskFileId and tf.FolderKey = @FolderKey
--tables join to get lender info
left join HCP_Authentication a on a.UserName = t.AssignedBy
left join webpages_UsersInRoles l on l.UserId = a.userid
left join webpages_Roles r on r.roleid = l.roleid
--table join to get user's review status
left join [$(TaskDB)].dbo.ReviewFileStatus rf on tfparent.TaskFileId = rf.TaskFileId and xref.AssignedTo = rf.ReviewerUserId and xref.ViewId = rf.ReviewerProdViewId
left join [$(TaskDB)].dbo.ReviewStatusList rsl on rsl.StatusId = rf.[Status]
--table join to get file's review status
left join ( 
			select rfs1.[TaskFileId],rfs1.[ModifiedOn],[ReviewerUserId],[ReviewerProdViewId],[Status] from [$(TaskDB)].[dbo].[ReviewFileStatus] rfs1 
			inner join 
			(select TaskFileId, max(ModifiedOn) as ModifiedOn 
			from [$(TaskDB)].[dbo].[ReviewFileStatus]

			group by TaskFileId) rfs2 on rfs2.TaskFileId = rfs1.TaskFileId and rfs2.ModifiedOn = rfs1.ModifiedOn
			
)fileStatus  on fileStatus.TaskFileId = tfparent.TaskFileId
left join [$(TaskDB)].dbo.ReviewStatusList rfs on rfs.StatusId = fileStatus.[Status]
--table join to get users details who made recent update on file
left join webpages_UsersInRoles ru on ru.UserId = fileStatus.ReviewerUserId
left join HCP_Authentication ra on ra.UserID = fileStatus.ReviewerUserId
left join webpages_Roles rr on rr.roleid = ru.roleid
left join [$(TaskDB)].[dbo].[Prod_View] rv on rv.[ViewId] = fileStatus.[ReviewerProdViewId]
--join to get latest user's comment 
left join  (select rfc.FileTaskId,rfc.comment,rfc.CreatedBy,rfc.ReviewerProdViewId from [$(TaskDB)].dbo.ReviewFileComment rfc
			inner join 
			(select FileTaskId,CreatedBy, max(CreatedOn) as MaxDate 
			 from [$(TaskDB)].dbo.[ReviewFileComment]  group by FileTaskId , [CreatedBy] )rfcl on rfc.FileTaskId = rfcl.FileTaskId and rfc.CreatedOn = rfcl.MaxDate and  rfc.CreatedBy = rfcl.CreatedBy)rfcFinal on rfcFinal.FileTaskId = tfparent.TaskFileId and  xref.AssignedTo = rfcFinal.CreatedBy and xref.ViewId = rfcFinal.ReviewerProdViewId
-- table join to get history comments 
left join  (select rfc.FileTaskId,rfc.comment from [$(TaskDB)].dbo.ReviewFileComment rfc
			inner join 
			(select FileTaskId, max(CreatedOn) as MaxDate 
			 from [$(TaskDB)].dbo.[ReviewFileComment]  group by FileTaskId  )rfcl on rfc.FileTaskId = rfcl.FileTaskId and rfc.CreatedOn = rfcl.MaxDate )rfcHistory on rfcHistory.FileTaskId = tfparent.TaskFileId

--join to get latest version of uploaded file
left join (select tf1.FileId, tf1.TaskFileId,tf1.[FileName],tf1.FileData,tf1.fileSize,tf1.UploadTime,tf2.ParentTaskFileId 
		from [$(TaskDB)].dbo.TaskFile tf1
		inner join ( select   tfh.ChildTaskFileId ,tfh.ParentTaskFileId
								from [$(TaskDB)].dbo.TaskFile tfc 
								inner join [$(TaskDB)].dbo.TaskFileHistory tfh on tfc.TaskFileId = tfh.ParentTaskFileId
								inner join  (select tfpf.ParentTaskFileId, max(tfpf.CreatedOn) as MaxDate
											 from [$(TaskDB)].dbo.TaskFileHistory tfpf
											 group by tfpf.ParentTaskFileId) basetable on basetable.ParentTaskFileId = tfh.ParentTaskFileId 
																					  and basetable.MaxDate = tfh.CreatedOn 	) tf2 on tf1.TaskFileId = tf2.ChildTaskFileId
            ) tfchild on tfparent.TaskFileId = tfchild.ParentTaskFileId

  where tfparent.TaskInstanceId =  @TaskInstanceId
    and  r.RoleName in ('LenderAccountManager','BackupAccountManager','LenderAccountRepresentative')  
	--and  (rr.RoleName in ('ProductionUser', 'ProductionWlm','Reviewer')  or r.RoleName in ('LenderAccountManager','BackupAccountManager')) 

end
else if(@ViewId=0)

begin 

select actionTaken, name, userRole, comment, submitDate, isFileHistory, isFileComment, taskFileId, fileId, downloadFileId,fileName, fileRename, fileExt, fileSize, isReqAddInfo,isApprove, max(Approve)  Approve,raiComments, fileReviewStatusId
from ( 
select actionTaken, name, userRole, comment, submitDate, isFileHistory, isFileComment, taskFileId, fileId, downloadFileId, fileName,fileRename, fileExt, fileSize, isReqAddInfo, isapprove,Approve, raiComments, fileReviewStatusId
, ROW_NUMBER() OVER (PARTITION BY actionTaken, name, userRole, comment, submitDate, isFileHistory, isFileComment, taskFileId, fileId, downloadFileId, fileName,fileRename, fileExt, fileSize, isReqAddInfo, fileReviewStatusId ORDER BY Approve Desc, raicomments) rnum
from ( 
select  distinct
			--CASE WHEN rfs.[StatusId] =1then 'File Upload ( '+ rfs.[StatusName] +' )'
			--WHEN  rfs.[StatusId] = 2 then 'File Upload ( '+ rfs.[StatusName] +' )'
			--WHEn  rfs.[StatusId] = 3then 'File Upload ( '+ rfs.[StatusName] +' )'
			--WHEn  rfs.[StatusId] = 4 then  rfs.[StatusName] 
			--WHEN rfs.[StatusId] =5 then 'File Upload ( '+ rfs.[StatusName] +' )' end as actionTaken,

			CASE WHEN rfs.[StatusId] =1then 'File Upload '
			WHEN  rfs.[StatusId] = 2 then 'File Upload '
			WHEn  rfs.[StatusId] = 3then 'File Upload'
			WHEn  rfs.[StatusId] = 4 then  rfs.[StatusName] 
			WHEN rfs.[StatusId] =5 then 'File Upload ' 
			WHEN rfs.[StatusId] =6 then rfs.[StatusName] 
			end as actionTaken, 


			CASE WHEN rfs.[StatusId] =4 then  (ra.FirstName + ' ' + ra.LastName) 
			else (A.FirstName + ' ' + A.LastName)end  as name,

			CASE WHEN rfs.[StatusId] =4 then  rr.RoleName +'('+rv.[ViewName] +')' 
			else r.RoleName end  as userRole,

			null as comment,

			CASE WHEN rfs.[StatusId] =4 then  fileStatus.ModifiedOn 
			else  COALESCE(tfchild.UploadTime, tfparent.UploadTime) end   as submitDate,

			CAST(
				CASE 
					WHEN tfchild.TaskFileId is null 
						THEN 0 
					ELSE 1
				END as bit) as isFileHistory,

		     CAST(
				CASE 
					WHEN rfcHistory.Comment is null 
						THEN 0 
					ELSE 1
				END as bit) as isFileComment,

	   tfparent.TaskFileId as taskFileId,
	   tfparent.FileId as fileId ,
	    COALESCE(tfchild.[FileId], tfparent.[FileId]) as [downloadFileId] ,

		 COALESCE(tfchild.[FileName], tfparent.[FileName])  as [fileName],
              
		
		reverse(Substring(reverse(tfparent.[FileName]), Charindex('.', reverse(tfparent.[FileName]))+1,Len(tfparent.[FileName])))as fileRename,

	  reverse(left(reverse(tfparent.[FileName]), charindex('.', reverse(tfparent.[FileName])) -1)) as fileExt,

	COALESCE(tfchild.fileSize, tfparent.fileSize)  as fileSize,

	   CAST(
				CASE 
					WHEN rf.[Status] = 4
						THEN 1
					WHEN rf.[Status] = 6
						THEN 1  
					ELSE 0
				END as bit) as isReqAddInfo,
				CAST(
				CASE 
					WHEN rf.[Status] = 5
						THEN 1 
					ELSE 0
				END as bit) as isApprove,
				CAST(  
					 CASE   
					 WHEN rf.[Status] = 5  
					  THEN 1   
					 ELSE 0  
					END as int
				) as Approve, 
				raiComments=CASE 
					WHEN rf.[Status] = 4
						THEN rfcFinal.Comment  
					WHEN rf.[Status] = 6
						THEN rfcFinal.Comment
					WHEN rf.[Status] = 5
						THEN COALESCE(rfcFinal.Comment  , '') 
					ELSE ''
					end,

		fileStatus.[Status] as fileReviewStatusId
		
from [$(TaskDB)].dbo.TaskFile tfparent

-- join tables task and xref table
inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
left join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskInstanceId = t.TaskInstanceId
--table join to folder structure
inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on tf.[TaskFileId] = tfparent.TaskFileId and tf.FolderKey = @FolderKey
--tables join to get lender info
left join HCP_Authentication a on a.UserName = t.AssignedBy
left join webpages_UsersInRoles l on l.UserId = a.userid
left join webpages_Roles r on r.roleid = l.roleid
--table join to get user's review status
left join [$(TaskDB)].dbo.ReviewFileStatus rf on tfparent.TaskFileId = rf.TaskFileId and xref.AssignedTo = rf.ReviewerUserId and xref.ViewId = rf.ReviewerProdViewId
left join [$(TaskDB)].dbo.ReviewStatusList rsl on rsl.StatusId = rf.[Status]
--table join to get file's review status
left join ( 
			--select rfs1.[TaskFileId],rfs1.[ModifiedOn],[ReviewerUserId],[ReviewerProdViewId],[Status] from [$(TaskDB)].[dbo].[ReviewFileStatus] rfs1 
			--inner join 
			--(select TaskFileId, max(ModifiedOn) as ModifiedOn 
			--from [$(TaskDB)].[dbo].[ReviewFileStatus]

			--group by TaskFileId) rfs2 on rfs2.TaskFileId = rfs1.TaskFileId and rfs2.ModifiedOn = rfs1.ModifiedOn
			select rfs1.[TaskFileId],rfs1.[ModifiedOn],[ReviewerUserId],[ReviewerProdViewId],[Status] from [$(TaskDB)].[dbo].[ReviewFileStatus] rfs1 
			inner join 
			(select rf.TaskFileId, max(rf.ModifiedOn) as ModifiedOn 
			from [$(TaskDB)].[dbo].[ReviewFileStatus] rf
			inner join [$(TaskDB)].[dbo].[TaskFile] tf on tf.[TaskFileId] = rf.TaskFileId
			inner join [$(TaskDB)].[dbo].[Prod_TaskXref] tx on tx.TaskInstanceId = tf.[TaskInstanceId]
			inner join [dbo].[webpages_UsersInRoles] ur on ur.[UserId] = rf.ReviewerUserId
			inner join  [dbo].[webpages_Roles] r on r.[RoleId] = ur.[RoleId]
			where tx.AssignedTo = rf.ReviewerUserId and tx.ViewId = rf.ReviewerProdViewId 
			      --and  tx.CompletedOn is null
				  --and r.[RoleName] != 'Reviewer' 
			group by rf.TaskFileId) rfs2 on rfs2.TaskFileId = rfs1.TaskFileId and rfs2.ModifiedOn = rfs1.ModifiedOn
)fileStatus  on fileStatus.TaskFileId = tfparent.TaskFileId
left join [$(TaskDB)].dbo.ReviewStatusList rfs on rfs.StatusId = fileStatus.[Status]
--table join to get users details who made recent update on file
left join webpages_UsersInRoles ru on ru.UserId = fileStatus.ReviewerUserId
left join HCP_Authentication ra on ra.UserID = fileStatus.ReviewerUserId
left join webpages_Roles rr on rr.roleid = ru.roleid
left join [$(TaskDB)].[dbo].[Prod_View] rv on rv.[ViewId] = fileStatus.[ReviewerProdViewId]
--join to get latest user's comment 
left join  (select rfc.FileTaskId,rfc.comment,rfc.CreatedBy,rfc.ReviewerProdViewId from [$(TaskDB)].dbo.ReviewFileComment rfc
			inner join 
			(select FileTaskId,CreatedBy, max(CreatedOn) as MaxDate 
			 from [$(TaskDB)].dbo.[ReviewFileComment]  group by FileTaskId , [CreatedBy] )rfcl on rfc.FileTaskId = rfcl.FileTaskId and rfc.CreatedOn = rfcl.MaxDate and  rfc.CreatedBy = rfcl.CreatedBy)rfcFinal on rfcFinal.FileTaskId = tfparent.TaskFileId and  xref.AssignedTo = rfcFinal.CreatedBy and xref.ViewId = rfcFinal.ReviewerProdViewId
-- table join to get history comments 
left join  (select rfc.FileTaskId,rfc.comment from [$(TaskDB)].dbo.ReviewFileComment rfc
			inner join 
			(select FileTaskId, max(CreatedOn) as MaxDate 
			 from [$(TaskDB)].dbo.[ReviewFileComment]  group by FileTaskId  )rfcl on rfc.FileTaskId = rfcl.FileTaskId and rfc.CreatedOn = rfcl.MaxDate )rfcHistory on rfcHistory.FileTaskId = tfparent.TaskFileId

--join to get latest version of uploaded file
left join (select tf1.FileId, tf1.TaskFileId,tf1.[FileName],tf1.FileData,tf1.fileSize,tf1.UploadTime,tf2.ParentTaskFileId 
		from [$(TaskDB)].dbo.TaskFile tf1
		inner join ( select   tfh.ChildTaskFileId ,tfh.ParentTaskFileId
								from [$(TaskDB)].dbo.TaskFile tfc 
								inner join [$(TaskDB)].dbo.TaskFileHistory tfh on tfc.TaskFileId = tfh.ParentTaskFileId
								inner join  (select tfpf.ParentTaskFileId, max(tfpf.CreatedOn) as MaxDate
											 from [$(TaskDB)].dbo.TaskFileHistory tfpf
											 group by tfpf.ParentTaskFileId) basetable on basetable.ParentTaskFileId = tfh.ParentTaskFileId 
																					  and basetable.MaxDate = tfh.CreatedOn 	) tf2 on tf1.TaskFileId = tf2.ChildTaskFileId
            ) tfchild on tfparent.TaskFileId = tfchild.ParentTaskFileId

  where tfparent.TaskInstanceId =  @TaskInstanceId  
		--and --xref.AssignedTo = @UserId   and 
		--xref.ViewId = @ViewId 		
		and  (rr.RoleName in ('ProductionUser', 'ProductionWlm','Reviewer')  or r.RoleName in ('LenderAccountManager','BackupAccountManager','LenderAccountRepresentative')) 
 )t 
  ) temp
 WHERE rnum = 1
 GROUP BY actionTaken, name, userRole, comment, submitDate, isFileHistory, isFileComment, taskFileId, fileId, downloadFileId,fileName, fileRename, fileExt, fileSize,isApprove, isReqAddInfo,raiComments, fileReviewStatusId

end

else

begin

select actionTaken, name, userRole, comment, submitDate, isFileHistory, isFileComment, taskFileId, fileId, downloadFileId,fileName, fileRename, fileExt, fileSize, isReqAddInfo,isApprove, max(Approve)  Approve,raiComments, fileReviewStatusId
from ( 
select actionTaken, name, userRole, comment, submitDate, isFileHistory, isFileComment, taskFileId, fileId, downloadFileId, fileName,fileRename, fileExt, fileSize, isReqAddInfo, isapprove,Approve, raiComments, fileReviewStatusId
, ROW_NUMBER() OVER (PARTITION BY actionTaken, name, userRole, comment, submitDate, isFileHistory, isFileComment, taskFileId, fileId, downloadFileId, fileName,fileRename, fileExt, fileSize, isReqAddInfo, fileReviewStatusId ORDER BY Approve Desc, raicomments) rnum
from ( 
select  distinct
			--CASE WHEN rfs.[StatusId] =1then 'File Upload ( '+ rfs.[StatusName] +' )'
			--WHEN  rfs.[StatusId] = 2 then 'File Upload ( '+ rfs.[StatusName] +' )'
			--WHEn  rfs.[StatusId] = 3then 'File Upload ( '+ rfs.[StatusName] +' )'
			--WHEn  rfs.[StatusId] = 4 then  rfs.[StatusName] 
			--WHEN rfs.[StatusId] =5 then 'File Upload ( '+ rfs.[StatusName] +' )' end as actionTaken,

			CASE WHEN rfs.[StatusId] =1then 'File Upload '
			WHEN  rfs.[StatusId] = 2 then 'File Upload '
			WHEn  rfs.[StatusId] = 3then 'File Upload'
			WHEn  rfs.[StatusId] = 4 then  rfs.[StatusName] 
			WHEN rfs.[StatusId] =5 then 'File Upload ' 
			WHEN rfs.[StatusId] =6 then rfs.[StatusName] 
			end as actionTaken, 


			CASE WHEN rfs.[StatusId] =4 then  (ra.FirstName + ' ' + ra.LastName) 
			else (A.FirstName + ' ' + A.LastName)end  as name,

			CASE WHEN rfs.[StatusId] =4 then  rr.RoleName +'('+rv.[ViewName] +')' 
			else r.RoleName end  as userRole,

			null as comment,

			CASE WHEN rfs.[StatusId] =4 then  fileStatus.ModifiedOn 
			else  COALESCE(tfchild.UploadTime, tfparent.UploadTime) end   as submitDate,

			CAST(
				CASE 
					WHEN tfchild.TaskFileId is null 
						THEN 0 
					ELSE 1
				END as bit) as isFileHistory,

		     CAST(
				CASE 
					WHEN rfcHistory.Comment is null 
						THEN 0 
					ELSE 1
				END as bit) as isFileComment,

	   tfparent.TaskFileId as taskFileId,
	   tfparent.FileId as fileId ,
	    COALESCE(tfchild.[FileId], tfparent.[FileId]) as [downloadFileId] ,

		 COALESCE(tfchild.[FileName], tfparent.[FileName])  as [fileName],
              
		
		reverse(Substring(reverse(tfparent.[FileName]), Charindex('.', reverse(tfparent.[FileName]))+1,Len(tfparent.[FileName])))as fileRename,

	  reverse(left(reverse(tfparent.[FileName]), charindex('.', reverse(tfparent.[FileName])) -1)) as fileExt,

	COALESCE(tfchild.fileSize, tfparent.fileSize)  as fileSize,

	   CAST(
				CASE 
					WHEN rf.[Status] = 4
						THEN 1
					WHEN rf.[Status] = 6
						THEN 1  
					ELSE 0
				END as bit) as isReqAddInfo,
				CAST(
				CASE 
					WHEN rf.[Status] = 5
						THEN 1 
					ELSE 0
				END as bit) as isApprove,
				CAST(  
					 CASE   
					 WHEN rf.[Status] = 5  
					  THEN 1   
					 ELSE 0  
					END as int
				) as Approve, 
				raiComments=CASE 
					WHEN rf.[Status] = 4
						THEN rfcFinal.Comment  
					WHEN rf.[Status] = 6
						THEN rfcFinal.Comment
					WHEN rf.[Status] = 5
						THEN COALESCE(rfcFinal.Comment  , '') 
					ELSE ''
					end,

		fileStatus.[Status] as fileReviewStatusId
		
from [$(TaskDB)].dbo.TaskFile tfparent

-- join tables task and xref table
inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
left join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskInstanceId = t.TaskInstanceId
--table join to folder structure
inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on tf.[TaskFileId] = tfparent.TaskFileId and tf.FolderKey = @FolderKey
--tables join to get lender info
left join HCP_Authentication a on a.UserName = t.AssignedBy
left join webpages_UsersInRoles l on l.UserId = a.userid
left join webpages_Roles r on r.roleid = l.roleid
--table join to get user's review status
left join [$(TaskDB)].dbo.ReviewFileStatus rf on tfparent.TaskFileId = rf.TaskFileId and xref.AssignedTo = rf.ReviewerUserId and xref.ViewId = rf.ReviewerProdViewId
left join [$(TaskDB)].dbo.ReviewStatusList rsl on rsl.StatusId = rf.[Status]
--table join to get file's review status
left join ( 
			--select rfs1.[TaskFileId],rfs1.[ModifiedOn],[ReviewerUserId],[ReviewerProdViewId],[Status] from [$(TaskDB)].[dbo].[ReviewFileStatus] rfs1 
			--inner join 
			--(select TaskFileId, max(ModifiedOn) as ModifiedOn 
			--from [$(TaskDB)].[dbo].[ReviewFileStatus]

			--group by TaskFileId) rfs2 on rfs2.TaskFileId = rfs1.TaskFileId and rfs2.ModifiedOn = rfs1.ModifiedOn
			select rfs1.[TaskFileId],rfs1.[ModifiedOn],[ReviewerUserId],[ReviewerProdViewId],[Status] from [$(TaskDB)].[dbo].[ReviewFileStatus] rfs1 
			inner join 
			(select rf.TaskFileId, max(rf.ModifiedOn) as ModifiedOn 
			from [$(TaskDB)].[dbo].[ReviewFileStatus] rf
			inner join [$(TaskDB)].[dbo].[TaskFile] tf on tf.[TaskFileId] = rf.TaskFileId
			inner join [$(TaskDB)].[dbo].[Prod_TaskXref] tx on tx.TaskInstanceId = tf.[TaskInstanceId]
			inner join [dbo].[webpages_UsersInRoles] ur on ur.[UserId] = rf.ReviewerUserId
			inner join  [dbo].[webpages_Roles] r on r.[RoleId] = ur.[RoleId]
			where tx.AssignedTo = rf.ReviewerUserId and tx.ViewId = rf.ReviewerProdViewId 
			      --and  tx.CompletedOn is null
				  --and r.[RoleName] != 'Reviewer' 
			group by rf.TaskFileId) rfs2 on rfs2.TaskFileId = rfs1.TaskFileId and rfs2.ModifiedOn = rfs1.ModifiedOn
)fileStatus  on fileStatus.TaskFileId = tfparent.TaskFileId
left join [$(TaskDB)].dbo.ReviewStatusList rfs on rfs.StatusId = fileStatus.[Status]
--table join to get users details who made recent update on file
left join webpages_UsersInRoles ru on ru.UserId = fileStatus.ReviewerUserId
left join HCP_Authentication ra on ra.UserID = fileStatus.ReviewerUserId
left join webpages_Roles rr on rr.roleid = ru.roleid
left join [$(TaskDB)].[dbo].[Prod_View] rv on rv.[ViewId] = fileStatus.[ReviewerProdViewId]
--join to get latest user's comment 
left join  (select rfc.FileTaskId,rfc.comment,rfc.CreatedBy,rfc.ReviewerProdViewId from [$(TaskDB)].dbo.ReviewFileComment rfc
			inner join 
			(select FileTaskId,CreatedBy, max(CreatedOn) as MaxDate 
			 from [$(TaskDB)].dbo.[ReviewFileComment]  group by FileTaskId , [CreatedBy] )rfcl on rfc.FileTaskId = rfcl.FileTaskId and rfc.CreatedOn = rfcl.MaxDate and  rfc.CreatedBy = rfcl.CreatedBy)rfcFinal on rfcFinal.FileTaskId = tfparent.TaskFileId and  xref.AssignedTo = rfcFinal.CreatedBy and xref.ViewId = rfcFinal.ReviewerProdViewId
-- table join to get history comments 
left join  (select rfc.FileTaskId,rfc.comment from [$(TaskDB)].dbo.ReviewFileComment rfc
			inner join 
			(select FileTaskId, max(CreatedOn) as MaxDate 
			 from [$(TaskDB)].dbo.[ReviewFileComment]  group by FileTaskId  )rfcl on rfc.FileTaskId = rfcl.FileTaskId and rfc.CreatedOn = rfcl.MaxDate )rfcHistory on rfcHistory.FileTaskId = tfparent.TaskFileId

--join to get latest version of uploaded file
left join (select tf1.FileId, tf1.TaskFileId,tf1.[FileName],tf1.FileData,tf1.fileSize,tf1.UploadTime,tf2.ParentTaskFileId 
		from [$(TaskDB)].dbo.TaskFile tf1
		inner join ( select   tfh.ChildTaskFileId ,tfh.ParentTaskFileId
								from [$(TaskDB)].dbo.TaskFile tfc 
								inner join [$(TaskDB)].dbo.TaskFileHistory tfh on tfc.TaskFileId = tfh.ParentTaskFileId
								inner join  (select tfpf.ParentTaskFileId, max(tfpf.CreatedOn) as MaxDate
											 from [$(TaskDB)].dbo.TaskFileHistory tfpf
											 group by tfpf.ParentTaskFileId) basetable on basetable.ParentTaskFileId = tfh.ParentTaskFileId 
																					  and basetable.MaxDate = tfh.CreatedOn 	) tf2 on tf1.TaskFileId = tf2.ChildTaskFileId
            ) tfchild on tfparent.TaskFileId = tfchild.ParentTaskFileId

  where tfparent.TaskInstanceId =  @TaskInstanceId  
		and --xref.AssignedTo = @UserId   and 
		xref.ViewId = @ViewId 		
		and  (rr.RoleName in ('ProductionUser', 'ProductionWlm','Reviewer')  or r.RoleName in ('LenderAccountManager','BackupAccountManager','LenderAccountRepresentative')) 
 )t 
  ) temp
 WHERE rnum = 1
 GROUP BY actionTaken, name, userRole, comment, submitDate, isFileHistory, isFileComment, taskFileId, fileId, downloadFileId,fileName, fileRename, fileExt, fileSize,isApprove, isReqAddInfo,raiComments, fileReviewStatusId

end

end


-- with old one I am not able to see read only file from lender side and from pam report when it is not assigned to any production user from production que

--CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetOpaHistory_FileList]
--(
--@TaskInstanceId uniqueidentifier,
--@UserId int,
--@ViewId int,
--@FolderKey int
--)
--AS
--/*  
--Date		Changed by		Reason  
--====		===========     ======   
--03/28/2018  Rashmi			Filter the results by production roles
--			Venkatesh
--			skumar    
--04/02/2018  skumar			HUD user that’s not assigned to the task goes to the PAM report and clicks on the form but can’t see the documents uploaded there.  
--04/05/2018  skumar			Lender is seeing Multiple HUD comments under Folder name   
--05/21/2018	skumar			show uploaded Lender Documents (before and after hud user assigment)  under review grid
--07/03/2018	skumar			Executing Closing a documents are not showing up in the read only task.
--07/09/2018	skumar			Closing Showing documents in duplication(taskfileids)
--*/
--BEGIN

--DECLARE @LenderId int 
-- select @LenderId = count (au.UserID) from [dbo].[HCP_Authentication] au
--  inner join [dbo].[webpages_UsersInRoles] wu on wu.UserId = au.UserID
--  inner join [dbo].[webpages_Roles] r on r.RoleId = wu.RoleId
--   where r.RoleName in ('Lender','LenderAccountManager','LenderAccountRepresentative','BackupAccountManager','InspectionContractor')
--  --where r.RoleName in ('Lender','LenderAccountManager','LenderAccountRepresentative','BackupAccountManager','InspectionContractor','HUDAdmin')
--  and au.UserID = @UserId


--if (@FolderKey = 1)
--begin

--select  distinct
--			case 
--			when cnf.[ChildTaskNewFileId] is null then 'New File Request(files not uploaded)'
--			else 'New File Request(New Files uploaded)'  end as actionTaken,

			
--			 (A.FirstName + ' ' + A.LastName)  as name,

--			r.RoleName +'('+rv.[ViewName] +')'  as userRole,

--			nf.[Comments] as comment,
			
--			nf.[RequestedOn]   as submitDate,

--			null as isFileHistory,
--		    null as isFileComment,
--			null as taskFileId,
--			null as fileId ,
--			null as [downloadFileId] ,

--			null as [fileName],
              		
--			null as fileRename,

--			null as fileExt,

--			null  as fileSize,

--			null as isApprove,
				
--			null as raiComments

--from [$(TaskDB)].[dbo].[NewFileRequest] nf
----tables join to get user info
--left join HCP_Authentication a on a.[UserID] = nf.[RequestedBy]
--left join webpages_UsersInRoles l on l.UserId = a.userid
--left join webpages_Roles r on r.roleid = l.roleid
--inner join [$(TaskDB)].[dbo].[Prod_View] rv on rv.[ViewId] = nf.[ReviewerProdViewId]
---- table to get status of the request
--left join [$(TaskDB)].[dbo].[ChildTaskNewFiles] cnf on cnf.[ChildTaskInstanceId] = nf.[ChildTaskInstanceId]
--left join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.[ChildTaskInstanceId] = nf.[ChildTaskInstanceId]
--left join [$(TaskDB)].[dbo].[Prod_TaskXref] xref on xref.[TaskXrefid] = pc.ParentTaskInstanceId
--where xref.[TaskInstanceId]= @TaskInstanceId


--end
--else if (@FolderKey = 2)
--begin
--select 
--			submitresult.actionTaken as actionTaken,			
--			submitresult.name  as name,
--			submitresult.userRole   as userRole,
--			submitresult.comment as comment,			
--			submitresult.submitDate   as submitDate,
--			null as isFileHistory,
--		    null as isFileComment,
--			null as taskFileId,
--			null as fileId ,
--			null as [downloadFileId] ,
--			null as [fileName],              		
--			null as fileRename,
--			null as fileExt,
--			null  as fileSize,
--			null as isApprove,				
--			null as raiComments
--			from (
--				select distinct
--							pcc.[ChildTaskInstanceId] ,
--							ts.[TaskDesc] as actionTaken,

			
--							(A.FirstName + ' ' + A.LastName)  as name,
--							r.RoleName   as userRole,
--							t.[Notes]as comment,			
--							t.[StartTime]   as submitDate,
--							null as isFileHistory,
--							null as isFileComment,
--							null as taskFileId,
--							null as fileId ,
--							null as [downloadFileId] ,
--							null as [fileName],              		
--							null as fileRename,
--							null as fileExt,
--							null  as fileSize,
--							null as isApprove,				
--							null as raiComments
--				from [$(TaskDB)].[dbo].[Task]  t
--				inner join [$(TaskDB)].[dbo].[TaskStep] ts on ts.[TaskStepId] = t.[TaskStepId]
--				left join [$(TaskDB)].[dbo].[ParentChildTask] pcc on pcc.[ChildTaskInstanceId] = t.[TaskInstanceId]
--				--tables join to get lender info
--				left join HCP_Authentication a on a.[UserName] = t.[AssignedBy]
--				left join webpages_UsersInRoles l on l.UserId = a.userid
--				left join webpages_Roles r on r.roleid = l.roleid
--				left join [$(TaskDB)].[dbo].[Prod_TaskXref] xref on xref.[TaskXrefid] = pcc.ParentTaskInstanceId
--				--where xref.[TaskInstanceId]= @TaskInstanceId or t.TaskInstanceId =  @TaskInstanceId
--				 --or pcc.ParentTaskInstanceId =  @TaskInstanceId
--				 where (xref.[TaskInstanceId]= @TaskInstanceId or t.TaskInstanceId =  @TaskInstanceId
--				 or pcc.ParentTaskInstanceId =  @TaskInstanceId) and  r.RoleName in ('ProductionUser', 'ProductionWlm','Reviewer','LenderAccountManager')
--				 group by pcc.[ChildTaskInstanceId],TaskDesc,(A.FirstName + ' ' + A.LastName),r.RoleName,t.[Notes],t.[StartTime]
--				 ) submitresult


--end

--else if (@LenderId > 0)
--begin


--select  distinct
--			CASE 
			
--			WHEn  rfs.[StatusId] = 4 then  rfs.[StatusName] 
--			else 'File Upload' end as actionTaken,

--			CASE WHEN rfs.[StatusId] =4 then  (ra.FirstName + ' ' + ra.LastName) 
--			else (A.FirstName + ' ' + A.LastName)end  as name,

--			CASE WHEN rfs.[StatusId] =4 then  rr.RoleName +'('+rv.[ViewName] +')' 
--			else r.RoleName end  as userRole,

--			null as comment,

--			CASE WHEN rfs.[StatusId] =4 then  fileStatus.ModifiedOn 
--			else  COALESCE(tfchild.UploadTime, tfparent.UploadTime) end   as submitDate,

--			CAST(
--				CASE 
--					WHEN tfchild.TaskFileId is null 
--						THEN 0 
--					ELSE 1
--				END as bit) as isFileHistory,

--		     CAST(
--				CASE 
--					WHEN rfcHistory.Comment is null 
--						THEN 0 
--					ELSE 1
--				END as bit) as isFileComment,

--	   tfparent.TaskFileId as taskFileId,
--	   tfparent.FileId as fileId ,
--	    COALESCE(tfchild.[FileId], tfparent.[FileId]) as [downloadFileId] ,

--		 COALESCE(tfchild.[FileName], tfparent.[FileName])  as [fileName],
              
		
--		reverse(Substring(reverse(tfparent.[FileName]), Charindex('.', reverse(tfparent.[FileName]))+1,Len(tfparent.[FileName])))as fileRename,

--	  reverse(left(reverse(tfparent.[FileName]), charindex('.', reverse(tfparent.[FileName])) -1)) as fileExt,

--	COALESCE(tfchild.fileSize, tfparent.fileSize)  as fileSize,

--	  null as isReqAddInfo,
--				null as isApprove,
				
--	null as 	raiComments,
--	fileStatus.[Status] as fileReviewStatusId
		
--from [$(TaskDB)].dbo.TaskFile tfparent

---- join tables task and xref table
--inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
--left join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskInstanceId = t.TaskInstanceId
----table join to folder structure
--inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on tf.[TaskFileId] = tfparent.TaskFileId and tf.FolderKey = @FolderKey
----tables join to get lender info
--left join HCP_Authentication a on a.UserName = t.AssignedBy
--left join webpages_UsersInRoles l on l.UserId = a.userid
--left join webpages_Roles r on r.roleid = l.roleid
----table join to get user's review status
--left join [$(TaskDB)].dbo.ReviewFileStatus rf on tfparent.TaskFileId = rf.TaskFileId and xref.AssignedTo = rf.ReviewerUserId and xref.ViewId = rf.ReviewerProdViewId
--left join [$(TaskDB)].dbo.ReviewStatusList rsl on rsl.StatusId = rf.[Status]
----table join to get file's review status
--left join ( 
--			select rfs1.[TaskFileId],rfs1.[ModifiedOn],[ReviewerUserId],[ReviewerProdViewId],[Status] from [$(TaskDB)].[dbo].[ReviewFileStatus] rfs1 
--			inner join 
--			(select TaskFileId, max(ModifiedOn) as ModifiedOn 
--			from [$(TaskDB)].[dbo].[ReviewFileStatus]

--			group by TaskFileId) rfs2 on rfs2.TaskFileId = rfs1.TaskFileId and rfs2.ModifiedOn = rfs1.ModifiedOn
			
--)fileStatus  on fileStatus.TaskFileId = tfparent.TaskFileId
--left join [$(TaskDB)].dbo.ReviewStatusList rfs on rfs.StatusId = fileStatus.[Status]
----table join to get users details who made recent update on file
--left join webpages_UsersInRoles ru on ru.UserId = fileStatus.ReviewerUserId
--left join HCP_Authentication ra on ra.UserID = fileStatus.ReviewerUserId
--left join webpages_Roles rr on rr.roleid = ru.roleid
--left join [$(TaskDB)].[dbo].[Prod_View] rv on rv.[ViewId] = fileStatus.[ReviewerProdViewId]
----join to get latest user's comment 
--left join  (select rfc.FileTaskId,rfc.comment,rfc.CreatedBy,rfc.ReviewerProdViewId from [$(TaskDB)].dbo.ReviewFileComment rfc
--			inner join 
--			(select FileTaskId,CreatedBy, max(CreatedOn) as MaxDate 
--			 from [$(TaskDB)].dbo.[ReviewFileComment]  group by FileTaskId , [CreatedBy] )rfcl on rfc.FileTaskId = rfcl.FileTaskId and rfc.CreatedOn = rfcl.MaxDate and  rfc.CreatedBy = rfcl.CreatedBy)rfcFinal on rfcFinal.FileTaskId = tfparent.TaskFileId and  xref.AssignedTo = rfcFinal.CreatedBy and xref.ViewId = rfcFinal.ReviewerProdViewId
---- table join to get history comments 
--left join  (select rfc.FileTaskId,rfc.comment from [$(TaskDB)].dbo.ReviewFileComment rfc
--			inner join 
--			(select FileTaskId, max(CreatedOn) as MaxDate 
--			 from [$(TaskDB)].dbo.[ReviewFileComment]  group by FileTaskId  )rfcl on rfc.FileTaskId = rfcl.FileTaskId and rfc.CreatedOn = rfcl.MaxDate )rfcHistory on rfcHistory.FileTaskId = tfparent.TaskFileId

----join to get latest version of uploaded file
--left join (select tf1.FileId, tf1.TaskFileId,tf1.[FileName],tf1.FileData,tf1.fileSize,tf1.UploadTime,tf2.ParentTaskFileId 
--		from [$(TaskDB)].dbo.TaskFile tf1
--		inner join ( select   tfh.ChildTaskFileId ,tfh.ParentTaskFileId
--								from [$(TaskDB)].dbo.TaskFile tfc 
--								inner join [$(TaskDB)].dbo.TaskFileHistory tfh on tfc.TaskFileId = tfh.ParentTaskFileId
--								inner join  (select tfpf.ParentTaskFileId, max(tfpf.CreatedOn) as MaxDate
--											 from [$(TaskDB)].dbo.TaskFileHistory tfpf
--											 group by tfpf.ParentTaskFileId) basetable on basetable.ParentTaskFileId = tfh.ParentTaskFileId 
--																					  and basetable.MaxDate = tfh.CreatedOn 	) tf2 on tf1.TaskFileId = tf2.ChildTaskFileId
--            ) tfchild on tfparent.TaskFileId = tfchild.ParentTaskFileId

--  where tfparent.TaskInstanceId =  @TaskInstanceId
--   and  rr.RoleName in ('ProductionUser', 'ProductionWlm','Reviewer','LenderAccountManager')  


--end
--else

--begin 
--select actionTaken, name, userRole, comment, submitDate, isFileHistory, isFileComment, taskFileId, fileId, downloadFileId,fileName, fileRename, fileExt, fileSize, isReqAddInfo,isApprove, max(Approve)  Approve,raiComments, fileReviewStatusId
--from ( 
--select actionTaken, name, userRole, comment, submitDate, isFileHistory, isFileComment, taskFileId, fileId, downloadFileId, fileName,fileRename, fileExt, fileSize, isReqAddInfo, isapprove,Approve, raiComments, fileReviewStatusId
--, ROW_NUMBER() OVER (PARTITION BY actionTaken, name, userRole, comment, submitDate, isFileHistory, isFileComment, taskFileId, fileId, downloadFileId, fileName,fileRename, fileExt, fileSize, isReqAddInfo, fileReviewStatusId ORDER BY Approve Desc, raicomments) rnum
--from (
--select  distinct
--			--CASE WHEN rfs.[StatusId] =1then 'File Upload ( '+ rfs.[StatusName] +' )'
--			--WHEN  rfs.[StatusId] = 2 then 'File Upload ( '+ rfs.[StatusName] +' )'
--			--WHEn  rfs.[StatusId] = 3then 'File Upload ( '+ rfs.[StatusName] +' )'
--			--WHEn  rfs.[StatusId] = 4 then  rfs.[StatusName] 
--			--WHEN rfs.[StatusId] =5 then 'File Upload ( '+ rfs.[StatusName] +' )' end as actionTaken,

--			CASE WHEN rfs.[StatusId] =1then 'File Upload '
--			WHEN  rfs.[StatusId] = 2 then 'File Upload '
--			WHEn  rfs.[StatusId] = 3then 'File Upload'
--			WHEn  rfs.[StatusId] = 4 then  rfs.[StatusName] 
--			WHEN rfs.[StatusId] =5 then 'File Upload ' 
--			WHEN rfs.[StatusId] =6 then rfs.[StatusName] end as actionTaken,

--			CASE WHEN rfs.[StatusId] =4 then  (ra.FirstName + ' ' + ra.LastName) 
--			else (A.FirstName + ' ' + A.LastName)end  as name,

--			CASE WHEN rfs.[StatusId] =4 then  rr.RoleName +'('+rv.[ViewName] +')' 
--			else r.RoleName end  as userRole,

--			null as comment,

--			CASE WHEN rfs.[StatusId] =4 then  fileStatus.ModifiedOn 
--			else  COALESCE(tfchild.UploadTime, tfparent.UploadTime) end   as submitDate,

--			CAST(
--				CASE 
--					WHEN tfchild.TaskFileId is null 
--						THEN 0 
--					ELSE 1
--				END as bit) as isFileHistory,

--		     CAST(
--				CASE 
--					WHEN rfcHistory.Comment is null 
--						THEN 0 
--					ELSE 1
--				END as bit) as isFileComment,

--	   tfparent.TaskFileId as taskFileId,
--	   tfparent.FileId as fileId ,
--	    COALESCE(tfchild.[FileId], tfparent.[FileId]) as [downloadFileId] ,

--		 COALESCE(tfchild.[FileName], tfparent.[FileName])  as [fileName],
              
		
--		reverse(Substring(reverse(tfparent.[FileName]), Charindex('.', reverse(tfparent.[FileName]))+1,Len(tfparent.[FileName])))as fileRename,

--	  reverse(left(reverse(tfparent.[FileName]), charindex('.', reverse(tfparent.[FileName])) -1)) as fileExt,

--	COALESCE(tfchild.fileSize, tfparent.fileSize)  as fileSize,

--	   CAST(
--				CASE 
--					WHEN rf.[Status] = 4
--						THEN 1
--					WHEN rf.[Status] = 6
--						THEN 1  
--					ELSE 0
--				END as bit) as isReqAddInfo,
--				CAST(
--				CASE 
--					WHEN rf.[Status] = 5
--						THEN 1 
--					ELSE 0
--				END as bit) as isApprove,
--				CAST(  
--					 CASE   
--					 WHEN rf.[Status] = 5  
--					  THEN 1   
--					 ELSE 0  
--					END as int
--				) as Approve, 
--				raiComments=CASE 
--					WHEN rf.[Status] = 4
--						THEN rfcFinal.Comment  
--					WHEN rf.[Status] = 6
--						THEN rfcFinal.Comment
--					WHEN rf.[Status] = 5
--						THEN COALESCE(rfcFinal.Comment  , '') 
--					ELSE ''
--					end,

--		fileStatus.[Status] as fileReviewStatusId
		
--from [$(TaskDB)].dbo.TaskFile tfparent

---- join tables task and xref table
--inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
--inner join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskInstanceId = t.TaskInstanceId
----table join to folder structure
--inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on tf.[TaskFileId] = tfparent.TaskFileId and tf.FolderKey = @FolderKey
----tables join to get lender info
--left join HCP_Authentication a on a.UserName = t.AssignedBy
--left join webpages_UsersInRoles l on l.UserId = a.userid
--left join webpages_Roles r on r.roleid = l.roleid
----table join to get user's review status
--left join [$(TaskDB)].dbo.ReviewFileStatus rf on tfparent.TaskFileId = rf.TaskFileId and xref.AssignedTo = rf.ReviewerUserId and xref.ViewId = rf.ReviewerProdViewId
--left join [$(TaskDB)].dbo.ReviewStatusList rsl on rsl.StatusId = rf.[Status]
----table join to get file's review status
--left join ( 
--			--select rfs1.[TaskFileId],rfs1.[ModifiedOn],[ReviewerUserId],[ReviewerProdViewId],[Status] from [$(TaskDB)].[dbo].[ReviewFileStatus] rfs1 
--			--inner join 
--			--(select TaskFileId, max(ModifiedOn) as ModifiedOn 
--			--from [$(TaskDB)].[dbo].[ReviewFileStatus]

--			--group by TaskFileId) rfs2 on rfs2.TaskFileId = rfs1.TaskFileId and rfs2.ModifiedOn = rfs1.ModifiedOn
--			select rfs1.[TaskFileId],rfs1.[ModifiedOn],[ReviewerUserId],[ReviewerProdViewId],[Status] from [$(TaskDB)].[dbo].[ReviewFileStatus] rfs1 
--			inner join 
--			(select rf.TaskFileId, max(rf.ModifiedOn) as ModifiedOn 
--			from [$(TaskDB)].[dbo].[ReviewFileStatus] rf
--			inner join [$(TaskDB)].[dbo].[TaskFile] tf on tf.[TaskFileId] = rf.TaskFileId
--			inner join [$(TaskDB)].[dbo].[Prod_TaskXref] tx on tx.TaskInstanceId = tf.[TaskInstanceId]
--			inner join [dbo].[webpages_UsersInRoles] ur on ur.[UserId] = rf.ReviewerUserId
--			inner join  [dbo].[webpages_Roles] r on r.[RoleId] = ur.[RoleId]
--			where tx.AssignedTo = rf.ReviewerUserId and tx.ViewId = rf.ReviewerProdViewId 
--			      --and  tx.CompletedOn is null
--				  --and r.[RoleName] != 'Reviewer' 
--			group by rf.TaskFileId) rfs2 on rfs2.TaskFileId = rfs1.TaskFileId and rfs2.ModifiedOn = rfs1.ModifiedOn
--)fileStatus  on fileStatus.TaskFileId = tfparent.TaskFileId
--left join [$(TaskDB)].dbo.ReviewStatusList rfs on rfs.StatusId = fileStatus.[Status]
----table join to get users details who made recent update on file
--left join webpages_UsersInRoles ru on ru.UserId = fileStatus.ReviewerUserId
--left join HCP_Authentication ra on ra.UserID = fileStatus.ReviewerUserId
--left join webpages_Roles rr on rr.roleid = ru.roleid
--left join [$(TaskDB)].[dbo].[Prod_View] rv on rv.[ViewId] = fileStatus.[ReviewerProdViewId]
----join to get latest user's comment 
--left join  (select rfc.FileTaskId,rfc.comment,rfc.CreatedBy,rfc.ReviewerProdViewId from [$(TaskDB)].dbo.ReviewFileComment rfc
--			inner join 
--			(select FileTaskId,CreatedBy, max(CreatedOn) as MaxDate 
--			 from [$(TaskDB)].dbo.[ReviewFileComment]  group by FileTaskId , [CreatedBy] )rfcl on rfc.FileTaskId = rfcl.FileTaskId and rfc.CreatedOn = rfcl.MaxDate and  rfc.CreatedBy = rfcl.CreatedBy)rfcFinal on rfcFinal.FileTaskId = tfparent.TaskFileId and  xref.AssignedTo = rfcFinal.CreatedBy and xref.ViewId = rfcFinal.ReviewerProdViewId
---- table join to get history comments 
--left join  (select rfc.FileTaskId,rfc.comment from [$(TaskDB)].dbo.ReviewFileComment rfc
--			inner join 
--			(select FileTaskId, max(CreatedOn) as MaxDate 
--			 from [$(TaskDB)].dbo.[ReviewFileComment]  group by FileTaskId  )rfcl on rfc.FileTaskId = rfcl.FileTaskId and rfc.CreatedOn = rfcl.MaxDate )rfcHistory on rfcHistory.FileTaskId = tfparent.TaskFileId

----join to get latest version of uploaded file
--left join (select tf1.FileId, tf1.TaskFileId,tf1.[FileName],tf1.FileData,tf1.fileSize,tf1.UploadTime,tf2.ParentTaskFileId 
--		from [$(TaskDB)].dbo.TaskFile tf1
--		inner join ( select   tfh.ChildTaskFileId ,tfh.ParentTaskFileId
--								from [$(TaskDB)].dbo.TaskFile tfc 
--								inner join [$(TaskDB)].dbo.TaskFileHistory tfh on tfc.TaskFileId = tfh.ParentTaskFileId
--								inner join  (select tfpf.ParentTaskFileId, max(tfpf.CreatedOn) as MaxDate
--											 from [$(TaskDB)].dbo.TaskFileHistory tfpf
--											 group by tfpf.ParentTaskFileId) basetable on basetable.ParentTaskFileId = tfh.ParentTaskFileId 
--																					  and basetable.MaxDate = tfh.CreatedOn 	) tf2 on tf1.TaskFileId = tf2.ChildTaskFileId
--            ) tfchild on tfparent.TaskFileId = tfchild.ParentTaskFileId

--  where tfparent.TaskInstanceId =  @TaskInstanceId  --and xref.AssignedTo = @UserId 
--  and xref.ViewId = @ViewId  -- and  rr.RoleName in ('ProductionUser', 'ProductionWlm','Reviewer')
--	and  (rr.RoleName in ('ProductionUser', 'ProductionWlm','Reviewer')  or r.RoleName in ('LenderAccountManager')) 
--)t 
--  ) temp
-- WHERE rnum = 1
-- GROUP BY actionTaken, name, userRole, comment, submitDate, isFileHistory, isFileComment, taskFileId, fileId, downloadFileId,fileName, fileRename, fileExt, fileSize,isApprove, isReqAddInfo,raiComments, fileReviewStatusId

--end

--end
--GO

