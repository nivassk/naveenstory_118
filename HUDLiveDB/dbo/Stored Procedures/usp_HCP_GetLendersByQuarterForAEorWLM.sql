﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetLendersByQuarterForAEorWLM]
(
@UserName varchar(50),
@MonthsInPeriod int,
@Year int,
@User_role varchar(50)
)
AS


if(@User_role = 'InternalSpecialOptionUser')
begin

SELECT DISTINCT
	A.UserName, 
	A.FirstName,
	A.LastName,
	A.PreferredTimeZone,
	MR.RoleName, 
	LF.LenderID AS LenderID,
	A.UserID
FROM [dbo].[HCP_Authentication] A
INNER JOIN [dbo].[webpages_UsersInRoles] UIR
ON A.UserID = UIR.UserId
INNER JOIN [dbo].[webpages_Roles] MR
ON UIR.RoleId = MR.RoleId
LEFT JOIN [dbo].User_lender ul
ON A.UserID = ul.[User_ID]
LEFT JOIN ProjectInfo pf
on (PF.FHANumber  =  ul.FHANumber)
LEFT JOIN [dbo].Lender_FHANumber LF
ON PF.FHANumber = LF.FHANumber AND LF.FHA_EndDate IS NULL
WHERE A.UserName = @UserName

end
else 
begin

SELECT DISTINCT
	A.UserName, 
	A.FirstName,
	A.LastName,
	A.PreferredTimeZone,
	MR.RoleName, 
	LF.LenderID AS LenderID,
	A.UserID
FROM [dbo].[HCP_Authentication] A
INNER JOIN [dbo].[webpages_UsersInRoles] UIR
ON A.UserID = UIR.UserId
INNER JOIN [dbo].[webpages_Roles] MR
ON UIR.RoleId = MR.RoleId
LEFT JOIN [dbo].User_WLM_PM UPM
ON A.UserID = UPM.UserID
LEFT JOIN ProjectInfo pf
on (PF.HUD_Project_Manager_ID = UPM.HUD_Project_Manager_ID OR PF.HUD_WorkLoad_Manager_ID = UPM.HUD_WorkLoad_Manager_ID)
LEFT JOIN [dbo].Lender_FHANumber LF
ON PF.FHANumber = LF.FHANumber AND LF.FHA_EndDate IS NULL
WHERE A.UserName = @UserName
End 
GO

