﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetFhasByLenderIds]
(
@LenderIds varchar(MAX)
)
AS

SELECT DISTINCT
	FHA.FHANumber,
	FHA.FHADescription,
	LF.LenderID as id,
	LI.Lender_Name
FROM dbo.fn_StringSplitterToInt(@LenderIds) LID
INNER JOIN dbo.Lender_FHANumber LF
ON ((LID.id = -1) or (LID.id = LF.LenderID)) AND LF.FHA_EndDate IS NULL
INNER JOIN dbo.FHAInfo_iREMS FHA
ON LF.FHANumber = FHA.FHANumber
LEFT JOIN dbo.LenderInfo LI
ON LF.LenderID = LI.LenderID
ORDER BY 
LF.LenderID,
FHA.FHADescription

