﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetRiskSummaryReportDetail]
(
@MonthsInPeriod INT,
@Year INT,
@MinVal INT,
@MaxVal INT
)
AS
SELECT DISTINCT
LDL.LDI_ID,
LDL.ProjectName,
LDL.ServiceName,
LDL.PeriodEnding,
LDL.OperatorOwner,
LDL.FinancialStatementType,
LDL.UnitsInFacility,
LDL.MonthsInPeriod,
LDL.FHANumber,
LDL.OperatingCash,
LDL.Investments,
LDL.ReserveForReplacementEscrowBalance,
LDL.AccountsReceivable,
LDL.CurrentAssets,
LDL.CurrentLiabilities,
LDL.TotalExpenses,
LDL.RentLeaseExpense,
LDL.DepreciationExpense,
LDL.AmortizationExpense,
LDL.TotalExpenses,
LDL.FHAInsuredPrincipalPayment,
LDL.FHAInsuredInterestPayment,
LDL.MortgageInsurancePremium,
LDL.ReserveForReplacementDeposit,
LDL.DebtCoverageRatio,
LDL.WorkingCapital,
LDL.DaysCashOnHand,
LDL.DaysInAcctReceivable,
LDL.AvgPaymentPeriod,
LDL.ReserveForReplacementBalancePerUnit,
LDL.DebtCoverageRatioScore,
LDL.WorkingCapitalScore,
LDL.DaysCashOnHandScore,
LDL.DaysInAcctReceivableScore,
LDL.AvgPaymentPeriodScore,
LDL.ScoreTotal,
CASE WHEN CM.LDI_ID IS NULL THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END AS HasComment
FROM dbo.Lender_DataUpload_Live LDL
INNER JOIN dbo.Lender_FHANumber LF
ON LDL.FHANumber = LF.FHANumber
AND LDL.LenderID = LF.LenderID
LEFT JOIN dbo.Comment CM
ON LDL.LDI_ID = CM.LDI_ID
WHERE LDL.MonthsInPeriod = @MonthsInPeriod AND YEAR(LDL.PeriodEnding) = @Year 
AND LDL.ScoreTotal >= @MinVal AND LDL.ScoreTotal < @MaxVal
AND ((LF.FHA_EndDate IS NULL OR LF.FHA_EndDate > (ISNULL((SELECT Max(PeriodEnding) from dbo.Lender_DataUpload_Live LDL
														WHERE CONVERT(int,MonthsInPeriod) = @MonthsInPeriod AND Year(PeriodEnding) = @Year
														AND LDL.FHANumber = LF.FHANumber)
														,ISNULL(
														 [$(IntermDB)].dbo.fn_HCP_GetPeriodEndingDate(LF.FHANumber,@MonthsInPeriod,@Year),getdate()))))
		AND
	 (LF.FHA_StartDate <= (ISNULL((SELECT Max(PeriodEnding) from dbo.Lender_DataUpload_Live LDL
									WHERE CONVERT(int,MonthsInPeriod) = @MonthsInPeriod AND Year(PeriodEnding) = @Year
									AND LDL.FHANumber = LF.FHANumber)
									,ISNULL(
									[$(IntermDB)].dbo.fn_HCP_GetPeriodEndingDate(LF.FHANumber,@MonthsInPeriod,@Year),getdate()))))
	)





GO

