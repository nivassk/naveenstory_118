﻿
-- =============================================
-- Author:		<Author,,Rashmi Ganapathy>
-- Create date: <Create Date,02/02/2016>
-- Description:	<Description,Get FHA# Request by TaskinstanceId>
-- =============================================
Create PROCEDURE [dbo].[usp_HCP_Prod_GetFhaRequestByTaskInstanceId]
	(@TaskinstanceId uniqueidentifier )
AS
BEGIN
	select fr.*,ISNULL(fr.IsLIHTC,convert(bit,0)) as IsLIHTC,
	ISNULL(tf.FileId,0) as FileId, 
	case when (fr.IsPortfolioCancelled = 1 AND tx.ViewId = 9) then tx.Comments 
	when (fr.IsPortfolioCancelled = 1 AND tx.ViewId != 9) then (select Comments from [$(TaskDB)].dbo.Prod_TaskXref where TaskInstanceId = tx.TaskInstanceId and ViewId = 9)
	end as PortfolioCancelComments,
	case when (fr.IsCreditCancelled = 1 AND tx.ViewId = 10)then tx.Comments 
	when (fr.IsCreditCancelled = 1 AND tx.ViewId != 10) then (select Comments from [$(TaskDB)].dbo.Prod_TaskXref where TaskInstanceId = tx.TaskInstanceId and ViewId = 10)
	end as CreditCancelComments,
	tf.FileName as CreditLetterFileName
	from [$(TaskDB)].dbo.Prod_TaskXref tx
	join dbo.Prod_FHANumberRequest fr
	on tx.TaskInstanceId = fr.TaskinstanceId
	left join [$(TaskDB)].dbo.TaskFile tf
	on tf.TaskInstanceId = fr.TaskinstanceId
	where TaskXrefid = @TaskinstanceId

	union 

	select distinct fr.*,ISNULL(fr.IsLIHTC,convert(bit,0)) as IsLIHTC,	
	ISNULL(tf.FileId,0) as FileId,	
	case when (fr.IsPortfolioCancelled = 1 AND tx.ViewId = 9) then tx.Comments 
	when (fr.IsPortfolioCancelled = 1 AND tx.ViewId != 9) then (select Comments from [$(TaskDB)].dbo.Prod_TaskXref where TaskInstanceId = fr.TaskInstanceId and ViewId = 9)
	end as PortfolioCancelComments,
	case when (fr.IsCreditCancelled = 1 AND tx.ViewId = 10)then tx.Comments 
	when (fr.IsCreditCancelled = 1 AND tx.ViewId != 10) then (select Comments from [$(TaskDB)].dbo.Prod_TaskXref where TaskInstanceId = fr.TaskInstanceId and ViewId = 10)
	end as CreditCancelComments,
	tf.FileName as CreditLetterFileName
	from dbo.Prod_FHANumberRequest fr
	left join [$(TaskDB)].dbo.TaskFile tf
	on tf.TaskInstanceId = fr.TaskinstanceId
	left join [$(TaskDB)].dbo.Prod_TaskXref tx
	on tx.TaskInstanceId = fr.TaskinstanceId
	where fr.TaskinstanceId = @TaskinstanceId
END

GO

