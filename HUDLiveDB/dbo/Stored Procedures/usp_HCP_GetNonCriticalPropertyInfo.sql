﻿CREATE PROCEDURE [dbo].[usp_HCP_GetNonCriticalPropertyInfo]

(
@fhaNumber varchar(MAX)
)
AS

select  npi.PropertyId as PropertyId,
		ISNULL(p.ProjectName, ' ' ) as PropertyName,
		ISNULL(CONVERT(VARCHAR(10),npi.ClosingDate,111),' ') as ClosingDate,
		ISNULL(CONVERT(VARCHAR(10),npi.EndingDate,111),' ') as EndingDate,
		ISNULL(CONVERT(VARCHAR(10),npi.ExtensionApprovalDate,111), ' ') as ExtensionApprovalDate,
		ISNULL((case when Exists((select * from NonCriticalRequestExtensions where FhaNumber = npi.FhaNumber)) 
		 then
			(CASE
			when nce.ExtensionRequestStatus = 1 then CAST(1 AS BIT)
			end)
			else CAST(0 AS BIT)

		end),' ' ) as IsAnyPendingExtensionRequestExist,
		npi.InitialNCREBalance as NonCriticalAccountBalance,
		npi.CurrentBalance as NCRECurrentBalance,
		ISNULL(p.Troubled_Code,' ') as TroubledCode,
		ISNULL(npi.IsAmountConfirmed,' ') as IsNCREValidForFHANumber
from NonCritical_ProjectInfo npi
join Lender_FHANumber lf on npi.FhaNumber = lf.FHANumber and npi.LenderId = lf.LenderID and lf.FHA_EndDate is null
join ProjectInfo p on npi.PropertyId = p.PropertyID and npi.FHANumber = p.FHANumber and npi.LenderId = p.LenderID
left join NonCriticalRequestExtensions nce on npi.PropertyId = nce.PropertyId and npi.PropertyId = nce.PropertyId and npi.FhaNumber = nce.FhaNumber 
where npi.FhaNumber = @fhaNumber
GO


