﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetLenderUsersByRoleLenderId]
(
@LenderId int,
@RoleName nvarchar(50)
)
AS

SELECT DISTINCT
	A.UserName, 
	A.FirstName,
	A.LastName,
	A.UserID
FROM [dbo].[HCP_Authentication] A
INNER JOIN [dbo].[webpages_UsersInRoles] UIR
ON A.UserID = UIR.UserId
INNER JOIN [dbo].[webpages_Roles] MR
ON UIR.RoleId = MR.RoleId
LEFT JOIN [dbo].[User_Lender] UL
ON A.UserID = UL.User_ID

WHERE MR.RoleName = @RoleName
AND UL.Lender_ID = @LenderId

ORDER BY  A.FirstName


GO

