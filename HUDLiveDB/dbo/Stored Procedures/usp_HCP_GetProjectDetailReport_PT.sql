
CREATE    PROCEDURE [dbo].[usp_HCP_GetProjectDetailReport_PT]
@UserType NVARCHAR(100),
@Username  NVARCHAR(100),
@QueryDate NVARCHAR(50)
AS 

Begin
declare @LenderIntermediate TABLE (

FHANumber nvarchar(15)  NULL,
ProjectName nvarchar(100)  NULL,
ServiceName nvarchar(100) NULL,
UnitsInFacility INT NULL,
PeriodEnding NVARCHAR(25) NULL,
MonthsInPeriod NVARCHAR(5),
NOICumulative DECIMAL(19,2) NULL,
QuaterlyNoi DECIMAL(19,2) NULL,
NOIQuaterbyQuater DECIMAL(19,2) NULL,
NOIPercentageChange DECIMAL(19,2) NULL,
CumulativeRevenue DECIMAL(19,2) NULL,
CumulativeExpenses DECIMAL(19,2) NULL,
QuaterlyOperatingRevenue  DECIMAL(19,2) NULL,
QuarterlyOperatingExpense DECIMAL(19,2) NULL,
RevenueQuarterbyQuarter DECIMAL(19,2) NULL,
RevenuePercentageChange DECIMAL(19,2) NULL,
DSCRDifferencePerQuarter DECIMAL(19,2) NULL,
DSCRPercentageChange DECIMAL(19,2) NULL,
CumulativeResidentDays DECIMAL(19,2) NULL,
QuarterlyResidentDays int NULL,
ADRperQuarter DECIMAL(19,2) NULL,
ADRDifferencePerQuarter DECIMAL(19,2) NULL,
ADRPercentageChange DECIMAL(19,2) NULL,
DebtCoverageRatio2 DECIMAL(19,2) NULL,
AverageDailyRateRatio DECIMAL(19,2) NULL,
QuarterlyDSCR DECIMAL(19,2) NULL,
IsQtrlyORevError INT NULL,
IsQtrlyNOIError INT NULL,
IsQtrlyNOIPTError INT NULL,
IsQtrlyDSCRError INT NULL,
IsQtrlyDSCRPTError INT NULL,
IsADRError INT NULL,
IsProjectError int NULL
)

declare @dt  Datetime
set @dt=convert(datetime, @QueryDate, 101)

INSERT INTO @LenderIntermediate SELECT DISTINCT  FHANumber ,ProjectName,ServiceName,   UnitsInFacility,PeriodEnding ,MonthsInPeriod ,NOICumulative , QuaterlyNoi ,NOIQuaterbyQuater ,NOIPercentageChange  , CumulativeRevenue,CumulativeExpenses ,QuaterlyOperatingRevenue ,QuarterlyOperatingExpense,RevenueQuarterbyQuarter ,RevenuePercentageChange , DSCRDifferencePerQuarter,DSCRPercentageChange ,CumulativeResidentDays,CONVERT (int, QuarterlyResidentDays ) as QuarterlyResidentDays,ADRperQuarter,ADRDifferencePerQuarter,ADRPercentageChange,DebtCoverageRatio2,AverageDailyRateRatio,QuarterlyDSCR ,IsQtrlyORevError,IsQtrlyNOIError,IsQtrlyNOIPTError,IsQtrlyDSCRError,IsQtrlyDSCRPTError,IsADRError,IsProjectError           
from [$(IntermDB)].dbo. Lender_DataUpload_Intermediate a join
(select FHANumber as FHA  , MonthsInPeriod as MIP , max ( DataInserted) as createddt,YEAR( CONVERT ( DATETIME, PeriodEnding )) as YR
from [$(IntermDB)].dbo. Lender_DataUpload_Intermediate 
group by FHANumber,MonthsInPeriod,YEAR( CONVERT ( DATETIME, PeriodEnding ))) b
on a .FHANumber = b.FHA and a. DataInserted = b .createddt and    CONVERT(varchar(max ),cast(a.datainserted as date), 101)>@dt

--Union

--SELECT DISTINCT  FHANumber ,ProjectName,ServiceName,   UnitsInFacility,PeriodEnding ,MonthsInPeriod ,NOICumulative , QuaterlyNoi ,NOIQuaterbyQuater ,NOIPercentageChange  , CumulativeRevenue,CumulativeExpenses ,QuaterlyOperatingRevenue ,QuarterlyOperatingExpense,RevenueQuarterbyQuarter ,RevenuePercentageChange , DSCRDifferencePerQuarter,DSCRPercentageChange ,CumulativeResidentDays,CONVERT (int, QuarterlyResidentDays ) as QuarterlyResidentDays,ADRperQuarter,ADRDifferencePerQuarter,ADRPercentageChange,DebtCoverageRatio2,AverageDailyRateRatio,QuarterlyDSCR ,IsQtrlyORevError,IsQtrlyNOIError,IsQtrlyNOIPTError,IsQtrlyDSCRError,IsQtrlyDSCRPTError,IsADRError,IsProjectError           
--from [$(IntermDB)].dbo. Lender_DataUpload_Intermediate_InActive a join
--(select FHANumber as FHA  , MonthsInPeriod as MIP , max ( DataInserted) as createddt,YEAR( CONVERT ( DATETIME, PeriodEnding )) as YR
--from [$(IntermDB)].dbo. Lender_DataUpload_Intermediate_InActive 
--group by FHANumber,MonthsInPeriod,YEAR( CONVERT ( DATETIME, PeriodEnding ))) b
--on a .FHANumber = b.FHA and a. DataInserted = b .createddt and    CONVERT(varchar(max ),cast(a.datainserted as date), 101)>@dt

 --SELECT * FROM  @LenderIntermediate
--SET @UserType='AccountExecutive'
--SET @Username ='EDWARD.E.CHLYSTEK@hud.gov'

 IF (@UserType ='InternalSpecialOptionUser')
BEGIN
SELECT   LDI.FHANumber , LDI.ProjectName, LDI.ServiceName, wlm.HUD_WorkLoad_Manager_Name,ae.HUD_Project_Manager_Name,LDI.UnitsInFacility,CONVERT(varchar(max ),cast(LDI .PeriodEnding as date), 101) AS PeriodEnding ,CONVERT ( int, LDI.MonthsInPeriod ) AS MonthsInPeriod ,LDI.NOICumulative , CASE WHEN IsQtrlyNOIPTError=1 THEN '<font color="red">' + CAST(LDI.QuaterlyNoi as varchar(max )) + '</font>' ELSE CAST(LDI.QuaterlyNoi as varchar(max )) END as QuaterlyNoi ,CASE WHEN IsQtrlyNOIError = 1 or IsQtrlyNOIPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.NOIQuaterbyQuater as varchar(max )) ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.NOIQuaterbyQuater as varchar(max ))  ,'NA') END as NOIQuaterbyQuater  ,CASE WHEN IsQtrlyNOIError = 1 or IsQtrlyNOIPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.NOIPercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.NOIPercentageChange as varchar(max )) + '%' ,'NA') END as NOIPercentageChange  , LDI.CumulativeRevenue,LDI.CumulativeExpenses ,CASE WHEN IsQtrlyORevError = 1 THEN '<font color="red">' + CAST(LDI.QuaterlyOperatingRevenue as varchar(max )) + '</font>' ELSE CAST(LDI.QuaterlyOperatingRevenue as varchar(max )) END as QuaterlyOperatingRevenue  ,LDI.QuarterlyOperatingExpense,CASE WHEN IsQtrlyORevError = 1   THEN '<font color="red">' + ISNULL(CAST(LDI.RevenueQuarterbyQuarter as varchar(max ))  ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.RevenueQuarterbyQuarter as varchar(max ))  ,'NA') END as RevenueQuarterbyQuarter ,CASE WHEN IsQtrlyORevError = 1   THEN '<font color="red">' + ISNULL(CAST(LDI.RevenuePercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.RevenuePercentageChange as varchar(max )) + '%' ,'NA') END as RevenuePercentageChange , CASE WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.DSCRDifferencePerQuarter as varchar(max )) ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.DSCRDifferencePerQuarter as varchar(max ))  ,'NA') END as DSCRDifferencePerQuarter,CASE WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.DSCRPercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.DSCRPercentageChange as varchar(max )) + '%' ,'NA') END as DSCRPercentageChange ,CONVERT (int, LDI.CumulativeResidentDays ) as CumulativeResidentDays,LDI.QuarterlyResidentDays,CASE WHEN IsADRError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.ADRperQuarter as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.ADRperQuarter as varchar(max )) + '%' ,'NA') END as ADRperQuarter,CASE WHEN IsADRError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.ADRDifferencePerQuarter as varchar(max ))  ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.ADRDifferencePerQuarter as varchar(max ))  ,'NA') END as ADRDifferencePerQuarter,CASE WHEN IsADRError = 1   THEN '<font color="red">' + ISNULL(CAST(LDI.ADRPercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.ADRPercentageChange as varchar(max )) + '%' ,'NA') END as ADRPercentageChange,LDI.DebtCoverageRatio2,ldi.AverageDailyRateRatio,CASE WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError=1  THEN '<font color="red">' + CAST(LDI.QuarterlyDSCR as varchar(max )) + '</font>' ELSE CAST(LDI.QuarterlyDSCR as varchar(max )) END as QuarterlyDSCR,IsQtrlyORevError,IsQtrlyNOIError,IsQtrlyNOIPTError,IsQtrlyDSCRError,IsQtrlyDSCRPTError,IsADRError,IsProjectError
FROM  @LenderIntermediate LDI
LEFT JOIN dbo.ProjectInfo p ON
LDI.FHANumber = p.FHANumber
JOIN User_Lender ul on ul.FHANumber = p.FHANumber
JOIN HCP_Authentication auth on auth.userid = ul.User_ID
JOIN dbo.HUD_Project_Manager ae ON p.HUD_Project_Manager_ID = ae.HUD_Project_Manager_ID
JOIN address a ON a.AddressID = ae.AddressID
JOIN dbo.HUD_WorkLoad_Manager wlm ON p.HUD_WorkLoad_Manager_ID = wlm.HUD_WorkLoad_Manager_ID 
WHERE  auth.username =  @Username ORDER BY LDI.FHANumber,CONVERT ( DATETIME, LDI.PeriodEnding )
END
ELSE IF (@UserType ='AccountExecutive')
BEGIN
SELECT   LDI.FHANumber , LDI.ProjectName, LDI.ServiceName, wlm.HUD_WorkLoad_Manager_Name,ae.HUD_Project_Manager_Name,LDI.UnitsInFacility,CONVERT(varchar(max ),cast(LDI .PeriodEnding as date), 101) AS PeriodEnding ,CONVERT ( int, LDI.MonthsInPeriod ) AS MonthsInPeriod ,LDI.NOICumulative , CASE WHEN IsQtrlyNOIPTError=1 THEN '<font color="red">' + CAST(LDI.QuaterlyNoi as varchar(max )) + '</font>' ELSE CAST(LDI.QuaterlyNoi as varchar(max )) END as QuaterlyNoi ,CASE WHEN IsQtrlyNOIError = 1 or IsQtrlyNOIPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.NOIQuaterbyQuater as varchar(max )) ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.NOIQuaterbyQuater as varchar(max ))  ,'NA') END as NOIQuaterbyQuater  ,CASE WHEN IsQtrlyNOIError = 1 or IsQtrlyNOIPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.NOIPercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.NOIPercentageChange as varchar(max )) + '%' ,'NA') END as NOIPercentageChange  , LDI.CumulativeRevenue,LDI.CumulativeExpenses ,CASE WHEN IsQtrlyORevError = 1 THEN '<font color="red">' + CAST(LDI.QuaterlyOperatingRevenue as varchar(max )) + '</font>' ELSE CAST(LDI.QuaterlyOperatingRevenue as varchar(max )) END as QuaterlyOperatingRevenue  ,LDI.QuarterlyOperatingExpense,CASE WHEN IsQtrlyORevError = 1   THEN '<font color="red">' + ISNULL(CAST(LDI.RevenueQuarterbyQuarter as varchar(max ))  ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.RevenueQuarterbyQuarter as varchar(max ))  ,'NA') END as RevenueQuarterbyQuarter ,CASE WHEN IsQtrlyORevError = 1   THEN '<font color="red">' + ISNULL(CAST(LDI.RevenuePercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.RevenuePercentageChange as varchar(max )) + '%' ,'NA') END as RevenuePercentageChange , CASE WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.DSCRDifferencePerQuarter as varchar(max )) ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.DSCRDifferencePerQuarter as varchar(max ))  ,'NA') END as DSCRDifferencePerQuarter,CASE WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.DSCRPercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.DSCRPercentageChange as varchar(max )) + '%' ,'NA') END as DSCRPercentageChange ,CONVERT (int, LDI.CumulativeResidentDays ) as CumulativeResidentDays,LDI.QuarterlyResidentDays,CASE WHEN IsADRError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.ADRperQuarter as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.ADRperQuarter as varchar(max )) + '%' ,'NA') END as ADRperQuarter,CASE WHEN IsADRError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.ADRDifferencePerQuarter as varchar(max ))  ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.ADRDifferencePerQuarter as varchar(max ))  ,'NA') END as ADRDifferencePerQuarter,CASE WHEN IsADRError = 1   THEN '<font color="red">' + ISNULL(CAST(LDI.ADRPercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.ADRPercentageChange as varchar(max )) + '%' ,'NA') END as ADRPercentageChange,LDI.DebtCoverageRatio2,ldi.AverageDailyRateRatio,CASE WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError=1  THEN '<font color="red">' + CAST(LDI.QuarterlyDSCR as varchar(max )) + '</font>' ELSE CAST(LDI.QuarterlyDSCR as varchar(max )) END as QuarterlyDSCR,IsQtrlyORevError,IsQtrlyNOIError,IsQtrlyNOIPTError,IsQtrlyDSCRError,IsQtrlyDSCRPTError,IsADRError,IsProjectError
FROM  @LenderIntermediate LDI
LEFT JOIN dbo.ProjectInfo p ON
LDI.FHANumber = p.FHANumber
JOIN dbo.HUD_Project_Manager ae ON p.HUD_Project_Manager_ID = ae.HUD_Project_Manager_ID
JOIN address a ON a.AddressID = ae.AddressID
JOIN dbo.HUD_WorkLoad_Manager wlm ON p.HUD_WorkLoad_Manager_ID = wlm.HUD_WorkLoad_Manager_ID 
WHERE  a.email =  @Username ORDER BY LDI.FHANumber,CONVERT ( DATETIME, LDI.PeriodEnding )
END

ELSE IF (@UserType = 'WorkflowManager')
BEGIN
SELECT  LDI.FHANumber , LDI.ProjectName, LDI.ServiceName, wlm.HUD_WorkLoad_Manager_Name,ae.HUD_Project_Manager_Name,LDI.UnitsInFacility,CONVERT(varchar(max ),cast(LDI .PeriodEnding as date), 101) AS PeriodEnding,CONVERT ( int, LDI.MonthsInPeriod ) AS MonthsInPeriod ,LDI.NOICumulative , CASE WHEN  IsQtrlyNOIPTError=1 THEN '<font color="red">' + CAST(LDI.QuaterlyNoi as varchar(max )) + '</font>' ELSE CAST(LDI.QuaterlyNoi as varchar(max )) END as QuaterlyNoi ,CASE WHEN IsQtrlyNOIError = 1 or IsQtrlyNOIPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.NOIQuaterbyQuater as varchar(max ))  ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.NOIQuaterbyQuater as varchar(max ))  ,'NA') END as NOIQuaterbyQuater  ,CASE WHEN IsQtrlyNOIError = 1 or IsQtrlyNOIPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.NOIPercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.NOIPercentageChange as varchar(max )) + '%' ,'NA') END as NOIPercentageChange  , LDI.CumulativeRevenue,LDI.CumulativeExpenses ,CASE WHEN IsQtrlyORevError = 1 THEN '<font color="red">' + CAST(LDI.QuaterlyOperatingRevenue as varchar(max )) + '</font>' ELSE CAST(LDI.QuaterlyOperatingRevenue as varchar(max ))  END as QuaterlyOperatingRevenue ,LDI.QuarterlyOperatingExpense,CASE WHEN IsQtrlyORevError = 1   THEN '<font color="red">' + ISNULL(CAST(LDI.RevenueQuarterbyQuarter as varchar(max ))  ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.RevenueQuarterbyQuarter as varchar(max ))  ,'NA') END as RevenueQuarterbyQuarter ,
CASE WHEN IsQtrlyORevError = 1   THEN '<font color="red">' + ISNULL(CAST(LDI.RevenuePercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.RevenuePercentageChange as varchar(max )) + '%' ,'NA') END as RevenuePercentageChange , CASE WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.DSCRDifferencePerQuarter as varchar(max ))  ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.DSCRDifferencePerQuarter as varchar(max ))  ,'NA') END as DSCRDifferencePerQuarter,CASE WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.DSCRPercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.DSCRPercentageChange as varchar(max )) + '%' ,'NA') END as DSCRPercentageChange ,CONVERT (int, LDI.CumulativeResidentDays ) as CumulativeResidentDays,LDI.QuarterlyResidentDays,CASE WHEN IsADRError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.ADRperQuarter as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.ADRperQuarter as varchar(max )) + '%' ,'NA') END as ADRperQuarter,CASE WHEN IsADRError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.ADRDifferencePerQuarter as varchar(max ))  ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.ADRDifferencePerQuarter as varchar(max ))  ,'NA') END as ADRDifferencePerQuarter,CASE WHEN IsADRError = 1   THEN '<font color="red">' + ISNULL(CAST(LDI.ADRPercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.ADRPercentageChange as varchar(max )) + '%' ,'NA') END as ADRPercentageChange,LDI.DebtCoverageRatio2,ldi.AverageDailyRateRatio,CASE WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError=1  THEN '<font color="red">' + CAST(LDI.QuarterlyDSCR as varchar(max )) + '</font>' ELSE CAST(LDI.QuarterlyDSCR as varchar(max )) END as QuarterlyDSCR,IsQtrlyORevError,IsQtrlyNOIError,IsQtrlyNOIPTError,IsQtrlyDSCRError,IsQtrlyDSCRPTError,IsADRError,IsProjectError
FROM  @LenderIntermediate LDI
LEFT JOIN dbo.ProjectInfo p ON
LDI.FHANumber = p.FHANumber
JOIN dbo.HUD_Project_Manager ae ON p.HUD_Project_Manager_ID = ae.HUD_Project_Manager_ID
JOIN address a ON a.AddressID = ae.AddressID
JOIN dbo.HUD_WorkLoad_Manager wlm ON p.HUD_WorkLoad_Manager_ID = wlm.HUD_WorkLoad_Manager_ID
AND P.HUD_WorkLoad_Manager_ID = (SELECT HUD_WorkLoad_Manager_ID FROM [$(DatabaseName)].dbo.HUD_WorkLoad_Manager WHERE AddressID =(SELECT AddressID FROM [$(DatabaseName)].dbo.Address WHERE Email = @Username)) ORDER BY LDI.FHANumber,CONVERT ( DATETIME, LDI.PeriodEnding )
END 

ELSE IF (@UserType = 'HUDAdmin' OR @UserType = 'SuperUser' OR @UserType = 'HUDDirector')
BEGIN
SELECT  LDI.FHANumber , LDI.ProjectName, LDI.ServiceName, wlm.HUD_WorkLoad_Manager_Name,ae.HUD_Project_Manager_Name,LDI.UnitsInFacility,CONVERT(varchar(max ),cast(LDI .PeriodEnding as date), 101) AS PeriodEnding ,CONVERT ( int, LDI.MonthsInPeriod ) AS MonthsInPeriod ,LDI.NOICumulative , CASE WHEN  IsQtrlyNOIPTError=1 THEN '<font color="red">' + CAST(LDI.QuaterlyNoi as varchar(max )) + '</font>' ELSE CAST(LDI.QuaterlyNoi as varchar(max )) END as QuaterlyNoi ,CASE WHEN IsQtrlyNOIError = 1 or IsQtrlyNOIPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.NOIQuaterbyQuater as varchar(max ))  ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.NOIQuaterbyQuater as varchar(max ))  ,'NA') END as NOIQuaterbyQuater   ,CASE WHEN IsQtrlyNOIError = 1 or IsQtrlyNOIPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.NOIPercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.NOIPercentageChange as varchar(max )) + '%' ,'NA') END as NOIPercentageChange  ,ISNULL(CAST(LDI.NOIPercentageChange as varchar(max )) + '%' ,'NA') AS NOIPercentageChange  , LDI.CumulativeRevenue,LDI.CumulativeExpenses ,CASE WHEN IsQtrlyORevError = 1 THEN '<font color="red">' + CAST(LDI.QuaterlyOperatingRevenue as varchar(max )) + '</font>' ELSE CAST(LDI.QuaterlyOperatingRevenue as varchar(max )) END as QuaterlyOperatingRevenue ,LDI.QuarterlyOperatingExpense,CASE WHEN IsQtrlyORevError = 1   THEN '<font color="red">' + ISNULL(CAST(LDI.RevenueQuarterbyQuarter as varchar(max ))  ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.RevenueQuarterbyQuarter as varchar(max ))  ,'NA') END as RevenueQuarterbyQuarter ,
CASE WHEN IsQtrlyORevError = 1   THEN '<font color="red">' + ISNULL(CAST(LDI.RevenuePercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.RevenuePercentageChange as varchar(max )) + '%' ,'NA') END as RevenuePercentageChange , CASE WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.DSCRDifferencePerQuarter as varchar(max ))  ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.DSCRDifferencePerQuarter as varchar(max ))  ,'NA') END as DSCRDifferencePerQuarter,CASE WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.DSCRPercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.DSCRPercentageChange as varchar(max )) + '%' ,'NA') END as DSCRPercentageChange ,CONVERT (int, LDI.CumulativeResidentDays ) as CumulativeResidentDays,LDI.QuarterlyResidentDays,CASE WHEN IsADRError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.ADRperQuarter as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.ADRperQuarter as varchar(max )) + '%' ,'NA') END as ADRperQuarter,CASE WHEN IsADRError=1   THEN '<font color="red">' + ISNULL(CAST(LDI.ADRDifferencePerQuarter as varchar(max ))  ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.ADRDifferencePerQuarter as varchar(max )) ,'NA') END as ADRDifferencePerQuarter,CASE WHEN IsADRError = 1   THEN '<font color="red">' + ISNULL(CAST(LDI.ADRPercentageChange as varchar(max )) + '%' ,'NA') + '</font>' ELSE ISNULL(CAST(LDI.ADRPercentageChange as varchar(max )) + '%' ,'NA') END as ADRPercentageChange,LDI.DebtCoverageRatio2,ldi.AverageDailyRateRatio,CASE WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError=1  THEN '<font color="red">' + CAST(LDI.QuarterlyDSCR as varchar(max )) + '</font>' ELSE CAST(LDI.QuarterlyDSCR as varchar(max )) END as QuarterlyDSCR,IsQtrlyORevError,IsQtrlyNOIError,IsQtrlyNOIPTError,IsQtrlyDSCRError,IsQtrlyDSCRPTError,IsADRError,IsProjectError
FROM  @LenderIntermediate LDI
LEFT JOIN dbo.ProjectInfo p ON
LDI.FHANumber = p.FHANumber
JOIN dbo.HUD_Project_Manager ae ON p.HUD_Project_Manager_ID = ae.HUD_Project_Manager_ID
JOIN address a ON a.AddressID = ae.AddressID
JOIN dbo.HUD_WorkLoad_Manager wlm ON p.HUD_WorkLoad_Manager_ID = wlm.HUD_WorkLoad_Manager_ID ORDER BY LDI.FHANumber,CONVERT ( DATETIME, LDI.PeriodEnding )
END


End
GO

