﻿CREATE PROCEDURE [dbo].[usp_HCP_EmailIssueCorrection]
	
AS
	DECLARE @subj NVARCHAR (255)

DECLARE @body_text NVARCHAR (MAX)
DECLARE @RowsToProcess  int
DECLARE @CurrentRow     int
Declare @Query1 varchar(max)
Declare @Query3 varchar(max)
Declare @Query2 int
DECLARE @EmailTable TABLE (RowID int not null primary key identity(1,1), EmailId int )  
	INSERT into @EmailTable (EmailId) SELECT EmailId FROM [$(DatabaseName)].dbo.HCP_Email
	where ModifiedOn > '2018-05-12 00:00:00.000' and ModifiedOn < '2018-05-18 00:00:00.000'
	--where ModifiedOn >= '2018-05-04 20:39:35.533' and ModifiedOn <= '2018-05-04 22:46:16.067'
	--where ModifiedOn >= '2018-05-04 21:16:47.760' and ModifiedOn <= '2018-05-04 21:58:58.300'
	SET @RowsToProcess=@@ROWCOUNT
	
	SET @CurrentRow=0
	WHILE @CurrentRow<@RowsToProcess
		BEGIN
			SET @CurrentRow=@CurrentRow+1
			DECLARE @Text AS VARCHAR(100)
			DECLARE @Cmd AS VARCHAR(1000)
			DECLARE @EmailTo VARCHAR (MAX)
			SELECT @Query2=EmailId FROM @EmailTable	WHERE RowID=@CurrentRow
			Set @subj = (SELECT Subject FROM [$(DatabaseName)].dbo.HCP_Email where EmailId = @Query2)
			Set @body_text = (SELECT ContentHtml FROM [$(DatabaseName)].dbo.HCP_Email where EmailId = @Query2)
			Set @EmailTo = (SELECT EmailTo FROM [$(DatabaseName)].dbo.HCP_Email where EmailId = @Query2)
			-- Print @EmailTo
			--EXEC msdb.dbo.sp_send_dbmail  
   --            @profile_name = 'CloudMail' ,
   --            @from_address = 'hhcpsupport@232hudhealthcare.com' ,
			--   @recipients = @EmailTo,             
			--   @subject = @subj,
			--   @body = @body_text,
   --            @body_format = 'HTML' ,
   --            @importance = 'High'
			
			--update HCP_Email set IsSent = 1 where EmailId = @Query2


		END
RETURN 0
