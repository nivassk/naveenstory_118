﻿Create PROCEDURE [dbo].[usp_HCP_GetOpaHistoryOld]
(
@TaskInstanceId uniqueidentifier
)
AS
BEGIN

select 'File Upload' as actionTaken,
		(A.FirstName + ' ' + A.LastName) as name,
		CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
			WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
			WHEn r.RoleName = 'LenderAccountManager' then 'LAM'
			WHEN r.RoleName = 'OperatorAccountRepresentative' then 'OAR' end as userRole,
			null as comment,
			[FileName] as [fileName],
			DATALENGTH(FileData)/1024 as fileSize,
			uploadTime as uploadDate,
			null as submitDate,
			fileId,
			case when t.TaskInstanceId is null then CONVERT(BIT,1)
				 when t.TaskStepId = 16 and t.StartTime < tf.UploadTime then CONVERT(BIT,1)  
				 else CONVERT(BIT,0) end as editable,
			uploadTime as actionDate
	from hcp_task.dbo.TaskFile tf
	inner join HCP_Authentication a on a.UserID = tf.CreatedBy
	inner join webpages_UsersInRoles l on l.UserId = a.userid
	inner join webpages_Roles r on r.roleid = l.roleid
	left join
		(select top 1 * from hcp_task.dbo.Task where TaskInstanceId = @TaskInstanceId order by SequenceId desc) t on t.TaskInstanceId = tf.TaskInstanceId
	where tf.TaskInstanceId = @TaskInstanceId

Union

	select case when t.TaskStepId = 14 then 'OPA Submit'
			when t.TaskStepId = 15 and o.RequestStatus = 2 then 'OPA Approve'
			when t.TaskStepId = 15 and o.RequestStatus = 5 then 'OPA Deny'
			when t.TaskStepId = 16 then 'Request Addtional Information'
			end as actionTaken,
		(A.FirstName + ' ' + A.LastName) as name,
		CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
			WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
			WHEn r.RoleName = 'LenderAccountManager' then 'LAM'
			WHEN r.RoleName = 'OperatorAccountRepresentative' then 'OAR' 
			else 'AE' end as userRole,	
			Notes as comment,
			null as [fileName],
			null as fileSize,
			null as uploadDate,
			starttime as submitDate,
			null as fileId,
			CONVERT(BIT,0) as editable,
			starttime as actionDate
	from opaform o 
	inner join hcp_task.dbo.task t on t.TaskInstanceId = o.TaskInstanceId
	inner join HCP_Authentication a on a.UserName = t.AssignedBy
	left join webpages_UsersInRoles l on l.UserId = a.userid and t.TaskStepId = 14
	left join webpages_Roles r on r.roleid = l.roleid
	where o.TaskInstanceId = @TaskInstanceId
    order by actionDate desc
end