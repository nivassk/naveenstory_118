﻿
CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetSubFolder]
(
@ParentKey int,
@ProjectId nchar(10),
@FHANumber nchar(10)
)
AS

BEGIN

SELECT sf.[FolderKey]
      ,sf.[FolderName]
      ,sf.[ParentKey]
      ,sf.[ViewTypeId]
      ,sf.[FolderSortingNumber]
      ,sf.[SubfolderSequence]
      ,sf.[FhaNo]
      ,sf.[ProjectNo]
	  ,sf.[PageTypeId]
      ,sf.[GroupTaskInstanceId]
  FROM [$(TaskDB)].[dbo].[Prod_SubFolderStructure] sf where sf.ParentKey = @ParentKey and sf.ProjectNo = @ProjectId  and sf.FhaNo=@FHANumber
			  

		
end
GO
--CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetSubFolder]
--(
--@ParentKey int,
--@ProjectId nchar(10)
--)
--AS

--BEGIN

--SELECT sf.[FolderKey]
--      ,sf.[FolderName]
--      ,sf.[ParentKey]
--      ,sf.[ViewTypeId]
--      ,sf.[FolderSortingNumber]
--      ,sf.[SubfolderSequence]
--      ,sf.[FhaNo]
--      ,sf.[ProjectNo]
--  FROM [$(TaskDB)].[dbo].[Prod_SubFolderStructure] sf where sf.ParentKey = @ParentKey and sf.ProjectNo = @ProjectId  
			  		
--end

--GO

