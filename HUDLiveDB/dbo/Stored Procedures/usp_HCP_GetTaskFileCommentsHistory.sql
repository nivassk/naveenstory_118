﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetTaskFileCommentsHistory]

(

@TaskFileId int


)

AS
/*
Date			Changed by					Reason
====			===========					======	
03/28/2018		Rashmi/Venkatesh/Suresh		Filter the results by production roles  

*/
BEGIN

select distinct a.FirstName +a.LastName as name,

case 
			when fc.ReviewerProdViewId  is not null then r.RoleName +'('+v.[ViewName] +')'
			else 'AE'  end as userRole,

--'AE' as userRole,

fc.Comment as comment,

fc.CreatedOn as uploadDate

from 

[$(TaskDB)].dbo.ReviewFileComment fc

inner join HCP_Authentication a on fc.CreatedBy = a.UserID

inner join [$(TaskDB)].dbo.TaskFile tf on tf.TaskFileId = fc.FileTaskId

inner  join [$(TaskDB)].[dbo].[ReviewFileStatus] rs on rs.TaskFileId = fc.FileTaskId and rs.ReviewerUserId = fc.CreatedBy

left join [$(TaskDB)].[dbo].[Prod_View] v on fc.ReviewerProdViewId = v.ViewId

left join webpages_UsersInRoles l on l.UserId = a.userid

left join webpages_Roles r on r.roleid = l.roleid

where tf.FileId = @TaskFileId  and rs.[Status] != 6 and r.RoleName in ('ProductionUser', 'ProductionWlm', 'Reviewer')

order by fc.CreatedOn desc

END




GO

