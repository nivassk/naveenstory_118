﻿
CREATE PROCEDURE [dbo].[usp_HCP_Get_Admin_ManageUsers_View]
AS

DECLARE @UtcNow DateTime
--SET @UtcNow = GETUTCDATE()

SELECT 
* 
FROM
(SELECT DISTINCT
    HA.UserID AS UserID, 
    HA.LastName AS LastName, 
    HA.FirstName AS FirstName, 
    HA.UserMInitial AS UserMInitial, 
    HA.UserName AS UserName, 
    HA.ModifiedOn AS ModifiedOn, 
    HA.ModifiedBy AS ModifiedBy, 
    HA.OnBehalfOfBy AS OnBehalfOfBy, 
    HA.AddressID AS AddressID, 
    HA.Deleted_Ind AS Deleted_Ind, 
    HA.PreferredTimeZone AS PreferredTimeZone, 
    HA.PasswordHist AS PasswordHist,
	--R.RoleName AS SelectedRole, 
    WM.IsConfirmed AS IsRegisterComplete,
				ISNULL(UL.Lender_ID, LF.LenderID) AS LenderID,
                LI.Lender_Name + ' (' + CAST(ISNULL(UL.Lender_ID, LF.LenderID) AS varchar(50)) + ')' AS LenderName,
                UL.ServicerID,
                SI.ServicerName + ' (' + CAST(UL.ServicerID AS varchar(50)) + ')' AS ServicerName             
FROM dbo.HCP_Authentication AS HA
LEFT OUTER JOIN dbo.User_Lender UL ON UL.User_ID = HA.UserID
LEFT JOIN Lender_FHANumber LF ON UL.FHANumber = LF.FHANumber AND LF.FHA_EndDate IS NULL
LEFT OUTER JOIN dbo.LenderInfo LI ON ISNULL(UL.Lender_ID, LF.LenderID) = LI.LenderID
LEFT OUTER JOIN dbo.ServicerInfo SI ON SI.ServicerID = UL.ServicerID
LEFT OUTER JOIN dbo.webpages_Membership WM ON WM.UserId = HA.UserID) T1

LEFT OUTER JOIN
  (SELECT tt.Login_Time,tt.Logout_Time,tt.RoleName,tt.User_id,CASE
  WHEN tt.Login_Time is not NULL THEN 'Yes'
  Else
  NULL END as History
FROM [$(DatabaseName)].[dbo].[HCP_User_LoginTime] tt
INNER JOIN
    (SELECT User_id, MAX(createdon) AS MaxDateTime
    FROM [$(DatabaseName)].[dbo].[HCP_User_LoginTime]
    GROUP BY User_id) groupedtt 
ON tt.User_id = groupedtt.User_id 
AND tt.createdon = groupedtt.MaxDateTime) T2

on T1.UserID=T2.[User_id]

