﻿using HUDHealthcarePortal.BusinessService;
using System;
using System.Net.Mail;
using System.Web.Mvc;

namespace HUDHealthCarePortal.Controllers
{
    public class EmailController : Controller
    {
        //
        // GET: /Email/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendEmail(FormCollection collection)
        {
            try
            {
                var emailMgr = new EmailManager();
                MailMessage mail = new MailMessage();

                SmtpClient smtpServer = new SmtpClient();
                mail.From = new MailAddress(emailMgr.C3SupportEmail);
                mail.To.Add(emailMgr.C3SupportEmail);
                mail.Subject = "Request from HUD Healthcare Portal user";
                mail.Body = "How to recover password?";

                smtpServer.Send(mail);

                return RedirectToAction("Contact", "Home");
            }
// ReSharper disable once UnusedVariable
            catch(Exception e)
            {
                return View("Contact");
            }
        }

    }
}
