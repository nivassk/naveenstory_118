﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService;
using System.Web.Mvc;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using SpreadsheetGear;

namespace HUDHealthcarePortal.Controllers
{
    public class AdhocReportController : Controller
    {

        IAdhocReportManager adhocReportMngr;
        IAccountManager accountMgr;

        public AdhocReportController()
            : this(new AdhocReportManager(), new AccountManager())
        {

        }

        public AdhocReportController(IAdhocReportManager adhocReportManager, IAccountManager accountManager)
        {
            this.adhocReportMngr = adhocReportManager;
            this.accountMgr = accountManager;
        }

        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        public ActionResult Index()
        {
            var model = new AdhocReportViewModel();
            var excelModel = new AdhocReportExcelModel();
            var columnInfo = excelModel.GetAdhocReportColumns(null);
            int rowId = 0;
            foreach (var row in columnInfo)
            {
                rowId++;
                model.CheckBoxModels.Add(new ColumnCheckBoxModel()
                {
                    ColumnName = row.ColumnName,
                    Id = rowId,
                    Selected = row.Selected
                });
            }
            model.AvailableWlms =
                accountMgr.GetWorkloadManagers().ToList()
                    .Where(p => !p.Value.Trim().Equals("NOT ASSIGNED", StringComparison.OrdinalIgnoreCase))
                    .Select(p => new SelectListItem() { Text = p.Value, Value = p.Key.ToString() }).ToList();
            model.AvailableAes = accountMgr.GetAccountExecutives().ToList()
                    .Where(p => !p.Value.Trim().Equals("NOT ASSIGNED", StringComparison.OrdinalIgnoreCase))
                    .Select(p => new SelectListItem() { Text = p.Value, Value = p.Key.ToString() }).ToList();
            model.AvailableLenders = accountMgr.GetLendersByUserRole().ToList()
                    .Select(p => new SelectListItem() { Text = p.Value, Value = p.Key.ToString() }).ToList();
            return View("AdhocReport", model);
        }

        [HttpGet]
        public JsonResult GetAesAndLendersFromWLM(string wlmIdList, string aeIdList)
        {
            string wlmIds = String.IsNullOrEmpty(wlmIdList) ? "" : wlmIdList.Substring(1);
            string aeIds = String.IsNullOrEmpty(aeIdList) ? "" : aeIdList.Substring(1);
            var aeIdsArray = aeIds.Split(',');

            var listOfAEs = adhocReportMngr.GetAEsbyWLMList(wlmIds).ToList()
                    .Where(p => !p.Value.Trim().Equals("NOT ASSIGNED", StringComparison.OrdinalIgnoreCase))
                    .Select(p => new SelectListItem() { Text = p.Key.ToString(), Value = p.Value }).ToList();

            if (!String.IsNullOrEmpty(aeIdsArray[0]))
            {
                foreach (var ae in aeIdsArray)
                {
                    if (listOfAEs.Any(p => p.Text == ae))
                    {
                        aeIds += "," + ae;
                    }
                }
            }
            else
            {
                foreach (var ae in listOfAEs)
                {
                    aeIds += "," + ae.Text;
                }
            }

            aeIds = aeIds.Substring(1);

            var listOfLenders = adhocReportMngr.GetLendersbyWLMAEList(wlmIds, aeIds).ToList()
                    .Where(p => !p.Value.Trim().Equals("NOT ASSIGNED", StringComparison.OrdinalIgnoreCase))
                    .Select(p => new SelectListItem() { Text = p.Key.ToString(), Value = p.Value }).ToList();

            var results = new { AES = listOfAEs, Lenders = listOfLenders };

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAesFromWl(string wlmIdList)
        {
            string wlmIds = String.IsNullOrEmpty(wlmIdList) ? "" : wlmIdList.Substring(1);
            var results = adhocReportMngr.GetAEsbyWLMList(wlmIds).ToList()
                    .Where(p => !p.Value.Trim().Equals("NOT ASSIGNED", StringComparison.OrdinalIgnoreCase))
                    .Select(p => new SelectListItem() { Text = p.Key.ToString(), Value = p.Value }).ToList();
            JsonResult json = Json(results, JsonRequestBehavior.AllowGet);
            return json;
        }

        [HttpGet]
        public JsonResult GetLendersFromAe(string wlmIdList, string aeIdList)
        {
            string wlmIds = String.IsNullOrEmpty(wlmIdList) ? "" : wlmIdList.Substring(1);
            string aeIds = String.IsNullOrEmpty(aeIdList) ? "" : aeIdList.Substring(1);
            var results = adhocReportMngr.GetLendersbyWLMAEList(wlmIds, aeIds).ToList()
                    .Where(p => !p.Value.Trim().Equals("NOT ASSIGNED", StringComparison.OrdinalIgnoreCase))
                    .Select(p => new SelectListItem() { Text = p.Key.ToString(), Value = p.Value }).ToList();
            JsonResult json = Json(results, JsonRequestBehavior.AllowGet);
            return json;
        }

        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        [HttpPost]
        public void GetAdhocSpreadsheet(AdhocReportViewModel model)
        {
            string wlmList = model.SelectedWlms == null ? "" : model.SelectedWlms.Substring(1);
            string aeList = model.SelectedAes == null ? "" : model.SelectedAes.Substring(1);
            string lenderList = model.SelectedLenders == null ? "" : model.SelectedLenders.Substring(1);
            string partialFhaNumber = model.PartialFhaNumber ?? "";
            //TODO: DateTime.MinValue is before beginning of time in SQL Server
            DateTime minPeriodEnd = model.MinPeriodEnding == null ? DateTime.Parse("1/1/1753 12:00:00 AM") : DateTime.Parse(model.MinPeriodEnding);
            DateTime maxPeriodEnd = model.MaxPeriodEnding == null ? DateTime.MaxValue : DateTime.Parse(model.MaxPeriodEnding);

            var AdhocReportDetail = adhocReportMngr.GetAdhocReport(wlmList,
                                                                   aeList,
                                                                   minPeriodEnd,
                                                                   maxPeriodEnd,
                                                                   partialFhaNumber,
                                                                   lenderList,
                                                                   model.ActiveAllSelected);
            Dictionary<string, bool> columnsChosen = new Dictionary<string, bool>();
            foreach (var column in model.CheckBoxModels)
            {
                columnsChosen.Add(column.ColumnName, column.Selected);
            }
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "AdhocReport");

            ExcelAndPrintHelper.SetDataValuesForAdhocReport(cells, AdhocReportDetail, columnsChosen, "AdhocReport");
            GetResponse(workbook, "AdhocReport");

            //return View("AdhocReport", model);
        }

        public void GetResponse(IWorkbook workbook, string worksheetName)
        {
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + worksheetName + ".xls");
            workbook.SaveToStream(Response.OutputStream, FileFormat.Excel8);
            Response.End();
        }
    }
}
