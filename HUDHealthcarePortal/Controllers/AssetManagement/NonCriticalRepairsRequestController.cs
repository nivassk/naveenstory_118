﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using BusinessService.AssetManagement;
using BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Helpers;
using HUDHealthCarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using System.IO;

namespace HUDHealthcarePortal.Controllers.AssetManagement
{
    public class NonCriticalRepairsRequestController : Controller
    {
        private IUploadDataManager uploadDataManager;
        private INonCriticalRepairsRequestManager nonCriticalRepairsManager;
        private IAccountManager accountManager;
        private ITaskManager taskManager;
        private IBackgroundJobMgr backgroundJobManager;
        private IEmailManager emailManager;

        public NonCriticalRepairsRequestController()
            : this(new UploadDataManager(), new NonCriticalRepairsRequestManager(),
                new AccountManager(), new TaskManager(), new BackgroundJobMgr(), new EmailManager())
        {

        }

        private NonCriticalRepairsRequestController(IUploadDataManager uploadManager, INonCriticalRepairsRequestManager _nonCriticalRepairsManager,
            IAccountManager _accountManager, ITaskManager _taskManager, IBackgroundJobMgr backgroundManager, IEmailManager _emailManager)
        {
            uploadDataManager = uploadManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager;
            accountManager = _accountManager;
            taskManager = _taskManager;
            backgroundJobManager = backgroundManager;
            emailManager = _emailManager;
        }


        [HttpGet]
        [AccessDeniedAuthorize(
            Roles =
                "LenderAccountManager,BackupAccountManager,LenderAccountRepresentative")]
        [MvcSiteMapNode(Title = "Non-Critical Repair Request Escrow Form", ParentKey = "AMId")]
        [SiteMapTitle("NonCritical", Target = AttributeTarget.CurrentNode)]
        public ActionResult Index()
        {
            var model = new NonCriticalRepairsViewModel();
            PopulateFHANumberList(model);
            GetDisclaimerTextNCR(model);
            model.LenderId = UserPrincipal.Current.UserData.LenderId.Value;
            model.LenderName = nonCriticalRepairsManager.GetLenderName(UserPrincipal.Current.UserData.LenderId.Value);
            model.SubmittedDate = DateTime.UtcNow;
            model.CreatedBy = UserPrincipal.Current.UserId;
            model.CreatedOn = DateTime.UtcNow;
            model.ModifiedOn = DateTime.UtcNow;
            model.SubmitByUserId = UserPrincipal.Current.UserId;
            // model.RulesCheckList = nonCriticalRepairsManager.GetNonCrticalRulesCheckList(FormType.Ncr);
            //model.RulesCheckList = nonCriticalRepairsManager.GetNonCrticalReferReasons( );

            TempData["FormAttachmentFileTypes"] =
                       ConfigurationManager.AppSettings["FormAttachmentFileTypes"].ToString();

            return View("~/Views/AssetManagement/NonCriticalRepairsRequestView.cshtml", model);
        }

        public JsonResult GetPropertyInfo(string selectedFhaNumber)
        {
            var resultOne = nonCriticalRepairsManager.GetNonCriticalPropertyInfo(selectedFhaNumber);
            JsonResult json = null;
            if (resultOne != null)
            {
                var resultTwo = nonCriticalRepairsManager.GetNonCriticalStatusAndDrawInfo(resultOne, selectedFhaNumber);
                if (resultTwo != null)
                {
                    json = Json(resultTwo, JsonRequestBehavior.AllowGet);
                    if (TempData["NCREValid"] == null)
                    {
                        TempData["NCREValid"] = resultTwo.IsNCREValidForFHANumber;
                    }

                }
            }
            return json;
        }

        public ActionResult SubmitForm(NonCriticalRepairsViewModel model, Guid? taskGuid, int? sequenceId, byte[] concurrency, bool isAgreementAccepted)
        {
            if (ModelState.IsValid)
            {

                if (model.File92464 != null)
                {
                    TaskHelper.CacheFileData(TempData, model.File92464, "File92464");
                }
                else if (model.File92464 == null && TempData["File92464"] != null)
                {
                    model.File92464 = (HttpPostedFileBase)TempData["File92464"];
                }

                if (model.File92117 != null)
                {
                    TaskHelper.CacheFileData(TempData, model.File92117, "File92117");
                }
                else if (model.File92117 == null && TempData["File92117"] != null)
                {
                    model.File92117 = (HttpPostedFileBase)TempData["File92117"];
                }
                if (model.FileScopeCertification != null)
                {
                    TaskHelper.CacheFileData(TempData, model.FileScopeCertification, "FileScopeCertification");
                }
                else if (model.FileScopeCertification == null && TempData["FileScopeCertification"] != null)
                {
                    model.FileScopeCertification = (HttpPostedFileBase)TempData["FileScopeCertification"];
                }
                if (model.FileClearTitle != null)
                {
                    TaskHelper.CacheFileData(TempData, model.FileClearTitle, "FileClearTitle");
                }
                else if (model.FileClearTitle == null && TempData["FileClearTitle"] != null)
                {
                    model.FileClearTitle = (HttpPostedFileBase)TempData["FileClearTitle"];
                }
                if (model.FilePCNA != null)
                {
                    TaskHelper.CacheFileData(TempData, model.FilePCNA, "FilePCNA");
                }
                else if(model.FilePCNA == null && TempData["FilePCNA"] != null)
                {
                    model.FilePCNA = (HttpPostedFileBase)TempData["FilePCNA"];
                }

                if (model.FileDefaultRequestExtension != null)
                {
                    TaskHelper.CacheFileData(TempData, model.FileDefaultRequestExtension, "FileDefaultRequestExtension");
                }
                else if (model.FileDefaultRequestExtension == null && TempData["FileDefaultRequestExtension"] != null)
                {
                    model.FileDefaultRequestExtension = (HttpPostedFileBase)TempData["FileDefaultRequestExtension"];
                }

                if (model.FileContract != null)
                {
                    TaskHelper.CacheFileData(TempData, model.FileContract, "FileContract");
                }
                else if (model.FileContract == null && TempData["FileContract"] != null)
                {
                    model.FileContract = (HttpPostedFileBase)TempData["FileContract"];
                }
                if (TempData["NCREValid"] != null && (bool)TempData["NCREValid"])
                {
                    model.IsNCREAmountValid = true;
                }
                if (model.IsNCREAmountValid)
                {
                    TempData["NCREValid"] = model.IsNCREAmountValid;
                }

                model.IsRequestAutoApproved = false;
                model.IsAgreementAccepted = isAgreementAccepted;

                //User Story 1901 
                //if (!model.IsAgreementAccepted)
                //{
                //    model.IsAgreementAccepted = true;
                //    PopulateFHANumberList(model);
                //    PopupHelper.ConfigPopup(TempData, 720, 300, "Disclaimer", false, false, true, false, false, true,
                //        "../NonCriticalRepairsRequest/NonCriticalCertifyPopUp");
                //    return View("~/Views/AssetManagement/NonCriticalRepairsRequestView.cshtml", model);
                //}

                //check conditions for auto approval
                model.IsFile92117Uploaded = model.File92117 != null;
                model.IsFile92464Uploaded = model.File92464 != null;
                model.IsFilePCNAUploaded = model.FilePCNA != null;
                if (UserPrincipal.Current.LenderId != null) model.LenderId = (int)UserPrincipal.Current.LenderId;
                model.IsFileReqExtnUploaded = (model.IsExtensionAttachmentRequired && model.FileDefaultRequestExtension != null);
                //Auto approval only on Lender delegate - User Story 1810
                /*if (model.IsScopeOfWorkChanged || model.IsThisAnAdvance || !model.IsFile92117Uploaded || !model.IsFile92464Uploaded ||
                    (model.NonCriticalAccountBalance != 0 && model.PaymentAmountRequested > model.NCRECurrentBalance) ||
                    ((DateTime.Now - (DateTime)model.ClosingDate).Days > 365 && !model.IsFileReqExtnUploaded) ||
                    (model.NonCriticalAccountBalance < 1000000) ||
                    (model.TroubledCode == "T") || (model.PaymentAmountRequested > 50000) ||
                    (!model.IsNCREAmountValid || model.NonCriticalAccountBalance == 0))
                {
                    model.IsRequestAutoApproved = false;
                }*/
                model.SubmitByUserId = UserPrincipal.Current.UserId;
                model.SubmitToUserId = nonCriticalRepairsManager.GetAeUserIdByFhaNumber(model.FHANumber);
                model.CreatedBy = UserPrincipal.Current.UserId;
                model.CreatedOn = DateTime.UtcNow;
                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;

                if (model.IsLenderDelegate == true)
                {
                    model.IsRequestAutoApproved = true;
                }

                if (model.IsScopeOfWorkChanged || model.IsThisAnAdvance || model.IsFinalDraw)
                {
                    model.IsRequestAutoApproved = false;
                }

                if (model.IsRequestAutoApproved)
                {
                    model.RequestStatus = (int)RequestStatus.AutoApprove;
                    TempData["RequestStatus"] = 4;
                }
                else
                {
                    model.RequestStatus = (int)RequestStatus.Submit;
                    TempData["RequestStatus"] = 1;
                }

                model.NonCriticalRequestId = Guid.NewGuid();
                //Insert the non critical request and get the generated id, insert into view model
                var nonCriticalRequestId = nonCriticalRepairsManager.SaveNonCriticalRepairsRequest(model);

                var propertyDetailsTxt = "";
                if (!string.IsNullOrEmpty(model.FHANumber))
                {

                    IReserveForReplacementManager reserveForReplacementManager = new ReserveForReplacementManager();
                    var propertyInfo = reserveForReplacementManager.GetPropertyInfo(model.FHANumber);
                    if (model.PropertyAddress != null)
                    {
                        if (model.PropertyAddress.AddressLine1 != null &&
                            model.PropertyAddress.AddressLine1 != propertyInfo.StreetAddress)
                        {
                            propertyDetailsTxt += "<p>Street Address: " + model.PropertyAddress.AddressLine1 + "</p>";
                        }
                        if (model.PropertyAddress.City != null &&
                            model.PropertyAddress.City != propertyInfo.City)
                        {
                            propertyDetailsTxt += "<p>City: " + model.PropertyAddress.City + "</p>";
                        }
                        if (model.PropertyAddress.StateCode != null &&
                            model.PropertyAddress.StateCode != propertyInfo.State)
                        {
                            propertyDetailsTxt += "<p>State: " + model.PropertyAddress.StateCode + "</p>";
                        }
                        if (model.PropertyAddress.ZIP != null &&
                            model.PropertyAddress.ZIP != propertyInfo.Zipcode)
                        {
                            propertyDetailsTxt += "<p>Zip: " + model.PropertyAddress.ZIP + "</p>";
                        }

                        if (!string.IsNullOrEmpty(propertyDetailsTxt))
                        {
                            model.IsUpdateiREMS = true;
                        }
                    }
                }

                var taskList = new List<TaskModel>();
                var task = new TaskModel();

                if (model.SubmitToUserId != 0)
                {
                    var toUser = accountManager.GetUserById(model.SubmitToUserId.Value);
                    task.AssignedTo = toUser.UserName;
                }
                else
                {
                    task.AssignedTo = nonCriticalRepairsManager.GetAeEmailByFhaNumber(model.FHANumber);
                }

                task.AssignedBy = UserPrincipal.Current.UserName;
                task.Notes = model.ServicerRemarks;
                task.DataStore1 = XmlHelper.Serialize(model, typeof(NonCriticalRepairsViewModel), Encoding.Unicode);
                task.SequenceId = 0;
                task.StartTime = DateTime.UtcNow;
                task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.TaskInstanceId = Guid.NewGuid();
                task.IsReAssigned = false;
                if (model.IsRequestAutoApproved)
                {
                    task.TaskStepId = (int)TaskStep.NonCriticalRepairComplete;
                }
                else
                {
                    task.TaskStepId = (int)TaskStep.NonCriticalRequest;
                }

                taskList.Add(task);

                var taskFile = UpdateTaskFileModelForTask(model, task);

                try
                {
                    if (model.IsUpdateiREMS)
                    {
                        //email notification to update iREMS
                        backgroundJobManager.SendUpdateiREMSNotificationEmail(emailManager, propertyDetailsTxt,
                            model.PropertyName, model.FHANumber);
                    }

                    taskManager.SaveTask(taskList, taskFile);
                    //Latest task id been updated on submission
                    model.TaskId = nonCriticalRepairsManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                    nonCriticalRepairsManager.UpdateTaskId(model);

                    // email notification for auto approve

                    backgroundJobManager.SendNonCriticalRequestApprovalNotificationEmail(emailManager, task.AssignedTo, accountManager.GetUserById(model.SubmitByUserId.Value).UserName, model);


                    //return to mytasks
                    UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                    string myTaskUrl = u.AbsoluteAction("MyTasks", "Task", null);


                    if (!model.IsDisplayAcceptPopup)
                    {
                        model.IsDisplayAcceptPopup = true;
                        PopulateFHANumberList(model);
                        PopupHelper.ConfigPopup(TempData, 720, 200, "Disclaimer", false, false, true, false, false, true,
                            "../NonCriticalRepairsRequest/NonCriticalConfirmationPopUp");
                        return View("~/Views/AssetManagement/NonCriticalRepairsRequestView.cshtml", model);
                    }

                }
                catch
                {

                }
            }
            return View("~/Views/AssetManagement/NonCriticalRepairsRequestView.cshtml", model);
        }


        [HttpPost]
        public ActionResult NonCriticalCertifyPopUp()
        {
            return PartialView("~/Views/AssetManagement/NonCriticalCertifyPopUp.cshtml");
        }


        [HttpPost]
        public ActionResult NonCriticalDisclaimerPopUp()
        {
            return PartialView("~/Views/AssetManagement/NonCriticalDisclaimerPopUp.cshtml");
        }

        [HttpPost]
        public ActionResult NonCriticalConfirmationPopUp()
        {
            return PartialView("~/Views/AssetManagement/NonCriticalConfirmationPopUp.cshtml");
        }

        [HttpGet]
        [MvcSiteMapNode(Title = "Task Detail", ParentKey = "TaskId")]
        [SiteMapTitle("TaskName", Target = AttributeTarget.CurrentNode)]
        public ActionResult GetNonCriticalRepairsRequestFormDetail(bool isEditMode)
        {
            //Dictionary<string, string> fhaNumberList;
            var model = (NonCriticalRepairsViewModel)(TempData["NonCriticalRepairsViewModel"] ?? new NonCriticalRepairsViewModel());
            //  var model = CreateTempModel();
            model.RulesCheckList = nonCriticalRepairsManager.GetNonCrticalRulesCheckList(FormType.Ncr);
            model.ReferalReasons = nonCriticalRepairsManager.GetNonCrticalReferReasons(model.NonCriticalRequestId);

            ControllerHelper.PopulateParentNodeRouteValuesForPageSort(
                    SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
            ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
            if (model != null)
            {
                model.IsTransactionLedger = nonCriticalRepairsManager.GetTransactionLedgerStatus(model.FHANumber);
                model.ManageMode = "ReadOnly";
                model.IsDisplayAcceptPopup = false;
                model.IsAgreementAccepted = false;
                if (isEditMode)
                {
                    if (RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName) || 
                        RoleManager.IsInternalSpecialOptionUser(UserPrincipal.Current.UserName))
                    {
                        model.ManageMode = "Edit";
                        TempData["FormAttachmentFileTypes"] =
                            ConfigurationManager.AppSettings["FormAttachmentFileTypes"].ToString();
                        PopulateFHANumberList(model);
                        // store opened by user to TaskOpenStatus column
                        if (model.TaskOpenStatus != null &&
                            !model.TaskOpenStatus.TaskOpenStatus.Exists(
                                p => p.Key.Equals(UserPrincipal.Current.UserName)))
                        {
                            model.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                        }
                        model.IsAgreementAccepted = true;
                        GetDisclaimerTextNCR(model);
                        return View("~/Views/AssetManagement/NonCriticalRepairsAEandReadonly.cshtml", model);
                    }
                }
                else if (model.SequenceId.HasValue
                    && model.SequenceId < 2
                    && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName)
                    && !model.IsPAMReport
                    && RoleManager.IsUserLenderRole(model.AssignedTo))
                {
                    var task = new TaskModel();
                    var taskList = new List<TaskModel>();
                    task.AssignedBy = model.AssignedBy;
                    task.AssignedTo = model.AssignedTo;
                    task.StartTime = DateTime.UtcNow;
                    task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                    task.TaskInstanceId = model.TaskGuid ?? Guid.NewGuid();
                    task.TaskStepId = (int)TaskStep.NonCriticalRepairComplete;
                    task.TaskOpenStatus = XmlHelper.Serialize(model.TaskOpenStatus, typeof(TaskOpenStatusModel), Encoding.Unicode);
                    task.SequenceId = model.SequenceId.HasValue ? model.SequenceId.Value + 1 : 0;
                    task.IsReAssigned = model.IsReassigned;
                    var taskFile = new TaskFileModel();
                    try
                    {
                        task.DataStore1 = XmlHelper.Serialize(model, typeof(NonCriticalRepairsViewModel), Encoding.Unicode);
                        taskList.Add(task);
                        taskManager.SaveTask(taskList, taskFile);
                        model.TaskId = nonCriticalRepairsManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                        nonCriticalRepairsManager.UpdateTaskId(model);
                    }
                    catch (Exception e)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                        throw new InvalidOperationException(
                            string.Format("Error Non critical form detail: {0}", e.InnerException), e.InnerException);
                    }
                }
            }
            model.IsAgreementAccepted = true;
            GetDisclaimerTextNCR(model);
            return View("~/Views/AssetManagement/NonCriticalRepairsAEandReadonly.cshtml", model);
        }

        private void HandleConcurrentUpdateException(NonCriticalRepairsViewModel model)
        {
            model.IsConcurrentUserUpdateFound = true;
        }

        /// <summary>
        /// Approving the Non critcal request, AE only can do this now
        /// </summary>
        /// <param name="model"></param>
        /// <param name="taskGuid"></param>
        /// <param name="sequenceId"></param>
        /// <param name="concurrency"></param>
        /// <returns></returns>
        [MvcSiteMapNode(Title = "Task Detail", ParentKey = "TaskId")]
        [SiteMapTitle("TaskName", Target = AttributeTarget.CurrentNode)]
        public ActionResult ApproveOrDisApprove(NonCriticalRepairsViewModel model, Guid? taskGuid, int? sequenceId, byte[] concurrency)
        {

            if (ModelState.IsValid)
            {
                if (model.Decision == (int)RequestStatus.Approve)
                    TempData["RequestStatus"] = (int)RequestStatus.Approve;
                else if (model.Decision == (int)RequestStatus.ApproveWithChanges)
                    TempData["RequestStatus"] = (int)RequestStatus.ApproveWithChanges;
                else if (model.Decision == (int)RequestStatus.Deny)
                    TempData["RequestStatus"] = (int)RequestStatus.Deny;
                model.RequestStatus = model.Decision;

                if (model.RequestStatus == (int)RequestStatus.Approve ||
                    model.RequestStatus == (int)RequestStatus.ApproveWithChanges ||
                    model.RequestStatus == (int)RequestStatus.AutoApprove)
                {
                    model.IsTransactionLedger = true;
                }
                //if (!model.IsDisplayAcceptPopup)
                //{
                //    model.IsDisplayAcceptPopup = true;
                //  //  PopulateFHANumberNumberDrawList(model);
                //    PopupHelper.ConfigPopup(TempData, 720, 200, "Disclaimer", false, false, true, false, false, true,
                //        "../NonCriticalRepairsRequest/NonCriticalDisclaimerPopUp");
                //    ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
                //    return View("~/Views/AssetManagement/NonCriticalRepairsAEandReadonly.cshtml", model);
                //}
                //added to retain pagination parameters
                ControllerHelper.PopulateParentNodeRouteValuesForPageSort(
                    SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
                TempData["routeparams"] = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
                //update to database
                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;
                model.ApprovedDate = DateTime.UtcNow;
                //Approve is true, approval changes is false, we are setting differnt statuses
                //update the model
                nonCriticalRepairsManager.UpdateNonCriticalRepairsRequest(model);
                model.RulesCheckList = nonCriticalRepairsManager.GetNonCrticalRulesCheckList(FormType.Ncr);
                model.ReferalReasons = nonCriticalRepairsManager.GetNonCrticalReferReasons(model.NonCriticalRequestId);
                if (model.NonCriticalRequestId != Guid.Empty)
                {
                    var taskList = new List<TaskModel>();
                    // save task to finish
                    var task = new TaskModel();
                    var toEmail = model.AssignedBy;
                    task.AssignedBy = UserPrincipal.Current.UserName;
                    task.AssignedTo = model.AssignedBy;

                    task.DataStore1 = XmlHelper.Serialize(model, typeof(NonCriticalRepairsViewModel), Encoding.Unicode);
                    task.Notes = model.Reason;
                    task.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0; // incrementing from 0
                    task.StartTime = DateTime.UtcNow;
                    task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                    task.TaskInstanceId = taskGuid ?? Guid.NewGuid();
                    task.Concurrency = concurrency;
                    task.TaskStepId = (int)TaskStep.NonCriticalRepairComplete;
                    var taskOpenStatusModel = new TaskOpenStatusModel();
                    taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                    task.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel), Encoding.Unicode);
                    task.IsReAssigned = model.IsReassigned;
                    taskList.Add(task);
                    var taskFile = UpdateTaskFileModelForTask(model, task);
                    try
                    {
                        taskManager.SaveTask(taskList, taskFile);
                        //Latest task id been updated on Approve
                        model.TaskId = nonCriticalRepairsManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                        nonCriticalRepairsManager.UpdateTaskId(model);
                        backgroundJobManager.SendNonCriticalRequestApprovalNotificationEmail(emailManager, task.AssignedBy, accountManager.GetUserById(model.SubmitByUserId.Value).UserName, model);
                        model.IsDisplayAcceptPopup = false;
                        model.IsAgreementAccepted = true;
                        GetDisclaimerTextNCR(model);
                        model.ManageMode = "ReadOnly";
                        //return View("~/Views/AssetManagement/NonCriticalRepairsAEandReadonly.cshtml", model);
                        return RedirectToAction("MyTasks", "Task");
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        HandleConcurrentUpdateException(model);
                    }
                }
                
            }

            model.IsAgreementAccepted = true;
            GetDisclaimerTextNCR(model);
            return View("~/Views/AssetManagement/NonCriticalRepairsRequestView.cshtml", model);
        }

        [HttpGet]
        public FileResult DownloadTaskFile(Guid? taskInstanceId, FileType taskFileType)
        {
            TaskFileModel taskFile = taskManager.GetTaskFileByTaskInstanceAndFileTypeId(taskInstanceId.Value, taskFileType);
            if (taskFile != null)
            {
                return File(taskFile.FileData, "text/text", taskFile.FileName);
            }
            return null;
        }

        private IList<TaskFileModel> UpdateTaskFileModelForTask(NonCriticalRepairsViewModel model, TaskModel task)
        {
            IList<TaskFileModel> listTaskModel = new List<TaskFileModel>();

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.File92464 == null && TempData["File92464"] != null)
            {
                model.File92464 = (HttpPostedFileBase)TempData["File92464"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.File92117 == null && TempData["File92117"] != null)
            {
                model.File92117 = (HttpPostedFileBase)TempData["File92117"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.FileScopeCertification == null && TempData["FileScopeCertification"] != null)
            {
                model.FileScopeCertification = (HttpPostedFileBase)TempData["FileScopeCertification"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.FileContract == null && TempData["FileContract"] != null)
            {
                model.FileContract = (HttpPostedFileBase)TempData["FileContract"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.FileDefaultRequestExtension == null && TempData["FileDefaultRequestExtension"] != null)
            {
                model.FileDefaultRequestExtension = (HttpPostedFileBase)TempData["FileDefaultRequestExtension"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.FileClearTitle == null && TempData["FileClearTitle"] != null)
            {
                model.FileClearTitle = (HttpPostedFileBase)TempData["FileClearTitle"];
            }

            // Get the file data.
            if (model.File92117 != null)
            {
                ControllerHelper.PopulateTaskFileModel(model.File92117, FileType.BorrowerCertification92117, task, listTaskModel);
            }

            if (model.FilePCNA != null)
            {
                ControllerHelper.PopulateTaskFileModel(model.FilePCNA, FileType.PCNA, task, listTaskModel);
            }

            if (model.File92464 != null)
            {
                ControllerHelper.PopulateTaskFileModel(model.File92464, FileType.ApprovalAdvance92464, task, listTaskModel);
            }

            if (model.FileScopeCertification != null)
            {
                ControllerHelper.PopulateTaskFileModel(model.FileScopeCertification, FileType.ScopeCertification, task, listTaskModel);
            }

            if (model.FileClearTitle != null)
            {
                ControllerHelper.PopulateTaskFileModel(model.FileClearTitle, FileType.ClearTitle, task, listTaskModel);
            }

            if (model.FileContract != null)
            {
                ControllerHelper.PopulateTaskFileModel(model.FileContract, FileType.FileContract, task, listTaskModel);
            }

            if (model.FileDefaultRequestExtension != null)
            {
                ControllerHelper.PopulateTaskFileModel(model.FileDefaultRequestExtension, FileType.DefaultRequestExtension, task, listTaskModel);
            }
            return listTaskModel;
        }


        private void PopulateFHANumberList(NonCriticalRepairsViewModel model)
        {
            var fhaNumberList = new List<string>();
            if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                fhaNumberList =
                    nonCriticalRepairsManager.GetAllowedNonCriticalFhaByLenderUserId(
                        UserPrincipal.Current.UserId);
            }

            model.AvailableFHANumbersList = fhaNumberList;


        }

        public ActionResult GetTransactionLedger(int propertyId, int lenderId, string fhaNumber)
        {
            var model = nonCriticalRepairsManager.GetNonCrticalNonCrticalTransactions(propertyId, lenderId, fhaNumber);
            return PartialView("~/Views/AssetManagement/TransactionView.cshtml", model);
        }

        private void GetDisclaimerTextNCR(NonCriticalRepairsViewModel model)
        {

            model.DisclaimerText = nonCriticalRepairsManager.GetDisclaimerMsgByPageType("NCRE");


        }
    }
}
