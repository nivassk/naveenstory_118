﻿using BusinessService.Interfaces;
using BusinessService.Interfaces.InternalExternalTask;
using BusinessService.InternalExternalTask;
using Core;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Controllers.AssetManagement
{
    public class InternalExternalGroupTaskController : Controller
    {
        //
        // GET: /InternalExternalGroupTask/
        private IInternalExternalTaskManager internalExternalTaskManger;
        private IAccountManager accountManager;

        public InternalExternalGroupTaskController():this(new InternalExternalTaskManager(),new AccountManager())

        {
                
        }
        private InternalExternalGroupTaskController(IInternalExternalTaskManager _internalExternalTaskManager , IAccountManager _accountManager)
        {
            internalExternalTaskManger = _internalExternalTaskManager;
            accountManager = _accountManager;
        }
        public ActionResult Index()
        {
            return View("~/Views/InternalExternalTask/InternalExternalGroupTask/Index.cshtml");
        }
        public JsonResult GetInternalExternalGroupTask(string sidx, string sord, int page, int rows)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            //string userName = "";
            var grouTasks = (from task in internalExternalTaskManger.GetAllTasks().Where(m=>m.StatusID==(int)InternalExternalTaskStatus.Draft)
                             let userName = task.InUse != null ? accountManager.GetUserById((int)task.InUse).UserName : ""
                             select new
                             {
                                 id=task.InternalExternalTaskID,
                                 InternalExternalTaskName =string.Format("({0}) {1}",task.FHANumber, EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeID.ToString()))),                                
                                 Status = EnumType.GetEnumDescription(EnumType.Parse<InternalExternalTaskStatus>(task.StatusID.ToString())),
                                 Usertype = EnumType.GetEnumDescription(EnumType.Parse<HUDRoleSource>(task.UserTypeID.ToString())),                              
                                 //InUse = task.InUse!=null?accountManager.GetUserById((int)task.InUse).UserName:"",                              
                                 InUse=userName,
                                 Role = (userName!=null ||!userName.Equals(""))?RoleManager.GetUserRoles(userName).FirstOrDefault():"",
                                 task.CreatedDate,
                                 Unlock = task.InUse!=null?task.InUse==UserPrincipal.Current.UserId?true:false:false,

                             }).OrderByDescending(m=>m.id);

            
            int totalrecods = grouTasks.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var results = grouTasks.Skip(pageIndex * pageSize).Take(pageSize);



            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

    }
}
