﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Controllers.ImportExcel
{
    public class ImportExcelController : Controller
    {
        //
        // GET: /ImportExcel/
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /ImportExcel/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /ImportExcel/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ImportExcel/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /ImportExcel/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /ImportExcel/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /ImportExcel/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /ImportExcel/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
