﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Windows.Forms;
using BusinessService.Interfaces;
using Elmah;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using BusinessService.ProjectAction;
using HUDHealthCarePortal.Helpers;
using Model.ProjectAction;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using WebGrease.Css.Extensions;
using BusinessService.AssetManagement;
using BusinessService;
using Model;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using Model.Production;
using System.Net;// User Story 1901
using Repository.Interfaces;
using Repository;


namespace HUDHealthcarePortal.Controllers.OPAForm
{
    public class OPAFormController : Controller
    {
        private IProjectActionFormManager projectActionFormManager;
        private IProjectActionManager projectActionManager;
        private IGroupTaskManager groupTaskManager;
        private ITaskManager taskManager;
        private ISubFolderStructureRepository folderManager;
        private IAccountManager accountManager;
        private IEmailManager emailManager;
        private IBackgroundJobMgr backgroundJobManager;
        private INonCriticalRepairsRequestManager nonCriticalRepairsManager; // User Story 1901
        private IParentChildTaskManager parentChildTaskManager;
        private IRequestAdditionalInfoFileManager requestAdditionalInfoFilesManager;
        private IReviewFileStatusManager reviewFileStatusManager;
        private IProd_RestfulWebApiDownload WebApiDownload;
        private IProd_RestfulWebApiUpdate WebApiUpdate;
        private IProd_RestfulWebApiDelete WebApiDelete;
        private IProd_RestfulWebApiTokenRequest WebApiTokenRequest;
        private IProd_RestfulWebApiDocumentUpload WebApiDocumentUpload;
        private IProd_RestfulWebApiReplace WebApiDocumentReplace;
        private IProd_GroupTasksManager prod_GroupTasksManager;
        private IOPAManager iOPAForm;
        IWebSecurityWrapper webSecurity;
        private IAppProcessManager appProcessManager;

        //
        // GET: /OPAForm/

        public OPAFormController()
            : this(
                new ProjectActionFormManager(), new TaskManager(), new AccountManager(), new EmailManager(),
                new BackgroundJobMgr(), new ProjectActionManager(), new GroupTaskManager(),
                new NonCriticalRepairsRequestManager(), new ParentChildTaskManager(),
                new RequestAdditionalInfoFilesManager(), new ReviewFileStatusManager(), new WebSecurityWrapper(), new OPAManager(),
                new Prod_RestfulWebApiDownload(), new Prod_RestfulWebApiTokenRequest(), new Prod_RestfulWebApiUpdate(),
                new Prod_RestfulWebApiDocumentUpload(), new Prod_RestfulWebApiReplace(), new Prod_RestfulWebApiDelete(), new SubFolderStructureRepository(), new Prod_GroupTasksManager(), new AppProcessManager())
        {
        }

        private OPAFormController(IProjectActionFormManager _projectActionFormManager, ITaskManager _taskManager,
            IAccountManager _accountManager, IEmailManager _emailManager, IBackgroundJobMgr backgroundManager,
            IProjectActionManager _projectActionManager, IGroupTaskManager _groupTaskManager,
            INonCriticalRepairsRequestManager _nonCriticalRepairsManager,
            IParentChildTaskManager _parentChildTaskManager,
            IRequestAdditionalInfoFileManager _requestAdditionalInfoFilesManager,
            IReviewFileStatusManager _reviewFileStatusManager,
            IWebSecurityWrapper webSec,
            IOPAManager _opaManager,
            IProd_RestfulWebApiDownload _WebApiDownload,
            IProd_RestfulWebApiTokenRequest _WebApiTokenRequest,
            IProd_RestfulWebApiUpdate _WebApiUpdate,
            IProd_RestfulWebApiDocumentUpload _WebApiDocumentUpload,
            IProd_RestfulWebApiReplace _WebApiDocumentReplace,
            IProd_RestfulWebApiDelete _WebApiDelete,
            ISubFolderStructureRepository _folderManager,
            IProd_GroupTasksManager _prod_GroupTasksManager,
            IAppProcessManager _AppProcessManager)
        {
            projectActionFormManager = _projectActionFormManager;
            taskManager = _taskManager;
            accountManager = _accountManager;
            emailManager = _emailManager;
            backgroundJobManager = backgroundManager;
            projectActionManager = _projectActionManager;
            groupTaskManager = _groupTaskManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager; // User Story 1901
            parentChildTaskManager = _parentChildTaskManager;
            requestAdditionalInfoFilesManager = _requestAdditionalInfoFilesManager;
            reviewFileStatusManager = _reviewFileStatusManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager;// User Story 1901
            webSecurity = webSec;
            iOPAForm = _opaManager;
            WebApiDownload = _WebApiDownload;
            WebApiTokenRequest = _WebApiTokenRequest;
            WebApiUpdate = _WebApiUpdate;
            WebApiDelete = _WebApiDelete;
            WebApiDocumentUpload = _WebApiDocumentUpload;
            WebApiDocumentReplace = _WebApiDocumentReplace;
            folderManager = _folderManager;
            prod_GroupTasksManager = _prod_GroupTasksManager;
            appProcessManager = _AppProcessManager;
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        // [MvcSiteMapNode(Title = "Project Action Request Form", ParentKey = "AMId", PreservedRouteParameters = "", Key = "OPAkey")]
        [MvcSiteMapNode(Title = "Project Action Request Form", ParentKey = "AMId")]
        [SiteMapTitle("PAMReport", Target = AttributeTarget.CurrentNode)]
        public ActionResult Index()
        {
            OPAViewModel OPAprojectActionViewModel = new OPAViewModel();
            InitializeViewModel(OPAprojectActionViewModel);
            //OPAWorkProduct(OPAprojectActionViewModel.PropertyWorkProduct.TaskInstanceId);
            return View("~/Views/OPAForm/OPARequestForm.cshtml", OPAprojectActionViewModel);
        }

        // to attach a form for AE
        public void OPAWorkProduct(Guid taskInstanceId, string fileType)
        {
            //List<OPAWorkProductModel> OPAprojectActionViewModel = iOPAForm.getAllOPAWorkProducts(taskInstanceId, fileType);
            List<OPAWorkProductModel> OPAprojectActionViewModel = iOPAForm.getAllOPAWorkProducts(taskInstanceId, fileType);
            //return PartialView("~/Views/OPAForm/_OPAUploadForAe.cshtml", OPAprojectActionViewModel);
        }

        public JsonResult GetOPAWorkProduct(string sidx, string sord, int page, int rows, Guid taskInstanceId, string fileType)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            var opaWorkProduct = iOPAForm.getAllOPAWorkProducts(taskInstanceId, fileType);

            //Setting the date based on the Time Zone
            if (opaWorkProduct != null)
            {
                foreach (var item in opaWorkProduct)
                {
                    item.UploadTime = TimezoneManager.ToMyTimeFromUtc(item.UploadTime);

                }
            }
            if (opaWorkProduct != null)
            {
                int totalrecods = opaWorkProduct.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
                var result = opaWorkProduct.ToList().OrderBy(s => s.FileId);
                var results = result.Skip(pageIndex * pageSize).Take(pageSize);

                var jsonData = new
                {
                    total = totalpages,
                    page,
                    records = totalrecods,
                    rows = results,
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var jsonData = new
                {
                    total = 1,
                    page = 1,
                    records = 1,
                    rows = opaWorkProduct,

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult OPADeleteGroupTaskFile(string taskFileId, string taskInstanceID,
            OPAViewModel OPAProjectActionViewModel, string IsFromGroupTask, string fhaNumber, string ServicerComments,
            string addressschanged)
        {
            string GroupTaskId = string.Empty;
            GroupTaskId = Request.QueryString[0];
            // OPAProjectActionViewModel.FhaNumber = fhaNumber;
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskFileId));
            var OPAModel = new OPAViewModel();

            int success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");
                success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskFileId));
                System.IO.File.Delete(pathForSaving + taskFile.SystemFileName);
            }

            OPAModel = groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(Convert.ToInt32(GroupTaskId));
            OPAModel.ProjectActionName = projectActionFormManager.GetProjectActionName(OPAModel.ProjectActionTypeId);
            IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(3);
            foreach (var model in projectActionList)
            {
                OPAModel.ProjectActionTypeList.Add(new SelectListItem()
                {
                    Text = model.ProjectActionName,
                    Value = model.ProjectActionID.ToString()
                });
            }

            OPAModel.OpaHistoryViewModel = GetOpaHistory((Guid)OPAModel.GroupTaskInstanceId);

            var result = projectActionFormManager.GetPropertyInfo(fhaNumber);
            if (result != null)
            {
                OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                OPAModel.PropertyAddress.City = result.City ?? "";
                OPAModel.PropertyAddress.StateCode = result.State ?? "";
                OPAModel.PropertyAddress.ZIP = result.Zipcode ?? "";

            }

            var IsViewFromGroupTask = Request.Form["IsViewFromGroupTask"];

            if (IsViewFromGroupTask.ToUpper().Contains("TRUE"))
            {
                OPAModel.IsViewFromGroupTask = true;
            }
            else
            {
                OPAModel.IsViewFromGroupTask = false;
            }

            if (addressschanged.ToUpper() == "TRUE")
            {
                OPAModel.IsAddressChange = true;
            }
            else
            {
                OPAModel.IsAddressChange = false;
            }
            OPAModel.ServicerComments = ServicerComments;
            GetDisclaimerText(OPAModel); // User Story 1901
            if (IsViewFromGroupTask.ToUpper().Contains("TRUE"))
            {
                OPAModel.IsAgreementAccepted = true; // User Story 1901
                return PartialView("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", OPAModel);
            }
            OPAModel.IsAgreementAccepted = false; // User Story 1901
            return PartialView("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);
        }

        [HttpPost]
        public ActionResult DeleteGroupTaskFile(string taskFileId, string taskInstanceID,
            OPAViewModel OPAProjectActionViewModel, string IsFromGroupTask, string fhaNumber, string ServicerComments,
            string addressschanged)
        {
            string GroupTaskId = string.Empty;
            GroupTaskId = Request.QueryString[0];
            // OPAProjectActionViewModel.FhaNumber = fhaNumber;
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskFileId));
            var OPAModel = new OPAViewModel();

            int success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");
                success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskFileId));
                System.IO.File.Delete(pathForSaving + taskFile.SystemFileName);
            }

            OPAModel = groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(Convert.ToInt32(GroupTaskId));
            OPAModel.ProjectActionName = projectActionFormManager.GetProjectActionName(OPAModel.ProjectActionTypeId);
            IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(3);
            foreach (var model in projectActionList)
            {
                OPAModel.ProjectActionTypeList.Add(new SelectListItem()
                {
                    Text = model.ProjectActionName,
                    Value = model.ProjectActionID.ToString()
                });
            }

            OPAModel.OpaHistoryViewModel = GetOpaHistory((Guid)OPAModel.GroupTaskInstanceId);

            var result = projectActionFormManager.GetPropertyInfo(fhaNumber);
            if (result != null)
            {
                OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                OPAModel.PropertyAddress.City = result.City ?? "";
                OPAModel.PropertyAddress.StateCode = result.State ?? "";
                OPAModel.PropertyAddress.ZIP = result.Zipcode ?? "";

            }

            var IsViewFromGroupTask = Request.Form["IsViewFromGroupTask"];

            if (IsViewFromGroupTask.ToUpper().Contains("TRUE"))
            {
                OPAModel.IsViewFromGroupTask = true;
            }
            else
            {
                OPAModel.IsViewFromGroupTask = false;
            }

            if (addressschanged.ToUpper() == "TRUE")
            {
                OPAModel.IsAddressChange = true;
            }
            else
            {
                OPAModel.IsAddressChange = false;
            }
            OPAModel.ServicerComments = ServicerComments;
            GetDisclaimerText(OPAModel); // User Story 1901
            if (IsViewFromGroupTask.ToUpper().Contains("TRUE"))
            {
                OPAModel.IsAgreementAccepted = true; // User Story 1901
                return PartialView("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", OPAModel);
            }
            OPAModel.IsAgreementAccepted = false; // User Story 1901
            return PartialView("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);
        }

        [HttpPost]
        public ActionResult Facade(string submitButton, OPAViewModel OPAprojectActionViewModel)
        {
            var temp = Request.QueryString[0];
            string[] str = temp.Split(',');

            var grouptaskid = str[0];

            if (!string.IsNullOrEmpty(grouptaskid))
            {
                OPAprojectActionViewModel.GroupTaskId = Convert.ToInt32(grouptaskid);
            }

            submitButton = str[1];
            switch (submitButton)
            {
                case "Submit":
                    // delegate sending to another controller action
                    return (Submit(OPAprojectActionViewModel));
                case "Save":
                    // call another action to perform the Save
                    return Save(OPAprojectActionViewModel);
                case @"Save and Check-in":
                    // call another action to perform the cancellation
                    return (SaveCheckin(OPAprojectActionViewModel));

                default:
                    // If they've submitted the form without a submitButton, 
                    // just return the view again.
                    return (View());
            }
        }

        private ActionResult Submit(OPAViewModel model)
        {
            var OPAModel = new OPAViewModel();
            string GroupTaskId = string.Empty;
            string FHA = string.Empty;
            string disclaimer = string.Empty;

            string requestDate = string.Empty;
            GroupTaskId = Request.Form["GroupTaskId"];
            requestDate = Request.Form["requestDate"];
            disclaimer = Request.Form["disclaimer"];
            var ServicerComments = Request.Form["ServicerComments"];
            var addressschanged = Request.Form["addressschanged"];

            FHA = Request.Form["FHA"];
            //model.PropertyId = projectActionFormManager.GetPropertyInfo(FHA).PropertyId;
            if (disclaimer.ToUpper() == "TRUE")
            {
                model.IsAgreementAccepted = true;
            }
            else
            {
                model.IsAgreementAccepted = false;
            }

            if (addressschanged.ToUpper() == "FALSE")
            {
                model.IsAddressChange = false;
            }
            else
            {
                model.IsAddressChange = true;
            }
            if (string.IsNullOrEmpty(GroupTaskId))
            {
                if (ModelState.IsValid)
                {
                    OPAModel.ProjectActionName = projectActionFormManager.GetProjectActionName(model.ProjectActionTypeId);
                    OPAModel.ServicerComments = model.ServicerComments;
                    OPAModel.PropertyName = model.PropertyName;
                    OPAModel.RequesterName = model.RequesterName;
                    OPAModel.ProjectActionTypeId = model.ProjectActionTypeId;
                    //  var  FHA = Request.Form["FHA"];
                    OPAModel.IsAddressChange = model.IsAddressChange;
                    //model.FhaNumber = FHA;
                    //Insert the non critical request and get the generated id, insert into view model
                    OPAModel.FhaNumber = FHA;
                    OPAModel.ServicerSubmissionDate = DateTime.UtcNow;
                    OPAModel.RequestStatus = (int)RequestStatus.Submit;
                    OPAModel.CreatedOn = DateTime.UtcNow;
                    OPAModel.ModifiedOn = DateTime.UtcNow;
                    OPAModel.CreatedBy = UserPrincipal.Current.UserId;
                    OPAModel.ModifiedBy = UserPrincipal.Current.UserId;
                    OPAModel.ProjectActionDate = DateTime.UtcNow;
                    OPAModel.RequestDate = Convert.ToDateTime(requestDate);
                    var projectActionFormId = projectActionFormManager.SaveOPAForm(OPAModel);
                    OPAModel.ProjectActionFormId = projectActionFormId;
                    OPAModel.SequenceId = 0;
                    OPAModel.IsAgreementAccepted = model.IsAgreementAccepted;
                    OPAModel.RequesterName = model.RequesterName;
                    var result = projectActionFormManager.GetPropertyInfo(OPAModel.FhaNumber);
                    if (result != null)
                    {
                        OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                        OPAModel.PropertyAddress.City = result.City ?? "";
                        OPAModel.PropertyAddress.StateCode = result.State ?? "";
                        OPAModel.PropertyAddress.ZIP = result.Zipcode ?? "";

                    }
                    var taskList = new List<TaskModel>();
                    var task = new TaskModel();
                    var InstanceId = Guid.NewGuid();
                    task.AssignedTo = projectActionFormManager.GetAeEmailByFhaNumber(OPAModel.FhaNumber);
                    OPAModel.TaskInstanceId = InstanceId;
                    task.AssignedBy = UserPrincipal.Current.UserName;

                    task.Notes = ServicerComments ?? string.Empty;
                    task.SequenceId = 0;
                    task.StartTime = DateTime.UtcNow;
                    task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                    task.TaskInstanceId = InstanceId;

                    var tempmodel = new OPAViewModel();
                    if (model.GroupTaskId != null)
                    {
                        tempmodel =
                            groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(Convert.ToInt32(model.GroupTaskId));
                        task.TaskInstanceId = tempmodel.GroupTaskInstanceId.Value;
                        tempmodel.RequestStatus = (int)RequestStatus.Submit;
                        OPAModel.TaskInstanceId = tempmodel.GroupTaskInstanceId.Value;
                    }
                    task.DataStore1 = XmlHelper.Serialize(OPAModel, typeof(OPAViewModel), Encoding.Unicode);
                    //adding fhanumber and pagetype 
                    task.FHANumber = OPAModel.FhaNumber;
                    task.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");

                    task.TaskStepId = (int)TaskStep.ProjectActionRequest;
                    taskList.Add(task);
                    var taskFile = new TaskFileModel();
                    try
                    {
                        taskManager.SaveTask(taskList, taskFile);
                        //Latest task id been updated on submission
                        OPAModel.TaskInstanceId = task.TaskInstanceId;
                        projectActionFormManager.UpdateProjectActionRequestForm(OPAModel);
                        OPAModel.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                        projectActionFormManager.UpdateTaskId(OPAModel);

                        // Save ReviewFileStatus
                        string assignedto = task.AssignedTo;
                        int userId = webSecurity.GetUserId(assignedto);
                        Guid taskInstanceID = task.TaskInstanceId;
                        int reviewerUserId = userId;
                        int currentUserId = UserPrincipal.Current.UserId;
                        taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);

                        if (tempmodel != null)
                            groupTaskManager.UpdateOPAGroupTask(tempmodel);
                        // projectActionFormManager.InsertAEChecklistStatus(model);

                        if (OPAModel.ProjectActionName.Equals("TPA (Light) - Preliminary approval "))
                        {
                            backgroundJobManager.SendTPALightPreliminarySubmissionEmail(emailManager, task.AssignedTo,
                                accountManager.GetUserById(OPAModel.CreatedBy).UserName, OPAModel);
                        }
                        else
                        {

                            backgroundJobManager.SendOPASubmissionEmail(emailManager, task.AssignedTo,
                                accountManager.GetUserById(OPAModel.CreatedBy).UserName, OPAModel);
                        }

                        if (addressschanged.ToUpper() == "FALSE")
                        {
                            backgroundJobManager.SendUpdateiREMSNotificationEmail(emailManager,
                                "The property address is not listed correctly",
                                      OPAModel.PropertyName, OPAModel.FhaNumber);
                        }
                        UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                        string myTaskUrl = u.AbsoluteAction("MyTasks", "Task", null);
                        return RedirectToAction("MyTasks", "Task");
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            else
            {
                var IsViewFromGroupTask = Request.Form["IsViewFromGroupTask"];
                if (ModelState.IsValid || IsViewFromGroupTask.ToUpper().Contains("TRUE"))
                {

                    OPAModel =
                        groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(Convert.ToInt32(model.GroupTaskId));
                    OPAModel.ProjectActionName =
                        projectActionFormManager.GetProjectActionName(OPAModel.ProjectActionTypeId);
                    var taskFileModel =
                        taskManager.GetAllTaskFileByTaskInstanceId(new Guid(OPAModel.GroupTaskInstanceId.ToString()));
                    OPAModel.TaskFileList = taskFileModel.ToList();
                    OPAModel.ServicerComments = model.ServicerComments;
                    OPAModel.RequesterName = model.RequesterName;

                    OPAModel.IsAddressChange = model.IsAddressChange;
                    //  var  FHA = Request.Form["FHA"];

                    //model.FhaNumber = FHA;
                    //Insert the non critical request and get the generated id, insert into view model
                    OPAModel.ServicerSubmissionDate = DateTime.UtcNow;
                    OPAModel.RequestStatus = (int)RequestStatus.Submit;
                    OPAModel.CreatedOn = DateTime.UtcNow;
                    OPAModel.ModifiedOn = DateTime.UtcNow;
                    OPAModel.CreatedBy = UserPrincipal.Current.UserId;
                    OPAModel.ModifiedBy = UserPrincipal.Current.UserId;
                    OPAModel.ProjectActionDate = DateTime.UtcNow;
                    var projectActionFormId = projectActionFormManager.SaveOPAForm(OPAModel);
                    OPAModel.ProjectActionFormId = projectActionFormId;
                    groupTaskManager.UpdateOPAGroupTask(OPAModel);
                    var taskList = new List<TaskModel>();
                    var task = new TaskModel();
                    OPAModel.SequenceId = 0;
                    OPAModel.IsAgreementAccepted = model.IsAgreementAccepted;
                    var result = projectActionFormManager.GetPropertyInfo(OPAModel.FhaNumber);
                    if (result != null)
                    {
                        OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                        OPAModel.PropertyAddress.City = result.City ?? "";
                        OPAModel.PropertyAddress.StateCode = result.State ?? "";
                        OPAModel.PropertyAddress.ZIP = result.Zipcode ?? "";

                    }

                    task.AssignedTo = projectActionFormManager.GetAeEmailByFhaNumber(OPAModel.FhaNumber);
                    OPAModel.TaskInstanceId = OPAModel.GroupTaskInstanceId.Value;
                    task.AssignedBy = UserPrincipal.Current.UserName;
                    task.DataStore1 = XmlHelper.Serialize(OPAModel, typeof(OPAViewModel), Encoding.Unicode);
                    task.Notes = ServicerComments ?? string.Empty;
                    task.SequenceId = 0;
                    task.StartTime = DateTime.UtcNow;
                    task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                    task.TaskInstanceId = OPAModel.GroupTaskInstanceId.Value;
                    //adding fhanumber and pagetype 
                    task.FHANumber = OPAModel.FhaNumber;
                    task.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                    task.TaskStepId = (int)TaskStep.ProjectActionRequest;
                    taskList.Add(task);
                    var taskFile = new TaskFileModel();
                    try
                    {
                        taskManager.SaveTask(taskList, taskFile);
                        //Latest task id been updated on submission
                        OPAModel.TaskInstanceId = task.TaskInstanceId;
                        projectActionFormManager.UpdateProjectActionRequestForm(OPAModel);
                        OPAModel.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                        projectActionFormManager.UpdateTaskId(OPAModel);
                        //Review Filestatus Saving
                        string assignedto = task.AssignedTo;
                        int userId = webSecurity.GetUserId(assignedto);
                        Guid taskInstanceID = task.TaskInstanceId;
                        int reviewerUserId = userId;
                        int currentUserId = UserPrincipal.Current.UserId;
                        taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);
                        // projectActionFormManager.InsertAEChecklistStatus(model);
                        //*************** Place a condition here for the TPA (light)  ********************
                       
                        if (OPAModel.ProjectActionName.Equals("TPA (Light) - Preliminary approval "))
                        {
                            backgroundJobManager.SendTPALightPreliminarySubmissionEmail(emailManager, task.AssignedTo,
                                accountManager.GetUserById(OPAModel.CreatedBy).UserName, OPAModel);
                        }
                        else
                        {

                            backgroundJobManager.SendOPASubmissionEmail(emailManager, task.AssignedTo,
                                accountManager.GetUserById(OPAModel.CreatedBy).UserName, OPAModel);
                        }
                        //Send Mail on address Change
                        if (addressschanged.ToUpper() == "FALSE")
                        {
                            backgroundJobManager.SendUpdateiREMSNotificationEmail(emailManager,
                                "The property address is not listed correctly",
                                      OPAModel.PropertyName, OPAModel.FhaNumber);
                        }
                        UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                        string myTaskUrl = u.AbsoluteAction("MyTasks", "Task", null);
                        return RedirectToAction("MyTasks", "Task");
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            OPAModel.OpaHistoryViewModel = GetOpaHistory((Guid)OPAModel.GroupTaskInstanceId);
            OPAModel.IsAddressChange = model.IsAddressChange;
            return View("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);
        }

        [HttpPost]
        public ViewResult SubmitPopup()
        {
            TempData["RequesStatus"] = "Save";
            return View("~/Views/OPAForm/ConfirmPopup.cshtml");
        }

        private ActionResult Save(OPAViewModel OPAprojectActionViewModel)
        {
            string disclaimer = string.Empty;
            disclaimer = Request.Form["disclaimer"];
            // User Story 1901
            //if (disclaimer.ToUpper() == "TRUE")
            //{
            //    OPAprojectActionViewModel.IsAgreementAccepted = true;
            //}
            //else
            //{
            //    OPAprojectActionViewModel.IsAgreementAccepted = false;
            //}

            var addressschanged = string.Empty;
            addressschanged = Request.Form["addressschanged"];
            if (addressschanged.ToUpper() == "TRUE")
            {
                OPAprojectActionViewModel.IsAddressChange = true;
            }
            else
            {
                OPAprojectActionViewModel.IsAddressChange = false;
            }

            if (OPAprojectActionViewModel.GroupTaskId == null)
            {
                var FHA = Request.Form["FHA"];
                OPAprojectActionViewModel.FhaNumber = FHA;
                var groupTaskModel = new GroupTaskModel();
                groupTaskModel.TaskInstanceId = Guid.NewGuid();
                groupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                groupTaskModel.InUse = UserPrincipal.Current.UserId;
                groupTaskModel.FhaNumber = OPAprojectActionViewModel.FhaNumber;
                groupTaskModel.PropertyName = OPAprojectActionViewModel.PropertyName;
                groupTaskModel.RequestDate = OPAprojectActionViewModel.RequestDate;
                groupTaskModel.RequesterName = OPAprojectActionViewModel.RequesterName;
                groupTaskModel.ServicerSubmissionDate = OPAprojectActionViewModel.ServicerSubmissionDate;
                groupTaskModel.ProjectActionStartDate = DateTime.UtcNow;
                groupTaskModel.ProjectActionTypeId = OPAprojectActionViewModel.ProjectActionTypeId;
                groupTaskModel.IsDisclimerAccepted = OPAprojectActionViewModel.IsAgreementAccepted;
                groupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                groupTaskModel.CreatedOn = DateTime.UtcNow;
                groupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                groupTaskModel.ModifiedOn = DateTime.UtcNow;
                groupTaskModel.IsAddressChanged = OPAprojectActionViewModel.IsAddressChange;
                groupTaskModel.ServicerComments = OPAprojectActionViewModel.ServicerComments;
                OPAprojectActionViewModel.GroupTaskId = groupTaskManager.AddNewGroupTask(groupTaskModel);
                OPAprojectActionViewModel.IsEditMode = true;
                OPAprojectActionViewModel.RequestStatus = (int)RequestStatus.Draft;
                groupTaskModel.TaskId = (int)OPAprojectActionViewModel.GroupTaskId;
                OPAprojectActionViewModel.GroupTaskInstanceId = groupTaskModel.TaskInstanceId;
                OPAprojectActionViewModel.GroupTaskCreatedOn = DateTime.UtcNow;
                OPAprojectActionViewModel.ProjectActionDate = groupTaskModel.ProjectActionStartDate.Value;
            }
            else
            {
                UploadFile(OPAprojectActionViewModel);
                groupTaskManager.LockGroupTask((int)OPAprojectActionViewModel.GroupTaskId);
                OPAprojectActionViewModel.RequestStatus = (int)RequestStatus.Draft;
                groupTaskManager.UpdateOPAGroupTask(OPAprojectActionViewModel);
                //PopupHelper.ConfigPopup(TempData, 720, 350, "Save", false, false, true, false, false, true,
                //            "../OPAForm/ConfirmPopup");

                PopupHelper.ConfigPopup(TempData, 520, 200, "Disclaimer", false, false, true, false, false, true,
                               "../NonCriticalRequestExtension/ConfirmPopup");



            }

            //var OPAModel = new OPAProjectActionViewModel();

            //OPAModel = groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(Convert.ToInt32(OPAprojectActionViewModel.GroupTaskId));
            //OPAModel.ProjectActionName = projectActionFormManager.GetProjectActionName(OPAprojectActionViewModel.ProjectActionTypeId);
            //var taskFileModel = taskManager.GetAllTaskFileByTaskInstanceId(new Guid(OPAModel.GroupTaskInstanceId.ToString()));
            //OPAModel.TaskFileList = taskFileModel.ToList();

            //IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActions();
            //foreach (var model in projectActionList)
            //{
            //    OPAModel.ProjectActionTypeList.Add(new SelectListItem() { Text = model.ProjectActionName, Value = model.ProjectActionID.ToString() });
            //}


            return RedirectToAction("Index", "GroupTask");
            // return View("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);
        }

        private ActionResult SaveCheckin(OPAViewModel OPAprojectActionViewModel)
        {
            string disclaimer = string.Empty;
            disclaimer = Request.Form["disclaimer"];
            //User Story 1901
            //if (disclaimer.ToUpper() == "TRUE")
            //{
            //    OPAprojectActionViewModel.IsAgreementAccepted = true;
            //}
            //else
            //{
            //    OPAprojectActionViewModel.IsAgreementAccepted = false;
            //}


            var addressschanged = string.Empty;

            addressschanged = Request.Form["addressschanged"];

            if (addressschanged.ToUpper() == "TRUE")
            {
                OPAprojectActionViewModel.IsAddressChange = true;
            }
            else
            {
                OPAprojectActionViewModel.IsAddressChange = false;
            }

            if (OPAprojectActionViewModel.GroupTaskId == null)
            {
                var FHA = Request.Form["FHA"];

                OPAprojectActionViewModel.FhaNumber = FHA;
                var groupTaskModel = new GroupTaskModel();
                groupTaskModel.TaskInstanceId = Guid.NewGuid();
                groupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                groupTaskModel.InUse = 0;
                groupTaskModel.FhaNumber = OPAprojectActionViewModel.FhaNumber;
                groupTaskModel.PropertyName = OPAprojectActionViewModel.PropertyName;
                groupTaskModel.RequestDate = OPAprojectActionViewModel.RequestDate;
                groupTaskModel.RequesterName = OPAprojectActionViewModel.RequesterName;
                groupTaskModel.ServicerSubmissionDate = OPAprojectActionViewModel.ServicerSubmissionDate;
                groupTaskModel.ProjectActionStartDate = DateTime.UtcNow;
                groupTaskModel.ProjectActionTypeId = OPAprojectActionViewModel.ProjectActionTypeId;
                groupTaskModel.IsDisclimerAccepted = OPAprojectActionViewModel.IsAgreementAccepted;
                groupTaskModel.IsAddressChanged = OPAprojectActionViewModel.IsAddressChange;
                groupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                groupTaskModel.CreatedOn = DateTime.UtcNow;
                groupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                groupTaskModel.ModifiedOn = DateTime.UtcNow;
                groupTaskModel.ServicerComments = OPAprojectActionViewModel.ServicerComments;
                OPAprojectActionViewModel.GroupTaskId = groupTaskManager.AddNewGroupTask(groupTaskModel);
                OPAprojectActionViewModel.IsEditMode = true;
                OPAprojectActionViewModel.RequestStatus = (int)RequestStatus.Draft;
                groupTaskModel.TaskId = (int)OPAprojectActionViewModel.GroupTaskId;
                OPAprojectActionViewModel.GroupTaskInstanceId = groupTaskModel.TaskInstanceId;
                OPAprojectActionViewModel.GroupTaskCreatedOn = DateTime.UtcNow;
                OPAprojectActionViewModel.ProjectActionDate = groupTaskModel.ProjectActionStartDate.Value;
            }
            else
            {
                UploadFile(OPAprojectActionViewModel);
                groupTaskManager.UnlockGroupTask((int)OPAprojectActionViewModel.GroupTaskId);
                OPAprojectActionViewModel.RequestStatus = (int)RequestStatus.Draft;
                groupTaskManager.UpdateOPAGroupTask(OPAprojectActionViewModel);
                TempData["RequesStatus"] = "Checkin";
                PopupHelper.ConfigPopup(TempData, 520, 200, "Disclaimer", false, false, true, false, false, true,
                               "../NonCriticalRequestExtension/ConfirmPopup");
            }

            //var OPAModel = new OPAProjectActionViewModel();

            //OPAModel = groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(Convert.ToInt32(OPAprojectActionViewModel.GroupTaskId));
            //OPAModel.ProjectActionName = projectActionFormManager.GetProjectActionName(OPAprojectActionViewModel.ProjectActionTypeId);
            //var taskFileModel = taskManager.GetAllTaskFileByTaskInstanceId(new Guid(OPAModel.GroupTaskInstanceId.ToString()));
            //OPAModel.TaskFileList = taskFileModel.ToList();

            //IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActions();
            //foreach (var model in projectActionList)
            //{
            //    OPAModel.ProjectActionTypeList.Add(new SelectListItem() { Text = model.ProjectActionName, Value = model.ProjectActionID.ToString() });
            //}

            //return View("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);

            return RedirectToAction("Index", "GroupTask");
        }

        private void InitializeViewModel(OPAViewModel OPAprojectActionViewModel)
        {
            ViewBag.SaveCheckListFormURL = Url.Action("ApproveCheckListForm");
            PopulateFHANumberList(OPAprojectActionViewModel);
            OPAprojectActionViewModel.IsEditMode = false;
            IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(3).OrderBy(a => a.ProjectActionName).ToList();
            foreach (var model in projectActionList)
            {
                if (model.ProjectActionName != "Other")
                {
                    OPAprojectActionViewModel.ProjectActionTypeList.Add(new SelectListItem()
                    {
                        Text = model.ProjectActionName,
                        Value = model.ProjectActionID.ToString()
                    });
                }
            }
            foreach (var model in projectActionList)
            {
                if (model.ProjectActionName == "Other")
                {
                    OPAprojectActionViewModel.ProjectActionTypeList.Add(new SelectListItem()
                    {
                        Text = model.ProjectActionName,
                        Value = model.ProjectActionID.ToString()
                    });
                }
            }
            OPAprojectActionViewModel.LenderId = UserPrincipal.Current.UserData.LenderId.Value;
            OPAprojectActionViewModel.LenderName =
                projectActionFormManager.GetLenderName(UserPrincipal.Current.UserData.LenderId.Value);
            OPAprojectActionViewModel.RequesterName = UserPrincipal.Current.FullName;
            OPAprojectActionViewModel.ProjectActionDate = DateTime.Today;
            OPAprojectActionViewModel.ServicerSubmissionDate = null;
            OPAprojectActionViewModel.IsAgreementAccepted = false;
            OPAprojectActionViewModel.RequestDate = DateTime.UtcNow;
            OPAprojectActionViewModel.CreatedBy = UserPrincipal.Current.UserId;
            OPAprojectActionViewModel.CreatedOn = DateTime.UtcNow;
            OPAprojectActionViewModel.ModifiedOn = DateTime.UtcNow;
            OPAprojectActionViewModel.ModifiedBy = UserPrincipal.Current.UserId;
            IList<TaskFileModel> TasFilelist = new List<TaskFileModel>();
            OPAprojectActionViewModel.TaskFileList = TasFilelist;
            OPAprojectActionViewModel.IsViewFromGroupTask = false;
            List<OPAHistoryViewModel> History = new List<OPAHistoryViewModel>();
            OPAprojectActionViewModel.OpaHistoryViewModel = History;
            // User Story 1901
            GetDisclaimerText(OPAprojectActionViewModel);

        }

        private void PopulateFHANumberList(OPAViewModel OPAProjectActionViewModel)
        {
            var fhaNumberList = new List<string>();
            if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                fhaNumberList =
                    projectActionFormManager.GetAllowedFhaByLenderUserId(UserPrincipal.Current.UserId);
            }

            OPAProjectActionViewModel.AvailableFHANumbersList = fhaNumberList;
        }

        public JsonResult GetPropertyInfo(string selectedFhaNumber)
        {
            var result = projectActionFormManager.GetPropertyInfo(selectedFhaNumber);
            JsonResult json = null;
            if (result != null)
            {
                json = Json(result, JsonRequestBehavior.AllowGet);
            }
            return json;
        }

        //public ContactAddressViewModel GetContactAddressInfo(OPAProjectActionViewModel OPAProjectActionViewModel)
        //{
        //    return projectActionFormManager.GetContactAddressInfo(OPAProjectActionViewModel).ContactAddressViewModel;
        //}

        public string CheckIfProjectActionExistsForFhaNumber(int projectActionTypeId, string fhaNumber)
        {
            var result = groupTaskManager.GroupTaskbyFHAProjectAction(projectActionTypeId, fhaNumber);
            if (result != null)
            {
                return result.RequestStatus.ToString();
            }
            else
            {
                var OpaResult = projectActionFormManager.OPATaskbyFHAProjectAction(projectActionTypeId, fhaNumber);
                if (OpaResult != null)
                {
                    return OpaResult.RequestStatus.ToString();
                }
                else
                {
                    return "NA";
                }
            }
        }

        //[HttpPost]
        public virtual ActionResult UploadFile(OPAViewModel OPAProjectActionViewModel)
        {

            string disclaimer = string.Empty;
            disclaimer = Request.Form["disclaimer"];

            if (disclaimer.ToUpper() == "TRUE")
            {
                OPAProjectActionViewModel.IsAgreementAccepted = true;
            }
            else
            {
                OPAProjectActionViewModel.IsAgreementAccepted = false;
            }

            var addressschanged = string.Empty;

            addressschanged = Request.Form["addressschanged"];


            bool isUploaded = false;
            string message = "File upload failed";
            string groupTaskInstanceId = string.Empty;

            string FHA = string.Empty;
            string PropertyName = string.Empty;
            string requestDate = string.Empty;
            string RequesterName = string.Empty;
            string ServicerSubmissionDate = string.Empty;
            string GroupTaskId = string.Empty;
            string Mode = string.Empty;
            string ProjectActionTypeId = string.Empty;
            FHA = Request.Form["FHA"];
            PropertyName = Request.Form["PropertyName"];
            requestDate = Request.Form["requestDate"];
            RequesterName = Request.Form["RequesterName"];
            ServicerSubmissionDate = Request.Form["ServicerSubmissionDate"];
            GroupTaskId = Request.Form["GroupTaskId"];
            ProjectActionTypeId = Request.Form["ProjectActionTypeId"];
            var OPAModel = new OPAViewModel();
            string IsViewFromGroupTask = string.Empty;
            IsViewFromGroupTask = Request.Form["IsViewFromGroupTask"];



            //Populate OPAProjectActionViewModel

            OPAProjectActionViewModel.FhaNumber = FHA;
            //if (!string.IsNullOrEmpty(ProjectActionTypeId))
            //{
            //    OPAProjectActionViewModel.ProjectActionTypeId = Convert.ToInt32(ProjectActionTypeId);
            //}

            //OPAProjectActionViewModel.PropertyName = PropertyName;
            //OPAProjectActionViewModel.RequestDate = Convert.ToDateTime(requestDate);
            //OPAProjectActionViewModel.ServicerSubmissionDate = Convert.ToDateTime(ServicerSubmissionDate);
            //if(!string.IsNullOrEmpty(GroupTaskId))
            //{
            //    OPAProjectActionViewModel.GroupTaskId =  Convert.ToInt32(GroupTaskId);
            //}
            string data = Request.QueryString[0];
            string taskid = string.Empty;
            //taskid
            var arr = data.Split(',');
            if (arr.Length > 1)
            {
                taskid = arr[0];
            }
            else
            {
                taskid = data;
            }
            if (!string.IsNullOrEmpty(taskid))
            {
                OPAProjectActionViewModel.GroupTaskId = Convert.ToInt32(taskid);
            }

            if (ModelState.IsValid)
            {
                var groupTaskModel = new GroupTaskModel();
                if (OPAProjectActionViewModel != null)
                {

                    if (OPAProjectActionViewModel.GroupTaskId == null)
                    {
                        groupTaskModel.TaskInstanceId = Guid.NewGuid();
                        groupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                        groupTaskModel.InUse = UserPrincipal.Current.UserId;
                        groupTaskModel.FhaNumber = OPAProjectActionViewModel.FhaNumber;
                        groupTaskModel.PropertyName = OPAProjectActionViewModel.PropertyName;
                        groupTaskModel.RequestDate = OPAProjectActionViewModel.RequestDate;
                        groupTaskModel.RequesterName = OPAProjectActionViewModel.RequesterName;
                        groupTaskModel.ServicerSubmissionDate = OPAProjectActionViewModel.ServicerSubmissionDate;
                        groupTaskModel.ProjectActionStartDate = DateTime.UtcNow;
                        groupTaskModel.ProjectActionTypeId = OPAProjectActionViewModel.ProjectActionTypeId;
                        groupTaskModel.IsDisclimerAccepted = OPAProjectActionViewModel.IsAgreementAccepted;
                        groupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.CreatedOn = DateTime.UtcNow;
                        groupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.ModifiedOn = DateTime.UtcNow;
                        groupTaskModel.ServicerComments = OPAProjectActionViewModel.ServicerComments;
                        OPAProjectActionViewModel.GroupTaskId = groupTaskManager.AddNewGroupTask(groupTaskModel);
                        OPAProjectActionViewModel.IsEditMode = true;
                        OPAProjectActionViewModel.RequestStatus = (int)RequestStatus.Draft;
                        groupTaskModel.TaskId = (int)OPAProjectActionViewModel.GroupTaskId;
                        OPAProjectActionViewModel.GroupTaskInstanceId = groupTaskModel.TaskInstanceId;
                        OPAProjectActionViewModel.GroupTaskCreatedOn = DateTime.UtcNow;
                        OPAProjectActionViewModel.ProjectActionDate = groupTaskModel.ProjectActionStartDate.Value;
                    }
                    else
                    {
                        OPAModel =
                            groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(
                                Convert.ToInt32(OPAProjectActionViewModel.GroupTaskId));
                        OPAModel.ServicerComments = OPAProjectActionViewModel.ServicerComments;
                        OPAModel.ServicerSubmissionDate = OPAProjectActionViewModel.ServicerSubmissionDate;
                        OPAModel.PropertyId = OPAProjectActionViewModel.PropertyId;
                        groupTaskManager.UpdateOPAGroupTask(OPAModel);

                        OPAProjectActionViewModel.GroupTaskInstanceId = OPAModel.GroupTaskInstanceId;
                        // OPAProjectActionViewModel.GroupTaskInstanceId =  new Guid(Request.QueryString[0]);
                        //OPAProjectActionViewModel.GroupTaskCreatedOn = DateTime.UtcNow;
                    }
                    OPAProjectActionViewModel.ProjectActionName =
                        projectActionFormManager.GetProjectActionName(OPAProjectActionViewModel.ProjectActionTypeId);
                }
                //return View("~/Views/ProjectAction/ProjectActionUploadCheckList.cshtml", OPAProjectActionViewModel);
            }

            //From Group task

            if (IsViewFromGroupTask.ToUpper().Contains("TRUE"))
            {
                OPAModel =
                    groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(
                        Convert.ToInt32(OPAProjectActionViewModel.GroupTaskId));
                OPAModel.ServicerComments = OPAProjectActionViewModel.ServicerComments;
                OPAModel.ServicerSubmissionDate = OPAProjectActionViewModel.ServicerSubmissionDate;

                groupTaskManager.UpdateOPAGroupTask(OPAModel);

                OPAProjectActionViewModel.ProjectActionName =
                    projectActionFormManager.GetProjectActionName(OPAModel.ProjectActionTypeId);

                OPAProjectActionViewModel.ProjectActionTypeId = OPAModel.ProjectActionTypeId;
                OPAProjectActionViewModel.GroupTaskInstanceId = OPAModel.GroupTaskInstanceId;
                OPAModel.IsAgreementAccepted = false; // User Story 1901

            }
            else
            {
                OPAModel.IsAgreementAccepted = true; // User Story 1901
            }
            Random randoms = new Random();
            foreach (string Filename in Request.Files)
            {
                HttpPostedFileBase myFile = Request.Files[Filename];
                if (myFile != null && myFile.ContentLength != 0)
                {
                    double fileSize = (myFile.ContentLength) / 1024;


                    var UniqueId = randoms.Next(0, 99999);
                    // Debug.WriteLine("My debug string here" + UniqueId);

                    var systemFileName = OPAProjectActionViewModel.FhaNumber + "_" +
                                        OPAProjectActionViewModel.ProjectActionTypeId + "_" +
                                        OPAProjectActionViewModel.GroupTaskInstanceId + "_" + UniqueId +
                                        Path.GetExtension(myFile.FileName);
                    var tModel = taskManager.GetTaskFileByTaskFileName(systemFileName.ToString());
                    var taskModel = taskManager.GetTaskFileByTaskFileType(systemFileName.ToString());
                    while (taskModel != null || tModel != null)
                    {
                        UniqueId = randoms.Next(0, 99999);
                        taskModel = taskManager.GetTaskFileByTaskFileType(UniqueId.ToString());
                        //OPAProjectActionViewModel.GroupTaskInstanceId = Guid.NewGuid();

                    }




                    string pathForSaving = Server.MapPath("~/Uploads");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {
                            // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            isUploaded = true;
                            message = "File uploaded successfully!";
                        }
                        catch (Exception ex)
                        {
                            message = string.Format("File upload failed: {0}", ex.Message);
                        }
                    }
                    var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(),
                        DateTime.UtcNow.ToString(), systemFileName.ToString(),
                        new Guid(OPAProjectActionViewModel.GroupTaskInstanceId.ToString()));

                    //var taskFileModel = taskManager.GetGroupTaskFileByTaskInstanceAndFileTypeId(new Guid(groupTaskInstanceId), new Guid(chkListId));
                    //if (taskFileModel != null)
                    //{
                    //    taskManager.DeleteGroupTaskFile(new Guid(groupTaskInstanceId), new Guid(chkListId));
                    //}

                    taskManager.SaveGroupTaskFileModel(taskFile);

                    OPAModel =
                        groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(
                            Convert.ToInt32(OPAProjectActionViewModel.GroupTaskId));
                    OPAModel.ProjectActionName =
                        projectActionFormManager.GetProjectActionName(OPAProjectActionViewModel.ProjectActionTypeId);
                    var taskFileModel =
                        taskManager.GetAllTaskFileByTaskInstanceId(
                            new Guid(OPAProjectActionViewModel.GroupTaskInstanceId.ToString()));
                    OPAModel.TaskFileList = taskFileModel.ToList();

                    IList<ProjectActionTypeViewModel> projectActionList =
                        projectActionManager.GetAllProjectActionsByPageId(3);
                    foreach (var model in projectActionList)
                    {
                        OPAModel.ProjectActionTypeList.Add(new SelectListItem()
                        {
                            Text = model.ProjectActionName,
                            Value = model.ProjectActionID.ToString()
                        });
                    }

                }
            }

            var result = projectActionFormManager.GetPropertyInfo(OPAModel.FhaNumber);
            if (result != null)
            {
                OPAModel.PropertyId = result.PropertyId;
                OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                OPAModel.PropertyAddress.City = result.City ?? "";
                OPAModel.PropertyAddress.StateCode = result.State ?? "";
                OPAModel.PropertyAddress.ZIP = result.Zipcode ?? "";
            }

            OPAModel.OpaHistoryViewModel = GetOpaHistory((Guid)OPAModel.GroupTaskInstanceId);


            if (addressschanged.ToUpper() == "TRUE")
            {
                OPAModel.IsAddressChange = true;
            }
            else
            {
                OPAModel.IsAddressChange = false;
            }
            GetDisclaimerText(OPAModel); // User Story 1901
            if (IsViewFromGroupTask.ToUpper().Contains("TRUE"))
            {
                OPAModel.IsViewFromGroupTask = true;

                return PartialView("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", OPAModel);
            }
            OPAModel.IsAgreementAccepted = false; // User Story 1901
            return PartialView("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);

        }

        public JsonResult GetOpaHistoryGridContent(string sidx, string sord, int page, int rows, Guid taskInstanceId)
        {

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            // List<OPAHistoryViewModel> GridContent = new List<OPAHistoryViewModel>();

            var GridContent = GetOpaHistory(taskInstanceId);
            ////if (taskInstanceId == null)
            ////{ return Json(""); }
            ////var GridContent = GetOpaHistory(taskInstanceId.Value);


            //Setting the date based on the Time Zone
            if (GridContent != null)
            {
                foreach (var item in GridContent)
                {
                    if (item.uploadDate != null)
                    {
                        item.uploadDate = TimezoneManager.GetPreferredTimeFromUtc((DateTime)item.uploadDate);
                    }
                    if (item.submitDate != null)
                    {
                        item.submitDate = TimezoneManager.GetPreferredTimeFromUtc((DateTime)item.submitDate);
                    }

                }

            }

            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);


            var result = GridContent.ToList().OrderBy(s => s.fileId);
            var results = result.Skip(pageIndex * pageSize).Take(pageSize);
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public string OPAuploadOpaFiles(OPAViewModel oPAWorkProductModel)
        {
            string data = Request.QueryString[0];
            string taskid = string.Empty;

            string pathForSaving = Server.MapPath("~/Uploads");
            foreach (string Filename in Request.Files)
            {
                HttpPostedFileBase myFile = Request.Files[Filename];
                //string fileName = Path.GetFileName(myFile.FileName);
                double fileSize = (myFile.ContentLength) / 1024;
                if (myFile != null && myFile.ContentLength != 0)
                {
                    Random randoms = new Random();
                    var UniqueRandomId = randoms.Next(0, 99999);
                    string UniqueId;
                    if (oPAWorkProductModel.ButtonName.Equals("WP"))
                    {
                        UniqueId = "WP";
                    }
                    else
                    {
                        UniqueId = "FWP";
                    }

                    var systemFileName = UniqueId + "_" + Filename + "_" + UniqueRandomId + Path.GetExtension(myFile.FileName);

                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {
                            // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                        }
                        catch (Exception ex)
                        {
                            //message = string.Format("File upload failed: {0}", ex.Message);
                        }
                    }
                    var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(), DateTime.UtcNow.ToString(), systemFileName.ToString(), new Guid(oPAWorkProductModel.TaskInstanceId.ToString()));
                    taskManager.SaveGroupTaskFileModel(taskFile);

                }
            }
            if (oPAWorkProductModel.ButtonName.Equals("WP"))
            {
                return "Success";
            }
            else
            {
                return "Success FWP";
            }
        }


        // To upload the AE files to the TaskTable
        public string uploadOpaFiles(OPAViewModel oPAWorkProductModel)

        {
            OPAViewModel Model = projectActionFormManager.GetOPAByTaskInstanceId(oPAWorkProductModel.TaskInstanceId);
            Model.pageTypeId = oPAWorkProductModel.pageTypeId;
            var PageName = projectActionFormManager.FormNameBYPageTypeID(oPAWorkProductModel.pageTypeId);
            var result = projectActionFormManager.GetProdPropertyInfo(Model.FhaNumber);
            if (result != null)
            {
                oPAWorkProductModel.PropertyId = result.PropertyId;
                oPAWorkProductModel.FhaNumber = Model.FhaNumber;
            }

            string data = Request.QueryString[0];
            string taskid = string.Empty;
            var uploadmodel = new RestfulWebApiUploadModel()
            {
                propertyID = oPAWorkProductModel.PropertyId.ToString(),
                indexType = "1",
                indexValue = oPAWorkProductModel.FhaNumber,
                pdfConvertableValue = "false",
            };
            var WebApiUploadResult = new RestfulWebApiResultModel();
            var request = new RestfulWebApiTokenResultModel();
            request = WebApiTokenRequest.RequestToken();
            bool uploadStatus = true;

            string pathForSaving = Server.MapPath("~/Uploads");
            foreach (string Filename in Request.Files)
            {

                HttpPostedFileBase myFile = Request.Files[Filename];
                //string fileName = Path.GetFileName(myFile.FileName);
                double fileSize = (myFile.ContentLength) / 1024;
                if (myFile != null && myFile.ContentLength != 0)
                {
                    Random randoms = new Random();
                    var UniqueRandomId = randoms.Next(0, 99999);
                    string UniqueId;
                    if (oPAWorkProductModel.ButtonName.Equals("WP"))
                    {
                        UniqueId = "WP";
                    }
                    else
                    {
                        UniqueId = "FWP";
                        uploadmodel.folderNames = "Production/" + PageName+"/FirmCommitment";
                    }

                    var systemFileName = UniqueId + "_" + Filename + "_" + UniqueRandomId + Path.GetExtension(myFile.FileName);
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {
                            // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            //isUploaded = true;
                            //message = "File uploaded successfully!";
                        }
                        catch (Exception ex)
                        {
                            //message = string.Format("File upload failed: {0}", ex.Message);
                        }
                    }

                    WebApiUploadResult = WebApiDocumentUpload.UploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "OPA");
                    System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                    if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
                    {

                        var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(), DateTime.UtcNow.ToString(), systemFileName.ToString(), new Guid(oPAWorkProductModel.TaskInstanceId.ToString()));
                        taskFile.DocId = WebApiUploadResult.docId;
                        taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
                        taskFile.API_upload_status = WebApiUploadResult.status;
                        taskFile.DocTypeID = WebApiUploadResult.documentType;
                        taskManager.SaveGroupTaskFileModel(taskFile);
                    }
                    else
                    {
                        Exception ex = new Exception(WebApiUploadResult.message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                        uploadStatus = false;
                    }



                }
            }
            //List<OPAWorkProductModel> opaWorkProduct = new List<OPAWorkProductModel>();

            //opaWorkProduct = iOPAForm.getAllOPAWorkProducts(oPAWorkProductModel.TaskInstanceId);
            if (uploadStatus)
            {
                if (oPAWorkProductModel.ButtonName.Equals("WP"))
                {
                    return "Success";
                }
                else
                {
                    return "Success FWP";
                }
            }
            else
            {
                return "failed";
            }

        }

        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        [HttpGet]
        [MvcSiteMapNode(Title = "Task Detail", ParentKey = "TaskId")]
        [SiteMapTitle("TaskName", Target = AttributeTarget.CurrentNode)]
        public ActionResult GetOPAFormDetail(OPAViewModel model)
        {
            if (model == null)
            {

                model = (OPAViewModel)TempData.Peek("OPAFormData");
            }
            bool hasTaskOpenByUser = TempData["hasTaskOpenByUser"] != null
                ? (bool)TempData["hasTaskOpenByUser"]
                : false;
            ControllerHelper.PopulateParentNodeRouteValuesForPageSort(
                    SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
            ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
            model.ProjectActionName = projectActionFormManager.GetProjectActionName(model.ProjectActionTypeId);
            var propertyInfoModel = projectActionFormManager.GetPropertyInfo(model.FhaNumber);
            if (propertyInfoModel != null)
            {
                model.PropertyId = propertyInfoModel.PropertyId;
                model.PropertyAddress.AddressLine1 = propertyInfoModel.StreetAddress;
                model.PropertyAddress.City = propertyInfoModel.City;
                model.PropertyAddress.StateCode = propertyInfoModel.State;
                model.PropertyAddress.ZIP = propertyInfoModel.Zipcode;
            }
            model.IsRAI = false;
            if ((model.RequestStatus == 2 || model.RequestStatus == 5) && !hasTaskOpenByUser &&
                RoleManager.IsUserLenderRole(model.AssignedTo) && !model.IsPAMReport &&
                RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                var task = new TaskModel();
                var taskList = new List<TaskModel>();
                task.AssignedBy = model.AssignedBy;
                task.AssignedTo = model.AssignedTo;
                task.StartTime = DateTime.UtcNow;
                task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.TaskInstanceId = model.TaskGuid ?? Guid.NewGuid();
                task.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                task.TaskOpenStatus = XmlHelper.Serialize(model.TaskOpenStatus, typeof(TaskOpenStatusModel),
                    Encoding.Unicode);
                task.IsReAssigned = model.IsReassigned;
                task.SequenceId = model.SequenceId.HasValue ? model.SequenceId.Value + 1 : 0;
                //adding fhanumber and pagetype 
                task.FHANumber = model.FhaNumber;
                task.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                var taskFile = new TaskFileModel();
                try
                {
                    task.DataStore1 = XmlHelper.Serialize(model, typeof(OPAViewModel), Encoding.Unicode);
                    taskList.Add(task);
                    taskManager.SaveTask(taskList, taskFile);
                    if (!parentChildTaskManager.IsParentTaskAvailable(task.TaskInstanceId))
                    {
                        model.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                        projectActionFormManager.UpdateTaskId(model);
                    }

                }
                catch (Exception e)
                {
                    ErrorSignal.FromCurrentContext().Raise(e);
                    throw new InvalidOperationException(
                        string.Format("Error project action request form detail: {0}", e.InnerException),
                        e.InnerException);
                }
            }
            model.hasReviewComments = true;
            model.userName = UserPrincipal.Current.FullName;
            model.userRole = UserPrincipal.Current.UserRole;
            var isChild = false;
            if (parentChildTaskManager.IsParentTaskAvailable(model.ChildTaskInstanceId))
            {
                isChild = true;
                model.OpaHistoryViewModel =
                    projectActionFormManager.GetOpaHistoryForRequestAdditionalInfo(model.ChildTaskInstanceId);
            }
            else
            {
                model.OpaHistoryViewModel = GetOpaHistory((Guid)model.TaskGuid);
            }



            //Setting the date based on the Time Zone
            if (model.OpaHistoryViewModel != null)
            {
                foreach (var item in model.OpaHistoryViewModel)
                {
                    item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                }

            }

            model.GridVisibility = "ShowForLender";
            model.IsAgreementAccepted = true; // User Story 1901
            GetDisclaimerText(model); // User Story 1901          
            if (model.RequestStatus == 1 && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName) &&
                model.TaskStepId == 16)
            {
                model.IsAgreementAccepted = false; // User Story 1901
                return View("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", model);
            }
            else if (model.RequestStatus == 1 && RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName) &&
               model.TaskStepId == 16)
            {
                return View("~/Views/OPAForm/OPARequestAdditionalAEandReadOnly.cshtml", model);
            }
            else if (model.RequestStatus == 1 && model.TaskStepId == 15)
            {
                model.IsAgreementAccepted = true;

                return View("~/Views/OPAForm/OPARequestAdditionalAEandReadOnly.cshtml", model);
            }
            else if (model.RequestStatus == 1 && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                model.GridVisibility = string.Empty;
                return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", model);
            }

            else if (model.RequestStatus == 2 && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                if (isChild)
                {
                    return View("~/Views/OPAForm/OPARequestAdditionalAEandReadOnly.cshtml", model);
                }
                else
                {
                    return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", model);
                }
            }

            else if (model.RequestStatus == 5 && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", model);
            }

            else if (RoleManager.IsReviewer(UserPrincipal.Current.UserName))
            {
                return View("~/Views/OPAForm/OPARequestReviewerandReadOnly.cshtml", model);
            }
            else
            {
                if (isChild)
                {

                    return View("~/Views/OPAForm/OPARequestAdditionalAEandReadOnly.cshtml", model);
                }
                else
                {
                    model.ShowVisibility = true;
                    return View("~/Views/OPAForm/OPARequestAEandReadOnly.cshtml", model);
                }
            }
        }

        public ActionResult ApproveOrDeny(OPAViewModel model, Guid TaskInstanceId, string showPopup, string TaskGuid, int requestStatus, string SequenceId,
                                                              string Concurrency, string AssignedBy, string FHANumber, Guid ProjectActionFormId, int ProjectActionTypeId,
                                                              string IschildTask, string showcomments, int PropertyId, bool isAddressChange, string propertyName, string requesterName,
                                                              DateTime servicerSubmissionDate, string projectActionName, bool isEditMode, string AEComments, string StreetAddress,
                                                              string City, string StateCode, string ZIP)
        {

            bool isChildApproved = true;
            model.ShowVisibility = true;

            if (ModelState.IsValid)
            {
                List<TaskModel> childTaskList = projectActionFormManager.GetChildTasks(model.TaskInstanceId);


                if (showPopup != "No" && model.DenyStatus != "Yes" && model.DenyStatus == null)
                {

                    foreach (var childList in childTaskList)
                    {

                        if (childList.TaskStepId == 16)
                        {
                            isChildApproved = false;
                            break;
                        }

                    }

                    if (isChildApproved == false)
                    {
                        model.IsApprovedPopupDisplayedForAe = true;
                        model.ShowVisibility = false;
                        if (showPopup != "No")
                        {
                            PopupHelper.ConfigPopup(TempData, 350, 450, "Confirmation", false, false, true, false, false, true,
                                "../OPAForm/ApprovePopupForOPA");
                        }
                        return View("~/Views/OPAForm/OPARequestAEandReadOnly.cshtml", model);
                    }
                }


                if (model.RequestStatus != 5)
                {
                    model.RequestStatus = 2;
                }

                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;

                var taskList = new List<TaskModel>();
                var parentTask = new TaskModel();
                string assignedBy;
                string assignedTo;
                string alertTitle = String.Empty;
                var requestAdditionalInfoFilesList = new List<RequestAdditionalInfoFileModel>();
                model.GroupTaskInstanceId = model.TaskGuid;
                TempData["IsReAssigned"] = model.IsReassigned.HasValue && model.IsReassigned.Value == true
                    ? "true"
                    : "false";
                parentTask.DataStore1 = XmlHelper.Serialize(model, typeof(OPAViewModel), Encoding.Unicode);
                if (showPopup == "No")
                {
                    model.RequestStatus = 2;
                    model.FhaNumber = FHANumber;
                    model.ProjectActionFormId = ProjectActionFormId;
                    model.ProjectActionTypeId = ProjectActionTypeId;
                }
                if (model.RequestStatus == (int)RequestStatus.RequestAdditionalInfo)
                {
                    TempData["PopupText"] = "Additional Request is Sent.";
                    alertTitle = "Additional Request Sent";
                    var childTask = new TaskModel();
                    childTask.TaskInstanceId = Guid.NewGuid();
                    childTask.SequenceId = 0;
                    childTask.AssignedBy = UserPrincipal.Current.UserName;
                    childTask.AssignedTo = model.AssignedBy;
                    childTask.StartTime = DateTime.UtcNow;
                    childTask.TaskStepId = (int)TaskStep.OPAAddtionalInformation;
                    childTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(childTask.StartTime);
                    childTask.IsReAssigned = false;
                    childTask.Concurrency = model.Concurrency;
                    //adding fhanumber and pagetype      
                    childTask.FHANumber = model.FhaNumber;
                    childTask.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                    var childTaskOpenStatusModel = new TaskOpenStatusModel();
                    childTaskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                    childTask.TaskOpenStatus = XmlHelper.Serialize(childTaskOpenStatusModel,
                        typeof(TaskOpenStatusModel),
                        Encoding.Unicode);
                    taskList.Add(childTask);
                    model.RequestAdditionalChildTask.CreatedBy = UserPrincipal.Current.UserId;
                    model.RequestAdditionalChildTask.CreatedOn = DateTime.UtcNow;
                    model.RequestAdditionalChildTask.ParentTaskInstanceId = (Guid)model.TaskGuid;
                    model.RequestAdditionalChildTask.ChildTaskInstanceId = childTask.TaskInstanceId;
                    model.RequestAdditionalChildTask.ParentChildTaskId = Guid.NewGuid();
                    childTask.Notes = model.RequestAdditionalComment;
                    assignedBy = childTask.AssignedBy;
                    assignedTo = childTask.AssignedTo;
                    var taskFileIdList =
                        reviewFileStatusManager.GetRequestAdditionalFileIdByParentTaskInstanceId((Guid)model.TaskGuid);
                    foreach (var item in taskFileIdList)
                    {
                        var requestAdditionalInfoFiles = new RequestAdditionalInfoFileModel();
                        requestAdditionalInfoFiles.ChildTaskInstanceId = childTask.TaskInstanceId;
                        requestAdditionalInfoFiles.RequestAdditionalFileId = Guid.NewGuid();
                        requestAdditionalInfoFiles.TaskFileId = item;
                        requestAdditionalInfoFilesList.Add(requestAdditionalInfoFiles);
                    }

                    model.IsRAI = true;
                }
                else
                {
                    if (model.RequestStatus == (int)RequestStatus.Approve)
                    {
                        TempData["PopupText"] = "Project Action Request was approved.";
                        alertTitle = "Project Action Approved";
                        parentTask.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                    }
                    else
                    {
                        TempData["PopupText"] = "Project Action Request was denied.";
                        alertTitle = "Project Action Denied";
                        parentTask.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                    }
                    model.SequenceId++;
                    parentTask.SequenceId = (int)model.SequenceId;
                    parentTask.StartTime = DateTime.UtcNow;
                    parentTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(parentTask.StartTime);
                    parentTask.TaskInstanceId = model.TaskInstanceId;
                    parentTask.IsReAssigned = model.IsReassigned;
                    parentTask.Concurrency = model.Concurrency;
                    parentTask.AssignedTo = model.AssignedBy;
                    parentTask.AssignedBy = UserPrincipal.Current.UserName;
                    parentTask.Notes = model.AEComments;
                    //adding fhanumber and pagetype 
                    parentTask.FHANumber = model.FhaNumber;
                    parentTask.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                    var taskOpenStatusModel = new TaskOpenStatusModel();
                    taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                    parentTask.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel),
                        Encoding.Unicode);
                    taskList.Add(parentTask);
                    assignedBy = parentTask.AssignedBy;
                    assignedTo = parentTask.AssignedTo;
                }
                //model.Concurrency = concurrency;
                if (taskList != null && taskList.Any() && model.IsRAI)
                {
                    taskList[0].DataStore1 = parentTask.DataStore1;
                }

                if (childTaskList != null && childTaskList.Count > 0 && !model.IsRAI)
                {
                    var childTask = new TaskModel();
                    var childtaskOpenStatusModel = new TaskOpenStatusModel();

                    foreach (TaskModel childTaskListItem in childTaskList)
                    {
                        childTask = new TaskModel();

                        childTask.TaskInstanceId = childTaskListItem.TaskInstanceId;
                        childTaskListItem.SequenceId++;
                        childTask.SequenceId = (int)childTaskListItem.SequenceId;
                        childTask.AssignedBy = UserPrincipal.Current.UserName;
                        childTask.AssignedTo = model.AssignedBy;
                        childTask.StartTime = DateTime.UtcNow;
                        childTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(childTask.StartTime);
                        childTask.Notes = model.AEComments;
                        childTask.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                        childTask.DataStore1 = XmlHelper.Serialize(model, typeof(OPAViewModel), Encoding.Unicode);//
                        childtaskOpenStatusModel = new TaskOpenStatusModel();
                        childtaskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                        childTask.TaskOpenStatus = XmlHelper.Serialize(childtaskOpenStatusModel, typeof(TaskOpenStatusModel), Encoding.Unicode);
                        childTask.IsReAssigned = model.IsReassigned;
                        childTask.Concurrency = model.Concurrency;
                        childTask.FHANumber = model.FhaNumber;
                        childTask.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");

                        taskList.Add(childTask);
                    }
                }

                var taskFile = new List<TaskFileModel>();
                try
                {
                    taskManager.SaveTask(taskList, taskFile);
                    if (model.IsRAI)
                    {
                        parentChildTaskManager.AddParentChildTask(model.RequestAdditionalChildTask);
                        requestAdditionalInfoFilesManager.SaveRequestAdditionalInfoFiles(requestAdditionalInfoFilesList);
                    }
                    else
                    {
                        model.MytaskId = projectActionFormManager.GetLatestTaskId(parentTask.TaskInstanceId).TaskId;
                        projectActionFormManager.UpdateTaskId(model);
                        projectActionFormManager.UpdateProjectActionRequestForm(model);
                        groupTaskManager.UpdateOPAGroupTask(model);
                    }

                    if (backgroundJobManager.SendProjectActionSubmissionResponseEmail(emailManager, assignedTo,
                        assignedBy, model))
                    {
                        if (!model.IsApprovedPopupDisplayedForAe)
                        {
                            model.IsApprovedPopupDisplayedForAe = true;
                            PopupHelper.ConfigPopup(TempData, 380, 220, alertTitle, false, false, true, false, false,
                                true, "GenericPopup");
                            model.ProjectActionName =
                                projectActionFormManager.GetProjectActionName(model.ProjectActionTypeId);
                            model.OpaHistoryViewModel = GetOpaHistory((Guid)model.TaskGuid);
                            model.IsAgreementAccepted = true; // User Story 1901
                            GetDisclaimerText(model); // User Story 1901
                            return View("~/Views/OPAForm/OPARequestAEandReadOnly.cshtml", model);
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                }
            }
            return RedirectToAction("MyTasks", "Task");
        }

        [HttpPost]
        public ActionResult ApprovePopupForOPA(OPAViewModel model)
        {
            return PartialView("~/Views/OPAForm/ApprovePopupForOPA.cshtml", model);
        }

        public ActionResult LenderSubmit(OPAViewModel model, Guid? taskGuid, int? sequenceId, byte[] concurrency)
        {
            if (ModelState.IsValid)
            {
                // var updatedconcurrency = taskManager.GetConcurrencyTimeStamp((Guid)model.TaskGuid);
                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;
                var taskList = new List<TaskModel>();
                var task = new TaskModel();
                string alertTitle = String.Empty;
                task.AssignedTo = model.AssignedBy;
                task.AssignedBy = UserPrincipal.Current.UserName;
                model.RequestStatus = (int)RequestStatus.Submit;

                TempData["PopupText"] = "Additional Information is Sent.";
                alertTitle = "Additional Information Sent";
                task.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                model.Concurrency = concurrency;
                model.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0;
                task.DataStore1 = XmlHelper.Serialize(model, typeof(OPAViewModel), Encoding.Unicode);
                task.Notes = model.LenderComment;
                task.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0; // incrementing from 0
                task.StartTime = DateTime.UtcNow;
                task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.TaskInstanceId = taskGuid ?? Guid.NewGuid();
                task.IsReAssigned = model.IsReassigned;
                task.Concurrency = concurrency;
                //adding fhanumber and pagetype 
                task.FHANumber = model.FhaNumber;
                task.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                var taskOpenStatusModel = new TaskOpenStatusModel();
                taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                task.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel),
                    Encoding.Unicode);
                taskList.Add(task);
                var taskFile = new List<TaskFileModel>();

                // Update Status 
                Guid ChildTaskInstanceId = model.ChildTaskInstanceId;
                var results = reviewFileStatusManager.GetTaskFileIdByChildTaskInstanceId(ChildTaskInstanceId);
                var resultset = results;
                foreach (var newresults in results)
                {
                    reviewFileStatusManager.UpdateReviewFileStatusForTaskFileId(newresults.TaskFileId);
                }

                try
                {
                    taskManager.SaveTask(taskList, taskFile);
                    model.ServicerComments = model.LenderComment;
                    if (backgroundJobManager.SendOPASubmissionEmail(emailManager, task.AssignedTo, task.AssignedBy,
                        model))
                    {
                        if (!model.IsApprovedPopupDisplayedForAe)
                        {
                            model.IsApprovedPopupDisplayedForAe = true;
                            PopupHelper.ConfigPopup(TempData, 380, 220, alertTitle, false, false, true, false, false,
                                true, "GenericPopup");
                            //model.OpaHistoryViewModel = GetOpaHistory((Guid)model.TaskGuid);
                            model.ProjectActionName =
                                projectActionFormManager.GetProjectActionName(model.ProjectActionTypeId);
                            // return View("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", model);
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {

                }
            }
            return RedirectToAction("MyTasks", "Task");
        }

        //[HttpPost]
        public ActionResult GenericPopup()
        {
            return PartialView("~/Views/ProjectAction/_GenericPopup.cshtml");
        }

        public List<OPAHistoryViewModel> GetOpaHistory(Guid taskInstanceId)
        {
            return projectActionFormManager.GetOpaHistory(taskInstanceId);
        }

        [HttpGet]
        public FileResult OPADownloadTaskFile(string taskfileid)
        {
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid));
            reviewFileStatusManager.UpdateReviewFileStatusForFileDownload(taskFile.TaskFileId);
            if (taskFile != null)
            {
                string filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                var fileInfo = new System.IO.FileInfo(filePath);
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition",
                    String.Format("attachment;filename=\"{0}\"", taskFile.FileName));
                Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                Response.WriteFile(filePath);
                Response.End();
            }
            return null;
        }

        [HttpGet]
        public ActionResult DownloadTaskFile(string taskfileid)
        {

            string JsonSteing = "";

            StreamReader resultFile = null;
            Stream streamResult = null;
            // string JsonSteing = "{\"filename\":\"5091161.pdf\",\"folderName\":\"C:/em/FHA232/webtest1/\",\"documentType\":\"pdf\"}";
            RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();


TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid));
            reviewFileStatusManager.UpdateReviewFileStatusForFileDownload(taskFile.TaskFileId);
            string filePath = "";
            FileInfo fileInfo = null;
            if (taskFile != null)
            {
                filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                fileInfo = new System.IO.FileInfo(filePath);
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition",
                String.Format("attachment;filename=\"{0}\"", taskFile.FileName));
                #region MyRegion
                if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                {
                    request = WebApiTokenRequest.RequestToken();
                   JsonSteing = "{\"docId\":" + taskFile.DocId + ",\"version\":" + taskFile.Version + "}";
                     streamResult = WebApiDownload.DownloadDocumentUsingWebApi(JsonSteing, request.access_token, taskFile.FileName);
                }

                //else
                //{
                //    filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                //    fileInfo = new System.IO.FileInfo(filePath);
                //    Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                //    Response.WriteFile(filePath);
                //} 
                #endregion

                //karri replaced the above code since the above functionality is not working even after object is built
                if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                {
                    request = WebApiTokenRequest.RequestToken();
                    JsonSteing = "{\"docId\":" + taskFile.DocId + ",\"version\":" + taskFile.Version + "}";
                    resultFile = new StreamReader(streamResult);
                    #region Obsolete
                    // resultFile = streamResult;
                    //filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                    //fileInfo = new System.IO.FileInfo(filePath);
                    ////Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                    ////Response.WriteFile(filePath);

                    //#region file block
                    //using (FileStream fs = new FileInfo(filePath).Open(FileMode.Create, FileAccess.ReadWrite))
                    //{

                    //    try
                    //    {
                    //        StreamReader rdr = new StreamReader(streamResult);

                    //        string text = rdr.ReadToEnd();
                    //        byte[] byteData = System.Text.Encoding.ASCII.GetBytes(text);
                    //       // fs.Write(byteData, 0, byteData.Length);

                    //        //Response.ClearContent();
                    //        //Response.AddHeader("content-disposition", "attachment;filename=Contact.xls");
                    //        //Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    //        //Response.Write(
                    //        //Response.End();


                    //        //using (StreamReader sr = new StreamReader(streamResult))
                    //        //{
                    //        //    string text = rdr.ReadToEnd();
                    //        //    //byte[] byteData = System.Text.Encoding.ASCII.GetBytes(text);
                    //        //    //fs.Write(byteData, 0, byteData.Length);

                    //        //    string line;
                    //        //    do
                    //        //    {
                    //        //        try
                    //        //        {
                    //        //            line = sr.ReadLine();
                    //        //            byte[] byteData = System.Text.Encoding.ASCII.GetBytes(text);
                    //        //            fs.Write(byteData, 0, byteData.Length);
                    //        //        }
                    //        //        catch (System.IO.IOException)
                    //        //        {
                    //        //            break;
                    //        //        }
                    //        //    } while (line != null);

                    //        //}
                    //    }
                    //    catch (Exception ex1)
                    //    {
                    //        string a = ex1.Message;//karri
                    //    }
                    //}
                    //#endregion 
                    #endregion

                };

                //Response.End();
            }
           
             
            return new FileStreamResult(streamResult, "application/octet-stream");

           
        }

        [HttpPost]
        public virtual ActionResult ReAttachFile(OPAViewModel OPAProjectActionViewModel)
        {
            var FHA = Request.Form["FHA"];
            var ProjectActionTypeId = Request.Form["ProjectActionTypeId"];
            var TaskGuid = Request.Form["TaskGuid"];
            var lendercomment = Request.Form["lendercomment"];

            OPAProjectActionViewModel.FhaNumber = FHA;
            OPAProjectActionViewModel.ProjectActionTypeId = Convert.ToInt32(ProjectActionTypeId);
            OPAProjectActionViewModel.TaskGuid = new Guid(TaskGuid);
            foreach (string Filename in Request.Files)
            {
                HttpPostedFileBase myFile = Request.Files[Filename];
                if (myFile != null && myFile.ContentLength != 0)
                {
                    double fileSize = (myFile.ContentLength) / 1024;
                    Random randoms = new Random();

                    var UniqueId = randoms.Next(0, 99999);

                    var systemFileName = OPAProjectActionViewModel.FhaNumber + "_" +
                                         OPAProjectActionViewModel.ProjectActionTypeId + "_" +
                                         OPAProjectActionViewModel.GroupTaskInstanceId + "_" + UniqueId +
                                         Path.GetExtension(myFile.FileName);

                    string pathForSaving = Server.MapPath("~/Uploads");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {
                            // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(),
                        DateTime.UtcNow.ToString(), systemFileName.ToString(),
                        new Guid(OPAProjectActionViewModel.TaskGuid.ToString()));
                    taskManager.SaveGroupTaskFileModel(taskFile);
                }
            }

            OPAViewModel formUploadData = new OPAViewModel();

            var task = taskManager.GetLatestTaskByTaskInstanceId(new Guid(TaskGuid));
            if (task.DataStore1.Contains("OPAViewModel"))
            {
                formUploadData =
                    XmlHelper.Deserialize(typeof(OPAViewModel), task.DataStore1, Encoding.Unicode) as OPAViewModel;

                formUploadData.Concurrency = taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
                formUploadData.SequenceId = task.SequenceId;
                formUploadData.AssignedBy = task.AssignedBy;
                formUploadData.AssignedTo = task.AssignedTo;
                formUploadData.IsReassigned = task.IsReAssigned;
            }
            formUploadData.OpaHistoryViewModel = GetOpaHistory((Guid)OPAProjectActionViewModel.TaskGuid);
            formUploadData.LenderComment = lendercomment;
            GetDisclaimerText(formUploadData); //User Story 1901
            formUploadData.IsAgreementAccepted = false; //User Story 1901
            return PartialView("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", formUploadData);
        }

        public string OPADeleteFile(int taskfileid)
        {
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid));
            var success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");

                success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskfileid));
                if (success == 1)
                {
                    System.IO.File.Delete(pathForSaving + "\\" + taskFile.SystemFileName);
                    if (success == 1)
                        return "Success";
                }
            }
            return "Success";
        }

        public string DeleteFile(int taskfileid)
        {
            RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();
            RestfulWebApiResultModel APIresult = new RestfulWebApiResultModel();
            RestfulWebApiUpdateModel UpdateInfo = new RestfulWebApiUpdateModel();
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid));
            var success = 0;
            string pathForSaving = "";
            if (taskFile != null)
            {
                if (UserPrincipal.Current.UserId == taskFile.CreatedBy || (UserPrincipal.Current.UserRole == "BackupAccountManager" || UserPrincipal.Current.UserRole == "LenderAccountManager"))
                {

                    pathForSaving = Server.MapPath("~/Uploads");

                    success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskfileid));
                    if (success == 1)
                    {
                        if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                        {
                            request = WebApiTokenRequest.RequestToken();
                            //UpdateInfo.docId = taskFile.DocId;
                            //UpdateInfo.documentStatus = "Inactive";
                            //UpdateInfo.propertyId = "";
                            //UpdateInfo.documentType = "";
                            //UpdateInfo.indexType = "";
                            //UpdateInfo.indexValue = "500";

                            //APIresult = WebApiUpdate.UpdateDocument(UpdateInfo, request.access_token);
                            APIresult = WebApiDelete.DeleteDocumentUsingWebApi(taskFile.DocId, request.access_token);
                            if (!string.IsNullOrEmpty(APIresult.status) && APIresult.key == "200")
                            {
                                return "Success";
                            }
                        }
                        else
                        {
                            pathForSaving = Server.MapPath("~/Uploads");


                            System.IO.File.Delete(pathForSaving + "\\" + taskFile.SystemFileName);

                            if (success == 1)
                                return "Success";
                        }
                    }
                }
                else
                {
                    return "unauthorized";
                }
            }


            return "Success";
        }
        [HttpPost]
        public ActionResult OPARemovefile(string taskFileId, string taskInstanceID, OPAViewModel OPAProjectActionViewModel,
            string lendercomment)
        {
            string GroupTaskId = string.Empty;
            //var TaskGuid = Request.Form["TaskGuid"];
            var TaskGuid = Request.QueryString[0];

            OPAViewModel formUploadData = new OPAViewModel();
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskFileId));
            int success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");

                success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskFileId));
                System.IO.File.Delete(pathForSaving + taskFile.SystemFileName);
            }

            var task = taskManager.GetLatestTaskByTaskInstanceId(new Guid(TaskGuid));
            if (task.DataStore1.Contains("OPAViewModel"))
            {
                formUploadData =
                    XmlHelper.Deserialize(typeof(OPAViewModel), task.DataStore1, Encoding.Unicode) as OPAViewModel;

                formUploadData.Concurrency = taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
                formUploadData.SequenceId = task.SequenceId;
                formUploadData.AssignedBy = task.AssignedBy;
                formUploadData.AssignedTo = task.AssignedTo;
                formUploadData.IsReassigned = task.IsReAssigned;
            }
            formUploadData.OpaHistoryViewModel = GetOpaHistory((Guid)formUploadData.TaskGuid);
            formUploadData.LenderComment = lendercomment;
            GetDisclaimerText(formUploadData);
            formUploadData.IsAgreementAccepted = false;
            return PartialView("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", formUploadData);
        }


        [HttpPost]
        public ActionResult Removefile(string taskFileId, string taskInstanceID, OPAViewModel OPAProjectActionViewModel,
            string lendercomment)
        {
            string GroupTaskId = string.Empty;
            //var TaskGuid = Request.Form["TaskGuid"];
            var TaskGuid = Request.QueryString[0];

            OPAViewModel formUploadData = new OPAViewModel();
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskFileId));


            int success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");

                success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskFileId));
                System.IO.File.Delete(pathForSaving + taskFile.SystemFileName);
            }

            var task = taskManager.GetLatestTaskByTaskInstanceId(new Guid(TaskGuid));
            if (task.DataStore1.Contains("OPAViewModel"))
            {
                formUploadData =
                    XmlHelper.Deserialize(typeof(OPAViewModel), task.DataStore1, Encoding.Unicode) as OPAViewModel;

                formUploadData.Concurrency = taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
                formUploadData.SequenceId = task.SequenceId;
                formUploadData.AssignedBy = task.AssignedBy;
                formUploadData.AssignedTo = task.AssignedTo;
                formUploadData.IsReassigned = task.IsReAssigned;
            }
            formUploadData.OpaHistoryViewModel = GetOpaHistory((Guid)formUploadData.TaskGuid);
            formUploadData.LenderComment = lendercomment;
            GetDisclaimerText(formUploadData);
            formUploadData.IsAgreementAccepted = false;
            return PartialView("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", formUploadData);
        }

        // User Story 1901 
        private void GetDisclaimerText(OPAViewModel model)
        {
            model.DisclaimerText = nonCriticalRepairsManager.GetDisclaimerMsgByPageType("OPA");
        }

        public JsonResult GetRequestAdditionalGridForLender(string sidx, string sord, int page, int rows,
            Guid taskInstanceId)
        {
            var requestAddtionalTask = new List<OPAHistoryViewModel>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;


            requestAddtionalTask = GetOpaHistory(taskInstanceId);

            if (requestAddtionalTask.Count > 0)
            {

                int totalrecods = requestAddtionalTask.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
                if (sord.ToUpper() == "DESC")
                {

                    requestAddtionalTask = requestAddtionalTask.OrderByDescending(s => s.submitDate).ToList();

                    requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    requestAddtionalTask = requestAddtionalTask.OrderBy(s => s.submitDate).ToList();
                    requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }

                var jsonData = new
                {

                    total = totalpages,
                    page,
                    records = totalrecods,
                    rows = requestAddtionalTask,
                };




                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {



                var jsonData = new
                {
                    total = 1,
                    page = 1,
                    records = 1,
                    rows = requestAddtionalTask,

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        public string SaveToTaskFileHistory()
        {

            var fileId = Request.Form["fileId"];
            var childTaskInstanceId = Request.Form["childInstanceId"];
            var isNewFile = false;
            var files = Request.Files;
            var taskFileModel = new TaskFileModel();
            var opaProjectActionViewModel = new OPAViewModel();
            Guid taskInstanceIdForFileTable = new Guid();
            var childTaskNewFile = new ChildTaskNewFileModel();
            if (fileId != "")
            {
                taskFileModel = taskManager.GetTaskFileById(int.Parse(fileId));
                opaProjectActionViewModel = projectActionFormManager.GetOPAByTaskInstanceId(taskFileModel.TaskInstanceId);
            }
            else
            {
                opaProjectActionViewModel = projectActionFormManager.GetOPAByChildTaskId(new Guid(childTaskInstanceId));
            }

            foreach (string file in files)
            {
                var expectedFileName = Request.Form["expectedFileName"];
                HttpPostedFileBase myFile = Request.Files[file];
                if (myFile != null && myFile.ContentLength != 0)
                {

                    if (string.IsNullOrEmpty(expectedFileName) || expectedFileName == "null" || expectedFileName == "NA")
                    {
                        expectedFileName = Path.GetFileName(myFile.FileName);
                        isNewFile = true;
                        taskInstanceIdForFileTable = parentChildTaskManager.GetParentTask(new Guid(childTaskInstanceId)).ParentTaskInstanceId;
                    }
                    else if (expectedFileName != null &&
                             Path.GetExtension(expectedFileName) != Path.GetExtension(myFile.FileName))
                    {
                        expectedFileName = expectedFileName.Replace(Path.GetExtension(expectedFileName),
                            Path.GetExtension(myFile.FileName));
                        taskInstanceIdForFileTable = new Guid(childTaskInstanceId);
                    }
                    double fileSize = (myFile.ContentLength) / 1024;
                    Random randoms = new Random();
                    var UniqueId = randoms.Next(0, 99999);
                    var systemFileName = opaProjectActionViewModel.FhaNumber + "_" +
                                         opaProjectActionViewModel.ProjectActionTypeId + "_" +
                                         taskInstanceIdForFileTable + "_" + UniqueId +
                                         Path.GetExtension(myFile.FileName);
                    string pathForSaving = Server.MapPath("~/Uploads");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                    var taskFile = PoupuLateRequestAdditionalTaskFile(myFile, fileSize, UniqueId.ToString(),
                        expectedFileName, systemFileName.ToString(),
                        taskInstanceIdForFileTable);
                    var childTaskFileId = taskManager.SaveGroupTaskFileModel(taskFile);
                    if (isNewFile)
                    {
                        var reviewFileStatusModel = new ReviewFileStatusModel();
                        reviewFileStatusModel.ReviewFileStatusId = Guid.NewGuid();
                        reviewFileStatusModel.ReviewerUserId = taskManager.GetReviewerUserIdByTaskInstanceId(taskFile.TaskInstanceId);
                        reviewFileStatusModel.Status = 1;
                        reviewFileStatusModel.TaskFileId = taskFile.TaskFileId;
                        reviewFileStatusModel.ModifiedOn = DateTime.UtcNow;
                        reviewFileStatusModel.ModifiedBy = UserPrincipal.Current.UserId;
                        reviewFileStatusManager.SaveReviewFileStatus(reviewFileStatusModel);
                        childTaskNewFile = new ChildTaskNewFileModel();
                        childTaskNewFile.ChildTaskNewFileId = Guid.NewGuid();
                        childTaskNewFile.ChildTaskInstanceId = new Guid(childTaskInstanceId);
                        childTaskNewFile.TaskFileInstanceId = childTaskFileId;
                        projectActionFormManager.SaveChildTaskNewFiles(childTaskNewFile);
                    }
                    if (fileId != "")
                    {
                        var taskFileHistoryModel = new TaskFileHistoryModel();
                        taskFileHistoryModel.TaskFileHistoryId = Guid.NewGuid();
                        taskFileHistoryModel.ChildTaskInstanceId = new Guid(childTaskInstanceId);
                        taskFileHistoryModel.ParentTaskFileId = taskFileModel.TaskFileId;
                        taskFileHistoryModel.ChildTaskFileId = childTaskFileId;
                        projectActionFormManager.SaveLenderRequestAdditionalFiles(taskFileHistoryModel);
                        var result =
                            reviewFileStatusManager.UpdateReviewFileStatusForRequestAdditionalResponse(
                                taskFileModel.TaskFileId);
                        return result ? "Success" : "Failure";
                    }
                }
            }
            return "Success";
        }

        public string DeleteFileAndChildFile(int taskfileid)
        {
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid));
            var success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");

                success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskfileid));
                if (success == 1)
                {
                    System.IO.File.Delete(pathForSaving + taskFile.SystemFileName);
                    if (projectActionFormManager.IsFileHistoryExists(taskFile.TaskFileId))
                    {
                        success = projectActionFormManager.DeleteChildTaskFile(taskFile.TaskFileId);
                        if (success == 1)
                            return "Success";
                    }
                    return "Success";
                }
            }
            return "Failure";
        }

        private TaskFileModel PoupuLateRequestAdditionalTaskFile(HttpPostedFileBase fileStream, double fileSize, string checkListId, string expectedFileName, string systemFileName, Guid taskInstanceId)
        {
            var taskFile = new TaskFileModel();
            taskFile.FileName = expectedFileName;
            taskFile.GroupFileType = checkListId;
            taskFile.UploadTime = DateTime.UtcNow;
            // taskFile.FileName = fileStream.FileName;
            taskFile.FileSize = fileSize;
            taskFile.SystemFileName = systemFileName;
            taskFile.TaskInstanceId = taskInstanceId;
            taskFile.CreatedBy = UserPrincipal.Current.UserId;
            //BinaryReader rdr = null;
            try
            {
                //rdr = new BinaryReader(fileStream.InputStream);
                taskFile.FileData = null;//rdr.ReadBytes(fileStream.ContentLength);
            }
            finally
            {
            }
            return taskFile;
        }

        public string CheckIfFileExistsByGroupTaskId(int groupTaskId)
        {
            return groupTaskManager.CheckIfFileExistsByGroupTaskId(groupTaskId) ? "success" : "failure";
        }
    }
}
