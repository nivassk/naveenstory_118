﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        IAccountManager accountMgr;

        public ReportsController() : this(new AccountManager())
        {

        }

        public ReportsController(IAccountManager accountManager)
        {
            accountMgr = accountManager;
        }

        public ActionResult MultipleLoansProperty()
        {
            ViewBag.ExcelExportAction = "ExportMultiLoanReport";
            ViewBag.PrintAction = "PrintMultiLoanReport";
            var model = accountMgr.GetMultipleLoanProperty();
            return View(model);
        }
        

        //
        // GET: /Reports/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Reports/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Reports/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Reports/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Reports/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Reports/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Reports/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Reports/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
