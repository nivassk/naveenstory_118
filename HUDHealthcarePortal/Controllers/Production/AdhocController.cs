﻿using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Controllers.Production
{
    public class AdhocController : Controller
    {
        //
        // GET: /Adhoc/

        public ActionResult Index()
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(UserPrincipal.Current.Timezone);
            var reportDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneInfo).ToString("MM/dd/yyyy hh:mm:ss tt");
            Session["ReportDate"] = timeZoneInfo.SupportsDaylightSavingTime ? reportDate + " " + TimezoneManager.ShortTimeZoneFormat(timeZoneInfo.DaylightName) 
                : reportDate + " " + TimezoneManager.ShortTimeZoneFormat(timeZoneInfo.StandardName);
            return View("~/Views/Production/Adhoc/Index.cshtml");
        }

    }
}
