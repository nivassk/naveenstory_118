﻿var questionModule = angular.module('questionModule', ['ui.bootstrap']);

questionModule.controller('questionCtrl', ['$scope', '$http', 'questionService',
    function ($scope, $http, questionService) {
        $scope.divQuestion = false;
        $scope.questions;
        $scope.FilteredQuestions = [];
        $scope.currentPage = 1;
        $scope.numPerPage = 3;
        $scope.maxSize = 5;

        GetAllQuestions();
        //To Get All Records 
        function GetAllQuestions() {
            var getData = questionService.GetAllQuestions();
            getData.then(function (question) {
                $scope.questions = question.data;

                $scope.numPages = function () {
                    return Math.ceil($scope.questions.length / $scope.numPerPage);
                };

                $scope.$watch('currentPage + numPerPage', function () {
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = begin + $scope.numPerPage;
                    $scope.FilteredQuestions = $scope.questions.slice(begin, end);
                });

            }, function () {
                alert('Error in getting records');
            });
        };
       
      


      
     

         $scope.config = {
             itemsPerPage: 5,
             fillLastPage: true
         }


         $scope.editQuestion = function (question) {
             
             var getData = questionService.getQuestion(question);
            getData.then(function(question) {
                    $scope.question = question.data;
                    $scope.CheckListId = question.data.CheckListId;
                    $scope.QuestionId = question.data.QuestionId;
                    $scope.Question = question.data.Question;
                    $scope.Level = question.data.Level;
                    $scope.DisplayOrderId = question.data.DisplayOrderId;
                    $scope.StartDate = question.data.StartDate;
                    $scope.EndDate = question.data.EndDate;
                    $scope.Action = "Update";
                    $scope.divQuestion = true;
                },
                function() {
                    alert('Error in getting records');
                });
        };

        $scope.AddUpdateEmployee = function () {
            var questionViewModel = {
                CheckListId: $scope.CheckListId,
                QuestionId: $scope.QuestionId,
                Question: $scope.Question,
                Level: $scope.Level,
                DisplayOrderId: $scope.DisplayOrderId,
                ParentQuestionId: $scope.ParentQuestionId,
                StartDate: $scope.StartDate,
                EndDate: $scope.EndDate
            };

            var getAction = $scope.Action;
            var getData;
            if (getAction == "Update") {
                questionViewModel.Id = $scope.QuestionId;
                getData = questionService.updateQuestion(questionViewModel);
                  getData.then(function(msg) {
                    GetAllQuestions();
                    alert(msg.data);
                    $scope.divQuestion = false;
                }, function() {
                    alert('Error in updating record');
                });
            } else {
                getData = questionService.AddQuestion(questionViewModel);
                getData.then(function(msg) {
                    GetAllQuestions();
                    alert(msg.data);
                    $scope.divQuestion = false;
                }, function() {
                    alert('Error in adding record');
                });
            }
        };

        $scope.AddQuestion = function() {
            $scope.Action = "Add";
            $scope.divQuestion = true;
        };
    }
]);