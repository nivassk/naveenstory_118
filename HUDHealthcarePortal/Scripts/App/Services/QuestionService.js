﻿questionModule.factory('questionService', ['$http', function($http) {
    
    return {
         GetAllQuestions : function () {
            return $http.get("/Account/GetAllQuestions");
        },
   
         GetAllProjectActions: function () {
             return $http.get("/Account/GetAllProjectActions");
         },

         AddQuestion: function (questionViewModel) {
            var response = $http({
                method: "post",
                url: "/Account/AddQuestion",
                data: JSON.stringify(questionViewModel),
                dataType: "json"
            });
            return response;
        },

         getQuestion: function (questionViewModel) {
            var response = $http({
                method: "post",
                url: "/Account/GetQuestionById",
                data: JSON.stringify(questionViewModel),
                dataType: "json"
            });
            return response;
        },

        updateQuestion: function (questionViewModel) {
            var response = $http({
                method: "post",
                url: "/Account/UpdateQuestion",
                data: JSON.stringify(questionViewModel),
                dataType: "json"
            });
            return response;
        }
    }
}]);