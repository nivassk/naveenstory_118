﻿using Core;
using HUDHealthcarePortal.BusinessService;
using System;
using HUDHealthcarePortal.Core;

namespace HUDHealthcarePortal.Helpers
{
    public class XsltUtilHelper
    {
        public string GetPreferredTimeFromUtc(DateTime utcDt)
        {
            string currentTimeZone = UserPrincipal.Current.Timezone;
            string timezone = EnumType.GetEnumDescription(EnumType.Parse<HUDTimeZoneName>(currentTimeZone.Split(' ')[0]));
            return TimezoneManager.GetPreferredTimeFromUtc(utcDt).ToString("f")+" "+timezone;
        }

        public string GetUtcTimeFromPreferred(DateTime myDt)
        {
            return TimezoneManager.GetUtcTimeFromPreferred(myDt).ToString("f");
        }
    }
}
