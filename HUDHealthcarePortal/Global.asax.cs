﻿using System.Web.Mvc;
using System.Web.Optimization;
using Elmah;
using HUDHealthcarePortal.BusinessService;
using HUDHealthCarePortal.Controllers;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Framework;
using System;
using System.IO;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using HUDHealthCarePortal;

namespace HUDHealthCarePortal
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            ViewEngines.Engines.Add(new XsltViewEngine());
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            AutoMapperInitialize.Initialize();

            ModelBinders.Binders.Add(typeof(string), new TrimModelBinder());

            // check if application pool is recycled
            //ExceptionManager.WriteToEventLog(string.Format("HCP Application starts at: {0}", DateTime.Now), "Application", System.Diagnostics.EventLogEntryType.Error);
        }

        public void ErrorLog_Filtering(object sender, ExceptionFilterEventArgs e)
        {
            // don't log file not found exception to elmah
            if (e.Exception.GetBaseException() is FileNotFoundException)
                e.Dismiss();
        }

        // prevent after logout, back button can view prior screens
        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
        }

        
        void ErrorLog_Logged(object sender, ErrorLoggedEventArgs args)
        {           
            #if DEBUG
               //Don't redirect to custom error page
            #else //Release Code
                RedirectToError(args);
            #endif
        }
        
        private void RedirectToError(ErrorLoggedEventArgs args)
        {
            Server.ClearError();
            var routeData = new RouteData();
            routeData.Values["controller"] = "HomeController";
            routeData.Values["action"] = "Error";
            routeData.Values["id"] = args.Entry.Id;
            IController controller = new HomeController();
            var rc = new RequestContext(new HttpContextWrapper(Context), routeData);
            controller.Execute(rc);
        }

        
    }
}
