﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HUDHealthCarePortal
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            bool isOfflineMode;
            Boolean.TryParse(ConfigurationManager.AppSettings["IsOffline"], out isOfflineMode);
           // routes.MapMvcAttributeRoutes();
            if (isOfflineMode)
            {
                routes.MapRoute("Offline", "{controller}/{action}/{id}",
                    new
                    {
                        action = "Offline",
                        controller = "Home",
                        id = UrlParameter.Optional
                    },
                    new {constraint = new OfflineRouteConstraint()});

            }
            routes.MapRoute("NotFound", "{whatever}/{dotnetfile}.aspx", new { controller = "Home", action = "NotFound" });
            routes.MapRoute(
                name: "ResetPwdRoute",
                url: "Account/ResetPassword/{id}/{usr}",
                defaults: new { controller = "Account", action = "ResetPassword", id = UrlParameter.Optional, usr = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );
        }
    }

    public class OfflineRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            //List<string> allowedIPs = new List<string>() { "50.246.79.170", "170.97.67.112", "170.97.167.69", "::1" };
            List<string> allowedIPs = new List<string>() { "50.246.79.170", "170.97.67.112", "170.97.167.69", "71.241.250.132", "68.48.34.38", "71.163.98.173", "96.255.50.237" };
            string clientIp = httpContext.Request.ServerVariables["REMOTE_ADDR"];
            //Exception ex = new Exception(httpContext.Request.ServerVariables["REMOTE_ADDR"]);
            //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            return !allowedIPs.Contains(clientIp);
        }
    }
}